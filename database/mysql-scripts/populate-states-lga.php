<?php

  include '../../vendor/autoload.php';

  #load csv file into array 
  #parse the array for each state and and then each lga 

  $state = 2;
  $lga = 1;
  
  $row = 0;

  $all = [];

  if(($handle = fopen('./data.csv','r')) != false) {

       while(($data = fgetcsv($handle,10000,',')) != FALSE) {

           if($row == 0) {
              $row++;
	      continue;
           }

           array_push($all,$data);
          
           $row++;

       }

       fclose($handle);

  }
   
  $mysqli = new mysqli('localhost','root','netplus','saddle_');

  $states_covered = [];
  $state_count;
  $q = 0;

  $state_str;
  $state_id = 0;

  while($q < count($all)) {

    //echo 'entered while '.$q.PHP_EOL;
    //echo 'all value ' . $all[$q][$state].PHP_EOL;

    if((count($states_covered) < 1 || in_array($all[$q][$state],$states_covered) == false) && empty(trim($all[$q][$state])) == false) {

      $state_count = array_push($states_covered,$all[$q][$state]);
      $state_str = trim(str_replace("State", "",$all[$q][$state]));


      $query = 'insert into state (name) value ("'.$state_str.'")';

      $query_result = $mysqli->query($query);
      $state_id = $mysqli->insert_id;

      echo $state_str . PHP_EOL;

      if($query_result == false) {

         echo 'The following query failed';
         echo $query . PHP_EOL;
         die();

      }

      $i = 0;

      while($i < count($all)) {

         if($states_covered[$state_count - 1] == $all[$i][$state]) {

             $lga_str = $all[$i][$lga];
             $query2 = 'insert into lga (state_id,lga) value (' . $state_id . ',"' . $lga_str . '")'; 
             
             $query_result = $mysqli->query($query2);


             if($query_result == false) {
                echo 'The following query failed';
                echo $query2 . PHP_EOL;
                die();
             }

             echo '   ' . $lga_str . PHP_EOL;

         }

       $i++;

      }

    }

   $q++;

  }

  $mysqli->close();

?>
