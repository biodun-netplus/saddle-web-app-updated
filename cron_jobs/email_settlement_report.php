<?php
  include 'config.php';
  require_once $config['autoloadpath'];

  $q = 'select
  c.courier_name as "Courier Name",
  m.merchant_name as "Merchant Name",
  t.transaction_id as "Transaction Id",
  t.transaction_datetime as "Transaction Date",
    CASE
      WHEN t.transaction_status = 3 then "Fully Delivered By Agent"
      when t.transaction_status = 301 then "Picked Up By Customer"
      when t.transaction_status = 30 then "Partially Delivered By Agent"
    END  as "Transaction Status",
  t.transaction_total_amount as "Total Amount",
  t.updated_at as "Updated  At",
  t.delivery_cost as "Delivery Cost",
  t.transaction_order_no as "Transaction Order Number",
  t.pod_transaction as "POD",
  t.preauth_transaction as "Preauth",
  t.transaction_payment_type as "Transaction Payment"
from transactions t 
  left join merchants m on m.id = t.merchant_id 
  left join courier c on c.id = t.courier_id 
where t.transaction_status in ( 3, 301, 30) 
  and (t.updated_at > (NOW() - INTERVAL 1 MONTH) or transaction_datetime > (NOW() - INTERVAL 1 MONTH))';

  $conn = mysqli_connect($config['db']['hostname'],
            $config['db']['user'],
            $config['db']['password'],
            $config['db']['database'],
            $config['db']['port']);

	if (mysqli_connect_errno()) {
   		die("Failed to connect to MySQL: " . mysqli_connect_error());
  }

  $tempdir = $config['tempdir'];

  $csv_filename = 'settlements_report_'.date('Y-m-d').'.csv';
  $csv_export = '';

  $query = mysqli_query($conn, $q);
  $field = mysqli_field_count($conn);

  for($i = 0; $i < $field; $i++) {
    $csv_export.= mysqli_fetch_field_direct($query, $i)->name.',';
  }

  $csv_export.=PHP_EOL;

  while($row = mysqli_fetch_array($query)) {
    for($i = 0; $i < $field; $i++) {
      $csv_export.= '"'.$row[mysqli_fetch_field_direct($query, $i)->name].'",';
    }
    $csv_export.=PHP_EOL;
  }

  if(!file_exists($tempdir))
    mkdir($tempdir,0774);

  if(!is_dir($tempdir))
    die('temp file is not a directory');

  $attachmentPath = $tempdir.$csv_filename;
  $fh = fopen($attachmentPath, 'w') or die("Failed to create file");

  fwrite($fh, $csv_export) or die("Could not write to file");
  fclose($fh);

  $message = (new Swift_Message())
                ->setSubject('Monthy Settlements Report for '.date('F, Y',strtotime('-3 day', time())))
                ->setFrom($config['smtp']['from'])
                ->setTo($config['smtp']['to'])
                ->setBody('Please find attached successfully processed transactions for the month of '.date('F, Y', strtotime('-3 day', time())))
                ->addPart('<q>Please find attached the successfully processed transactions for the month of '.date('F, Y', strtotime('-3 day', time())).'</q>','text/html')
                ->attach(Swift_Attachment::fromPath($attachmentPath));

  $transport = (new Swift_SmtpTransport($config['smtp']['server'],$config['smtp']['port']))
                  ->setUsername($config['smtp']['user'])
                  ->setPassword($config['smtp']['password']);
  
  $mailer = new Swift_Mailer($transport);

  $result = $mailer->send($message);

  if($result) {
    echo 'successfully sent settlements csv file'.PHP_EOL;
  } else {
    echo 'unable to send message'.PHP_EOL;
  }

?>