<?php
  include 'config.php';
  //Get all failed saddle lite transactions
  $q = "select * from saddle_lite_transaction where status = 'PENDING' and transaction_type = 'CASH'";

  //Make connection to the database
  $conn = mysqli_connect($config['db']['hostname'],
  $config['db']['user'],
  $config['db']['password'],
  $config['db']['database'],
  $config['db']['port']);

  if (mysqli_connect_errno()) {
      die("Failed to connect to MySQL: " . mysqli_connect_error());
  }
    
  $query = mysqli_query($conn, $q);
  $j = mysqli_num_rows ( $query );
  $date = date("Y-m-d H:i:s");  
 
  //Loop through the data
  for($i = 0; $i < $j; $i++){
    while($row = $query->fetch_assoc()) {
        $agent_id = $row['agent_id'];
        $trans_id = $row['transaction_id'];
        //Get agent details
        $a_query = "select users.email, users.first_name, users.last_name, saddle_lite_transaction.transaction_id, saddle_lite_transaction.amount, courier.netpluspay_mid, courier.address,       courier.courier_email from users 
            inner join saddle_lite_transaction on users.id = saddle_lite_transaction.agent_id 
            inner join courier on saddle_lite_transaction.courier_id = courier.id 
            where users.id = '$agent_id' and saddle_lite_transaction.transaction_id = '$trans_id' and saddle_lite_transaction.status ='PENDING' and saddle_lite_transaction.transaction_type = 'CASH'";

        $agent_query = mysqli_query($conn, $a_query);


        if($agent_query){
            for($k = 0; $k < $j; $k++){
                while($row = $agent_query->fetch_assoc()) {
                    $fullname = $row['last_name'] . ' ' . $row['first_name'];
                    $order_id = '7UP'.mt_rand(10000, 99999).time();
                    $trans_id = $row['transaction_id'];
                    $amount = $row['amount'];
                    $data = array(
                        'full_name'=> $fullname,
                        'amount' => $amount,
                        'currency' => 'NGN',
                        'narration' => 'SaddleLTE CASH transaction',
                        'email' => $row['email'],
                        'merchantid' => $row['netpluspay_mid'],
                        'orderid' => $order_id
                    );
    
    
                    $req = json_encode($data);
                  
                    //Call netpluspay recurrrent
    
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $req);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($req)));
                    curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: Application/json;charset=UTF-8"));
                    curl_setopt($curl, CURLOPT_URL, "https://netpluspay.com/api/v1/recurrent");
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    
                    $result = curl_exec($curl);
                    if($result == false) {
                        echo curl_error($curl);
                    }
                 
                
                    $j_result = json_decode($result);
        
                   $rs = get_object_vars($j_result);
                 
                
                   if(($j_result->result =="SUCCESS" || $j_result->result == "PENDING") && $j_result->status  == "CAPTURED") { 
                        $status = 1;
                        $transaction_amount = $amount;
                        $code = '000';
                        $q = "update saddle_lite_transaction set netplus_id ='$order_id', status = 'SUCCESS',  updated_at = '$date' where transaction_id = '$trans_id'";
                        mysqli_query($conn, $q);
                        //mysqli_close($conn);
    
                    } else {
    
                        $status = 0;
                        $transaction_amount = 0;
                        $code = '003';
                        $q = "update saddle_lite_transaction set netplus_id ='$order_id', status = 'PENDING', updated_at = '$date' where transaction_id = '$trans_id'";
                        mysqli_query($conn, $q);
                     //   mysqli_close($conn);
                    }
                }
            }
        }else{
            echo "ERROR";
           // die;
        }
    }
  }
  die;
?>