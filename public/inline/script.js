(function() {

    $saddleng = {};

    $saddleng['init'] = function(config) {


        $saddleng['setup'] = function() {
            console.log('passing data to frame');
            var frame = window.frames['netplusFrame'];
            frame.contentWindow.postMessage(JSON.stringify(window.$saddleng['data']),"https://www.saddleng.com")
        }
    
        //console.log('in init')
        $saddleng['data'] = config;

        var onSuccess = config['onSuccess'];
        var onFailure = config['onFailure'];

        $saddleng['close'] = function(res) {
            var frame = window.frames['netplusFrame'];
            //console.log("recieved res " + JSON.stringify(res,null,2));
            if(res && res['status'] == "00" && onSuccess) {
                onSuccess(res["data"]);
            } else if(onFailure) {
                onFailure();
            }
            frame.remove();
        }

        function pop() {

            var url = 'https://www.saddleng.com/inline/frame.html';
            var ifrm = document.createElement("iframe");

            ifrm.setAttribute("src",'https://wwww.saddleng.com/inline/frame.html');
            ifrm.style.width = "100%";
            ifrm.style.height = "100vh";
            ifrm.style.zIndex = "10000000002";
            ifrm.style.background = "rgba(0,0,0,.4) url(https://www.saddleng.com/assets/inline/imgs/loader.svg) 50% 50% no-repeat";
            ifrm.style.position = "fixed";
            ifrm.style.top = "0";
            ifrm.style.border = "none";

            ifrm.setAttribute("id", "netplusFrame");
            document.querySelector('body').appendChild(ifrm);
            window.addEventListener("message",function(ev) {
                console.log("netplusframe ready received");
                if(ev.origin="https://www.saddleng.com" && ev.data == "netplusFrame_ready") {
                    $saddleng['setup']();
                    return;
                    //console.log(ev);
                }

                var payload = JSON.parse(ev.data);
                if((typeof payload === "object" && payload !== null) || typeof payload === "function") {
                  if(payload.message && (payload.message == "transaction_success" || payload.message == "transaction_incomplete")) {
                        $saddleng['close'](payload.data);
                  }
                }

            });


        }

        var link = document.createElement("link");
        link.href="https://fonts.googleapis.com/css?family=Raleway|Roboto";
        link.rel="stylesheet";

        var body = document.querySelector("body");
        body.appendChild(link);

        var span = document.createElement('span');

        span.style.width="150px";
        span.style.height = '70px';
        span.style.cursor = 'pointer';
        span.style.padding = '10px 12px';
        span.style["background-image"] = "linear-gradient(to right, #1DE154, #1FEE59)";
        span.style["font-family"] = "'Roboto'";
        span.style["color"] = "white";
        span.style['border-radius'] = "4px";
        span.style['border-bottom'] ="4px solid #17AE41";

        span.textContent = 'Pay With Debit On Delivery';
        var elem = document.querySelector(config['selection']);
        elem.appendChild(span);
        elem.addEventListener('click', function(e){
            pop();
        });

    }


})();