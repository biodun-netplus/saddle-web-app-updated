<?php
// Load Vendor Autoload
require_once __DIR__ . '/../vendor/autoload.php';

// Load Settings
$settings = require_once __DIR__ . '/../app/bootstrap/settings.php';
$app = new Slim\App($settings);

// Load Dependancies
require_once __DIR__ . '/../app/bootstrap/dependencies.php';

// Load Middleware
require_once __DIR__ . '/../app/bootstrap/middleware.php';

require_once __DIR__ . '/../app/bootstrap/services.php';

// Load Controllers
require_once __DIR__ . '/../app/bootstrap/controllers.php';

// Load Routes
foreach (glob(__DIR__ . '/../app/routes/*.php') as $filename)
{
    require_once $filename;
}

session_start();

// Run App
$app->run();
