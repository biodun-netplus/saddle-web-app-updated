<?php
return [
    'settings' => [
        
        'displayErrorDetails' => false,

        'db' => [
            'driver' => 'mysql', // Only MySQL Supported
            'host' => 'localhost', // Database Host
            'port' => 3306, // My SQL Port
            'database' => 'saddle_', // Database Name
            'username' => 'root', // Database Username
            'password' => '', // Database Password
            // 'username' => 'courierServer', // Database Username
            // 'password' => 'C0r3M3ltD0wn!', // Database Password
            'charset' => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix' => ''
        ], 
        'view' => [
            'template_path' => __DIR__ . '/../views/',
            'twig' => [
                'cache' => false, //__DIR__ . '/../../storage/cache/twig'
                'debug' => false,
                'auto_reload' => true,
            ],
        ],
        'logger' => [
            'name' => 'Dappur',
            'log_path' => __DIR__ . '/../../storage/log/monolog/dappur.log', // PATH_TO_LOG
            'le_token' => false, // Logentries Access Token
        ]
    ],
];
