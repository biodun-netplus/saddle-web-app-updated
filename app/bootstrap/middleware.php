<?php
$app->add(new \Dappur\Middleware\CsrfMiddleware($container));
//$app->add(new \Slim\Csrf\Guard());
$app->add(new \Slim\Middleware\JwtAuthentication([
    "secret" => "supersecretkeyyoushouldnotcommittogithub",
    "logger"=> $container['apilog'],
    "path" => "/api/v2",
    "attribute" => "jwt",
    "algorithm" => ["HS256", "HS384"],
    "secure" => false,
    "error" => function ($request, $response, $arguments) {
        $data["status"] = "error";
        $data["message"] = $arguments["message"];
        return $response
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
]));
//$app->add(new \Zeuxisoo\Whoops\Provider\Slim\WhoopsMiddleware($app));

