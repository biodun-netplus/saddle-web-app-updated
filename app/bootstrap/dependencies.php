<?php
// Initialize Container
$container = $app->getContainer();

// Configure Database
$db = $container['settings']['db'];
$capsule = new \Illuminate\Database\Capsule\Manager();
$capsule->addConnection($db);
$capsule->setAsGlobal();
$capsule->bootEloquent();


$container['db'] = function () use ($capsule) {
    return $capsule;
};



// Bind config table from database
$container['config'] = function () use ($container) {
    $config = new \Dappur\Dappurware\SiteConfig($container);
    return $config->getConfig();
};

// Bind Sentinel Authorization plugin
$container['auth'] = function () {
    $sentinel = new \Cartalyst\Sentinel\Native\Facades\Sentinel(
        new \Cartalyst\Sentinel\Native\SentinelBootstrapper(__DIR__ . '/sentinel.php')
    );

    return $sentinel->getSentinel();
};

// Bind User Permissions
$container['userAccess'] = function($container) {
    return (new \Dappur\Dappurware\Sentinel($container))->userAccess();
};

// Bind Flash Messages
$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

$container["jwt"] = function () use ($container) {
    return new StdClass;
};

// Bind Respect Validation
$container['validator'] = function () {
    return new \Awurth\Slim\Validation\Validator();
};

// Bind Cookies
$container['cookies'] = function ($container){
    return new \Dappur\Dappurware\Cookies($container);
};

$container['dateobject'] = function($container) {
    return new \ExpressiveDate;
};

// CSRF
$container['csrf'] = function ($container) {

    $guard = new \Slim\Csrf\Guard(
        $container->settings['csrf']['prefix'], 
        $storage, 
        null, 
        $container->settings['csrf']['storage_limit'], 
        $container->settings['csrf']['strength'], 
        $container->settings['csrf']['persist_tokens']);

    $guard->setFailureCallable(function ($request, $response, $next) use ($container) {
         return $container['view']
            ->render($response, 'errors/csrf.twig')
            ->withHeader('Content-type', 'text/html')
            ->withStatus(404);
    });

    return $guard;
};

// Bind Twig View
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(
        $container['settings']['view']['template_path'] . $container->config['theme'],
        $container['settings']['view']['twig']
    );

    $view->addExtension(new \Slim\Views\TwigExtension(
        $container['router'],
        $container['request']->getUri()
    ));
    $view->addExtension(new \Twig_Extension_Debug());
    $view->addExtension(new \Dappur\TwigExtension\Asset($container['request']));
    $view->addExtension(new \Dappur\TwigExtension\JsonDecode($container['request']));
    $view->addExtension(new \Awurth\Slim\Validation\ValidatorExtension($container['validator']));
    //$view->addExtension(new \Dappur\TwigExtension\Csrf($container['request']));

    $view->getEnvironment()->addGlobal('flash', $container['flash']);
    $view->getEnvironment()->addGlobal('auth', $container['auth']);
    $view->getEnvironment()->addGlobal('config', $container['config']);
    $view->getEnvironment()->addGlobal('userAccess', $container['userAccess']);
    $view->getEnvironment()->addGlobal('currentRoute', $container['request']->getUri()->getPath());
    $view->getEnvironment()->addGlobal('dateobject',$container['dateobject']);
    return $view;
};

// Bind Found Handler
$container['foundHandler'] = function() {
    return new \Slim\Handlers\Strategies\RequestResponseArgs();
};

$container['apilog'] = function($container) {

    $logger = new Monolog\Logger('api');
    $file_stream = new \Monolog\Handler\RotatingFileHandler($container['settings']['logger']['log_path'],0, Monolog\Logger::DEBUG, true, 0774, false);
    $logger->pushHandler($file_stream);

    return $logger;

};

// Bind Monolog Logging System if Enables
$container['logger'] = function($container) {

    $cfg = $container['settings']['db'];
    $host = $cfg['host'] ;
    $db   = $cfg['database'];
    //$user = 'root';
    //$pass = '';
    $charset = 'utf8';

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $pdo = new \PDO($dsn, $cfg['username'], $cfg['password']);
    
     //Stream log to a table in the db
    $db_stream = new \MySQLHandler\MySQLHandler($pdo,"log_activities",array('userid'));

    // Stream Log output to file
    $logger = new Monolog\Logger($container['settings']['logger']['name']);
    //$file_stream = new \Monolog\Handler\StreamHandler($container['settings']['logger']['log_path']);
    //$logger->pushHandler($file_stream);
    $logger->pushHandler($db_stream);
    
    //Stream log output to Logentries
    if ($container['settings']['logger']['le_token']) {
        $le_stream = new Logentries\Handler\LogentriesHandler($container['settings']['logger']['le_token']);
        $logger->pushHandler($le_stream);
    }
    
    return $logger;
};

//Override the default Not Found Handler
$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        return $container['view']->render($response->withStatus(404), '404.twig', [
            "myMagic" => "Let's roll"
        ]);
    };
};

$container['phpErrorHandler'] = function ($container) {
    return function ($request, $response, $exception) use ($container) {
        return $container['view']
        	->render($response, '500.twig', array("exception" => $exception))
        	->withStatus(500)
            ->withHeader('Content-type', 'text/html');
    };
};
$container['errorHandler'] = function ($container) {
    return function ($request, $response, $exception) use ($container) {
        return $container['view']
        	->render($response, '500.twig', array("exception" => $exception))
        	->withStatus(500)
            ->withHeader('Content-type', 'text/html');
    };
};

$container['mail'] = function($container) {
    //$mail_settings = $container['settings']['mail'];
    //use PHPMailer\PHPMailer\PHPMailer;
    $mail = new PHPMailer\PHPMailer\PHPMailer(true);
    return $mail; 
};


//I am thinking of injecting MODELS into the container. No need to keep instantiating Model in 
//Controller every time I need one.

//Transactions
$container['models'] = function($container) {
    //$mail_settings = $container['settings']['mail'];
    //use PHPMailer\PHPMailer\PHPMailer;
    $models = array();
    $models['transaction'] = new Dappur\Model\Transactions;
    $models['courier'] = new Dappur\Model\Courier;
    $models['bank'] = new Dappur\Model\Banks;
    $models['items'] = new Dappur\Model\Items;
    $models['merchant'] = new Dappur\Model\Merchants;
    $models['status'] = new Dappur\Model\Status;
    $models['courier-location'] = new Dappur\Model\CourierLocation;
    $models['courier-merchant'] = new Dappur\Model\CourierMerchants;
    $models['agent'] = new Dappur\Model\Agents;
    $models['state'] = new Dappur\Model\State;
    $models['lga'] = new Dappur\Model\Lga;
    $models['delivery_assignment'] = new Dappur\Model\DeliveryAssignment;

    return $models; 
};

?>