<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model {

    protected $table = 'deliverys';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'code',
    ];
}