<?php
namespace Dappur\Model;

use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SaddleLiteTransaction extends Model {

    protected $table = 'saddle_lite_transaction';
    protected $primaryKey = 'id';
    protected $fillable = [
          'transaction_id',
          'transaction_type', //CASH, CARD
          'amount',
          'agent_id',
          'courier_id',
          'status' //SUCCESS, FAILURE
    ];

    public function getAllTransactionsForCourier($courier_id) {
        return SaddleLiteTransaction::where("courier_id","=",$courier_id)->get();
    }

    public function getAllTransactionsForAgent($agent_id) {
        return SaddleLiteTransaction::where("agent_id","=",$agent_id)->get();
    }

    public function getAllTransactionsBetween($courier_id, $from, $to) {
        return SaddleLiteTransaction::whereDate("created_at","=>",$from)
                ->whereDate("created_at", "<=", $to)
                ->where("courier_id", "=", $courier_id)
                ->get();
    }

    public function getTodaysTransactions($courier_id) {
        return SaddleLiteTransaction::whereDate("created_at","=",date('j-m-Y'))
                ->get();
    }

    public function getTransaction($transation_id) {
        return SaddleLiteTransaction::where('id',"=",$transation_id)->first();
    }

    public function createNewTransaction($data) {
        
        try {

            $id = SaddleLiteTransaction::insertGetId($data); 
            if(is_null($id) || isset($id) == false) {

                $data = [
                    "status" => 0,
                    "msg" => "Unable to log transaction"
                ];   

            } else {

                $data = [
                    "status" => 1,
                    "msg" => "Successfully logged transaction"
                ];

            }

        } catch(\Illuminate\Database\QueryException $e) {

            $data = [
                "status" => 0,
                "msg" => "Unable to log transaction"
            ];       

        }

        return $data;
    }

    public function searchSaddleLiteTransactions($courier_id, $search_term, $order_info, $from_date, $to_date) {
        $count  = SaddleLiteTransaction::where("courier_id","=",$courier_id)->count();

        if(is_null($search_term) || empty($search_term)) {

            $transactionsQ = SaddleLiteTransaction::join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                ->where("saddle_lite_transaction.courier_id","=",$courier_id)
                ->select("saddle_lite_transaction.transaction_id",
                    "saddle_lite_transaction.transaction_type",
                    "saddle_lite_transaction.status",
                    DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                    "saddle_lite_transaction.amount",
                    "saddle_lite_transaction.created_at"
                )
                ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionsQ = SaddleLiteTransaction::join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                ->where("saddle_lite_transaction.courier_id","=",$courier_id)
                ->where("saddle_lite_transaction.transaction_id", "like", $search_term . '%')
                ->select("saddle_lite_transaction.transaction_id",
                    "saddle_lite_transaction.transaction_type",
                    "saddle_lite_transaction.status",
                    DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                    "saddle_lite_transaction.amount",
                    "saddle_lite_transaction.created_at"
                )
                ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionsQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];
    }

    public function searchSaddleLiteTransactionsAdmin($search_term, $order_info, $from_date, $to_date) {

        $count  = SaddleLiteTransaction::count();

        if(is_null($search_term) || empty($search_term)) {

            $transactionsQ = SaddleLiteTransaction::join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                ->select("saddle_lite_transaction.transaction_id",
                    "saddle_lite_transaction.transaction_type",
                    "saddle_lite_transaction.status",
                    DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                    "saddle_lite_transaction.amount",
                    "saddle_lite_transaction.created_at"
                )
                ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionsQ = SaddleLiteTransaction::join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                ->where("saddle_lite_transaction.transaction_id", "like", $search_term . '%')
                ->select("saddle_lite_transaction.transaction_id",
                    "saddle_lite_transaction.transaction_type",
                    "saddle_lite_transaction.status",
                    DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                    "saddle_lite_transaction.amount",
                    "saddle_lite_transaction.created_at"
                )
                ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionsQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];
    }

    public function filterData($courier_id, $filter_data, $order_info, $from_date, $to_date, $agent_id) {

        $count  = SaddleLiteTransaction::where("courier_id","=",$courier_id)->count();

        if(empty($filter_data)) {

            $transactionsQ = SaddleLiteTransaction::join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                                ->where("saddle_lite_transaction.courier_id","=",$courier_id)
                                ->select("saddle_lite_transaction.transaction_id",
                                    "saddle_lite_transaction.transaction_type",
                                    "saddle_lite_transaction.status",
                                    DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                                    "saddle_lite_transaction.amount",
                                    "saddle_lite_transaction.created_at")
                                    ->orderBy($order_info['name'],$order_info['dir']);;

        } else {
        
            $transactionsQ = DB::table("saddle_lite_transaction")
                                ->join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                                ->where("saddle_lite_transaction.courier_id","=",$courier_id)
                                ->where($filter_data)
                                ->select("saddle_lite_transaction.transaction_id",
                                "saddle_lite_transaction.transaction_type",
                                "saddle_lite_transaction.status",
                                 DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                                "saddle_lite_transaction.amount",
                                "saddle_lite_transaction.created_at")
                                ->orderBy($order_info['name'],$order_info['dir']);;
 
        }

        if(empty($from_date) == false) {
            $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        if($agent_id > 0) {
            $transactionsQ = $transactionsQ->where('users.id',"=",$agent_id);
        }

        $transactions = $transactionsQ->get();
        
        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    }

    public function filterDataAdmin($filter_data, $order_info, $from_date, $to_date, $agent_id) {

        $count  = SaddleLiteTransaction::count();

        if(empty($filter_data)) {

            $transactionsQ = SaddleLiteTransaction::join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                                ->select("saddle_lite_transaction.transaction_id",
                                    "saddle_lite_transaction.transaction_type",
                                    "saddle_lite_transaction.status",
                                    DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                                    "saddle_lite_transaction.amount",
                                    "saddle_lite_transaction.created_at")
                                    ->orderBy($order_info['name'],$order_info['dir']);;

        } else {
        
            $transactionsQ = DB::table("saddle_lite_transaction")
                                ->join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                                ->where($filter_data)
                                ->select("saddle_lite_transaction.transaction_id",
                                "saddle_lite_transaction.transaction_type",
                                "saddle_lite_transaction.status",
                                 DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                                "saddle_lite_transaction.amount",
                                "saddle_lite_transaction.created_at")
                                ->orderBy($order_info['name'],$order_info['dir']);;
 
        }

        if(empty($from_date) == false) {
            $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        if($agent_id > 0) {
            $transactionsQ = $transactionsQ->where('users.id',"=",$agent_id);
        }

        $transactions = $transactionsQ->get();
        
        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    }

    /**
     * 
     */
    public function includes (array $query) {

        $count = SaddleLiteTransaction::where($query)
                    ->count();

        return $count > 0;

    }

}