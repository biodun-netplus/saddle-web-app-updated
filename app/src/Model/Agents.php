<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agents extends Model {

    protected $table = 'agent';
    protected $primaryKey = 'id';
    protected $fillable = [
          'agent_id',
          'name',
          'phone',
          'courier_id',
          'status',
          'current_longtitude',
          'current_latitude', 
    ];

    public function getAgents()
    {
        return Agents::all()->sortBy("name");
    }

    public function assignAgent($transaction_id="", $item_id="", $agentid)
    {
        $result = DB::table('delivery_assignment')->insert(
            ['agent_id' => $agentid, 'transaction_id' => $transaction_id, 'item_id'=>$item_id]
        );
        
    }

}