<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;

class Banks extends Model {

    protected $table = 'banks';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'code',
    ];
}