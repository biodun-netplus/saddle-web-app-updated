<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;

/**
 * State ...
 */
class State extends Model {

    protected $table = 'state';

    public function lgas() {
        return $this->hasMany('Dappur\Model\Lga');
    }

}

?>