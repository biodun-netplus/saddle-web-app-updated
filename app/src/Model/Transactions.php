<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;
//use Illuminate\Support\Facades\DB;

class Transactions extends Model {
    protected $table = 'transactions';
    protected $primaryKey = 'id';

    public function items()
    {
        return $this->hasMany('Dappur\Model\Items','transaction_id');
    }
    
    public function getAllTransactions()
    {
        //d($this);
        //die;
        $transactions = DB::table('transactions')
                        ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                        ->select(
                                'transaction_id', 
                                'merchant_name', 
                                'delivery_handling',
                                'transaction_total_amount', 
                                'delivery_cost', 
                                'transaction_datetime'
                                )
                        ->get();
        return $transactions;
    }

    public function getAllTransactionsNew($columns, $search_value = '')
    {

        $total_count = Transactions::join('merchants', 'merchants.id', '=', 'transactions.merchant_id')->count();
        $searchString = "'" . $search_value . "'";

        $transaction_builder = DB::table('transactions')
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->select(
            'transaction_id', 
            'pickup_details',
            'merchant_name', 
            'transaction_status',
            'transaction_payment_type',
            'delivery_details',
            'delivery_handling',
            'transaction_total_amount', 
            'delivery_cost', 
            'transaction_datetime',
            'transaction_order_no', 
            'pod_transaction', 
            'preauth_transaction',
            'transactions.updated_at'
            );

            for ( $i=0 ; $i<count($columns) ; $i++ ) {
                if ( isset($columns[$i]) && $columns[$i]['searchable'] == "true" && $columns[$i]['search']['value'] != '' ) {  
                    if($columns[$i]['data'] == 'transaction_id' || $columns[$i]['data'] == 'updated_at'){
                        $transaction_builder->where('transactions.'.$columns[$i]['data'], 'like', '%' . $columns[$i]['search']['value'] . '%');
                    }else{
                         if(!empty($search_value)){
                            $transaction_builder->orWhere($columns[$i]['data'], 'like', '%' . $search_value . '%');
                        }else{
                            $transaction_builder->where($columns[$i]['data'], 'like', '%' . $columns[$i]['search']['value'] . '%');
                        }
                    }
                        
                }
            }
           $transactions = $transaction_builder->orderBy('transactions.updated_at', 'desc')->get();

           // echo $transactions;die;

            
            $newtransactions = $transactions->map(function($row){
                $address =  (array)json_decode($row->delivery_details);
                $pickup_details =  (array)json_decode($row->pickup_details);

                if(isset($pickup_details)) {
                    $row->pickup_details =  isset($pickup_details["merchant_address"]) ? $pickup_details["merchant_address"] : null;
                }

                if($row->delivery_handling == "to_customer" && isset($address))
                {
                    $row->delivery_location = isset($address["customer_state"]) ? $address["customer_state"] : null;
                    $row->delivery_add = isset($address["customer_address"]) ? $address["customer_address"] : null;
                    $row->delivery_phone = isset($address["customer_phone"]) ? $address["customer_phone"] : null;
                }
                elseif($row->delivery_handling == "customer_pickup")
                {
                    $row->delivery_location = "Courier Location";
                }
                else
                {
                    $row->delivery_location = "Unknown";
                }
                //var_dump($row);
                return $row;
            });
            $results = array();

            $results['total_count'] = $total_count;
            $results['total_filtered'] = $newtransactions->count();
            $results['total'] = $newtransactions;
        
            
            return $results;

    }
    public function getTransactions()
    {
        return Transactions::take(20)->orderBy('updated_at','desc')->get();
    }

    public function getMerchantsTransactions($merchant_id,$columns,$search_value='')
    {
        $total_count = Transactions::join('merchants', 'merchants.id', '=', 'transactions.merchant_id')->join('items', 'items.transaction_id', '=', 'transactions.transaction_id')->where('transactions.merchant_id', '=', $merchant_id)->orderBy('created_at','desc')->count();
        
        
            $transaction_builder = DB::table('transactions')
                 ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                 ->select(
                    'transactions.transaction_id', 
                    'merchant_name',
                    'pickup_details',
                    'transaction_status',
                    'transactions.updated_at',
                    'transaction_payment_type',
                    'delivery_details',
                    'delivery_handling',
                    'transaction_total_amount', 
                    'delivery_cost', 
                    'transaction_datetime',
                    'transaction_order_no', 
                    'pod_transaction', 
                    'preauth_transaction'
                 )->distinct("transactions.transaction_id");

            for ( $i=0 ; $i<count($columns) ; $i++ ) {
                if ( isset($columns[$i]) && $columns[$i]['searchable'] == "true" && $columns[$i]['search']['value'] != '' ) {  
                    if($columns[$i]['data'] == 'transaction_id' || $columns[$i]['data'] == 'updated_at'){
                        $transaction_builder->where('transactions.'.$columns[$i]['data'], 'like', '%' . $columns[$i]['search']['value'] . '%');
                    }else{
                        if(!empty($search_value)){
                            $transaction_builder->orWhere($columns[$i]['data'], 'like', '%' . $search_value . '%');
                        }else{
                            $transaction_builder->where($columns[$i]['data'], 'like', '%' . $columns[$i]['search']['value'] . '%');
                        }
                    }
                        
                }
            }

            $transactions = $transaction_builder->where('transactions.merchant_id','=',$merchant_id)->orderBy('transactions.updated_at', 'desc')->get();
            $newtransactions = $transactions->map(function($row){
            $address =  json_decode($row->delivery_details);
            $pickup_details =  json_decode($row->pickup_details);
            $row->pickup_details =  $pickup_details->merchant_address;
                
            if($row->delivery_handling == "to_customer")
            {
                $row->delivery_location = $address->customer_state;
                $row->delivery_add = $address->customer_address;
                $row->delivery_phone = $address->customer_phone;
            }
            elseif($row->delivery_handling == "customer_pickup")
            {
                $row->delivery_location = "Courier Location";
            }
            else
            {
                $row->delivery_location = "Unknown";
            }
            //var_dump($row);
            return $row;
        });
            $results = array();

            $results['total_count'] = $total_count;
            $results['total_filtered'] = $newtransactions->count();
            $results['total'] = $newtransactions;
        
            
            return $results;
//die;
    }

    public function getCourierTransactions($courier_id,$columns,$search_value='',$dateRange=null)
    {
        
        $total_count = Transactions::join('merchants', 'merchants.id', '=', 'transactions.merchant_id')->join('items', 'items.transaction_id', '=', 'transactions.transaction_id')->where('courier_id', '=', $courier_id)->orderBy('updated_at','desc')->count();
        
            $transaction_builder = DB::table('transactions')
                 ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                 ->select(
                    'transactions.transaction_id', 
                    'merchant_name',
                    'pickup_details',
                    'transaction_status',
                    'transactions.updated_at',
                    'transaction_payment_type',
                    'delivery_details',
                    'delivery_handling',
                    'transaction_total_amount', 
                    'delivery_cost', 
                    'transaction_datetime',
                    'transaction_order_no', 
                    'pod_transaction', 
                    'preauth_transaction'
                )->distinct("transactions.transaction_id")
                ->where('transactions.courier_id','=',$courier_id);

            for ( $i=0 ; $i<count($columns) ; $i++ ) {
                if ( isset($columns[$i]) && $columns[$i]['searchable'] == "true" && $columns[$i]['search']['value'] != '' ) {  
                    if($columns[$i]['data'] == 'transaction_id' || $columns[$i]['data'] == 'updated_at'){
                        $transaction_builder->where('transactions.'.$columns[$i]['data'], 'like', '%' . $columns[$i]['search']['value'] . '%');
                    }else{
                            $transaction_builder->where($columns[$i]['data'], 'like', '%' . $columns[$i]['search']['value'] . '%');
                    }
                }
                if($columns[$i]['data'] == 'delivery_location')
                    continue;
                if(!empty($search_value)){
                    if($i == 0){
                        if($columns[$i]['data'] == 'transaction_id' || $columns[$i]['data'] == 'updated_at'){
                            $transaction_builder->where('transactions.'.$columns[$i]['data'], 'like', '%' . $search_value . '%');
                        }else{
                            $transaction_builder->where($columns[$i]['data'], 'like', '%' . $search_value . '%');
                        }
                    }else{
                        if($columns[$i]['data'] == 'transaction_id' || $columns[$i]['data'] == 'updated_at'){
                            $transaction_builder->orWhere('transactions.'.$columns[$i]['data'], 'like', '%' . $search_value . '%');
                        }else{
                            $transaction_builder->orWhere($columns[$i]['data'], 'like', '%' . $search_value . '%');
                        }
                    }                            
                }
            }

            if($dateRange == null){
                $transactions = $transaction_builder->orderBy('transactions.updated_at', 'desc')->get();
            }else{
                $transactions = $transaction_builder->where('transactions.created_at', '>=', $dateRange['from'])->where('transactions.created_at', '<=', $dateRange['to'])->orderBy('transactions.updated_at', 'desc')->get();
            }

            $newtransactions = $transactions->map(function($row){
                $address =  (array)json_decode($row->delivery_details);
                $pickup_details =  (array)json_decode($row->pickup_details);

                if(isset($pickup_details)) {
                    $row->pickup_details =  isset($pickup_details["merchant_address"]) ? $pickup_details["merchant_address"] : null;
                }
                // mail('palash@geeksperhour.com', 'transadsgsction', print_r($pickup_details->merchant_address, true));
                
                if($row->delivery_handling == "to_customer" && isset($address))
                {
                    $row->delivery_location = isset($address["customer_state"]) ? $address["customer_state"] : null;
                    $row->delivery_add = isset($address["customer_address"]) ? $address["customer_address"] : null;
                    $row->delivery_phone = isset($address["customer_phone"]) ? $address["customer_phone"] : null;
                }
                elseif($row->delivery_handling == "customer_pickup")
                {
                    $row->delivery_location = "Courier Location";
                }
                else
                {
                    $row->delivery_location = "Unknown";
                }
                //var_dump($row);
                return $row;
            });
            $results = array();

            $results['total_count'] = $total_count;
            $results['total_filtered'] = $newtransactions->count();
            $results['total'] = $newtransactions;
        
            
            return $results;

    }

    public function getCourierTransactionsForDateRange($courier_id,$dateRange){
        
            $transactions = DB::table('transactions')
                ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                 ->select(
                    'transactions.transaction_id', 
                    'merchant_name',
                    'pickup_details',
                    'transaction_status',
                    'transactions.updated_at',
                    'transaction_payment_type',
                    'delivery_details',
                    'delivery_handling',
                    'transaction_total_amount', 
                    'delivery_cost', 
                    'transaction_datetime',
                    'transaction_order_no', 
                    'pod_transaction', 
                    'preauth_transaction'
                )
                ->where('courier_id','=',$courier_id)->distinct("transactions.transaction_id")
                ->where('transactions.created_at', '>=', $dateRange['from'])
                ->where('transactions.created_at', '<=', $dateRange['to'])
                ->orderBy('transactions.created_at','desc')
                ->get();

           $newtransactions = $transactions->map(function($row){
             $address =  json_decode($row->delivery_details);
             $pickup_details =  json_decode($row->pickup_details);
             $row->pickup_details =  $pickup_details->merchant_address;

             //print_r($pickup_details->merchant_address);  
            if($row->delivery_handling == "to_customer")
            {
                $row->delivery_location = $address->customer_state;
                $row->delivery_add = $address->customer_address;
                $row->delivery_phone = $address->customer_phone;
            }
            elseif($row->delivery_handling == "customer_pickup")
            {
                $row->delivery_location = "Courier Location";
            }
            else
            {
                $row->delivery_location = "Unknown";
            }
            // var_dump($row);die;

            return $row;
        });
        
        $results = array();

        
        $results['total'] = $newtransactions;
        
    
        
        return $results;
            
        
    }

    public function getMerchantsTransactionsForDateRange($merchant_id,$dateRange){

        $total_count = Transactions::where('merchant_id', '=', $merchant_id)->orderBy('created_at','desc')->count();

        $searchString = "'" . $search_value . "'";

        $transactions = DB::table('transactions')
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->select(
               'transactions.transaction_id', 
               'merchant_name', 
               'pickup_details',
               'transaction_status',
               'transactions.updated_at',
               'transaction_payment_type',
               'delivery_details',
               'delivery_handling',
               'transaction_total_amount', 
               'delivery_cost', 
               'transaction_datetime',
               'transaction_order_no', 
               'pod_transaction', 
               'preauth_transaction'
           )->distinct("transactions.transaction_id")
           ->where('transactions.merchant_id','=',$merchant_id)
           ->where('transactions.created_at', '>=', $dateRange['from'])
           ->where('transactions.created_at', '<=', $dateRange['to'])
           ->orderBy('transactions.created_at','desc')
           ->get();
            
            $newtransactions = $transactions->map(function($row){
            $address =  json_decode($row->delivery_details);
            $pickup_details =  json_decode($row->pickup_details);
            $row->pickup_details =  $pickup_details->merchant_address;

            if($row->delivery_handling == "to_customer")
            {
                $row->delivery_location = $address->customer_state;
                $row->delivery_add = $address->customer_address;
                $row->delivery_phone = $address->customer_phone;
            }
            elseif($row->delivery_handling == "customer_pickup")
            {
                $courier_location_id = (int)$address;
                $courierLocation = new \Dappur\Model\CourierLocation;

            //$transactions = $transaction->find($transactionID);
                $courier_location = $courierLocation->find($courier_location_id);
            if($courier_location->count() > 0)
            {
                $row->delivery_location = $courier_location->state;
            }

            }
            else
            {
                $row->delivery_location = "Unknown";
            }
            //var_dump($row);
                return $row;
            });
            $results = array();
 
                $results['total_count'] = $total_count;
                $results['total_filtered'] = $newtransactions->count();
                $results['total'] = $newtransactions;
           
            return $results;

    }

    public function getAllTransactionsForDateRange($dateRange){

        $total_count = Transactions::join('merchants', 'merchants.id', '=', 'transactions.merchant_id')->join('items', 'items.transaction_id', '=', 'transactions.transaction_id')->count();
        $searchString = "'" . $search_value . "'";

        $transaction_builder = DB::table('transactions')
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->select(
            'transactions.transaction_id', 
            'merchant_name',
            'pickup_details',
            'transaction_status',
            'transactions.updated_at',
            'transaction_payment_type',
            'delivery_details',
            'delivery_handling',
            'transaction_total_amount', 
            'delivery_cost', 
            'transaction_datetime',
            'transaction_order_no', 
            'pod_transaction', 
            'preauth_transaction'
            )->distinct("transactions.transaction_id");

        for ( $i=0 ; $i<count($columns) ; $i++ ) {
            if ( isset($columns[$i]) && $columns[$i]['searchable'] == "true" && $columns[$i]['search']['value'] != '' ) {  
                if($columns[$i]['data'] == 'transaction_id' || $columns[$i]['data'] == 'updated_at'){
                    $transaction_builder->where('transactions.'.$columns[$i]['data'], 'like', '%' . $columns[$i]['search']['value'] . '%');
                }else{
                        if(!empty($search_value)){
                        $transaction_builder->orWhere($columns[$i]['data'], 'like', '%' . $search_value . '%');
                    }else{
                        $transaction_builder->where($columns[$i]['data'], 'like', '%' . $columns[$i]['search']['value'] . '%');
                    }
                }
                    
            }
        }
        $transactions = $transaction_builder->orderBy('transactions.updated_at', 'desc')
                        ->where('transactions.created_at', '>=', $dateRange['from'])
                        ->where('transactions.created_at', '<=', $dateRange['to'])
                        ->get();

            
            $newtransactions = $transactions->map(function($row){
            $address =  json_decode($row->delivery_details);
            $pickup_details =  json_decode($row->pickup_details);
            $row->pickup_details =  $pickup_details->merchant_address;
                
            if($row->delivery_handling == "to_customer")
            {
                $row->delivery_location = $address->customer_state;
                $row->delivery_add = $address->customer_address;
                $row->delivery_phone = $address->customer_phone;
            }
            elseif($row->delivery_handling == "customer_pickup")
            {
                $row->delivery_location = "Courier Location";
            }
            else
            {
                $row->delivery_location = "Unknown";
            }
            //var_dump($row);
            return $row;
        });
            $results = array();

            $results['total_count'] = $total_count;
            $results['total_filtered'] = $newtransactions->count();
            $results['total'] = $newtransactions;
        
            
            return $results;

    }
    

    public function getTransactionsByCourier($courier_id)
    { 

        $transactions = Transactions::where('courier_id', '=', $courier_id)
            ->orderBy('transactions.updated_at','desc')
            ->limit(5000)
            ->get();

        return $transactions;

    }

    public function getTransactionsByCourierSettlement($courier_id)
    {

        $transactions = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('courier_id', '=', $courier_id)
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->orderBy('transactions.updated_at','desc')
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                "users.first_name",
                "users.last_name"
            )->distinct('transactions.transaction_id')
            ->limit(5000)
            ->get();

        return $transactions;

    }

    public function getTransactionsByMerchant($merchant_id)
    {

        $transactions = DB::table('transactions')
            ->where('merchant_id', '=', $merchant_id)
            ->orderBy('transactions.updated_at','desc')
            ->limit(5000)
            ->get();

        return $transactions;

    }

    public function getTransactionsByMerchantSettlement($merchant_id)
    {

        $transactions = DB::table("transactions")
        ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
        ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
        ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
        ->where('transactions.merchant_id', '=', $merchant_id)
        ->whereIn('transactions.transaction_status',[3,301,30])
        ->orderBy('transactions.updated_at','desc')
        ->select(                    
            'transactions.*', 
            'merchants.merchant_name', 
            "users.first_name",
            "users.last_name"
        )->distinct('transactions.transaction_id')
        ->limit(5000)
        ->get();

        return $transactions;

    }

    public function getTransactionValueByStatus($status,$typeID = null,$type=null)
    {
      //d($status);
      
        if($typeID == null || $type == null)
        {
            $transactions = DB::table('transactions')
                ->whereIn('transaction_status', $status)
                ->sum('transaction_total_amount');
        }
      elseif($type == 'Merchant' and $typeID != null)
      {
        $transactions = DB::table('transactions')
            ->whereIn('transaction_status', $status)
            ->where('merchant_id', '=', $typeID)
            ->sum('transaction_total_amount');
      }

      elseif($type == 'Courier' and $typeID != null)
      {
        $transactions = DB::table('transactions')
            ->whereIn('transaction_status', $status)
            ->where('courier_id', '=', $typeID)
            ->sum('transaction_total_amount');
      }



      return $transactions;
    }

    public function getTransaction($transaction_id)
    {
        $transaction = Transactions::where('transaction_id', '=', $transaction_id);

        d($transaction);
        die;
    }

    public function getLatestTransactionByStatus($status)
    {

      $transactions = DB::table('transactions')
                ->whereIn('transaction_status', [$status])
                //->where('transaction_status', '=', $status)
                ->latest()
                ->first();
        if($transactions == null)
          return "0000-00-00";
        else
          return $transactions->updated_at;
       //return Transactions::where('transaction_status', '=', $status);
    }

    public function getTransactionByStatus($status)
    {
        return Transactions::where('transaction_status', '=', $status)->get();
    }

    public function getLatestTransactions()
    {
        return Transactions::take(20)->get();
    }

    public function getClientName($client_id)
	{
	  	//$merchant = new Merchants;
	  	$merchant = Merchants::where('id', '=', $client_id)->first();
	  	return $merchant['merchant_name'];
    }

    public function getCourierName($courier_id)
    {
        $courier = Courier::where('id', '=', $courier_id)->first();
        return $courier['courier_name'];
    }

    public function getCourierAssignDetails($transactionPrimaryID, $item_id)
    {
        $deliveryAssignments = DB::table('delivery_assignment')
                ->where('item_id', '=', $item_id)
                ->where('type', '=', 'pickup')
                ->where('transaction_id', '=', $transactionPrimaryID)
                ->get();
        return $deliveryAssignments;
    }

    public function getTransactionStatus($status_id)
    {
    	$status = Status::where('status_id','=', $status_id)->first();
    	return $status['status'];

    }

    public function getTransactionPaymentType($trans_id)
    {
        $transaction = Transactions::find($trans_id);
        //d($transaction->pod_transaction);
        //d($transaction->preauth_transaction);

        if($transaction->pod_transaction == 0 and $transaction->preauth_transaction == 0 )
        { $payment_type = "Prepaid";}
        elseif($transaction->pod_transaction == 1 and $transaction->preauth_transaction == 0)
        {$payment_type = "POD";}
        elseif($transaction->pod_transaction == 0 and $transaction->preauth_transaction == 1)
        {$payment_type = "DOD / PreAuth";}
        else{$payment_type = "";}

       return $payment_type;
    }

    public function getTransactionDetails($transaction_id)
    {
        return Transactions::where('transaction_id','=',$transaction_id)->first();
    }


    public function getMerchantTransactionCountByStatus($status_id,$merchant_id)
    {
        $transactions = Transactions::where('merchant_id', $merchant_id)
                    ->where(function($q) use($status_id) {
                        foreach ($status_id as $item) {
                            $q->orWhere(function($q) use($item)
                            {
                                $q->where('transaction_status', $item);
                                //echo $item;
                            });
                        }
                    })->count();
            return $transactions;
    }

    public function getCourierTransactionCountByStatus($status_id,$courier_id)
    {
        
        $transactions = Transactions::where('courier_id', $courier_id)
                ->where(function($q) use($status_id) {
                    foreach ($status_id as $item) {
                         $q->orWhere(function($q) use($item)
                         {
                             $q->where('transaction_status', $item);
                             //echo $item;
                        });
                    }
                })->count();
        return $transactions;
    }

    public function getTransactionCountByStatus($status_id)
    {
        $transactions = Transactions::whereIn('transaction_status', [$status_id])->count();
        return $transactions;
        
   }

   public function countTransactionsLastFourDays($courier_id, $number_of_days)
   {
        $transactions = DB::select('select * from transactions where TO_DAYS(updated_at) > ? AND courier_id = ? ', [$number_of_days, $courier_id] );
        //d($transactions);
        //$transactions = Transactions::whereRaw('TO_DAYS(updated_at) = ? and courier_id = ?', [$number_of_days,$courier_id]);
        //$status = [0,1,14,11];
         //$transactions = DB::table('transactions')
           //        ->whereIn('transaction_status', [$status])
             //      ->get();
                   //->where('updated_at'), '=',  $number_of_days)
                   //->latest()
                  //->count();
        d($transactions);
        die;
        //return count($transactions);
   }

   public function getTransactionByOrderNo($order_no,$merchant_id)
   {
        //$transactions = Transactions::where('transaction_order_no', '=', $order_no)->count();
        $transactions = DB::select('select * from transactions where transaction_order_no = ? AND merchant_id = ? ', [$order_no, $merchant_id] );
        return count($transactions);
   }
   
   public function downloadCSV($type_id, $start_date, $end_date)
   {
	   
	   		 $transactions = DB::table('transactions')->get();
	   		//$transactions = DB::table('transactions')
         	//->join('merchants', 'transactions.merchant_id', '=', 'merchants.id')
            //->join('items', 'transactions.transaction_id', '=', 'items.transaction_id')
           // ->select('*')
            //->where('transactions.courier_id', '=', $type_id)
            //->whereBetween('transaction_datetime', [$start_date, $end_date])
           // ->get();
          
           //->join('merchants', 'transactions.merchant_id', '=', 'merchants.id')
            			 
			/** select 
			t.transaction_id, i.item_id, transaction_datetime,delivery_status,
			transaction_payment_type, IF(`pod_transaction` = 1, 'Yes', 'No') AS `POD`,
			transaction_total_amount, delivery_cost, transaction_status,  merchant_name, item_name, item_weight, delivery_details
			from transactions t, items i, merchants m 
			where 
			m.id = t.merchant_id and
			t.transaction_id = i.transaction_id and
			transaction_datetime between '2018-01-01' and now() 
			and courier_id = 18
			order by t.created_at desc **/
			//echo "hello world";   
		 
		 return $transactions->toArray();
   }

   public function getAllTransactionsforSettlement($columns, $search_value = '')
    {

        $total_count = Transactions::count();
        
        $transaction_builder = DB::table('transactions');

            for ( $i=0 ; $i<count($columns) ; $i++ ) {
                if ( isset($columns[$i]) && $columns[$i]['searchable'] == "true" && $columns[$i]['search']['value'] != '' ) {  
                    if($columns[$i]['data'] == 'transaction_id' || $columns[$i]['data'] == 'updated_at'){
                        $transaction_builder->where('transactions.'.$columns[$i]['data'], 'like', '%' . $columns[$i]['search']['value'] . '%');
                    }else{
                         if(!empty($search_value)){
                            $transaction_builder->orWhere($columns[$i]['data'], 'like', '%' . $search_value . '%');
                        }else{
                            $transaction_builder->where($columns[$i]['data'], 'like', '%' . $columns[$i]['search']['value'] . '%');
                        }
                    }
                        
                }
            }
           $transactions = $transaction_builder->orderBy('transactions.updated_at', 'desc')->get();
            
            $newtransactions = $transactions->map(function($transaction){

            $settled_amount = 0;
            $due_to_courier = 0;
            $due_to_merchant = 0; 
            $due_to_netplus = 0;
            $pay_type = "Prepaid";

            // $merchant = $this->models['merchant'];

            $mtype = Merchants::find($transaction->merchant_id);
            

            if ($transaction->transaction_status >= 3 && $transaction->transaction_status >= 301){

                if ($mtype->settlement_type == 'separated'){
                    $settled_amount = $transaction->delivery_cost;
                    $due_to_courier = $settled_amount *  0.9;
                    $due_to_netplus = $settled_amount * 0.1;

                }else{
                    $settled_amount = $transaction->transaction_total_amount + $transaction->delivery_cost;
                    $due_to_merchant = $settled_amount;
                }
                                         
            }


            if($transaction->transaction_payment_type == 'Prepaid'){
                 $settled_amount = $transaction->delivery_cost; 
                 $due_to_courier = $settled_amount *  0.9;
                 $due_to_netplus = $settled_amount * 0.1;
                 $due_to_merchant = $transaction->transaction_total_amount;
            }

            if ($transaction->transaction_payment_type == 'Card' && $transaction->pod_transaction == 1){
                $pay_type = "POD - Card";
                $total_cost_of_transaction = $transaction->transaction_total_amount + $transaction->delivery_cost;
                 $card_charge = 0.0075 * $total_cost_of_transaction;
                 $settled_amount =   $total_cost_of_transaction - $card_charge;
                $due_to_courier = $transaction->delivery_cost * 0.9; 
                  $due_to_netplus = $transaction->delivery_cost  * 0.1;
                  $due_to_merchant = $transaction->transaction_total_amount - $card_charge; 
            }


            if ($transaction->preauth_transaction == 1 && $transaction->transaction_payment_type == 'Card'){
                $pay_type = "DOD - Card";
              $total_cost_of_transaction = $transaction->transaction_total_amount + $transaction->delivery_cost;
              $card_charge = 0.0075 * $total_cost_of_transaction;
             $settled_amount =   $total_cost_of_transaction - $card_charge; 
             $due_to_courier = ($transaction->delivery_cost - $card_charge) * 0.9;
            $due_to_netplus = ($transaction->delivery_cost - $card_charge) * 0.1;
            $due_to_merchant = $transaction->transaction_total_amount - $card_charge;
            }

            if ($transaction->transaction_payment_type == 'Cash' and $transaction->pod_transaction == 1){

                $pay_type = "POD - Cash";
               $settled_amount = $transaction->transaction_total_amount + $transaction->delivery_cost;
               $due_to_courier = $transaction->delivery_cost * 0.9;
               $due_to_netplus = $transaction->delivery_cost * 0.1;
              $due_to_merchant = $transaction->transaction_total_amount;

            } 

            $transaction->due_to_courier = $due_to_courier;
            $transaction->due_to_netplus = $due_to_netplus;
            $transaction->due_to_merchant = $due_to_merchant;
            $transaction->pay_type = $pay_type;
            $transaction->settled_amount = $settled_amount;

          

            $merchant_name = $this->getClientName($transaction->merchant_id);
            $courier_name = $this->getCourierName($transaction->courier_id);

            $transaction->merchant_name = $merchant_name;
            $transaction->courier_name = $courier_name;
           
            
            $customer_addressBoolean = strpos($transaction->delivery_details, 'customer_address');

            $aaaa = strpos($transaction->delivery_details, 'address');

            if($customer_addressBoolean == true){
                
                $json = json_decode($transaction->delivery_details, true);
                $customerAddress = $json['customer_address'].", ".$json['customer_lga'].", ".$json['customer_state'];
                $transaction->customer_address_new = $customerAddress;
                
            }elseif($aaaa == true){
                
                $json = json_decode($transaction->delivery_details, true);
                $customerAddress = $json['address'].", ".$json['city'].", ".$json['state'].", ".$json['country'];
                $transaction->customer_address_new = $customerAddress;
                
            }else{
                $transaction->customer_address_new = $transaction->delivery_details;
            }
            return $transaction;
        });
            $results = array();

            $results['total_count'] = $total_count;
            $results['total_filtered'] = $newtransactions->count();
            $results['total'] = $newtransactions;
        
            
            return $results;

    }

    public function getMerchantsTransactionsforSettlement($merchant_id, $columns, $search_value = '')
    {

        $total_count = Transactions::where('merchant_id', '=', $merchant_id)->count();
        
        $transaction_builder = DB::table('transactions');

            for ( $i=0 ; $i<count($columns) ; $i++ ) {
                if ( isset($columns[$i]) && $columns[$i]['searchable'] == "true" && $columns[$i]['search']['value'] != '' ) {  
                    if($columns[$i]['data'] == 'transaction_id' || $columns[$i]['data'] == 'updated_at'){
                        $transaction_builder->where('transactions.'.$columns[$i]['data'], 'like', '%' . $columns[$i]['search']['value'] . '%');
                    }else{
                         if(!empty($search_value)){
                            $transaction_builder->orWhere($columns[$i]['data'], 'like', '%' . $search_value . '%');
                        }else{
                            $transaction_builder->where($columns[$i]['data'], 'like', '%' . $columns[$i]['search']['value'] . '%');
                        }
                    }
                        
                }
            }
           $transactions = $transaction_builder->orderBy('transactions.updated_at', 'desc')->where('merchant_id', '=', $merchant_id)->get();
            
            $newtransactions = $transactions->map(function($transaction){

            $settled_amount = 0;
            $due_to_courier = 0;
            $due_to_merchant = 0; 
            $due_to_netplus = 0;
            $pay_type = "Prepaid";

            // $merchant = $this->models['merchant'];

            $mtype = Merchants::find($transaction->merchant_id);
            

            if ($transaction->transaction_status >= 3 && $transaction->transaction_status >= 301){

                if ($mtype->settlement_type == 'separated'){
                    $settled_amount = $transaction->delivery_cost;
                    $due_to_courier = $settled_amount *  0.9;
                    $due_to_netplus = $settled_amount * 0.1;

                }else{
                    $settled_amount = $transaction->transaction_total_amount + $transaction->delivery_cost;
                    $due_to_merchant = $settled_amount;
                }
                                         
            }


            if($transaction->transaction_payment_type == 'Prepaid'){
                 $settled_amount = $transaction->delivery_cost; 
                 $due_to_courier = $settled_amount *  0.9;
                 $due_to_netplus = $settled_amount * 0.1;
                 $due_to_merchant = $transaction->transaction_total_amount;
            }

            if ($transaction->transaction_payment_type == 'Card' && $transaction->pod_transaction == 1){
                $pay_type = "POD - Card";
                $total_cost_of_transaction = $transaction->transaction_total_amount + $transaction->delivery_cost;
                 $card_charge = 0.0075 * $total_cost_of_transaction;
                 $settled_amount =   $total_cost_of_transaction - $card_charge;
                $due_to_courier = $transaction->delivery_cost * 0.9; 
                  $due_to_netplus = $transaction->delivery_cost  * 0.1;
                  $due_to_merchant = $transaction->transaction_total_amount - $card_charge; 
            }


            if ($transaction->preauth_transaction == 1 && $transaction->transaction_payment_type == 'Card'){
                $pay_type = "DOD - Card";
              $total_cost_of_transaction = $transaction->transaction_total_amount + $transaction->delivery_cost;
              $card_charge = 0.0075 * $total_cost_of_transaction;
             $settled_amount =   $total_cost_of_transaction - $card_charge; 
             $due_to_courier = ($transaction->delivery_cost - $card_charge) * 0.9;
            $due_to_netplus = ($transaction->delivery_cost - $card_charge) * 0.1;
            $due_to_merchant = $transaction->transaction_total_amount - $card_charge;
            }

            if ($transaction->transaction_payment_type == 'Cash' and $transaction->pod_transaction == 1){

                $pay_type = "POD - Cash";
               $settled_amount = $transaction->transaction_total_amount + $transaction->delivery_cost;
               $due_to_courier = $transaction->delivery_cost * 0.9;
               $due_to_netplus = $transaction->delivery_cost * 0.1;
              $due_to_merchant = $transaction->transaction_total_amount;

            } 

            $transaction->due_to_courier = $due_to_courier;
            $transaction->due_to_netplus = $due_to_netplus;
            $transaction->due_to_merchant = $due_to_merchant;
            $transaction->pay_type = $pay_type;
            $transaction->settled_amount = $settled_amount;

          

            $merchant_name = $this->getClientName($transaction->merchant_id);
            $courier_name = $this->getCourierName($transaction->courier_id);

            $transaction->merchant_name = $merchant_name;
            $transaction->courier_name = $courier_name;
           
            
            $customer_addressBoolean = strpos($transaction->delivery_details, 'customer_address');

            $aaaa = strpos($transaction->delivery_details, 'address');

            if($customer_addressBoolean == true){
                
                $json = json_decode($transaction->delivery_details, true);
                $customerAddress = $json['customer_address'].", ".$json['customer_lga'].", ".$json['customer_state'];
                $transaction->customer_address_new = $customerAddress;
                
            }elseif($aaaa == true){
                
                $json = json_decode($transaction->delivery_details, true);
                $customerAddress = $json['address'].", ".$json['city'].", ".$json['state'].", ".$json['country'];
                $transaction->customer_address_new = $customerAddress;
                
            }else{
                $transaction->customer_address_new = $transaction->delivery_details;
            }
            return $transaction;
        });
            $results = array();

            $results['total_count'] = $total_count;
            $results['total_filtered'] = $newtransactions->count();
            $results['total'] = $newtransactions;
        
            
            return $results;

    }

    public function getCourierTransactionsforSettlement($courier_id, $columns, $search_value = '')
    {

        $total_count = Transactions::where('courier_id', '=', $courier_id)->count();
        
        $transaction_builder = DB::table('transactions');

            for ( $i=0 ; $i<count($columns) ; $i++ ) {
                if ( isset($columns[$i]) && $columns[$i]['searchable'] == "true" && $columns[$i]['search']['value'] != '' ) {  
                    if($columns[$i]['data'] == 'transaction_id' || $columns[$i]['data'] == 'updated_at'){
                        $transaction_builder->where('transactions.'.$columns[$i]['data'], 'like', '%' . $columns[$i]['search']['value'] . '%');
                    }else{
                         if(!empty($search_value)){
                            $transaction_builder->orWhere($columns[$i]['data'], 'like', '%' . $search_value . '%');
                        }else{
                            $transaction_builder->where($columns[$i]['data'], 'like', '%' . $columns[$i]['search']['value'] . '%');
                        }
                    }
                        
                }
            }
           $transactions = $transaction_builder->where('courier_id', '=', $courier_id)->orderBy('transactions.updated_at', 'desc')->get();
            
            $newtransactions = $transactions->map(function($transaction){

            $settled_amount = 0;
            $due_to_courier = 0;
            $due_to_merchant = 0; 
            $due_to_netplus = 0;
            $pay_type = "Prepaid";

            // $merchant = $this->models['merchant'];

            $mtype = Merchants::find($transaction->merchant_id);
            

            if ($transaction->transaction_status >= 3 && $transaction->transaction_status >= 301){

                if ($mtype->settlement_type == 'separated'){
                    $settled_amount = $transaction->delivery_cost;
                    $due_to_courier = $settled_amount *  0.9;
                    $due_to_netplus = $settled_amount * 0.1;

                }else{
                    $settled_amount = $transaction->transaction_total_amount + $transaction->delivery_cost;
                    $due_to_merchant = $settled_amount;
                }
                                         
            }


            if($transaction->transaction_payment_type == 'Prepaid'){
                 $settled_amount = $transaction->delivery_cost; 
                 $due_to_courier = $settled_amount *  0.9;
                 $due_to_netplus = $settled_amount * 0.1;
                 $due_to_merchant = $transaction->transaction_total_amount;
            }

            if ($transaction->transaction_payment_type == 'Card' && $transaction->pod_transaction == 1){
                $pay_type = "POD - Card";
                $total_cost_of_transaction = $transaction->transaction_total_amount + $transaction->delivery_cost;
                 $card_charge = 0.0075 * $total_cost_of_transaction;
                 $settled_amount =   $total_cost_of_transaction - $card_charge;
                $due_to_courier = $transaction->delivery_cost * 0.9; 
                  $due_to_netplus = $transaction->delivery_cost  * 0.1;
                  $due_to_merchant = $transaction->transaction_total_amount - $card_charge; 
            }


            if ($transaction->preauth_transaction == 1 && $transaction->transaction_payment_type == 'Card'){
                $pay_type = "DOD - Card";
              $total_cost_of_transaction = $transaction->transaction_total_amount + $transaction->delivery_cost;
              $card_charge = 0.0075 * $total_cost_of_transaction;
             $settled_amount =   $total_cost_of_transaction - $card_charge; 
             $due_to_courier = ($transaction->delivery_cost - $card_charge) * 0.9;
            $due_to_netplus = ($transaction->delivery_cost - $card_charge) * 0.1;
            $due_to_merchant = $transaction->transaction_total_amount - $card_charge;
            }

            if ($transaction->transaction_payment_type == 'Cash' and $transaction->pod_transaction == 1){

                $pay_type = "POD - Cash";
               $settled_amount = $transaction->transaction_total_amount + $transaction->delivery_cost;
               $due_to_courier = $transaction->delivery_cost * 0.9;
               $due_to_netplus = $transaction->delivery_cost * 0.1;
              $due_to_merchant = $transaction->transaction_total_amount;

            } 

            $transaction->due_to_courier = $due_to_courier;
            $transaction->due_to_netplus = $due_to_netplus;
            $transaction->due_to_merchant = $due_to_merchant;
            $transaction->pay_type = $pay_type;
            $transaction->settled_amount = $settled_amount;

            $merchant_name = $this->getClientName($transaction->merchant_id);
            $courier_name = $this->getCourierName($transaction->courier_id);

            $transaction->merchant_name = $merchant_name;
            $transaction->courier_name = $courier_name;
           
            
            $customer_addressBoolean = strpos($transaction->delivery_details, 'customer_address');

            $aaaa = strpos($transaction->delivery_details, 'address');

            if($customer_addressBoolean == true){
                
                $json = json_decode($transaction->delivery_details, true);
                $customerAddress = $json['customer_address'].", ".$json['customer_lga'].", ".$json['customer_state'];
                $transaction->customer_address_new = $customerAddress;
                
            }elseif($aaaa == true){
                
                $json = json_decode($transaction->delivery_details, true);
                $customerAddress = $json['address'].", ".$json['city'].", ".$json['state'].", ".$json['country'];
                $transaction->customer_address_new = $customerAddress;
                
            }else{
                $transaction->customer_address_new = $transaction->delivery_details;
            }
            return $transaction;
        });
            $results = array();

            $results['total_count'] = $total_count;
            $results['total_filtered'] = $newtransactions->count();
            $results['total'] = $newtransactions;
        
            
            return $results;

    }

    public function filterSuccessfullB2BDeliveriesCourier($courier_id, $filter_data, $order_info, $from_date, $to_date) {

        $count  = DB::table('saddle_lite_transaction')
                    ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                    ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                    ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                    ->where('transactions.courier_id','=',$courier_id)
                    ->count();

        if(empty($filter_data)) {

            $transactionQ =  DB::table('saddle_lite_transaction')
                            ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                            ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                            ->where("transactions.courier_id","=",$courier_id)
                            ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                            ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                            ->groupBy('saddle_lite_transaction.transaction_id')
                            ->select('transactions.*','merchants.merchant_name',
                                DB::raw("(select courier_name from courier where courier.id = transactions.courier_id) as courier_name"),
                                DB::raw("sum(saddle_lite_transaction.amount) as recorded_payment"),
                                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'))
                            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ =  DB::table('saddle_lite_transaction')
                    ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                    ->where("transactions.courier_id","=",$courier_id)
                    ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                    ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                    ->where($filter_data)
                    ->groupBy('transactions.transaction_id')
                    ->select('transactions.*','merchants.merchant_name',
                        DB::raw("(select courier_name from courier where courier.id = transactions.courier_id) as courier_name"),
                        DB::raw("sum(saddle_lite_transaction.amount) as recorded_payment"),
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'))
                    ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    }

    public function filterSuccessfullB2BDeliveriesMerchants($merchant_id, $filter_data, $order_info, $from_date, $to_date) {
       
        $count  = DB::table('saddle_lite_transaction')
                    ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                    ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                    ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                    ->where('merchants.id','=',$merchant_id)
                    ->count();

        if(empty($filter_data)) {

            $transactionQ =  DB::table('saddle_lite_transaction')
                            ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                            ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                            ->where('merchants.id','=',$merchant_id)
                            ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                            ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                            ->groupBy('saddle_lite_transaction.transaction_id')
                            ->select('transactions.*','merchants.merchant_name',
                                DB::raw("(select courier_name from courier where courier.id = transactions.courier_id) as courier_name"),
                                DB::raw("sum(saddle_lite_transaction.amount) as recorded_payment"),
                                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'))
                            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ =  DB::table('saddle_lite_transaction')
                    ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                    ->where('merchants.id','=',$merchant_id)
                    ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                    ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                    ->where($filter_data)
                    ->groupBy('saddle_lite_transaction.transaction_id')
                    ->select('transactions.*','merchants.merchant_name',
                        DB::raw("sum(saddle_lite_transaction.amount) as recorded_payment"),
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'))
                    ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    }

    public function filterSuccessfullB2BDeliveriesAdmin($filter_data, $order_info, $from_date, $to_date) {
       
        $count  = DB::table('saddle_lite_transaction')
                    ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                    ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                    ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                    ->count();

        if(empty($filter_data)) {

            $transactionQ =  DB::table('saddle_lite_transaction')
                            ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                            ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                            ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                            ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                            ->groupBy('saddle_lite_transaction.transaction_id')
                            ->select('transactions.*','merchants.merchant_name',
                                DB::raw("(select courier_name from courier where courier.id = transactions.courier_id) as courier_name"),
                                DB::raw("sum(saddle_lite_transaction.amount) as recorded_payment"),
                                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'))
                            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ =  DB::table('saddle_lite_transaction')
                    ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                    ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                    ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                    ->where($filter_data)
                    ->groupBy('saddle_lite_transaction.transaction_id')
                    ->select('transactions.*','merchants.merchant_name',
                        DB::raw("(select courier_name from courier where courier.id = transactions.courier_id) as courier_name"),
                        DB::raw("sum(saddle_lite_transaction.amount) as recorded_payment"),
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'))
                    ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    }

    public function filterSuccessfullDeliveriesCourier($courier_id, $filter_data, $order_info, $from_date, $to_date) {

        $count  = Transactions::where("courier_id","=",$courier_id)
                    ->whereIn('transactions.transaction_status',[3,301,30])
                    ->count();

        if(empty($filter_data)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('courier_id', '=', $courier_id)
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('courier_id', '=', $courier_id)
            ->where($filter_data)
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(
                'transactions.*',
                'merchants.merchant_name',
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();
        
        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];
    
    }

    public function filterSuccessfullDeliveriesMerchants($merchant_id, $filter_data, $order_info, $from_date, $to_date) {

        $count  =  DB::table("transactions")
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
                    ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
                    ->where('merchants.id', '=', $merchant_id)
                    ->whereIn('transactions.transaction_status',[3,301,30])
                    ->select(                    
                        'transactions.*', 
                        'merchants.merchant_name', 
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                        DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
                    )->distinct('transactions.transaction_id')
                    ->count();

        if(empty($filter_data)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('merchants.id', '=', $merchant_id)
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('merchants.id', '=', $merchant_id)
            ->where($filter_data)
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();
        
        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];
    
    }

    public function filterSuccessfullDeliveriesAdmin($filter_data, $order_info, $from_date, $to_date) {

        $count  = Transactions::whereIn('transactions.transaction_status',[3,301,30])
                    ->count();

        if(empty($filter_data)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`"),
                DB::raw("(select `courier`.`courier_name` from `courier` where `courier`.`id` = `transactions`.`courier_id`) as `courier_name`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where($filter_data)
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`"),
                DB::raw("(select `courier`.`courier_name` from `courier` where `courier`.`id` = `transactions`.`courier_id`) as `courier_name`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);
        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    }

    public function searchSuccesfullB2BDeliveriesCourier($courier_id, $search_term, $order_info, $from_date, $to_date)  {

        $count  = DB::table('saddle_lite_transaction')
                    ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                    ->where('transactions.courier_id','=',$courier_id)
                    ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                    ->count();

        if(is_null($search_term) || empty($search_term)) {

            $transactionQ =  DB::table('saddle_lite_transaction')
                            ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                            ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                            ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                            ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                            ->where('transactions.courier_id','=',$courier_id)
                            ->groupBy('saddle_lite_transaction.transaction_id')
                            ->select('transactions.*','merchants.merchant_name',
                                DB::raw("(select courier_name from courier where courier.id = transactions.courier_id) as courier_name"),
                                DB::raw("sum(saddle_lite_transaction.amount) as recorded_payment"),
                                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'))
                            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ =  DB::table('saddle_lite_transaction')
                    ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                    ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                    ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                    ->where('transactions.courier_id','=',$courier_id)
                    ->where("transactions.transaction_id", "like", $search_term . '%')
                    ->groupBy('saddle_lite_transaction.transaction_id')
                    ->select('transactions.*','merchants.merchant_name',
                        DB::raw("(select courier_name from courier where courier.id = transactions.courier_id) as courier_name"),
                        DB::raw("sum(saddle_lite_transaction.amount) as recorded_payment"),
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'))
                    ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    }

    public function searchSuccesfullB2BDeliveriesMerchants($merchant_id, $search_term, $order_info, $from_date, $to_date)  {

        $count  = DB::table('saddle_lite_transaction')
                    ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                    ->where('merchants.merchant_id','=',$merchant_id)
                    ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                    ->count();

        if(is_null($search_term) || empty($search_term)) {

            $transactionQ =  DB::table('saddle_lite_transaction')
                            ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                            ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                            ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                            ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                            ->where('merchants.merchant_id','=',$merchant_id)
                            ->groupBy('saddle_lite_transaction.transaction_id')
                            ->select('transactions.*','merchants.merchant_name',
                                DB::raw("(select courier_name from courier where courier.id = transactions.courier_id) as courier_name"),
                                DB::raw("sum(saddle_lite_transaction.amount) as recorded_payment"),
                                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'))
                            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ =  DB::table('saddle_lite_transaction')
                    ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                    ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                    ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                    ->where('merchants.merchant_id','=',$merchant_id)
                    ->where("transactions.transaction_id", "like", $search_term . '%')
                    ->groupBy('saddle_lite_transaction.transaction_id')
                    ->select('transactions.*','merchants.merchant_name',
                        DB::raw("sum(saddle_lite_transaction.amount) as recorded_payment"),
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'))
                    ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    }

    public function searchSuccesfullB2BDeliveriesAdmin($search_term, $order_info, $from_date, $to_date)  {

        $count  = DB::table('saddle_lite_transaction')
                    ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                    ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                    ->count();

        if(is_null($search_term) || empty($search_term)) {

            $transactionQ =  DB::table('saddle_lite_transaction')
                            ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                            ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                            ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                            ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                            ->groupBy('saddle_lite_transaction.transaction_id')
                            ->select('transactions.*','merchants.merchant_name',
                                DB::raw("(select courier_name from courier where courier.id = transactions.courier_id) as courier_name"),
                                DB::raw("sum(saddle_lite_transaction.amount) as recorded_payment"),
                                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'))
                            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ =  DB::table('saddle_lite_transaction')
                    ->join('transactions','transactions.transaction_id','=','saddle_lite_transaction.transaction_id')
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->join("users",'users.id','=','saddle_lite_transaction.agent_id')
                    ->where("saddle_lite_transaction.status",'=',"SUCCESS")
                    ->where("saddle_lite_transaction.transaction_type",'=',"CASH")
                    ->where("transactions.transaction_id", "like", $search_term . '%')
                    ->groupBy('saddle_lite_transaction.transaction_id')
                    ->select('transactions.*','merchants.merchant_name',
                        DB::raw("(select courier_name from courier where courier.id = transactions.courier_id) as courier_name"),
                        DB::raw("sum(saddle_lite_transaction.amount) as recorded_payment"),
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'))
                    ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    }

    public function searchSuccessfullDeliveriesAdmin($search_term, $order_info, $from_date, $to_date)  {

        $count  = Transactions::whereIn('transactions.transaction_status',[3,301,30])
                    ->count();

        if(is_null($search_term) || empty($search_term)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`"),
                DB::raw("(select `courier`.`courier_name` from `courier` where `courier`.`id` = `transactions`.`courier_id`) as `courier_name`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where("transactions.transaction_id", "like", $search_term . '%')
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`"),
                DB::raw("(select `courier`.`courier_name` from `courier` where `courier`.`id` = `transactions`.`courier_id`) as `courier_name`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);
        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    }

    public function searchSuccessfullDeliveriesCourier($courier_id, $search_term, $order_info, $from_date, $to_date)  {
        
        $count  = Transactions::where("courier_id","=",$courier_id)
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->count();

        if(is_null($search_term) || empty($search_term)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where("transactions.courier_id",'=',$courier_id)
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where("transactions.courier_id",'=',$courier_id)
            ->where("transactions.transaction_id", "like", $search_term . '%')
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);
        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];
    }

    public function searchSuccessfullDeliveriesMerchants($merchant_id, $search_term, $order_info, $from_date, $to_date)  {
        
        $count  = DB::table("transactions")
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
                    ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
                    ->where("merchants.id","=",$merchant_id)
                    ->whereIn('transactions.transaction_status',[3,301,30])
                    ->select(                    
                        'transactions.*', 
                        'merchants.merchant_name', 
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                        DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
                        )
                    ->distinct('transactions.transaction_id')
                    ->orderBy($order_info['name'],$order_info['dir'])
                    ->count();

        if(is_null($search_term) || empty($search_term)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where("merchants.id","=",$merchant_id)
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where("merchants.id","=",$merchant_id)
            ->where("transactions.transaction_id", "like", $search_term . '%')
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];
    }

    public function searchDeliveriesAdmin($search_term, $order_info, $from_date, $to_date)  {

        $count  = Transactions::count();

        if(is_null($search_term) || empty($search_term)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select `courier`.`courier_name` from `courier` where `courier`.`id` = `transactions`.`courier_id`) as `courier_name`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where("transactions.transaction_id", "like", $search_term . '%')
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select `courier`.`courier_name` from `courier` where `courier`.`id` = `transactions`.`courier_id`) as `courier_name`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);
        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    }

    public function searchDeliveriesCourier($courier_id, $search_term, $order_info, $from_date, $to_date)  {
        
        $count  = Transactions::where("courier_id","=",$courier_id)->count();

        if(is_null($search_term) || empty($search_term)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where("transactions.courier_id",'=',$courier_id)
            ->select(
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where("transactions.courier_id",'=',$courier_id)
            ->where("transactions.transaction_id", "like", $search_term . '%')
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);
        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];
    }

    public function searchDeliveriesMerchants($merchant_id, $search_term, $order_info, $from_date, $to_date)  {
        
        $count  = DB::table("transactions")
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
                    ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
                    ->where("merchants.id","=",$merchant_id)
                    ->select(
                        'transactions.*', 
                        'merchants.merchant_name', 
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
                        )
                    ->distinct('transactions.transaction_id')
                    ->orderBy($order_info['name'],$order_info['dir'])
                    ->count();

        if(is_null($search_term) || empty($search_term)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where("merchants.id","=",$merchant_id)
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where("merchants.id","=",$merchant_id)
            ->where("transactions.transaction_id", "like", $search_term . '%')
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];
    }

    public function getStatusCodeArray($status) {

        $arr = [];
        switch($status) {
            case "delivered":
                $arr = [3, 301];
            break;

            case "processing":
                $arr = [1, 14, 101, 10, 4, 2, 7];
            break;

            case "partial":
                $arr = [30];
            break;

            case "returned":
                $arr = [5,6];
            break;

            case "cancelled":
                $arr = [8];
            break;

            case "pending":
                $arr = [0];
            break;

        }
        return $arr;
    }

    public function filterDeliveriesCourier($courier_id, $filter_data, $order_info, $from_date, $to_date, $status) {

        $count  = Transactions::where("courier_id","=",$courier_id)
                    ->count();

        if(empty($filter_data)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('courier_id', '=', $courier_id)
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('courier_id', '=', $courier_id)
            ->where($filter_data)
            ->select(
                'transactions.*',
                'merchants.merchant_name',
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        if(empty($status) == false) {
            $transactionQ = $transactionQ->whereIn('transaction_status',$this->getStatusCodeArray($status));
        }

        $transactions = $transactionQ->get();
        
        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];
    
    }

    public function filterDeliveriesMerchants($merchant_id, $filter_data, $order_info, $from_date, $to_date, $status) {

        $count  =  DB::table("transactions")
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
                    ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
                    ->where('merchants.id', '=', $merchant_id)
                    ->select(                    
                        'transactions.*', 
                        'merchants.merchant_name', 
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
                        )->distinct('transactions.transaction_id')->count();

        if(empty($filter_data)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('merchants.id', '=', $merchant_id)
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('merchants.id', '=', $merchant_id)
            ->where($filter_data)
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        if(empty($status) == false) {
            $transactionQ = $transactionQ->whereIn('transaction_status',$this->getStatusCodeArray($status));
        }

        $transactions = $transactionQ->get();
        
        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];
    
    }

    public function filterDeliveriesAdmin($filter_data, $order_info, $from_date, $to_date, $status) {

        $count  = Transactions::count();

        if(empty($filter_data)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select `courier`.`courier_name` from `courier` where `courier`.`id` = `transactions`.`courier_id`) as `courier_name`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where($filter_data)
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select `courier`.`courier_name` from `courier` where `courier`.`id` = `transactions`.`courier_id`) as `courier_name`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);
        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        if(empty($status) == false) {
            $transactionQ = $transactionQ->whereIn('transaction_status',$this->getStatusCodeArray($status));
        }

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    }

}
