<?php

namespace Dappur\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AgentDeposit extends Model {

    protected $table = "agent_deposits";
    protected $primaryKey = 'id';
    protected $fillable = [
        'agent_id',
        'deposit_value',
        'created_date',
        'updated_date'
    ];

}