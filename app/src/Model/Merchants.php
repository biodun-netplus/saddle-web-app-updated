<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;

class Merchants extends Model {

    protected $table = 'merchants';
    protected $primaryKey = 'id';
    protected $fillable = [
        'merchant_name',
        'email',
        'contact_number',
        'website',
        'status',
        'account_number',
        'bank_id',
        'account_name',
        'merchant_type',
        'return_percentage',
        'merchant_add',
        'settlement_type',
    ];

    public function getActiveMerchants()
    {
        $merchant = Merchants::where('status','=', 1)->get();
        return $merchant;
    }

    public function getMerchantCount()
    {
        return Merchants::count();
    }

    public function getMerchantById($merchant_id)
    {
        $merchant = Merchants::whereRaw('merchant_id = ? and status = ?', [$merchant_id, 1])->get();
        return $merchant;
    }

    public function getMerchantByIdForAddDelivery($merchant_id)
    {
        $merchant = Merchants::where('id', '=', $merchant_id)->first();
        return $merchant;
    }
    public function getMerchantName($client_id)
    {
        $merchant = Merchants::where('id', '=', $client_id)->first();
        return $merchant['merchant_name'];
    }

    public function getMerchantDetail($client_id)
    {
        $merchant = Merchants::where('id', '=', $client_id)->first();
        return $merchant;
    }


}
