<?php

namespace Dappur\Model;

use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Capsule\Manager as DB;

class RoleUsers extends EloquentUser {

    public const AGENT = 10;
    public const COURIER_ADMIN = 8;
    public const COURIER_USER = 11;
    public const ADMIN = 1;
    public const MERCHANT_USER = 9;

    protected $table = 'role_users';
    protected $fillable = [
        'user_id',
        'role_id'
    ];

    public function isAgent($user_id) {

        $count = DB::table("role_users")
                ->where("user_id",$user_id)
                ->where("role_id",RoleUsers::AGENT)
                ->count();

        return $count > 0 ? TRUE : FALSE;

    }

    public function isCourierAdmin($user_id) {
        $count = DB::table("role_users")
                ->where("user_id",$user_id)
                ->where("role_id",RoleUsers::COURIER_ADMIN)
                ->count();

        return $count > 0 ? TRUE : FALSE;
    }

    public function isCourierUser($user_id) {
        $count = DB::table("role_users")
                ->where("user_id",$user_id)
                ->where("role_id",RoleUsers::COURIER_USER)
                ->count();

        return $count > 0 ? TRUE : FALSE;
    }

    public function isAdmin($user_id) {
        $count = DB::table("role_users")
                ->where("user_id",$user_id)
                ->where("role_id",RoleUsers::ADMIN)
                ->count();

        return $count > 0 ? TRUE : FALSE;
    }

    public function isMerchantUser($user_id) {

        $count = DB::table("role_users")
        ->where("user_id",$user_id)
        ->where("role_id",RoleUsers::MERCHANT_USER)
        ->count();

        return $count > 0 ? TRUE : FALSE;

    }

}