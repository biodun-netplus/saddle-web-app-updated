<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Courier extends Model {

    protected $table = 'courier';
    protected $primaryKey = 'id';
    protected $fillable = [
        'courier_id',
        'courier_name',
        'courier_email',
        'courier_website',
        'status',
        'address',
        'split_percentage',
    ];



    public function  getCourierByEmail($email)
    {
        return Courier::where('courier_email', '=', $email)->first();
    }


    public function  getCourierByName($courier_name)
    {
        return Courier::where('courier_name', '=', $courier_name)->first();
    }

    public function hasCardOnFile($courierId)
    {
        $courier = Courier::find($courierId)['courier_id'];
        return $courier;

    }

    public function locations()
    {
        //return $this->hasMany('CourierLocation');
    }

    public function getCourierById($courier_id)
    {
        //Transactions::whereRaw('transaction_status = ? and courier_id = ?', [$status_id,$courier_id])->count()
        $courier = Courier::whereRaw('courier_id = ? and status = ?', [$courier_id, 1])->get()->first();
        return $courier;
    }

    public function getCourierByPrId($id)
    {
        //Transactions::whereRaw('transaction_status = ? and courier_id = ?', [$status_id,$courier_id])->count()
        $courier = Courier::whereRaw('id = ? and status = ?', [$id, 1])->get()->first();
        return $courier;
    }

    public function getCourierDetails($courier_id)
    {
      return Courier::where('courier_id', '=', $courier_id)->first();
    }
    public function getCourierCount()
    {
        return Courier::count();
    }

    public function getCouriers()
    {
        return Courier::all()->sortBy("courier_name");
    }
    public function getActiveCouriers()
    {
        $courier = Courier::where('status','=', 1)->get();
        return $courier;
    }

    public function getCourierName($courier_id)
    {
        $courier = Courier::where('id', '=', $courier_id)->first();
        return $courier['courier_name'];
    }
}
