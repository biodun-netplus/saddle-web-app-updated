<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;
class CourierMerchants extends Model {

    protected $table = 'courier_merchants';
    protected $fillable = [
        'merchant_id',
        'courier_id'
    ];

    public function getMerchantsCourier($merchant_id)
    {
        //remember the courier has to be active
        $couriers = CourierMerchants::whereRaw('merchant_id = ?', [$merchant_id])->get()->pluck('courier_id');
        $courier = Courier::get()->whereIn('id',$couriers->toArray());
        return $courier;
    }

    public function getCourierMerchants($courier_id){
      //remember that the merhcant has to be active
      //Merchants::where('status','=', 1)->get();
      $merchants = CourierMerchants::where('courier_id','=',$courier_id)->get()->pluck('merchant_id');
      $merchant = Merchants::whereIn('id',$merchants->toArray())
          ->orderBy('merchant_name', 'asc')
          ->get();
      //d($merchant->toArray());
      //die;
      return $merchant;
    }

    public function getCourier($userID)
    {
        //d($this);
       $transactions = DB::table('rate_card')->whereIn('courierNameId', function($query) use ($userID){
            $query->from('usertype')->join('courier_merchants', 'courier_merchants.merchant_id', '=', 'usertype.type_id')
                        ->join('rate_card', 'rate_card.courierNameId', '=', 'courier_merchants.courier_id')
                        ->select(
                                'courier_merchants.courier_id'
                                )
                        ->where('usertype.user_id', $userID);
            })->get();

       return $transactions;
        
        // $transactions = DB::table('usertype')
        //                 ->join('courier_merchants', 'courier_merchants.merchant_id', '=', 'usertype.type_id')
        //                 ->join('rate_card', 'rate_card.courierNameId', '=', 'courier_merchants.courier_id')
        //                 ->select(
        //                         'courier_merchants.courier_id',
        //                         'rate_card.id',
        //                         'rate_card.client_id',
        //                         'rate_card.state_to',
        //                         'rate_card.lga_from',
        //                         'rate_card.state_from',
        //                         'rate_card.lga_to',
        //                         'rate_card.weight',
        //                         'rate_card.price',
        //                         'rate_card.additionalPrice'
        //                         'rate_card.additionalPrice'
        //                         )
        //                 ->where('usertype.user_id', $userID)
        //                 ->get();
        

        
    }
    function getRecords($delivery_state, $pickup_state, $weight_value, $courier_id)
    {
        echo $courier_id;
        
//$courierID->id

        $priceWeight = DB::table('rate_card')
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight', '=<', $weight)
                        ->where('state_from', $pickup_state)
                        ->where('state_to', $delivery_state)
                        ->first();

          $total_shipping_price = 'djslj';
         
          if($priceWeight->weight < $weight_value){

            $extraWeight = $weight_value - $priceWeight->weight;
            $extarWeightTotal = $extraWeight*$priceWeight->additionalPrice;
            $total_shipping_price = $priceWeight->weight + $extarWeightTotal; 

          }

            
        return $priceWeight;
    }
}
