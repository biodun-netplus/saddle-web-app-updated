<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;

class Status extends Model {

    protected $table = 'status';
    protected $primaryKey = 'id';

    public const PENDING = 0;
    public const PICKED_UP_FROM_MERCHANT = 1;
    public const PARTIAL_PICK_UP_FROM_MERCHANT = 14;
    public const DELIVERED_TO_CUSTOMER = 3;
    public const RETURNED_TO_MERCHANT = 6;
    public const FAILED_DELIVERY_ATTEMPT = 4;
    public const CANCELLED = 8;
    
    public function getStatus()
    {
        return Status::all()->sortBy("status");
    }

    public function getStatusID($status)
    {
        $status = Status::where('status', '=', $status)->first();
        return $status['status_id'];
    }

}

