<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;

class CourierLocation extends Model {

    protected $table = 'courier_locations';
    protected $primaryKey = 'id';
    protected $fillable = [
        'address',
        'local_govt_area',
        'state',
        'courier_id',
    ];

    public function courier()
    {
    	 //return $this->belongsTo('Courier');
    }
}