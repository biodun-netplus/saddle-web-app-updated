<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;

class RateCard extends Model {

    protected $table = 'rate_card';
    protected $primaryKey = 'id';
    protected $fillable = [
        'client_id',
        'from',
        'to',
        'weight',
        'price',
       
    ];




    public function getRecord($customerFrom)
    {     

         if($customerFrom['customer_state2'] == $customerFrom['merchant_state'])
         {
            
            $ratecard = DB::table('rate_card')
                        ->where('lga_from','all' )
                        ->where('lga_to','all' )
                        ->where('state_to', $customerFrom['customer_state2'])
                        ->where('state_from',$customerFrom['merchant_state'] )
                        ->where('courierNameId',$customerFrom['merchant_name'] )
                        // ->where('weight','>=',$customerFrom['weight'] )
                        ->count();
            if($ratecard == 0){
            
                $ratecard = DB::table('rate_card')
                        ->where('lga_from',$customerFrom['merchant_lga'] )
                        ->where('lga_to',$customerFrom['customer_lga2'] )
                        ->where('state_to', $customerFrom['customer_state2'])
                        ->where('state_from',$customerFrom['merchant_state'] )
                        ->where('courierNameId',$customerFrom['merchant_name'] )
                        ->first();
            }else{
                $ratecard = DB::table('rate_card')
                        ->where('lga_from','all' )
                        ->where('lga_to','all' )
                        ->where('state_to', $customerFrom['customer_state2'])
                        ->where('state_from',$customerFrom['merchant_state'] )
                        ->where('courierNameId',$customerFrom['merchant_name'] )
                        // ->where('weight','>=',$customerFrom['weight'] )
                        ->first();
            }
         }else{
            
            $ratecard = DB::table('rate_card')
                        //->where('lga_from',$customerFrom['merchant_lga'] )
                        //->where('lga_to',$customerFrom['customer_lga2'] )
                        ->where('courierNameId',$customerFrom['merchant_name'] )
                        ->where('state_to', $customerFrom['customer_state2'])
                        ->where('state_from',$customerFrom['merchant_state'] )
                        ->first();
         }

    	 
                      
                        

        return $ratecard;
        
 
        // return $transactions;
        //return Card::where('transaction_id','=',$transaction_id)->first();
    }

    public function showRecord($courierid)
    {  
          $ratecardshow = DB::table('rate_card')
                        ->select('*')
                        //->where('lga_from',$customerFrom['merchant_lga'] )
                        //->where('lga_to',$customerFrom['customer_lga2'] )
                        ->where('courierNameId',$courierid )
                        ->get();
             return $ratecardshow;
         }

    public function deleteRecord($courierid)
    {  
     
          $ratecardshow = DB::table('rate_card')->where('id', $courierid)->delete();
             return $ratecardshow;
    }

         public function editRecord($record_id)
    {  
          $ratecardshow = DB::table('rate_card')
                         ->where('id', $record_id)
                         ->first();
             return $ratecardshow;
         }

         
                      
                        

      
    
}
?>