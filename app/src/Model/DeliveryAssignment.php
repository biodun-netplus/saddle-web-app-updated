<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;

class DeliveryAssignment extends Model {

    protected $table = 'delivery_assignment';
    protected $primaryKey = 'id';
    
}

