<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;

class UserType extends Model {

    protected $table = 'usertype';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'user_type',
        'type_id',
    ];
    
    public function getUserID($type_id, $user_id)
    {
        $user_id = UserType::whereRaw('type_id = ? and user_id = ?',[$type_id,$user_id])->get();
        //$merchant = Merchants::whereRaw('merchant_id = ? and status = ?', [$merchant_id, 1])->get();
        //d($user_id);
        //die;
        return $user_id;
    }

    public function getUserTypeName($user_id)
    {
        $user_type = UserType::where('user_id', '=', $user_id)->first();
        //d($user_type);
        //die;
        if($user_type['user_type'] == 'Courier')
            {
                $courier = new Courier;
                $name = $courier->getCourierName($user_type['type_id']);
                return $name;
            }
        elseif($user_type['user_type'] == 'Merchant')
        {
            $courier = Merchants::where('id', '=', $user_type['type_id'])->first();
            return $courier['merchant_name'];
        }
    }

    public function getUserType($user_id)
    {
        $user_type = UserType::where('user_id', '=', $user_id)->first();
        return $user_type['user_type'];
    }

    public function getUserTypeID($user_id)
    {
        $user_type = UserType::where('user_id', '=', $user_id)->first();
        return $user_type['type_id'];
    }

    public function getAgentForMap($type, $typeID)
    {

        if($type == 'Courier'){
            return UserType::join('users', 'users.id','=', 'usertype.user_id')->select('users.first_name','users.last_name','usertype.latitude', 'usertype.longitude')->where('usertype.type_id', $typeID)->where('usertype.latitude', '!=',NULL)->where('usertype.longitude', '!=',NULL)->get();
        }
        return UserType::join('users', 'users.id','=', 'usertype.user_id')->select('users.first_name','users.last_name','usertype.latitude', 'usertype.longitude')->where('usertype.latitude', '!=',NULL)->where('usertype.longitude', '!=',NULL)->get();
    }

    public function getCourierForUser($user_id)
    {
        $user_type = UserType::where('user_id', '=', $user_id)->first();

        

        if($user_type['user_type'] == 'Courier') {
            $courier = new Courier;
            $courier = $courier->getCourierByPrId($user_type['type_id']);
            return $courier;
        }

        return null;
    }
}