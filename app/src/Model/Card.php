<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Capsule\Manager as DB;

class Card extends Model {

    protected $table = 'card_on_file';
    protected $primaryKey = 'id';
    protected $fillable = [
        'courier_id',
        'agent_id',
        'status'
    ];

    public function transactionDetails($typeID = NULL, $type = NULL)
    {
        $transactions = NULL;
        $transactions = DB::table('transactions')
            ->whereIn('transaction_id', [$status]);
        return $transactions;
        //return Card::where('transaction_id','=',$transaction_id)->first();
    }

    public function insert(array $card_details) {
        return DB::table('card_on_file')->insertGetId($card_details);
    }

    public function logCardOnFileTransaction(array $cardOnFileTransaction) {
        $rs = DB::table('cardonfilepay')->insertGetId($cardOnFileTransaction);
        return $rs;
    }

    public function getCourierTransactions($courier_id)
    {
        $transactions = DB::table('transactions')
            ->join('cardonfilepay', 'cardonfilepay.transaction_id', '=', 'transactions.transaction_id')
            ->where('courier_id', $courier_id)
            ->orderBy('updated_at', 'desc')
            ->get();
        return $transactions;
    }

    public function getMerchantTransactions($client_id)
    {
        
        $transactions = DB::table('transactions')
            ->join('cardonfilepay', 'cardonfilepay.transaction_id', '=', 'transactions.transaction_id')
            ->where('merchant_id', $client_id)
            ->orderBy('updated_at', 'desc')
            ->get();

        return $transactions;
    }

    public function getAgentsActiveCardsOnFile($courier_id, $agent_id) {

        $card = DB::table('card_on_file')
        ->join('courier', 'card_on_file.courier_id', '=', 'courier.courier_id')
        ->join('users','card_on_file.agent_id','=','users.id')
        ->where('courier.id', '=', $courier_id)
        ->where("card_on_file.agent_id", '=', $agent_id)
        ->where('card_on_file.status','=',1)
        ->get();

        return $card;
    }

    public function getCouriersActiveCardsOnFile($courier_id) {

        $card = DB::table('card_on_file')
        ->join('courier', 'card_on_file.courier_id', '=', 'courier.courier_id')
        ->where('courier.id', '=', $courier_id)
        ->where(function($query) {
            $query->whereNull("card_on_file.agent_id")
            ->orWhere("card_on_file.agent_id", '=',0);
        })
        ->where('card_on_file.status','=',1)
        ->get();

        return $card;

    }

}

?>