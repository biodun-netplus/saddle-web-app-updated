<?php
namespace Dappur\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;

class Items extends Model {

    protected $table = 'items';
    protected $primaryKey = 'id';

    public function getItemDetails($item_id)
	  {
	  		$items = Items::where('item_id','=',$item_id)->first();
            //echo $item_id;
            //d($items);
            //die;
            return $items;
    }

    public function getItemsByTransactionFortracking($transaction_id, $api = '')
    {
        if($api == 'api'){
            return Items::join('status', 'status.status_id','=', 'items.delivery_status')->select('items.item_name','items.delivery_status','items.updated_at', 'status.status')->where('items.transaction_id', $transaction_id)->orderBy('items.id', 'desc')->get();
        }
        return Items::select('item_name','delivery_status','updated_at')->where('transaction_id', $transaction_id)->orderBy('id', 'desc')->first();
    }

    public function getItemsByTransaction($transaction_id)
    {
        return Items::where('transaction_id','=', $transaction_id)->get();
    }

    public function getItemsReadyForPickupByTransaction($transaction_id) {
        return Items::where('transaction_id','=',$transaction_id)->where('delivery_status','=',0)->get();
    }

    public function getItemsReadyForDropOffByTransaction($transaction_id) {
        return Items::where('transaction_id','=',$transaction_id)
        ->whereNotIn('delivery_status',[0,3,301])
        ->get();
    }

    public function getTransactionItemCount($transaction_id)
   {
        $items = Items::where('transaction_id', '=', $transaction_id)->count();
        //echo $courier_id;
        return $items;
   }   

}