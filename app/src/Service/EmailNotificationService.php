<?php

namespace Dappur\Services;

use Dappur\Model\Transactions;
use Dappur\Services\Service;
use Dappur\Model\Status;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;


use Illuminate\Database\Capsule\Manager as DB;

/**
 * Delivery Service
 */
class EmailNotificationService extends Service {

    protected $transactions;
    protected $status;
    protected $email_vailidator;

    public function __construct($container, $dispatch_email, $dispatch_email_server, $dispatch_email_port, $password) {
        parent::__construct($container);
        $this->transactions = new \Dappur\Model\Transactions;
        $this->email_validator = new EmailValidator;
        $this->status = new \Dappur\Model\Status;
        $this->dispatch_email = $dispatch_email;
        $this->dispatch_email_server = $dispatch_email_server;
        $this->dispatch_email_port = $dispatch_email_port;
        $this->password = $password;
    }

    private function sendMessage($customer_email, $merchant_email, $subject, $htmlContent) {

        $this->logger->debug("send message sending " . $htmlContent,[]);

        try {

            if(isset($customer_email) && $this->email_validator->isValid($customer_email, new RFCValidation())) {

                $message = (new \Swift_Message())
                ->setSubject($subject)
                ->setFrom($this->dispatch_email)
                ->setTo($customer_email)
                ->setBody($htmlContent)
                ->addPart($htmlContent,"text/html");

                if($this->email_validator->isValid($merchant_email, new RFCValidation())) {
                    $message->setBcc($merchant_email);
                } else {

                    $this->logger->debug("merchant email is invalid " . $merchant_email,[]);
                    
                }

                $transport = (new \Swift_SmtpTransport($this->dispatch_email_server,$this->dispatch_email_port))
                    ->setUsername($this->dispatch_email)
                    ->setPassword($this->password);

                $mailer = new \Swift_Mailer($transport);
                $mailer->send($message);
            
            } else {

                $this->logger->debug("customer email is invalid " . $customer_email,[]);

            }

        
        } catch (Swift_RfcComplianceException $e) {
            //an error occured while sending email
            $this->logger->debug("an error occured while sening email to " . $customer_email,[]);
        }

    }

    public function notifyCustomerOfPickup(string $transaction_id) {

        $transaction = $this->transactions->getTransactionDetails($transaction_id);

        if(is_null($transaction)) 
            return;
        
        $pickup_details = json_decode($transaction->pickup_details);
        $delivery_details = json_decode($transaction->delivery_details);
        $to = $delivery_details->customer_email;
        $created_at= strtotime($transaction->created_at);
        $updated_at= strtotime($transaction->updated_at);
        $todayDate = strtotime(date("Y/m/d"));
        $created_at = date('M-d', $created_at);
        $updated_at = date('M-d', $updated_at);
        $todayDate = date('M-d', $todayDate);
        $htmlContent = str_replace(":customer_name",$delivery_details->customer_name, EmailNotifications::PICKUP);
        $htmlContent = str_replace(":transaction_order_no", $transaction->transaction_id, $htmlContent);
        $htmlContent = str_replace(":created_at", $created_at, $htmlContent);
        $htmlContent = str_replace(":todays_date", $todayDate, $htmlContent);
        $htmlContent = str_replace(":updated_at", $updated_at, $htmlContent);

        $htmlContent = str_replace(":merchant_contactname", $pickup_details->merchant_contactname, $htmlContent);
        $htmlContent = str_replace(":merchant_fulladdress", $pickup_details->merchant_address.', '.$pickup_details->merchant_lga.', '.$pickup_details->merchant_state, $htmlContent);
        $htmlContent = str_replace(":merchant_phone", $pickup_details->merchant_phone, $htmlContent);

        $htmlContent = str_replace(":customer_fulladdress", $delivery_details->customer_address.', '.$delivery_details->customer_lga.', '.$delivery_details->customer_state, $htmlContent);
        $htmlContent = str_replace(":customer_phone", $delivery_details->customer_phone, $htmlContent);

        $this->sendMessage($to, $pickup_details->merchant_email, "Shipping Status - Order Picked-up", $htmlContent);

    }

    public function notifyCustomerOfDelivery(string $transactionID) {

        $transaction = $this->transactions->getTransactionDetails($transactionID);

        if(is_null($transaction)) 
        return;
        
        $pickup_details = json_decode($transaction->pickup_details);
        $delivery_details = json_decode($transaction->delivery_details);
        $to = $delivery_details->customer_email;
        $created_at= strtotime($transaction->created_at);
        $updated_at= strtotime($transaction->updated_at);
        $todayDate = strtotime(date("Y/m/d"));
        $created_at = date('M-d', $created_at);
        $updated_at = date('M-d', $updated_at);
        $todayDate = date('M-d', $todayDate);

        $this->logger->debug("delivery email is " . EmailNotifications::DELIVERY, []);



        $htmlContent = str_replace(":customer_name",$delivery_details->customer_name, EmailNotifications::DELIVERY);

        $this->logger->debug("first pass is " . $htmlContent,[]);


        $htmlContent = str_replace(":transaction_order_no", $transaction->transaction_id, $htmlContent);
        $htmlContent = str_replace(":created_at", $created_at, $htmlContent);
        $htmlContent = str_replace(":todays_date", $todayDate, $htmlContent);
        $htmlContent = str_replace(":updated_at", $updated_at, $htmlContent);

        $htmlContent = str_replace(":merchant_contactname", $pickup_details->merchant_contactname, $htmlContent);
        $htmlContent = str_replace(":merchant_fulladdress", $pickup_details->merchant_address.', '.$pickup_details->merchant_lga.', '.$pickup_details->merchant_state, $htmlContent);
        $htmlContent = str_replace(":merchant_phone", $pickup_details->merchant_phone, $htmlContent);

        $htmlContent = str_replace(":customer_fulladdress", $delivery_details->customer_address.', '.$delivery_details->customer_lga.', '.$delivery_details->customer_state, $htmlContent);
        $htmlContent = str_replace(":customer_phone", $delivery_details->customer_phone, $htmlContent);

        $this->logger->debug("last pass is " . $htmlContent,[]);

        $this->sendMessage($to, $pickup_details->merchant_email, "Shipping Status - Order Delivered", $htmlContent);

    }

    public function notifyCustomerOfReturn(string $transactionID) {

        $transaction = $this->transactions->getTransactionDetails($transactionID);

        if(is_null($transaction)) 
            return;
        
        $pickup_details = json_decode($transaction->pickup_details);
        $delivery_details = json_decode($transaction->delivery_details);
        $to = $delivery_details->customer_email;
        $created_at= strtotime($transaction->created_at);
        $updated_at= strtotime($transaction->updated_at);
        $todayDate = strtotime(date("Y/m/d"));
        $created_at = date('M-d', $created_at);
        $updated_at = date('M-d', $updated_at);
        $todayDate = date('M-d', $todayDate);

        $htmlContent = str_replace(":customer_name",$delivery_details->customer_name, EmailNotifications::RETURN);
        $htmlContent = str_replace(":transaction_order_no", $transaction->transaction_id, $htmlContent);
        $htmlContent = str_replace(":created_at", $created_at, $htmlContent);
        $htmlContent = str_replace(":todays_date", $todayDate, $htmlContent);
        $htmlContent = str_replace(":updated_at", $updated_at, $htmlContent);

        $htmlContent = str_replace(":merchant_contactname", $pickup_details->merchant_contactname, $htmlContent);
        $htmlContent = str_replace(":merchant_fulladdress", $pickup_details->merchant_address.', '.$pickup_details->merchant_lga.', '.$pickup_details->merchant_state, $htmlContent);
        $htmlContent = str_replace(":merchant_phone", $pickup_details->merchant_phone, $htmlContent);

        $htmlContent = str_replace(":customer_fulladdress", $delivery_details->customer_address.', '.$delivery_details->customer_lga.', '.$delivery_details->customer_state, $htmlContent);
        $htmlContent = str_replace(":customer_phone", $delivery_details->customer_phone, $htmlContent);

        $this->sendMessage($to, $pickup_details->merchant_email, "Shipping Status - Order Returned to merchant", $htmlContent);

    }

    public function notifyCustomerOfFailedDelivery($transactionID) {

        $transaction = $this->transactions->getTransactionDetails($transactionID);

        if(is_null($transaction)) 
            return;
        
        $pickup_details = json_decode($transaction->pickup_details);
        $delivery_details = json_decode($transaction->delivery_details);
        $to = $delivery_details->customer_email;
        $created_at= strtotime($transaction->created_at);
        $updated_at= strtotime($transaction->updated_at);
        $todayDate = strtotime(date("Y/m/d"));
        $created_at = date('M-d', $created_at);
        $updated_at = date('M-d', $updated_at);
        $todayDate = date('M-d', $todayDate);

        $htmlContent = str_replace(":customer_name",$delivery_details->customer_name, EmailNotifications::FAILED);
        $htmlContent = str_replace(":transaction_order_no", $transaction->transaction_id, $htmlContent);
        $htmlContent = str_replace(":created_at", $created_at, $htmlContent);
        $htmlContent = str_replace(":todays_date", $todayDate, $htmlContent);
        $htmlContent = str_replace(":updated_at", $updated_at, $htmlContent);

        $htmlContent = str_replace(":merchant_contactname", $pickup_details->merchant_contactname, $htmlContent);
        $htmlContent = str_replace(":merchant_fulladdress", $pickup_details->merchant_address.', '.$pickup_details->merchant_lga.', '.$pickup_details->merchant_state, $htmlContent);
        $htmlContent = str_replace(":merchant_phone", $pickup_details->merchant_phone, $htmlContent);

        $htmlContent = str_replace(":customer_fulladdress", $delivery_details->customer_address.', '.$delivery_details->customer_lga.', '.$delivery_details->customer_state, $htmlContent);
        $htmlContent = str_replace(":customer_phone", $delivery_details->customer_phone, $htmlContent);

        $this->sendMessage($to, $pickup_details->merchant_email, "Shipping Status - Failed Delivery", $htmlContent);

    }



}