<?php

namespace Dappur\Services;

use Dappur\Model\Users;
use Dappur\Model\Roles;
use Dappur\Model\RoleUsers;
use Dappur\Model\Card;
use Dappur\Model\Courier;
use Dappur\Model\UserType;


/**
 * Delivery Service
 */
class CourierService extends Service {

    protected $users;
    protected $roleUsers;
    protected $roles;
    protected $card;
    protected $courier;
    protected $userType;

    public function __construct($container) {

        parent::__construct($container);
        $this->users = new Users;
        $this->roles = new Roles;
        $this->roleUsers = new RoleUsers;
        $this->card = new Card;
        $this->courier = new Courier;

    }

    public function updateCouriersNetplusPayMID(int $courier_id, string $netpluspay_mid = null) {
        $this->courier->where("id","=",$courier_id)->update(["netpluspay_mid" => $netpluspay_mid]);
    }


}