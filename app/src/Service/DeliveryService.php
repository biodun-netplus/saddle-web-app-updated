<?php

namespace Dappur\Services;

use Dappur\Model\Transactions;
use Dappur\Model\CourierLocation;
use Dappur\Model\Courier;
use Dappur\Model\Merchants;
use Dappur\Model\UserType;
use Dappur\Model\Items;
use Dappur\Model\CourierMerchants;
use Dappur\Model\Users;
use Dappur\Services\Service;
use Dappur\Model\RateCard;
use Dappur\Model\Status;
use Dappur\Model\DeliveryAssignment;

use Illuminate\Database\Capsule\Manager as DB;

/**
 * Delivery Service
 */
class DeliveryService extends Service {

    protected $transaction;
    protected $item;
    protected $rateCard;
    protected $status;
    protected $deliveryAssignment;

    public function __construct($container) {
        parent::__construct($container);
        $this->transaction = new Transactions;
        $this->item = new Items;
        $this->rateCard = new RateCard;
        $this->status = new Status;
        $this->deliveryAssignment = new DeliveryAssignment;
        $this->accountsService = new AccountsService($container);
    }

    public function determineDeliveryCost($courier_id, $pickup_details, $dropoff_details, $items) {

        $delivery_state = trim($dropoff_details['customer_state']);
        $pickup_state = trim($pickup_details['merchant_state']);
        $courier_id = $courier_id;
        $delivery_lga = trim($dropoff_details['customer_lga']);
        $pickup_lga = trim($pickup_details['merchant_lga']);

        $weight_value = 0;

        for($i = 0; $i < count($items); $i++) {
            $weight_value = $weight_value + ((isset($items[$i]["item_weight"]) ? $items[$i]["item_weight"] : 1) * $items[$i]["item_qty"]);
        }

        $this->logger->debug("items count ",[count($items)]);
        $this->logger->debug("weight is ",[$weight_value]); 

        if(empty($delivery_state))
        {
            $this->logger->debug("delivery state is empty");
            return null;
        }

        if(empty($pickup_state))
        {
            $this->logger->debug("pickup state is empty");
            return null;
        }

        if(empty($delivery_lga))
        {
            $this->logger->debug("delivery lga is empty");
            return null;
        }

        if(empty($pickup_lga))
        {
            $this->logger->debug("pickup lga is empty");
            return null;
        }

        if(empty($weight_value))
        {
            $this->logger->debug("weight is empty");
            return null;
        }

        if(empty($courier_id))
        {
            $this->logger->debug("courier_id is empty");
            return null;
        }

        $price = $this->shippingPrice($delivery_state, $pickup_state, $weight_value, $courier_id, $pickup_lga, $delivery_lga);

        $this->logger->debug("price is ",["price" => $price]);

        return $price;
    }

    private function shippingPrice($delivery_state, $pickup_state, $weight_value, $courier_id, $pickup_lga, $delivery_lga, $courierIdCheck = false){

        $weight = $weight_value;

        $couriers = new \Dappur\Model\Courier;

        if(!$courierIdCheck){
          $courierID = $couriers->where('courier_id','=',$courier_id)->select('id')->first();
          $courier_id = $courierID->id;
        }else{
          $courier_id = $courier_id;
        }

        $priceWeight = $this->rateCard
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','=',$weight)
                        ->where('state_from', $pickup_state)
                        ->where('lga_to', $delivery_lga)
                        ->where('lga_from', $pickup_lga)
                        ->where('state_to', $delivery_state)
                        ->first();

        if(count($priceWeight) == 0) {

            $priceWeight = $this->rateCard
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','>',$weight)
                        ->where('state_from', $pickup_state)
                        ->where('lga_to', $delivery_lga)
                        ->where('lga_from', $pickup_lga)
                        ->where('state_to', $delivery_state)
                        ->first();
            
            if(count($priceWeight) == 0){
                $priceWeight = $this->rateCard
                            ->select('price','weight','additionalPrice')
                            ->where('courierNameId', $courier_id)
                            ->where('weight','<',$weight)
                            ->where('state_from', $pickup_state)
                            ->where('lga_to', $delivery_lga)
                            ->where('lga_from', $pickup_lga)
                            ->where('state_to', $delivery_state)
                            ->orderBy('weight','desc')
                            ->first();
            }

            if(count($priceWeight) == 0){
            
                $priceWeight = $this->rateCard
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','=',$weight)
                        ->where('lga_to', $delivery_lga)
                        ->where('lga_from', 'all')
                        ->where('state_from', $pickup_state)
                        ->where('state_to', $delivery_state)
                        ->first();

            } else {

                $extra = $priceWeight->additionalPrice;
                $tedt = $priceWeight->weight;
        
                if($tedt < $weight){
                    
                    $extraWeight = $weight - $tedt;
                    $extarWeightTotal = $extraWeight * $extra*2;
                    $total_shipping_price = $priceWeight->price + $extarWeightTotal; 

                } else {
                    $total_shipping_price = trim($priceWeight->price);
                }
                
                return (int)$total_shipping_price;

            }
      
        } else {
              
            $total_shipping_price = trim($priceWeight->price);
            return (int)$total_shipping_price;
        }

        if(count($priceWeight) == 0){

            $priceWeight = $this->rateCard
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','>',$weight)
                        ->where('state_from', $pickup_state)
                        ->where('lga_to', $delivery_lga)
                        ->where('lga_from', 'all')
                        ->where('state_to', $delivery_state)
                        ->first();
            
            if(count($priceWeight) == 0){
                // echo "ds";die;
                $priceWeight = $this->rateCard
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','<',$weight)
                        ->where('state_from', $pickup_state)
                        ->where('lga_to', $delivery_lga)
                        ->where('lga_from', 'all')
                        ->where('state_to', $delivery_state)
                        ->orderBy('weight','desc')
                        ->first();
            }

            if(count($priceWeight) == 0){

               $priceWeight = $this->rateCard
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','=',$weight)
                        ->where('lga_to', 'all')
                        ->where('lga_from', 'all')
                        ->where('state_from', $pickup_state)
                        ->where('state_to', $delivery_state)
                        ->first();

            } else {

                $extra = $priceWeight->additionalPrice;
                $tedt = $priceWeight->weight;
        
                if($tedt < $weight){
                    
                $extraWeight = $weight - $tedt;
                $extarWeightTotal = $extraWeight * $extra*2;
                $total_shipping_price = $priceWeight->price + $extarWeightTotal; 

                } else {

                $total_shipping_price = trim($priceWeight->price);
                }

                return (int)$total_shipping_price;

            }
      
        } else {
              
            $total_shipping_price = trim($priceWeight->price);
            return (int)$total_shipping_price;
        }

        if(count($priceWeight) == 0) {

            $priceWeight = $this->rateCard
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','>',$weight)
                        ->where('state_from', $pickup_state)
                        ->where('lga_to', 'all')
                        ->where('lga_from', 'all')
                        ->where('state_to', $delivery_state)
                        ->first();

            if(count($priceWeight) == 0) {
                $priceWeight = $this->rateCard
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','<',$weight)
                        ->where('state_from', $pickup_state)
                        ->where('lga_to', 'all')
                        ->where('lga_from', 'all')
                        ->where('state_to', $delivery_state)
                        ->orderBy('weight','desc')
                        ->first();
            }

            if(count($priceWeight) == 0) {
          
               $priceWeight = $this->rateCard
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','=',$weight)
                        ->where('lga_to', 'all')
                        ->where('lga_from', 'all')
                        ->where('state_from', $pickup_state)
                        ->where('state_to', $delivery_state)
                        ->first();

            } else {

                $extra = $priceWeight->additionalPrice;
                $tedt = $priceWeight->weight;
        
                if($tedt < $weight){
                    
                $extraWeight = $weight - $tedt;
                $extarWeightTotal = $extraWeight * $extra*2;
                $total_shipping_price = $priceWeight->price + $extarWeightTotal; 

                }
                else{
                
                    $total_shipping_price = trim($priceWeight->price);

                }
                
                return (int)$total_shipping_price;

            }
      
        } else {
            $total_shipping_price = trim($priceWeight->price);
            return (int)$total_shipping_price;
        }

        if(count($priceWeight) == 0){
            return 'no record found for this location';
        } else {
  
            $extra = $priceWeight->additionalPrice;
            $tedt = $priceWeight->weight;
       
            if($tedt < $weight){
                 
              $extraWeight = $weight - $tedt;
              $extarWeightTotal = $extraWeight * $extra*2;
              $total_shipping_price = $priceWeight->price + $extarWeightTotal; 
           
            } else {

             $total_shipping_price = trim($priceWeight->price);
             $total_shipping_price =  str_replace('"', '', $total_shipping_price);
                          
            }
            
            return (int)$total_shipping_price;
        }

         $extra = $priceWeight->additionalPrice;
          $tedt = $priceWeight->weight;
       
          if($tedt < $weight){
               
            $extraWeight = $weight - $tedt;
            $extarWeightTotal = $extraWeight * $extra*2;
            $total_shipping_price = $priceWeight->price + $extarWeightTotal; 

          }
           else{

              $total_shipping_price = trim($priceWeight->price);
            }
            
        return (int)$total_shipping_price;    
    }

    public function createDeliveryOrder($userID, $_transaction) {

        $transaction = new Transactions;
        $courier_location = new CourierLocation;

        $courier_id = $_transaction['courier_id'];

        $couriers = new Courier;
        $courier = $couriers->getCourierById($courier_id);

        $usertype = new UserType;
        $typeID = $usertype->getUserTypeID($userID);
        $merchants = new Merchants;

        //move this to the Merchants Model
        $merchant = $merchants->find($typeID);
        $courierMerchant = new CourierMerchants;

        $data = [];

        $order_exist = $transaction->getTransactionByOrderNo($_transaction['transaction_id'],$merchant->id);
        if( $courier == null || $courier->count() <= 0 )
        {
            $data['status'] = 0;
            $data['message'] = "Courier ID is either NOT Valid or Disabled";
        }

        //does merchant exist or is it disabled
        if($merchant->status == 0 )
        {
            $data['status'] = 0;
            $data['message'] = "Merchant is Disabled";
        } elseif ($order_exist > 0) {
          # code...
            $data['status'] = 0;
            $data['message'] = "Order Number exist already!";
       }
        else
        {

            $delivery_cost = $this->determineDeliveryCost($courier_id, $_transaction['pickup'], $_transaction['delivery'],$_transaction['orders']['items']);

            if(empty($delivery_cost)) {
                $data['status'] = 1;
                $data['message'] = "Unable to calculate delivery cost";
                $data["transaction_id"] = $_transaction['transaction_id'];
                return $data;
            }

            $courierid = $courier->id;
            $merchantid = $merchant->id;
            $pod = $_transaction['POD'];
            $pickup_handling = $_transaction['pickup_handling'];
            $delivery_handling = $_transaction['delivery_handling'];
            $transaction_datetime =  new \ExpressiveDate();
            $pre_auth = $_transaction['pre_auth'];

            //pickup_handling could be either of this merchant or courier
            if($pickup_handling == 'mercht-loc')
            {
                $pickup = json_encode($_transaction['pickup']);
            }
            elseif ($pickup_hanlding == 'courier-loc' ) {
                # code...
                //d()
            }

          //delivery handling could be either to_customer or customer_pickup
          if($delivery_handling == 'to_customer')
          {
            $delivery = json_encode($_transaction['delivery']);
          }

          //$transaction_id = $this->generateID(12);
          //$transaction_datetime =  new \ExpressiveDate();

          $transaction->transaction_id = isset($_transaction['transaction_id']) ? $_transaction['transaction_id'] : $this->generateOrderID(12) ;
          $transaction->transaction_datetime = $transaction_datetime;
          $transaction->pod_transaction = empty($pod) ? 0 : $pod;
          $transaction->preauth_transaction = empty($pre_auth) ? 0 : $pre_auth;
          $transaction->merchant_id = $merchantid;
          $transaction->pickup_handling = $pickup_handling;
          $transaction->delivery_handling = $delivery_handling;
          $transaction->courier_id = $courier->id;
          $transaction->transaction_status = Status::PENDING; //means it is new
          $transaction->pickup_details = $pickup;
          $transaction->delivery_details = $delivery;
          $transaction->delivery_cost = $delivery_cost;

          if(isset($delivery['order_id'])) {
            $transaction->transaction_order_no = $delivery['order_id'];
          }

          if($pod == 0 && $pre_auth == 0)
              $transaction->transaction_payment_type = "Prepaid";
          elseif($pod == 0 && $pre_auth == 1)
              $transaction->transaction_payment_type = "DOD";
          elseif ($pod == 1) {
            # code...
            $transaction->transaction_payment_type = "POD";
          }

          $items = $_transaction['orders']['items'];
          $count = count($_transaction['orders']['items']);

          $this->logger->debug("storing items",["items" => $items]);

          $itemIDArray = [];
          for ($i=0;$i < $count; $i++)
          {
              $item = new Items;
              $item->item_id = $this->generateOrderID(12);
              $itemIDArray[$i] = $item->item_id;
              $item->transaction_id = $_transaction['transaction_id'];
              $item->item_name = $items[$i]['item_name'];
              $item->item_color = $items[$i]['item_color'];
              $item->item_qty = empty($items[$i]['item_qty']) ? 0 : $items[$i]['item_qty'];
              $item->item_weight = empty($items[$i]['item_weight']) ? 1 : $items[$i]['item_weight'];
              $item->item_cost = $items[$i]['item_cost'];
              $item->item_size = $items[$i]['item_size'];
              $item->delivery_status = Status::Pending;
              $item->save();
              $total_amount[$i] = $items[$i]['item_cost'] * $items[$i]['item_qty'];
              //print($i."<br/>");
          }

          $transaction->transaction_total_amount = array_sum($total_amount);
          $transaction->save();

          $usertype = new UserType;
          $agent = new Users;

          //assign agent for pickup

          $users = $agent->getUsers();

          for($j=0; $j < count($itemIDArray); $j++) {

            for ($i=0; $i < count($users); $i++ ) {
                $role = $users[$i]->getRoles()[0]['name'];
                if ($role == "Agent") {
                    if( $typeID == $usertype->getUserTypeID($users[$i]->id) )
                    {
                      $agent_id = $users[$i]['id'];
                      $this->assignAgentForPickup($agent_id, $transaction->transaction_id, $itemIDArray[$j]);
                    }
                }
            }
        }

          $data['status'] = 1;
          $data['message'] = "A new delivery has been created";
          $data["transaction_id"] = $transaction->transaction_id;

        }

        return $data;
    }

    public function assignAgentForPickup(int $agent_id, int $transaction_id, string $item_id = NULL){

        $cnt = $this->transaction->where('id', '=', $transaction_id)->count();

        if($cnt < 1) {
            return 'ok';
        }

        if(is_null($agent_id) == false && $agent_id != "Unassigned")
        {

            $delivery_assignment = new DeliveryAssignment();
            $delivery_assignment->agent_id = $agent_id;
            $delivery_assignment->transaction_id = $transaction_id;
            $delivery_assignment->item_id = $item_id;
            $delivery_assignment->type = "pickup";
            $delivery_assignment->save();

        }
    
        return 'ok';

  }

  public function assignAgentForDropOff(int $agent_id, int $transaction_id, string $item_id, $partial = false) {

    $cnt = $this->transaction->where('id', '=', $transaction_id)->count();

    if($cnt < 1) {
        return 'ok';
    }

    if(is_null($agent_id) == false && $agent_id != "Unassigned")
    {

        $rows = DB::table("delivery_assignment")
                ->where("transaction_id","=", $transaction_id)
                ->where('agent_id','=',$agent_id)
                ->where('item_id','=',$item_id)
                ->where("type","=",'pickup')
                ->update(["type" => 'dropoff']);

        if($rows < 1) {
            $delivery_assignment = new DeliveryAssignment();
            $delivery_assignment->agent_id = $agent_id;
            $delivery_assignment->transaction_id = $transaction_id;
            $delivery_assignment->item_id = $item_id;
            $delivery_assignment->type = "dropoff";
            $delivery_assignment->save();
        }

        DB::table("delivery_assignment")
            ->where("item_id","=",$item_id)
            ->where("transaction_id","=", $transaction_id)
            ->where('agent_id','!=',$agent_id)
            ->delete();

    }

    return 'ok';

}

  public function generateOrderID($length)
  {
    $code = '';
    $possible = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    $maxlength = strlen($possible);
    if ($length > $maxlength) {
        $length = $maxlength;
    }

    $i = 0;
    while ($i < $length) {
        $char = substr($possible, mt_rand(0, $maxlength - 1), 1);
        if (!strstr($code, $char)) {
            $code .= $char;
            $i++;
        }
    }

    return 'SA'.$code;
  }

  public function filterSuccessfullDeliveriesForCourier(string $courier_id, array $cols, array $order_info, string $from_date, string $to_date) {

    return DataTableService::filterTable($cols, $from_date, $to_date, function ($filter_data) use ($courier_id, $order_info, $from_date, $to_date) {

        $count  = Transactions::where("courier_id","=",$courier_id)
                    ->whereIn('transactions.transaction_status',[3,301,30])
                    ->count();

        if(empty($filter_data)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('courier_id', '=', $courier_id)
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('courier_id', '=', $courier_id)
            ->where($filter_data)
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(
                'transactions.*',
                'merchants.merchant_name',
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();
        
        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    });
    
  }

  public function filterSuccessfullDeliveriesForMerchant(int $merchant_id, array $cols, array $order_info, string $from_date, string $to_date) {

    return DataTableService::filterTable($cols, $from_date, $to_date, function ($filter_data) use ($merchant_id, $order_info, $from_date, $to_date) {

        $count  =  DB::table("transactions")
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
                    ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
                    ->where('merchants.id', '=', $merchant_id)
                    ->whereIn('transactions.transaction_status',[3,301,30])
                    ->select(                    
                        'transactions.*', 
                        'merchants.merchant_name', 
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                        DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
                    )->distinct('transactions.transaction_id')
                    ->count();

        if(empty($filter_data)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('merchants.id', '=', $merchant_id)
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('merchants.id', '=', $merchant_id)
            ->where($filter_data)
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();
        
        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];
    });

  }

  public function filterSuccessfullDeliveriesForAdmin(array $cols, array $order_info, string $from_date, string $to_date) {

    return DataTableService::filterTable($cols, $from_date, $to_date, function ($filter_data) use ($order_info, $from_date, $to_date) {

        $count  = Transactions::whereIn('transactions.transaction_status',[3,301,30])
                    ->count();

        if(empty($filter_data)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`"),
                DB::raw("(select `courier`.`courier_name` from `courier` where `courier`.`id` = `transactions`.`courier_id`) as `courier_name`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where($filter_data)
            ->whereIn('transactions.transaction_status',[3,301,30])
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select sum(`saddle_lite_transaction`.`amount`) from `saddle_lite_transaction` where `saddle_lite_transaction`.`transaction_id` = `transactions`.`transaction_id` and `saddle_lite_transaction`.`status` = 'SUCCESS') as `recorded_payment`"),
                DB::raw("(select `courier`.`courier_name` from `courier` where `courier`.`id` = `transactions`.`courier_id`) as `courier_name`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);
        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    });

  }
 
  public function searchDeliveriesForAdmin(string $search_term, array $order_info, string $from_date, string $to_date)  {

    return DataTableService::searchTable($from_date, $to_date, function() use ($search_term, $order_info, $from_date, $to_date) {

        $count  = Transactions::count();

        if(is_null($search_term) || empty($search_term)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select `courier`.`courier_name` from `courier` where `courier`.`id` = `transactions`.`courier_id`) as `courier_name`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where("transactions.transaction_id", "like", $search_term . '%')
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                DB::raw("(select `courier`.`courier_name` from `courier` where `courier`.`id` = `transactions`.`courier_id`) as `courier_name`")
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);
        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    });

  }

  public function searchDeliveriesForCourier(string $search_term, array $order_info, string $from_date, string $to_date)  {

    return DataTableService::searchTable($from_date, $to_date, function() use ($courier_id, $search_term, $order_info, $from_date, $to_date) {

        $count  = Transactions::where("courier_id","=",$courier_id)->count();

        if(is_null($search_term) || empty($search_term)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where("transactions.courier_id",'=',$courier_id)
            ->select(
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where("transactions.courier_id",'=',$courier_id)
            ->where("transactions.transaction_id", "like", $search_term . '%')
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);
        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        $transactions = $transactionQ->get();

        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    });

  }

  public function searchDeliveriesForMerchant(string $search_term, array $order_info, string $from_date, string $to_date)  {

    if( (empty($from_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$from_date) == 0) || (empty($to_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$to_date) == 0) ) {

        $this->logger->debug("date interval is invalid");

        return [
            "error" => "invalid dates",
            "total_count" => 0,
            'total_filtered' => 0,
            "total" => []
        ];
    }

    return $this->transaction->searchDeliveriesMerchant($search_term, $order_info, $from_date, $to_date);

  }

  public function searchSuccessfullDeliveriesForAdmin(string $search_term, array $order_info, string $from_date, string $to_date)  {

    if( (empty($from_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$from_date) == 0) || (empty($to_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$to_date) == 0) ) {

        $this->logger->debug("date interval is invalid");

        return [
            "error" => "invalid dates",
            "total_count" => 0,
            'total_filtered' => 0,
            "total" => []
        ];
    }

    return $this->transaction->searchSuccessfullDeliveriesAdmin($search_term, $order_info, $from_date, $to_date);

  }

  public function searchSuccessfullDeliveriesForCourier(string $courier_id,  string $search_term, array $order_info, string $from_date, string $to_date)  {

    if(is_null($courier_id) || isset($courier_id) == false || empty($courier_id)) {

        $this->logger->debug("searchSuccessfullDeliveriesForCourier: courier id is null");
        return [
            "error" => "no courier id",
            "total_count" => 0,
            'total_filtered' => 0,
            "total" => []
        ];
    }

    if( (empty($from_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$from_date) == 0) || (empty($to_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$to_date) == 0) ) {

        $this->logger->debug("date interval is invalid");

        return [
            "error" => "invalid dates",
            "total_count" => 0,
            'total_filtered' => 0,
            "total" => []
        ];
    }

    return $this->transaction->searchSuccessfullDeliveriesCourier($courier_id, $search_term, $order_info, $from_date, $to_date);

  }

  public function searchSuccessfullDeliveriesForMerchant(int $merchant_id, $search_term, $order_info, $from_date, $to_date)  {

    if(is_null($merchant_id) || isset($merchant_id) == false || empty($merchant_id)) {

        $this->logger->debug("searchSuccessfullDeliveriesForMerchant: courier id is null");
        return [
            "error" => "no courier id",
            "total_count" => 0,
            'total_filtered' => 0,
            "total" => []
        ];
    }

    if( (empty($from_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$from_date) == 0) || (empty($to_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$to_date) == 0) ) {

        $this->logger->debug("date interval is invalid");

        return [
            "error" => "invalid dates",
            "total_count" => 0,
            'total_filtered' => 0,
            "total" => []
        ];
    }

    return $this->transaction->searchSuccessfullDeliveriesMerchants($merchant_id, $search_term, $order_info, $from_date, $to_date);
  
  }


  public static function populateCustomerAddressForTransaction($transaction) {

    $customer_addressBoolean = strpos($transaction->delivery_details, 'customer_address');
    $aaaa = strpos($transaction->delivery_details, 'address');

    if ($customer_addressBoolean == true) {

        $json = json_decode($transaction->delivery_details, true);
        $customerAddress = $json['customer_address'].", ".$json['customer_lga'].", ".$json['customer_state'];
        $transaction->customer_address_new = $customerAddress;
        
    } elseif ($aaaa == true) {

        $json = json_decode($transaction->delivery_details, true);
        $customerAddress = $json['address'].", ".$json['city'].", ".$json['state'].", ".$json['country'];
        $transaction->customer_address_new = $customerAddress;
        
    } else {

        $transaction->customer_address_new = $transaction->delivery_details;

    }

  }

  public function filterSuccessfullB2BDeliveriesForCourier(string $courier_id, array $cols, array $order_info, string $from_date, string $to_date) {

    $queryParams = [];

    for ($i=0; $i < count($cols); $i++) {
        if(empty($cols[$i]['search']['value']) == false && is_null($cols[$i]['search']['value']) == false) {
            array_push( $queryParams, [
                 $cols[$i]['data'], 
                 '=', 
                 $cols[$i]['search']['value'] ] );
        }
    }

    if( (empty($from_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$from_date) === 0) || (empty($to_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$to_date) === 0) ) {
        $this->logger->debug("date interval is  invalid");
        return [
            "total_count" => 0,
            'total_filtered' => 0,
            "total" => []
        ];
    }

    $this->logger->debug("filter successfull deliveries queryParams: ", $queryParams );

    return $this->transaction->filterSuccessfullB2BDeliveriesCourier($courier_id, $queryParams, $order_info, $from_date, $to_date);

  }

  public function filterSuccessfullB2BDeliveriesForMerchant(int $merchant_id, array $cols, array $order_info, string $from_date, string $to_date) {

    $queryParams = [];

    for ($i=0; $i < count($cols); $i++) {
        if(empty($cols[$i]['search']['value']) == false && is_null($cols[$i]['search']['value']) == false) {
            array_push( $queryParams, [
                 $cols[$i]['data'], 
                 '=', 
                 $cols[$i]['search']['value'] ] );
        }
    }

    if( (empty($from_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$from_date) === 0) || (empty($to_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$to_date) === 0) ) {
        $this->logger->debug("date interval is  invalid");
        return [
            "total_count" => 0,
            'total_filtered' => 0,
            "total" => []
        ];
    }

    $this->logger->debug("filterSuccessfullDeliveriesForMerchant queryParams: ", [empty($queryParams)] );

    return $this->transaction->filterSuccessfullB2BDeliveriesMerchants($merchant_id, $queryParams, $order_info, $from_date, $to_date);

  }

  public function filterSuccessfullB2BDeliveriesForAdmin(array $cols, array $order_info, string $from_date, string $to_date) {

    $queryParams = [];

    for ($i=0; $i < count($cols); $i++) {
        if(empty($cols[$i]['search']['value']) == false && is_null($cols[$i]['search']['value']) == false) {
            array_push( $queryParams, [
                 $cols[$i]['data'], 
                 '=', 
                 $cols[$i]['search']['value'] ] );
        }
    }

    if( (empty($from_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$from_date) === 0) || (empty($to_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$to_date) === 0) ) {
        $this->logger->debug("date interval is  invalid");
        return [
            "total_count" => 0,
            'total_filtered' => 0,
            "total" => []
        ];
    }

    $this->logger->debug("filterSuccessfullDeliveriesForAdmin queryParams: ", $queryParams );

    return $this->transaction->filterSuccessfullB2BDeliveriesAdmin($queryParams, $order_info, $from_date, $to_date);

  }

  public function searchSuccessfullB2BDeliveriesForAdmin(string $search_term, array $order_info, string $from_date, string $to_date)  {

    if( (empty($from_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$from_date) == 0) || (empty($to_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$to_date) == 0) ) {

        $this->logger->debug("date interval is invalid");

        return [
            "error" => "invalid dates",
            "total_count" => 0,
            'total_filtered' => 0,
            "total" => []
        ];
    }

    return $this->transaction->searchSuccessfullB2BDeliveriesAdmin($search_term, $order_info, $from_date, $to_date);

  }

  public function searchSuccessfullB2BDeliveriesForCourier(string $courier_id,  string $search_term, array $order_info, string $from_date, string $to_date)  {

    if(is_null($courier_id) || isset($courier_id) == false || empty($courier_id)) {

        $this->logger->debug("searchSuccessfullDeliveriesForCourier: courier id is null");
        return [
            "error" => "no courier id",
            "total_count" => 0,
            'total_filtered' => 0,
            "total" => []
        ];
    }

    if( (empty($from_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$from_date) == 0) || (empty($to_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$to_date) == 0) ) {

        $this->logger->debug("date interval is invalid");

        return [
            "error" => "invalid dates",
            "total_count" => 0,
            'total_filtered' => 0,
            "total" => []
        ];
    }

    return $this->transaction->searchSuccessfullB2BDeliveriesCourier($courier_id, $search_term, $order_info, $from_date, $to_date);

  }

  public function searchSuccessfullB2BDeliveriesForMerchant(int $merchant_id, $search_term, $order_info, $from_date, $to_date)  {

    if(is_null($merchant_id) || isset($merchant_id) == false || empty($merchant_id)) {

        $this->logger->debug("searchSuccessfullDeliveriesForMerchant: courier id is null");
        return [
            "error" => "no courier id",
            "total_count" => 0,
            'total_filtered' => 0,
            "total" => []
        ];
    }

    if( (empty($from_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$from_date) == 0) || (empty($to_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$to_date) == 0) ) {

        $this->logger->debug("date interval is invalid");

        return [
            "error" => "invalid dates",
            "total_count" => 0,
            'total_filtered' => 0,
            "total" => []
        ];
    }

    return $this->transaction->searchSuccessfullB2BDeliveriesMerchants($merchant_id, $search_term, $order_info, $from_date, $to_date);
  
  }

  public function getAgentPickup(int $agent_id) {

    $pickups = DB::table('delivery_assignment')
                ->distinct()
                ->where('agent_id','=', $agent_id)
                ->where('type','pickup')
                ->pluck('transaction_id');


    $data = $this->transactions
        ->join('items','transactions.transaction_id','=','items.transaction_id')
        ->where('transactions.pickup_handling','mercht-loc')
        ->whereIn('transactions.id',$pickups->toArray())
        ->where(function($query) {
            $query->where('transactions.transaction_status',0)
            ->orWhere('items.delivery_status',0);
        })
        ->select('transactions.*')->distinct('transactions.transaction_id')
        ->get();

    $result = $data->flatten();

  }

  public function assignAllAgentsForPickup(int $courier_id, int $transaction_id, string $item_id) {

    $cnt = $this->transaction->where('id', '=', $transaction_id)->count();

    if($cnt < 1) {
        return "ok";
    }

    $agents = $this->accountsService->getAgentsForCourier($courier_id);

    $this->logger->debug("get agent for courier",[$agents]);

    DB::table("delivery_assignment")->where("item_id","=",$item_id)->delete();

    for($i = 0; $i < count($agents); $i++) {
        $delivery_assignment = new DeliveryAssignment();
        $delivery_assignment->agent_id = $agents[$i]->id;
        $delivery_assignment->transaction_id = $transaction_id;
        $delivery_assignment->item_id = $item_id;
        $delivery_assignment->type = "pickup";
        $delivery_assignment->save();
    }

    return "ok";

  }


  public function assignAllAgentsForPickupBulk(int $courier_id, array $assignments) {

    if(is_null($assignments) || empty($assignments))
        return "ok";

    $q = [];
    $items = [];
    $agents = $this->accountsService->getAgentsForCourier($courier_id);

    for ($i=0; $i < count($assignments); $i++) {

        if(is_null($assignments[$i]["item_id"]) || empty($assignments[$i]["item_id"]) || is_null($assignments[$i]["transaction_id"]) || empty($assignments[$i]["transaction_id"])) {
            continue;
        }

        array_push($items, $assignments[$i]["item_id"]);

        if(isset($assignments[$i]["agent_id"])) {
            array_push($q,[
                "agent_id" => $assignments[$i]["agent_id"],
                "transaction_id" => $assignments[$i]["transaction_id"],
                "type" => "pickup",
                "item_id" => $assignments[$i]["item_id"]
            ]);
            continue;
        }

        for($j=0; $j < count($agents); $j++) {
            array_push($q, [
                "agent_id" => $agents[$j]->id,
                "transaction_id" => $assignments[$i]["transaction_id"],
                "type" => "pickup",
                "item_id" => $assignments[$i]["item_id"]
            ]);
        }

    }

    $this->logger->debug("agents->",$q);


    DB::table("delivery_assignment")->whereIn("item_id",$items)->delete();
    DeliveryAssignment::insert($q);

    return "ok";

  }

  public function assignAllAgentsForDropOff(int $courier_id, int $transaction_id, string $item_id) {

    $cnt = $this->transaction->where('id', '=', $transaction_id)->count();

    if($cnt < 1) {
        return "ok";
    }

    $agents = $this->accountsService->getAgentsForCourier($courier_id);

    DB::table("delivery_assignment")->where("item_id","=",$item_id)->delete();

    for($i = 0; $i < count($agents); $i++) {
        $delivery_assignment = new DeliveryAssignment();
        $delivery_assignment->agent_id = $agents[$i]->id;
        $delivery_assignment->transaction_id = $transaction_id;
        $delivery_assignment->item_id = $item_id;
        $delivery_assignment->type = "dropoff";
        $delivery_assignment->save();
    }

    return "ok";

  }

  public function filterDeliveriesForCourier(string $courier_id, array $cols, array $order_info, string $from_date, string $to_date, string $status) {

    return DataTableService::filterTable($cols, $from_date, $to_date, function ($filter_data) use ($courier_id, $order_info, $from_date, $to_date) {

        $count = Transactions::where("courier_id","=",$courier_id)
                    ->count();

        if(empty($filter_data)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('courier_id', '=', $courier_id)
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('courier_id', '=', $courier_id)
            ->where($filter_data)
            ->select(
                'transactions.*',
                'merchants.merchant_name',
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        if(empty($status) == false) {
            $transactionQ = $transactionQ->whereIn('transaction_status',$this->getStatusCodeArray($status));
        }

        $transactions = $transactionQ->get();
        
        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];
    });

  }

  public function filterDeliveriesForMerchant(int $merchant_id, array $cols, array $order_info, string $from_date, string $to_date, string $status) {

    return DataTableService::filterTable($cols, $from_date, $to_date, function ($filter_data) use ($merchant_id, $order_info, $from_date, $to_date, $agent_id) {

        $count  =  DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('merchants.id', '=', $merchant_id)
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
                )->distinct('transactions.transaction_id')->count();

        if(empty($filter_data)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('merchants.id', '=', $merchant_id)
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where('merchants.id', '=', $merchant_id)
            ->where($filter_data)
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        }

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        if(empty($status) == false) {
            $transactionQ = $transactionQ->whereIn('transaction_status', $this->getStatusCodeArray($status));
        }

        $transactions = $transactionQ->get();
        
        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];

    });

  }

  public function filterDeliveriesForAdmin(array $cols, array $order_info, string $from_date, string $to_date, string $status) {

    return DataTableService::filterTable($cols, $from_date, $to_date, function ($filter_data) use ($order_info, $from_date, $to_date) {
        
        $count  =  DB::table("transactions")
                    ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
                    ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
                    ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
                    ->select(                    
                        'transactions.*', 
                        'merchants.merchant_name', 
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
                        )->distinct('transactions.transaction_id')->count();

        if(empty($filter_data)) {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        } else {

            $transactionQ = DB::table("transactions")
            ->join('merchants', 'merchants.id', '=', 'transactions.merchant_id')
            ->leftJoin("delivery_assignment",'delivery_assignment.transaction_id',"=","transactions.id")
            ->leftJoin("users",'users.id','=','delivery_assignment.agent_id')
            ->where($filter_data)
            ->select(                    
                'transactions.*', 
                'merchants.merchant_name', 
                DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name')
            )->distinct('transactions.transaction_id')
            ->orderBy($order_info['name'],$order_info['dir']);

        }

        $this->logger->debug("from date is " . $from_date);
        $this->logger->debug("to date is " . $to_date);

        if(empty($from_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
        }

        if(empty($to_date) == false) {
            $transactionQ = $transactionQ->where("transactions.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
        }

        if(empty($status) == false) {
            $transactionQ = $transactionQ->whereIn('transaction_status',$this->getStatusCodeArray($status));
        }

        $transactions = $transactionQ->get();
        
        return [
            "total_count" => $count,
            'total_filtered' => $transactions->count(),
            "total" => $transactions
        ];
        
    });

  }

  /**
   * 
   */
  public function getItemsForTransaction(string $transaction_id) {

    return Items::where("transaction_id","=",$transaction_id)->get();

  }
  
  public function assignAllAgentsForDropoffBulk(int $courier_id, array $assignments) {

    if(is_null($assignments) || empty($assignments))
        return "ok";

    $q = [];
    $items = [];
    $agents = $this->accountsService->getAgentsForCourier($courier_id);

    for ($i=0; $i < count($assignments); $i++) {

        if(is_null($assignments[$i]["item_id"]) || empty($assignments[$i]["item_id"]) || is_null($assignments[$i]["transaction_id"]) || empty($assignments[$i]["transaction_id"])) {
            continue;
        }

        array_push($items, $assignments[$i]["item_id"]);

        if(isset($assignments[$i]["agent_id"])) {
            array_push($q,[
                "agent_id" => $assignments[$i]["agent_id"],
                "transaction_id" => $assignments[$i]["transaction_id"],
                "type" => "dropoff",
                "item_id" => $assignments[$i]["item_id"]
            ]);
            continue;
        }

        for($j=0; $j < count($agents); $j++) {
            array_push($q, [
                "agent_id" => $agents[$j]->id,
                "transaction_id" => $assignments[$i]["transaction_id"],
                "type" => "dropoff",
                "item_id" => $assignments[$i]["item_id"]
            ]);
        }

    }

    $this->logger->debug("agents->",$q);


    DB::table("delivery_assignment")->whereIn("item_id",$items)->delete();
    DeliveryAssignment::insert($q);

    return "ok";

  }

  public function removeAllDeliveryAssignmentsForItem(string $item_id) {
    DeliveryAssignment::where("item_id","=",$item_id)->delete();
    return "ok";
  }


}


?>