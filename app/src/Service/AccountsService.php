<?php

namespace Dappur\Services;

use Dappur\Model\Users;
use Dappur\Model\Roles;
use Dappur\Model\RoleUsers;
use Dappur\Model\Card;
use Dappur\Model\Courier;
use Dappur\Model\UserType;
use Dappur\Model\Merchants;

use Illuminate\Database\Capsule\Manager as DB;



/**
 * Delivery Service
 */
class AccountsService extends Service {

    protected $users;
    protected $roleUsers;
    protected $roles;
    protected $card;
    protected $courier;
    protected $userType;

    public function __construct($container) {

        parent::__construct($container);
        $this->users = new Users;
        $this->roles = new Roles;
        $this->roleUsers = new RoleUsers;
        $this->card = new Card;
        $this->courier = new Courier;
        $this->userType = new UserType;
        $this->merchants = new Merchants;

    }

    public function getAgentById(int $user_id) {

        $is_agent = $this->roleUsers->isAgent($user_id);

        if($is_agent == FALSE) {
            return null;
        }

        $agent = $this->users->where("id",$user_id)->select();

        return $agent;

    }

    public function isAgent($user_id) {
        $this->logger->debug("calling is agent",["user_id" => $user_id, "res" => $this->roleUsers->isAgent($user_id)]);
        return $this->roleUsers->isAgent($user_id);
    }

    public function isCourierUser($user_id) {
        return $this->roleUsers->isCourierUser($user_id);
    }

    public function isCourierAdmin($user_id) {
        return $this->roleUsers->isCourierAdmin($user_id);
    }

    public function isAdmin($user_id) {
        return $this->roleUsers->isAdmin($user_id);
    }

    public function isMerchantUser($user_id) {
        return $this->roleUsers->isMerchantUser($user_id);
    }

    public function registerCardOnFile($courier_id, $user_id) {

        $this->logger->debug("calling register card on file",["user_id" => $user_id, "courier_id" => $courier_id]);

        $is_agent = $this->isAgent($user_id);
        $courier_details = $this->courier->find($courier_id);

        if(is_null($courier_details) || empty($courier_details)) {
            $this->logger->debug("courier_details is null",["courier_id" => $courier_id, "res" => $courier_details]);
            return null;
        }

        $result = $this->card->insert(
            ['courier_id' => $courier_details['courier_id'],'agent_id' => $user_id, 'status'=>1]);

        return $result;

    }

    public function getAgentByEmail(string $email) {
        return null;
    }

    public function getAgentCardsOnFileForCourier($courier_id) {

        $this->logger->debug("calling get agent card on file",["courier_id" => $courier_id]);

        $courier = Courier::find($courier_id);

        $users = $this->users
                ->join("card_on_file","users.id","=","card_on_file.agent_id")
                ->join('role_users','card_on_file.agent_id','=','role_users.user_id')
                ->where("card_on_file.courier_id","=",$courier->courier_id)
                ->where('role_users.role_id','=',RoleUsers::AGENT)
                ->whereNotNull("card_on_file.agent_id")
                ->select("card_on_file.id","card_on_file.agent_id","card_on_file.status","users.first_name","users.last_name","users.email")
                ->get();

        return $users;

    }

    public function updateAgentCardStatus($card_id, $status) {
        $this->card
            ->where("id",$card_id)
            ->update(['status' => $status]);
    }

    public function getCourierForAgent($user_id) {

        if($this->isAgent($user_id) == false) {
            return null;
        } 

        return $this->userType->getCourierForUser($user_id);

    }

    public function getCourierForUser($user_id) {

        if($this->isAgent($user_id) == false  &&  $this->isCourierAdmin($user_id) == false && $this->isAdmin($user_id) == false  && $this->isCourierUser($user_id) == false ) {
            return null;
        }

        return $this->userType->getCourierForUser($user_id);

    }

    public function getCourierForMerchant($user_id) {

        if($this->isMerchantUser($user_id) == false) {
            return null;
        }

        return DB::table('courier')->where('id',function($q) use ($user_id){
            $q->from("usertype")
                ->join("courier_merchants","courier_merchants.merchant_id","=","usertype.type_id")
                ->where("user_id","=",$user_id)
                ->where("user_type","Merchant")
                ->select("courier_merchants.courier_id");
        })->first();

    }

    public function isCourierUserType(int $user_id) {

        $user_type = $this->userType->getUserType($user_id);

        return $user_type == 'Courier';

    }

    public function isMerchantUserType(int $user_id) {

        $user_type = $this->userType->getUserType($user_id);

        return $user_type == 'Merchant';

    }

    public function getMerchantForUser(int $user_id) {

        if($this->isMerchantUser($user_id) == false) {
            return null;
        }

        $user_type = UserType::where('user_id', '=', $user_id)->first();

        return Merchants::where("id","=",$user_type['type_id'])->first();

    }

    public function getUserType(int $user_id) {

        $user_type = UserType::where('user_id', '=', $user_id)->first();
        return $user_type['user_type'] == "" ? "admin" : $user_type['user_type'];

    }

    public function getCardOnFileEmail(string $courier_id, string  $courier_email, string $user_email) {

        $users = $this->users
                ->join("card_on_file","users.id","=","card_on_file.agent_id")
                ->where("card_on_file.courier_id","=",$courier_id)
                ->where('card_on_file.status','=',1)
                ->whereNotNull("card_on_file.agent_id")
                ->select("card_on_file.id","card_on_file.agent_id","card_on_file.status","users.first_name","users.last_name","users.email")
                ->get();

        $this->logger->debug('get card on file email, courier_id ', [$courier_id]);
        $this->logger->debug('get card on file email, users', [$users]);

        $running_email = "";

        for ($i=0; $i < count($users); $i++) {

            $running_email = $users[$i]->email;
            if($running_email ==  $user_email) {
                return $running_email;
            }

        }

        return empty($running_email) == false ? $courier_email : $running_email;

    }

    public function getAgentsForCourier(int $courier_id) {

        return DB::table("users")->join("role_users","role_users.user_id","=","users.id")
                ->where("role_users.role_id","=",RoleUsers::AGENT)
                ->whereIn("users.id",function($w) use ($courier_id) {
                    $w->select("user_id")->from("usertype")->where("user_type","=","Courier")->where("type_id","=",$courier_id)->get();
                })
                ->select("id", "email", "username", "first_name", "last_name")
                ->get();

    }
    
    public function getAgentsByUserName(string $username) {
        return Users::where("username",$username)->first();
    }

    public function getAllAgents() {
        return DB::table("users")->join("role_users","role_users.user_id","=","users.id")
        ->where("role_users.role_id","=",RoleUsers::AGENT)
        ->select("id", "email", "username", "first_name", "last_name")
        ->get();
    }

}


?>