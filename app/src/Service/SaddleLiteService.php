<?php

namespace Dappur\Services;

use Dappur\Model\Courier;
use Dappur\Model\AgentDeposit;
use Dappur\Model\SaddleLiteTransaction;

use Illuminate\Database\Capsule\Manager as DB;

/**
 * Saddle lite Service
 */
class SaddleLiteService extends Service {

    protected $courier;
    protected $saddleLiteTransaction;

    public function __construct($container) {

        parent::__construct($container);
        $this->courier = new Courier;
        $this->saddleLiteTransaction = new SaddleLiteTransaction;
        $this->accountsService = new AccountsService($container);

    }

    public function getSaddleLiteTransaction(string $transaction_id) {

        return $this->saddleLiteTransaction->getTransaction($transaction_id);

    }

    public function getSaddleLiteTransactionsForAgent(int $agent_id) {

        return $this->saddleLiteTransaction->getAllTransactionsForAgent($agent_id);

    }

    public function getSaddleLiteTransactionsForCourier(int $courier_id) {

        return $this->saddleLiteTransaction->getAllTransactionsForCourier($courier_id);

    }

    public function searchSaddleLiteTransactions(int $courier_id, string $search_term, array $order_info, string $from_date, string $to_date)  {

        if(is_null($courier_id) || isset($courier_id) == false || empty($courier_id)) {

            $this->logger->debug("searchSaddleLiteTransactions: courier id is null");

            return [
                "error" => "no courier id",
                "total_count" => 0,
                'total_filtered' => 0,
                "total" => []
            ];

        }

        return DataTableService::searchTable($from_date, $to_date, function() use ($courier_id, $search_term, $order_info, $from_date, $to_date) {

            $count  = SaddleLiteTransaction::where("courier_id","=",$courier_id)->count();

            if(is_null($search_term) || empty($search_term)) {
    
                $transactionsQ = SaddleLiteTransaction::join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                    ->where("saddle_lite_transaction.courier_id","=",$courier_id)
                    ->select("saddle_lite_transaction.transaction_id",
                        "saddle_lite_transaction.netplus_id",
                        "saddle_lite_transaction.transaction_type",
                        "saddle_lite_transaction.status",
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                        "saddle_lite_transaction.amount",
                        "saddle_lite_transaction.created_at"
                    )
                    ->orderBy($order_info['name'],$order_info['dir']);
    
            } else {
    
                $transactionsQ = SaddleLiteTransaction::join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                    ->where("saddle_lite_transaction.courier_id","=",$courier_id)
                    ->where("saddle_lite_transaction.transaction_id", "like", $search_term . '%')
                    ->select("saddle_lite_transaction.transaction_id",
                        "saddle_lite_transaction.netplus_id",
                        "saddle_lite_transaction.transaction_type",
                        "saddle_lite_transaction.status",
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                        "saddle_lite_transaction.amount",
                        "saddle_lite_transaction.created_at"
                    )
                    ->orderBy($order_info['name'],$order_info['dir']);
    
            }
    
            if(empty($from_date) == false) {
                $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
            }
    
            if(empty($to_date) == false) {
                $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
            }
    
            $transactions = $transactionsQ->get();
    
            return [
                "total_count" => $count,
                'total_filtered' => $transactions->count(),
                "total" => $transactions
            ];
        });

    }

    public function searchSaddleLiteTransactionsForAdmin(string $search_term, array $order_info, string $from_date, string $to_date)  {

        return DataTableService::searchTable($from_date, $to_date, function() use ($courier_id, $search_term, $order_info, $from_date, $to_date) {

            $count  = SaddleLiteTransaction::count();

            if(is_null($search_term) || empty($search_term)) {
    
                $transactionsQ = SaddleLiteTransaction::join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                    ->select("saddle_lite_transaction.transaction_id",
                        "saddle_lite_transaction.netplus_id",
                        "saddle_lite_transaction.transaction_type",
                        "saddle_lite_transaction.status",
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                        "saddle_lite_transaction.amount",
                        "saddle_lite_transaction.created_at"
                    )
                    ->orderBy($order_info['name'],$order_info['dir']);
    
            } else {
    
                $transactionsQ = SaddleLiteTransaction::join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                    ->where("saddle_lite_transaction.transaction_id", "like", $search_term . '%')
                    ->select("saddle_lite_transaction.transaction_id",
                        "saddle_lite_transaction.netplus_id",
                        "saddle_lite_transaction.transaction_type",
                        "saddle_lite_transaction.status",
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                        "saddle_lite_transaction.amount",
                        "saddle_lite_transaction.created_at"
                    )
                    ->orderBy($order_info['name'],$order_info['dir']);
    
            }
    
            if(empty($from_date) == false) {
                $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
            }
    
            if(empty($to_date) == false) {
                $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
            }
    
            $transactions = $transactionsQ->get();
    
            return [
                "total_count" => $count,
                'total_filtered' => $transactions->count(),
                "total" => $transactions
            ];
        });

    }

    public function getSaddleLiteTransactionBetweenDates(int $courier_id, string $from, string $to) {

        if(preg_match('/\d{1,2}-\d{1,2}-\d{4}/',$from) && preg_match('/\d{1,2}-\d{1,2}-\d{4}/',$to)) {

            return $this->saddleLiteTransaction->getAllTransactionsBetween($courier_id, $from, $to);

        }

        $this->logger->debug("saddle lite:  invalid getSaddleLiteTransactionBetweenDates: invalid dates ",$from, $to);
        return null;

    }

    public function createNewSaddleLiteTransaction(array $data) {

        if(is_null($data) || empty($data)) {
            return [
                "status" => 0,
                "msg" => "missing transaction data"
            ];
        }

        if(isset($data["transaction_id"]) == false  || is_null($data["transaction_id"])) {
            return [
                "status" => 0,
                "msg" => "invalid transaction id"
            ];
        }
    
        if((isset($data["transaction_type"]) == false  || is_null($data["transaction_type"])) && ( $data["transaction_type"] != 'CASH' || $data["transaction_type"] != 'CARD') ) {
            return [
                "status" => 0,
                "msg" => "invalid transaction type"
            ];
        }

        if(isset($data["amount"]) == false  || is_null($data["amount"]) ) {
            return [
                "status" => 0,
                "msg" => "invalid amount"
            ];
        }

        if(isset($data["agent_id"]) == false  || is_null($data["agent_id"]) ) {
            return [
                "status" => 0,
                "msg" => "invalid agent id"
            ];
        }

        if(isset($data["courier_id"]) == false  || is_null($data["courier_id"]) ) {
            return [
                "status" => 0,
                "msg" => "invalid courier id"
            ];
        }

        if((isset($data["status"]) == false  || is_null($data["status"])) && ( $data["status"] != 'SUCCESS' || $data["status"] != 'FAILURE') ) {
            return [
                "status" => 0,
                "msg" => "invalid transaction status"
            ];
        }

        //if transaction exist update transaction
        $transaction = $this->saddleLiteTransaction->where("transaction_id","=",$data["transaction_id"])->get();
        $transaction_count = $transaction->count();
        if($transaction_count > 0){
            $this->saddleLiteTransaction->where("transaction_id","=",$data["transaction_id"])->update(
                ["transaction_type" => $data["transaction_type"], "amount" => $data["amount"], "status" => $data["status"]]
            );
            //update transaction
            return [
                "status" => 1,
                "msg" => "updated successfully"

            ];
        }
      
        return $this->saddleLiteTransaction->createNewTransaction($data);

    }

    /**
     * 
     */
    public function isSearchRequest(array $cols) {

        $check = true;

        $this->logger->debug("is search request cols: ", $cols);

        for ($i=0; $i < count($cols) ; $i++) {

            $this->logger->debug("is search request checking: ", [ $cols[$i]['search']['value'] ] );
            $check = $check && empty($cols[$i]['search']['value']) == false;

            if($check == false)
                break;

        }

        $this->logger->debug("is search requests : ", [$check]);

        return $check;

    }
    
    public function handleColumnFilter(int $courier_id, array $cols, array $order_info, string $from_date, string $to_date, int $agent_id = 0) {

        return DataTableService::filterTable($cols, $from_date, $to_date, function ($filter_data) use ($courier_id, $order_info, $from_date, $to_date, $agent_id) {
            $count  = SaddleLiteTransaction::where("courier_id","=",$courier_id)->count();

            if(empty($filter_data)) {
    
                $transactionsQ = SaddleLiteTransaction::join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                                    ->where("saddle_lite_transaction.courier_id","=",$courier_id)
                                    ->select("saddle_lite_transaction.transaction_id",
                                        "saddle_lite_transaction.netplus_id",
                                        "saddle_lite_transaction.transaction_type",
                                        "saddle_lite_transaction.status",
                                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                                        "saddle_lite_transaction.amount",
                                        "saddle_lite_transaction.created_at")
                                        ->orderBy($order_info['name'], $order_info['dir']);
    
            } else {
            
                $transactionsQ = DB::table("saddle_lite_transaction")
                                    ->join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                                    ->where("saddle_lite_transaction.courier_id","=",$courier_id)
                                    ->where($filter_data)
                                    ->select("saddle_lite_transaction.transaction_id",
                                    "saddle_lite_transaction.netplus_id",
                                    "saddle_lite_transaction.transaction_type",
                                    "saddle_lite_transaction.status",
                                     DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                                    "saddle_lite_transaction.amount",
                                    "saddle_lite_transaction.created_at")
                                    ->orderBy($order_info['name'],$order_info['dir']);
     
            }
    
            if(empty($from_date) == false) {
                $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
            }
    
            if(empty($to_date) == false) {
                $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
            }
    
            if($agent_id > 0) {
                $transactionsQ = $transactionsQ->where('users.id',"=",$agent_id);
            }
    
            $transactions = $transactionsQ->get();
            
            return [
                "total_count" => $count,
                'total_filtered' => $transactions->count(),
                "total" => $transactions
            ];

        });

    }

    public function handleColumnFilterForAdmin(array $cols, array $order_info, string $from_date, string $to_date, $agent_id = 0) {
        
        return DataTableService::filterTable($cols, $from_date, $to_date, function ($filter_data) use ($order_info, $from_date, $to_date, $agent_id) {

            $count  = SaddleLiteTransaction::count();

            if(empty($filter_data)) {
    
                $transactionsQ = SaddleLiteTransaction::join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                                    ->select("saddle_lite_transaction.transaction_id",
                                        "saddle_lite_transaction.netplus_id",
                                        "saddle_lite_transaction.transaction_type",
                                        "saddle_lite_transaction.status",
                                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                                        "saddle_lite_transaction.amount",
                                        "saddle_lite_transaction.created_at")
                                        ->orderBy($order_info['name'],$order_info['dir']);;
    
            } else {
            
                $transactionsQ = DB::table("saddle_lite_transaction")
                                    ->join('users', 'users.id', '=', 'saddle_lite_transaction.agent_id')
                                    ->where($filter_data)
                                    ->select("saddle_lite_transaction.transaction_id",
                                    "saddle_lite_transaction.netplus_id",
                                    "saddle_lite_transaction.transaction_type",
                                    "saddle_lite_transaction.status",
                                     DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                                    "saddle_lite_transaction.amount",
                                    "saddle_lite_transaction.created_at")
                                    ->orderBy($order_info['name'],$order_info['dir']);;
     
            }

            if(empty($from_date) == false) {
                $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
            }

            if(empty($to_date) == false) {
                $transactionsQ = $transactionsQ->where("saddle_lite_transaction.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
            }

            if($agent_id > 0) {
                $transactionsQ = $transactionsQ->where('users.id',"=",$agent_id);
            }

            $transactions = $transactionsQ->get();

            return [
                "total_count" => $count,
                'total_filtered' => $transactions->count(),
                "total" => $transactions
            ];

        });

    }

    /**
     * log agent deposit
     */
    public function logAgentDeposit(int $agent_id, float $amount, String $slip_number) {

        if(isset($agent_id) == false || isset($amount) == false || isset($slip_number) == false)
            return false;

        $agent_deposit = new AgentDeposit;
        $agent_deposit->agent_id = $agent_id;
        $agent_deposit->deposit_value = $amount;
        $agent_deposit->slip_number = $slip_number; 

        $agent_deposit->save();

        return true;

    }

    public function handleDepositsFilter(int $courier_id, array $cols, array $order_info, string $from_date, string $to_date, int $agent_id = 0) {

        $agents = $this->accountsService->getAgentsForCourier($courier_id)->pluck("id");

        return DataTableService::filterTable($cols, $from_date, $to_date, function ($filter_data) use ($order_info, $from_date, $to_date, $agent_id, $agents) {

            $count  = AgentDeposit::whereIn("agent_id", $agents)->count();

            if(empty($filter_data)) {
    
                $transactionsQ = DB::table("agent_deposits")
                                    ->join('users', 'users.id', '=', 'agent_deposits.agent_id')
                                    ->whereIn("agent_deposits.agent_id", $agents)
                                    ->select("agent_deposits.agent_id",
                                        "agent_deposits.deposit_value",
                                        "agent_deposits.slip_number",
                                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                                        "agent_deposits.created_at")
                                    ->orderBy($order_info['name'], $order_info['dir']);
    
            } else {
            
                $transactionsQ = DB::table("agent_deposits")
                                    ->join('users', 'users.id', '=', 'agent_deposits.agent_id')
                                    ->whereIn("agent_deposits.agent_id", $agents)
                                    ->where($filter_data)
                                    ->select("agent_deposits.agent_id",
                                        "agent_deposits.deposit_value",
                                        "agent_deposit.slip_number",
                                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                                        "agent_deposits.created_at")
                                    ->orderBy($order_info['name'],$order_info['dir']);
     
            }
    
            if(empty($from_date) == false) {
                $transactionsQ = $transactionsQ->where("agent_deposits.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
            }
    
            if(empty($to_date) == false) {
                $transactionsQ = $transactionsQ->where("agent_deposits.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
            }
    
            if($agent_id > 0) {
                $transactionsQ = $transactionsQ->where('users.id',"=",$agent_id);
            }
    
            $transactions = $transactionsQ->get();
            
            return [
                "total_count" => $count,
                'total_filtered' => $transactions->count(),
                "total" => $transactions
            ];

        });

    }

    public function handleDepositsFilterForAdmin(array $cols, array $order_info, string $from_date, string $to_date, int $agent_id = 0) {

        return DataTableService::filterTable($cols, $from_date, $to_date, function ($filter_data) use ($order_info, $from_date, $to_date, $agent_id) {

            $count  = AgentDeposit::count();

            if(empty($filter_data)) {
    
                $transactionsQ = DB::table("agent_deposits")
                                    ->join('users', 'users.id', '=', 'agent_deposits.agent_id')
                                    ->select("agent_deposits.agent_id",
                                        "agent_deposits.deposit_value",
                                        "agent_deposits.slip_number",
                                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                                        "agent_deposits.created_at")
                                    ->orderBy($order_info['name'], $order_info['dir']);
    
            } else {
            
                $transactionsQ = DB::table("agent_deposits")
                                    ->join('users', 'users.id', '=', 'agent_deposits.agent_id')
                                    ->where($filter_data)
                                    ->select("agent_deposits.agent_id",
                                        "agent_deposits.deposit_value",
                                        "agent_deposit.slip_number",
                                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                                        "agent_deposits.created_at")
                                    ->orderBy($order_info['name'],$order_info['dir']);
     
            }
    
            if(empty($from_date) == false) {
                $transactionsQ = $transactionsQ->where("agent_deposits.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
            }
    
            if(empty($to_date) == false) {
                $transactionsQ = $transactionsQ->where("agent_deposits.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
            }
    
            if($agent_id > 0) {
                $transactionsQ = $transactionsQ->where('users.id',"=",$agent_id);
            }
    
            $transactions = $transactionsQ->get();
            
            return [
                "total_count" => $count,
                'total_filtered' => $transactions->count(),
                "total" => $transactions
            ];

        });

    }

    public function searchSaddleLiteDeposits(int $courier_id, string $search_term, array $order_info, string $from_date, string $to_date)  {

        $agents = $this->accountsService->getAgentsForCourier($courier_id)->pluck("id");

        return DataTableService::searchTable($from_date, $to_date, function() use ($search_term, $order_info, $from_date, $to_date, $agents) {

            $count  = AgentDeposit::whereIn("agent_id", $agents)->count();

            if(is_null($search_term) || empty($search_term)) {
    
                $transactionsQ = DB::table("agent_deposits")
                    ->join('users', 'users.id', '=', 'agent_deposits.agent_id')
                    ->whereIn("agent_deposits.agent_id", $agents)
                    ->select("agent_deposits.agent_id",
                        "agent_deposits.deposit_value",
                        "agent_deposits.slip_number",
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                        "agent_deposits.created_at")
                    ->orderBy($order_info['name'],$order_info['dir']);
    
            } else {
    
                $transactionsQ = DB::table("agent_deposits")
                    ->join('users', 'users.id', '=', 'agent_deposits.agent_id')
                    ->whereIn("agent_deposits.agent_id", $agents)
                    ->where("agent_deposits.slip_number", "like", $search_term . '%')
                    ->select("agent_deposits.agent_id",
                        "agent_deposits.deposit_value",
                        "agent_deposits.slip_number",
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                        "agent_deposits.created_at")
                    ->orderBy($order_info['name'],$order_info['dir']);
    
            }
    
            if(empty($from_date) == false) {
                $transactionsQ = $transactionsQ->where("agent_deposits.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
            }
    
            if(empty($to_date) == false) {
                $transactionsQ = $transactionsQ->where("agent_deposits.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
            }
    
            $transactions = $transactionsQ->get();
    
            return [
                "total_count" => $count,
                'total_filtered' => $transactions->count(),
                "total" => $transactions
            ];
        });

    }

    public function searchSaddleLiteDepositsForAdmin(string $search_term, array $order_info, string $from_date, string $to_date)  {

        return DataTableService::searchTable($from_date, $to_date, function() use ($search_term, $order_info, $from_date, $to_date) {

            $count  = AgentDeposit::count();

            if(is_null($search_term) || empty($search_term)) {
    
                $transactionsQ = DB::table("agent_deposits")
                    ->join('users', 'users.id', '=', 'agent_deposits.agent_id')
                    ->select("agent_deposits.agent_id",
                        "agent_deposits.deposit_value",
                        "agent_deposits.slip_number",
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                        "agent_deposits.created_at")
                    ->orderBy($order_info['name'],$order_info['dir']);
    
            } else {
    
                $transactionsQ = DB::table("agent_deposits")
                    ->join('users', 'users.id', '=', 'agent_deposits.agent_id')
                    ->where("agent_deposits.slip_number", "like", $search_term . '%')
                    ->select("agent_deposits.agent_id",
                        "agent_deposits.deposit_value",
                        "agent_deposits.slip_number",
                        DB::raw('CONCAT(users.first_name," ",users.last_name) as agent_name'),
                        "agent_deposits.created_at")
                    ->orderBy($order_info['name'],$order_info['dir']);
    
            }
    
            if(empty($from_date) == false) {
                $transactionsQ = $transactionsQ->where("agent_deposits.created_at",">=",date("Y-m-d 00:00:00", strtotime($from_date)));
            }
    
            if(empty($to_date) == false) {
                $transactionsQ = $transactionsQ->where("agent_deposits.created_at","<=",date("Y-m-d 23:59:59", strtotime($to_date)));
            }
    
            $transactions = $transactionsQ->get();
    
            return [
                "total_count" => $count,
                'total_filtered' => $transactions->count(),
                "total" => $transactions
            ];
        });

    }

}

?>