<?php

namespace Dappur\Services;

use Interop\Container\ContainerInterface;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;

class Service 
{
    protected $container;
    protected $logger;
    public function __construct($container)
    {
        ini_set('output_buffering', '4096');
        $this->container = $container;
        $this->logger =  new Logger('saddle-webapp');
        $this->logger->pushHandler(new RotatingFileHandler(__DIR__.'/../../logs/saddle-webapp.log', 4, Logger::DEBUG));
    }
}

?>