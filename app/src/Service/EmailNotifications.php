<?php

namespace Dappur\Services;

class EmailNotifications {


    public const PICKUP = '
    <!DOCTYPE html>
        <html>
        <head>
            <title></title>
            <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900|Open+Sans" rel="stylesheet"> 
            <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        </head>

        <body style="width: 50%;font-family: Open Sans, sans-serif;">
            <div style="background: #F79147; height: 100px;">
                <img src="/assets/default/img/saddle.png" 
                style="
                    width: 25%;
                    margin: auto;
                    display: block;
                    position: relative;
                    top: 50%;
                    transform: translateY(-50%);
                ">
            </div>
            <div style="height: 100px; background: black;">
                <p 
        style="
            font-family: "Montserrat", sans-serif;
            font-size: 2em;
            color: white;
            margin: 0;
            text-align: center;
            position: relative;
            top: 50%;
            transform: translateY(-50%);
        ">Your Shipping Status!</p>
            </div>
            <div style="display: flex; padding: 10px;">
                <div 
                style="
                    width: 60%;
                    margin: auto;
                    /*flex: 1 1 0;*/
                ">
                    <p  
                    style="
                        font-family: Montserrat, sans-serif;
                        font-size: 1.2em;
                        font-weight: 600;
                        margin-bottom: 10px;
                    ">
                        Hi <span style="color: #F79147;">:customer_name</span>
                    </p>
                    <p style="text-align: justify;">Congratulations! Your order has been picked up by an agent.</p>
                    <p style="text-align: justify;">
                        Shipping Status for:<br>
                        <b style="font-size: 1.3em;">ORDER ID: :transaction_order_no.</b>
                    </p>

                    <div style="padding: 10px 0px; float: left;">
                        <p style="margin: 0px 10px; padding: 8px 0px;">:created_at</p>
                        <p style="margin: 0px 10px; color: #F79147; padding: 8px 0px;"><b>:todays_date</b></p>
                        <p style="margin: 0px 10px; padding: 8px 0px 0px;">:updated_at</p>
                    </div>
                    <div style="padding: 10px 0px; width: 20px; float: left;">
                        <div style="background: black; width: 20px; height: 40px"></div>
                        <div style="background: #F79147; width: 20px; height: 40px"></div>
                        <div style="background: black; width: 20px; height: 40px"></div>

                    </div>
                    <div style="padding: 10px 0px; float: left;">
                        <p style="margin: 0px 10px; padding: 8px 0px;">Order has been assigned to an agent</p>
                        <p style="margin: 0px 10px; padding: 8px 0px; color: #F79147;"><b>Picked-up</b></p>
                        <p style="margin: 0px 10px; padding: 8px 0px;">Delivered</p>
                    </div>
            
                </div>
                <div  
                style="
                    width: 30%;
                    margin: 0px 20px;
                    flex: 1 1 0;
                ">
                    <div>
                        <div style="background: #F79147;">
                            <p style="padding: 10px; text-align: left;font-family: Montserrat, sans-serif;color: white;">Pick-up Address</p>
                        </div>
                        <div style="border-left: 2px solid black; padding-left: 10px">
                            <p>:merchant_contactname</p>
                            <p>:merchant_fulladdress</p>
                            <p>:merchant_phone</p>
                        </div>
                    </div>

                    <div>
                        <div style="background: #F79147;">
                            <p style="padding: 10px; text-align: left;font-family: Montserrat, sans-serif;color: white;">Delivery Address</p>
                        </div>
                        <div style="border-left: 2px solid black; padding-left: 10px">
                            <p>:customer_name</p>
                            <p>:customer_fulladdress</p>
                            <p>:customer_phone</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <div style="background: #fff;">
                <div  style="background: #f2f2f2;">
                    <p style="
                        font-family: Montserrat, sans-serif;
                        font-weight: 500;
                        padding: 20px;
                        font-size: 1.1em;
                        text-align: center;
                    ">Have questions? Visit our <span style="color: #F79147;">support page</span> for more information.</p>
                </div>
                <div  
                style="
                    margin:30px auto;
                      width:220px;
                      text-align:center;
                ">
                    <div style="float: left;margin: 0px 15px; cursor: pointer;">
                        <img src="/assets/default/img/facebook.png" width="40" height="40">
                    </div>
                    <div style="float: left;margin: 0px 15px; cursor: pointer;">
                        <img src="/assets/default/img/twitter.png" width="40" height="40">
                    </div>
                    <div style="float: left;margin: 0px 15px; cursor: pointer;">
                        <img src="/assets/default/img/linkedin.png" width="40" height="40">
                    </div>
                </div>
            </div>
            
        </body>
        </html>';

    public const DELIVERY = '
    <!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900|Open+Sans" rel="stylesheet"> 
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>

<body style="width: 50%;font-family: "Open Sans", sans-serif;">
    <div style="background: #F79147; height: 100px;">
        <img src="saddle.png" 
        style="
            width: 25%;
            margin: auto;
            display: block;
            position: relative;
            top: 50%;
            transform: translateY(-50%);
        ">
    </div>
    <div style="height: 100px; background: black;">
        <p 
        style="
            font-family: Montserrat, sans-serif;
            font-size: 2em;
            color: white;
            margin: 0;
            text-align: center;
            position: relative;
            top: 50%;
            transform: translateY(-50%);
        ">Your Shipping Status!</p>
    </div>
    <div style="display: flex; padding: 10px;">
        <div 
        style="
            width: 60%;
            margin: auto;
            /*flex: 1 1 0;*/
        ">
            <p  
            style="
                font-family: Montserrat, sans-serif;
                font-size: 1.2em;
                font-weight: 600;
                margin-bottom: 10px;
            ">
                Hi <span style="color: #F79147;">:customer_name</span>
            </p>
            <p style="text-align: justify;">Congratulations! Your order has been delivered by an agent.</p>
            <p style="text-align: justify;">
                Shipping Status for:<br>
                <b style="font-size: 1.3em;">ORDER ID: :transaction_order_no</b>
            </p>

            <!-- <div style="padding: 10px 0px;">
                <span style="margin: 0px 10px">Nov 05</span>
                <span style="background: #000; margin: 0px 10px">asd</span>
                <span>Order has been assigned to an agent</span>
            </div>
            <div style="padding: 10px 0px;">
                <span style="margin: 0px 10px; color: #F79147;">Nov 06</span>
                <span style="background: #000; margin: 0px 10px">asd</span>
                <span>Picked-up</span>
            </div>
            <div style="padding: 10px 0px;">
                <span style="margin: 0px 10px">Nov 10</span>
                <span style="background: #F79147; color: #F79147 margin: 0px 10px">asd</span>
                <span><b>Delivered</b></span>
            </div> -->

            <div style="padding: 10px 0px; float: left;">
                <p style="margin: 0px 10px; padding: 8px 0px;">:created_at</p>
                <p style="margin: 0px 10px; padding: 8px 0px;">:updated_at</p>
                <p style="margin: 0px 10px; color: #F79147; padding: 8px 0px;"><b>:todays_date</b></p>
            </div>
            <div style="padding: 10px 0px; width: 20px; float: left;">
                <div style="background: black;width: 20px; height: 40px"></div>
                <div style="background: black;width: 20px; height: 40px"></div>
                <div style="background: #F79147 ;width: 20px; height: 40px"></div>

            </div>
            <div style="padding: 10px 0px; float: left;">
                <p style="margin: 0px 10px; padding: 8px 0px;">Order has been assigned to an agent</p>
                <p style="margin: 0px 10px; padding: 8px 0px;">Picked-up</p>
                <p style="margin: 0px 10px; color: #F79147; padding: 8px 0px;"><b>Delivered</b></p>
            </div>
    
        </div>
        <div  
        style="
            width: 30%;
            margin: 0px 20px;
            flex: 1 1 0;
        ">
            <div>
                <div style="background: #F79147;">
                    <p style="padding: 10px; text-align: left;font-family: Montserrat, sans-serif;color: white;">Pick-up Address</p>
                </div>
                <div style="border-left: 2px solid black; padding-left: 10px">
                    <p>:merchant_contactname</p>
                    <p>:merchant_fulladdress</p>
                    <p>:merchant_phone</p>
                </div>
            </div>

            <div>
                <div style="background: #F79147;">
                    <p style="padding: 10px; text-align: left;font-family: Montserrat, sans-serif;color: white;">Delivery Address</p>
                </div>
                <div style="border-left: 2px solid black; padding-left: 10px">
                    <p>:customer_name</p>
                    <p>:customer_fulladdress</p>
                    <p>:customer_phone</p>
                </div>
            </div>
        </div>
        
    </div>
    <div style="background: #fff;">
        <div  style="background: #f2f2f2;">
            <p style="
                font-family: Montserrat, sans-serif;
                font-weight: 500;
                padding: 20px;
                font-size: 1.1em;
                text-align: center;
            ">Have questions? Visit our <span style="color: #F79147;">support page</span> for more information.</p>
        </div>
        <div  
        style="
            margin:30px auto;
              width:220px;
              text-align:center;
        ">
            <div style="float: left;margin: 0px 15px; cursor: pointer;">
                <img src="facebook.png" width="40" height="40">
            </div>
            <div style="float: left;margin: 0px 15px; cursor: pointer;">
                <img src="twitter.png" width="40" height="40">
            </div>
            <div style="float: left;margin: 0px 15px; cursor: pointer;">
                <img src="linkedin.png" width="40" height="40">
            </div>
        </div>
    </div>
    
</body>
</html>';


    public const RETURN = '
    <!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900|Open+Sans" rel="stylesheet"> 
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>

<body style="width: 50%;font-family: "Open Sans", sans-serif;">
    <div style="background: #F79147; height: 100px;">
        <img src="saddle.png" 
        style="
            width: 25%;
            margin: auto;
            display: block;
            position: relative;
            top: 50%;
            transform: translateY(-50%);
        ">
    </div>
    <div style="height: 100px; background: black;">
        <p 
        style="
            font-family: Montserrat, sans-serif;
            font-size: 2em;
            color: white;
            margin: 0;
            text-align: center;
            position: relative;
            top: 50%;
            transform: translateY(-50%);
        ">Your Shipping Status!</p>
    </div>
    <div style="display: flex; padding: 10px;">
        <div 
        style="
            width: 60%;
            margin: auto;
            /*flex: 1 1 0;*/
        ">
            <p  
            style="
                font-family: Montserrat, sans-serif;
                font-size: 1.2em;
                font-weight: 600;
                margin-bottom: 10px;
            ">
                Hi <span style="color: #F79147;">:customer_name</span>
            </p>
            <p style="text-align: justify;">Your order has been returned to merchant.</p>
            <p style="text-align: justify;">
                Shipping Status for:<br>
                <b style="font-size: 1.3em;">ORDER ID: :transaction_order_no</b>
            </p>

            <!-- <div style="padding: 10px 0px;">
                <span style="margin: 0px 10px">Nov 05</span>
                <span style="background: #000; margin: 0px 10px">asd</span>
                <span>Order has been assigned to an agent</span>
            </div>
            <div style="padding: 10px 0px;">
                <span style="margin: 0px 10px; color: #F79147;">Nov 06</span>
                <span style="background: #000; margin: 0px 10px">asd</span>
                <span>Picked-up</span>
            </div>
            <div style="padding: 10px 0px;">
                <span style="margin: 0px 10px">Nov 10</span>
                <span style="background: #F79147; color: #F79147 margin: 0px 10px">asd</span>
                <span><b>Delivered</b></span>
            </div> -->

            <div style="padding: 10px 0px; float: left;">
                <p style="margin: 0px 10px; padding: 8px 0px;">:created_at</p>
                <p style="margin: 0px 10px; padding: 8px 0px;">:updated_at</p>
                <p style="margin: 0px 10px; color: #F79147; padding: 8px 0px;"><b>:todays_date</b></p>
            </div>
            <div style="padding: 10px 0px; width: 20px; float: left;">
                <div style="background: black;width: 20px; height: 40px"></div>
                <div style="background: black;width: 20px; height: 40px"></div>
                <div style="background: #F79147 ;width: 20px; height: 40px"></div>

            </div>
            <div style="padding: 10px 0px; float: left;">
                <p style="margin: 0px 10px; padding: 8px 0px;">Order has been assigned to an agent</p>
                <p style="margin: 0px 10px; padding: 8px 0px;">Picked-up</p>
                <p style="margin: 0px 10px; color: #F79147; padding: 8px 0px;"><b>Returned to merchant</b></p>
            </div>
    
        </div>
        <div  
        style="
            width: 30%;
            margin: 0px 20px;
            flex: 1 1 0;
        ">
            <div>
                <div style="background: #F79147;">
                    <p style="padding: 10px; text-align: left;font-family: Montserrat, sans-serif;color: white;">Pick-up Address</p>
                </div>
                <div style="border-left: 2px solid black; padding-left: 10px">
                    <p>:merchant_contactname</p>
                    <p>:merchant_fulladdress</p>
                    <p>merchant_phone</p>
                </div>
            </div>

            <div>
                <div style="background: #F79147;">
                    <p style="padding: 10px; text-align: left;font-family: Montserrat, sans-serif;color: white;">Delivery Address</p>
                </div>
                <div style="border-left: 2px solid black; padding-left: 10px">
                    <p>:customer_name</p>
                    <p>:customer_fulladdress</p>
                    <p>:customer_phone</p>
                </div>
            </div>
        </div>
        
    </div>
    <div style="background: #fff;">
        <div  style="background: #f2f2f2;">
            <p style="
                font-family: Montserrat, sans-serif;
                font-weight: 500;
                padding: 20px;
                font-size: 1.1em;
                text-align: center;
            ">Have questions? Visit our <span style="color: #F79147;">support page</span> for more information.</p>
        </div>
        <div  
        style="
            margin:30px auto;
              width:220px;
              text-align:center;
        ">
            <div style="float: left;margin: 0px 15px; cursor: pointer;">
                <img src="facebook.png" width="40" height="40">
            </div>
            <div style="float: left;margin: 0px 15px; cursor: pointer;">
                <img src="twitter.png" width="40" height="40">
            </div>
            <div style="float: left;margin: 0px 15px; cursor: pointer;">
                <img src="linkedin.png" width="40" height="40">
            </div>
        </div>
    </div>
    
</body>
</html>';



public const FAILED = '
<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900|Open+Sans" rel="stylesheet"> 
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>

<body style="width: 50%;font-family: "Open Sans", sans-serif;">
<div style="background: #F79147; height: 100px;">
    <img src="saddle.png" 
    style="
        width: 25%;
        margin: auto;
        display: block;
        position: relative;
        top: 50%;
        transform: translateY(-50%);
    ">
</div>
<div style="height: 100px; background: black;">
    <p 
    style="
        font-family: Montserrat, sans-serif;
        font-size: 2em;
        color: white;
        margin: 0;
        text-align: center;
        position: relative;
        top: 50%;
        transform: translateY(-50%);
    ">Your Shipping Status!</p>
</div>
<div style="display: flex; padding: 10px;">
    <div 
    style="
        width: 60%;
        margin: auto;
        /*flex: 1 1 0;*/
    ">
        <p  
        style="
            font-family: Montserrat, sans-serif;
            font-size: 1.2em;
            font-weight: 600;
            margin-bottom: 10px;
        ">
            Hi <span style="color: #F79147;">:customer_name</span>
        </p>
        <p style="text-align: justify;">Your order has failed.</p>
        <p style="text-align: justify;">
            Shipping Status for:<br>
            <b style="font-size: 1.3em;">ORDER ID: :transaction_order_no</b>
        </p>

        <!-- <div style="padding: 10px 0px;">
            <span style="margin: 0px 10px">Nov 05</span>
            <span style="background: #000; margin: 0px 10px">asd</span>
            <span>Order has been assigned to an agent</span>
        </div>
        <div style="padding: 10px 0px;">
            <span style="margin: 0px 10px; color: #F79147;">Nov 06</span>
            <span style="background: #000; margin: 0px 10px">asd</span>
            <span>Picked-up</span>
        </div>
        <div style="padding: 10px 0px;">
            <span style="margin: 0px 10px">Nov 10</span>
            <span style="background: #F79147; color: #F79147 margin: 0px 10px">asd</span>
            <span><b>Delivered</b></span>
        </div> -->

        <div style="padding: 10px 0px; float: left;">
            <p style="margin: 0px 10px; padding: 8px 0px;">:created_at</p>
            <p style="margin: 0px 10px; padding: 8px 0px;">:updated_at</p>
            <p style="margin: 0px 10px; color: #F79147; padding: 8px 0px;"><b>:todays_date</b></p>
        </div>
        <div style="padding: 10px 0px; width: 20px; float: left;">
            <div style="background: black;width: 20px; height: 40px"></div>
            <div style="background: black;width: 20px; height: 40px"></div>
            <div style="background: #F79147 ;width: 20px; height: 40px"></div>

        </div>
        <div style="padding: 10px 0px; float: left;">
            <p style="margin: 0px 10px; padding: 8px 0px;">Order has been assigned to an agent</p>
            <p style="margin: 0px 10px; padding: 8px 0px;">Picked-up</p>
            <p style="margin: 0px 10px; color: #F79147; padding: 8px 0px;"><b>Failed Delivery</b></p>
        </div>

    </div>
    <div  
    style="
        width: 30%;
        margin: 0px 20px;
        flex: 1 1 0;
    ">
        <div>
            <div style="background: #F79147;">
                <p style="padding: 10px; text-align: left;font-family: Montserrat, sans-serif;color: white;">Pick-up Address</p>
            </div>
            <div style="border-left: 2px solid black; padding-left: 10px">
                <p>:merchant_contactname</p>
                <p>"merchant_fulladdress</p>
                <p>:merchant_phone</p>
            </div>
        </div>

        <div>
            <div style="background: #F79147;">
                <p style="padding: 10px; text-align: left;font-family: Montserrat, sans-serif;color: white;">Delivery Address</p>
            </div>
            <div style="border-left: 2px solid black; padding-left: 10px">
                <p>customer_name</p>
                <p>customer_fulladdress</p>
                <p>:customer_phone</p>
            </div>
        </div>
    </div>
    
</div>
<div style="background: #fff;">
    <div  style="background: #f2f2f2;">
        <p style="
            font-family: Montserrat, sans-serif;
            font-weight: 500;
            padding: 20px;
            font-size: 1.1em;
            text-align: center;
        ">Have questions? Visit our <span style="color: #F79147;">support page</span> for more information.</p>
    </div>
    <div  
    style="
        margin:30px auto;
          width:220px;
          text-align:center;
    ">
        <div style="float: left;margin: 0px 15px; cursor: pointer;">
            <img src="facebook.png" width="40" height="40">
        </div>
        <div style="float: left;margin: 0px 15px; cursor: pointer;">
            <img src="twitter.png" width="40" height="40">
        </div>
        <div style="float: left;margin: 0px 15px; cursor: pointer;">
            <img src="linkedin.png" width="40" height="40">
        </div>
    </div>
</div>

</body>
</html>';

}

?>