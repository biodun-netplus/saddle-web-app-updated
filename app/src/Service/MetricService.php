<?php

namespace Dappur\Services;

use Illuminate\Database\Capsule\Manager as DB;

use Dappur\Services\Service;
use Dappur\Model\SaddleLiteTransaction;

/**
 * Metric Service
 */
class MetricService extends Service {

    public function __construct($container) {

        parent::__construct($container);

    }

    public function getTodaysSaddleLiteTransactionSummaryForCourier(int $courier_id) {

        $query = "SELECT 
            (SELECT SUM(amount) FROM saddle_lite_transaction WHERE status = 'SUCCESS' AND (created_at BETWEEN :from_1 AND :to_1) AND courier_id = :courier_id_1) AS total_amount,
            (SELECT COUNT(id) FROM saddle_lite_transaction WHERE transaction_type = 'CARD' AND  status = 'SUCCESS' AND (created_at BETWEEN :from_2 AND :to_2) AND courier_id = :courier_id_2) AS card_transactions,
            (SELECT COUNT(id) FROM saddle_lite_transaction WHERE transaction_type = 'CASH' AND  status = 'SUCCESS' AND (created_at BETWEEN :from_3 AND :to_3) AND courier_id = :courier_id_3) AS cash_transactions,
            (SELECT COUNT(id) FROM saddle_lite_transaction WHERE  status = 'SUCCESS' AND (created_at BETWEEN :from_4 AND :to_4) AND courier_id = :courier_id_4) AS successful_transactions,
            (SELECT COUNT(id) FROM saddle_lite_transaction WHERE  status = 'FAILED' AND (created_at BETWEEN :from_5 AND :to_5) AND courier_id = :courier_id_5) AS failed_transactions,
            count(id) as total_transactions 
        FROM saddle_lite_transaction 
        WHERE (created_at BETWEEN :from_6 AND :to_6) AND  courier_id = :courier_id_6";

        $from = date("Y-m-d 00:00:00");
        $to = date("Y-m-d 23:59:59");

        $data = DB::select($query,[ 
            "from_1" =>$from, 
            "to_1" => $to,
            "courier_id_1" => $courier_id,
            "from_2" => $from, 
            "to_2" => $to,
            "courier_id_2" => $courier_id,
            "from_3" => $from, 
            "to_3" => $to,
            "courier_id_3" => $courier_id,
            "from_4" => $from, 
            "to_4" => $to,
            "courier_id_4" => $courier_id,
            "from_5" => $from, 
            "to_5" => $to,
            "courier_id_5" => $courier_id,
            "from_6" => $from, 
            "to_6" => $to,
            "courier_id_6" => $courier_id
            ]
        );

        $this->logger->debug("get saddle lite transactions summary",[ $data[0]]);

        return [
            "total_amount" => $data[0]->total_amount,
            "success_rate" => $data[0]->total_transactions < 1 ? 0 : (($data[0]->successful_transactions / $data[0]->total_transactions) * 100 ),
            "failure_rate" => $data[0]->total_transactions < 1 ? 0 : (($data[0]->failed_transactions / $data[0]->total_transactions) * 100 ),
            "card_tx" => $data[0]->successful_transactions < 1 ? 0 : (($data[0]->card_transactions / $data[0]->successful_transactions) * 100 ),
            "cash_tx" => $data[0]->successful_transactions < 1 ? 0 : (($data[0]->cash_transactions / $data[0]->successful_transactions) * 100 )
        ];

    }

    public function getTodaysSaddleLiteSuccessfullPaymentsPerHourForCourier(int $courier_id) {

        $query = 'SELECT 
                    DATE_FORMAT(created_at, "%Y-%m-%dT%H:00:00") as "time",
                    SUM(amount) as amount
                  FROM saddle_lite_transaction
                  WHERE (created_at BETWEEN ? AND ?) AND  status = "SUCCESS" AND courier_id = ?
                  GROUP BY hour(created_at)
                  ORDER BY "time" ASC';

        $data = DB::select($query, [date("Y-m-d 00:00:00"), date("Y-m-d 23:59:59"), $courier_id]);


        $this->logger->debug("get successful payments per hour for courier",$data);

        return $data;

    }

    public function getTodaysSaddleLiteAgentTransactionBreakDown(int $courier_id) {

        $query = "SELECT
                saddle_lite.agent_id,
                CONCAT(users.first_name,' ',users.last_name) AS agent_name,
                saddle_lite.transaction_type AS 'payment_type',
                SUM(saddle_lite.amount) AS total_amount,
                MAX(saddle_lite.created_at) AS last_seen,
                COUNT(saddle_lite.agent_id) AS number_of_transactions,
                (SELECT COUNT(id) FROM saddle_lite_transaction WHERE (created_at BETWEEN :from_1 AND :to_1) AND courier_id = :courier_id_1 AND status = 'SUCCESS') AS successful_transactions,
                (SELECT SUM(deposit_value) FROM agent_deposits a WHERE a.agent_id = saddle_lite.agent_id AND (a.created_at BETWEEN :from_2 AND :to_2) ) AS amount_deposited
            FROM saddle_lite_transaction saddle_lite JOIN users users on users.id = saddle_lite.agent_id
            WHERE (saddle_lite.created_at BETWEEN :from_3 AND :to_3) AND saddle_lite.courier_id = :courier_id_2 AND saddle_lite.status = 'SUCCESS'
            GROUP BY saddle_lite.agent_id, saddle_lite.transaction_type
            ORDER BY total_amount";

        $from = date("Y-m-d 00:00:00");
        $to = date("Y-m-d 23:59:59");

        $data = DB::select($query,[
            "from_1" =>$from, 
            "to_1" => $to,
            "courier_id_1" => $courier_id,
            "from_2" => $from, 
            "to_2" => $to,
            "from_3" => $from, 
            "to_3" => $to,
            "courier_id_2" => $courier_id
            ]
        );

        $this->logger->debug("get todays  agent tx breakdown for courier", $data);

        $response = [];

        for($i = 0; $i < count($data); $i++) {

            $key = (string) $data[$i]->agent_id;

            if(is_null($response[$key]) == false) {

                $arr = $response[$key];

                if($data[$i]->payment_type == 'CASH') {

                    $arr["cash_transactions"] = $data[$i]->total_amount;
                    $arr["number_of_cash_transactions"] = $data[$i]->number_of_transactions;

                } else if ($data[$i]->payment_type == 'CARD') {

                    $arr["card_transactions"] = $data[$i]->total_amount;
                    $arr["number_of_card_transactions"] = $data[$i]->number_of_transactions;

                }

                $arr["number_of_transactions"] = $arr["number_of_transactions"] + $data[$i]->number_of_transactions;
                $arr["total_amount"] = $arr["total_amount"] + $data[$i]->total_amount;
                $arr["holding"] = $arr["cash_transactions"] - $arr["amount_deposited"];

                $response[$key] = $arr;

            } else {

                $response[$key] = [
                    "agent_name" => $data[$i]->agent_name,
                    "cash_transactions" => $data[$i]->payment_type == 'CASH' ? $data[$i]->total_amount : 0,
                    "card_transactions" => $data[$i]->payment_type == 'CARD' ? $data[$i]->total_amount : 0,
                    "total_amount" => $data[$i]->total_amount,
                    "number_of_card_transactions" => $data[$i]->payment_type == 'CARD' ? $data[$i]->number_of_transactions : 0,
                    "number_of_cash_transactions" => $data[$i]->payment_type == 'CASH' ? $data[$i]->number_of_transactions : 0,
                    "number_of_transactions" => $data[$i]->number_of_transactions,
                    "amount_deposited" => $data[$i]->amount_deposited == null ? 0 : $data[$i]->amount_deposited,
                    "last_seen" =>  date("d-m-Y h:m:s",strtotime($data[$i]->last_seen))
                ]; 
                $response[$key]["holding"] = $response[$key]["cash_transactions"] - $response[$key]["amount_deposited"];

            }

        }

        $this->logger->debug("getTodaysSaddleLiteAgentTransactionBreakDown",$response);

        return $response;

    }

    public function getTodaysSaddleLiteAgentActivityBreakDown(int $courier_id) {

        $query = "SELECT 
                    CONCAT(users.first_name,' ',users.last_name) AS agent_name, 
                    SUM(saddle_lite.amount) AS total_amount,
                    MAX(saddle_lite.created_at) AS last_seen,
                    COUNT(agent_id) AS number_of_transactions,
                    (SELECT COUNT(id) FROM saddle_lite_transaction WHERE (created_at BETWEEN :from_1 AND :to_1) AND courier_id = :courier_id_1 AND status = 'SUCCESS') AS successful_transactions
                  FROM saddle_lite_transaction saddle_lite JOIN users users on users.id = saddle_lite.agent_id
                  WHERE (saddle_lite.created_at BETWEEN :from_2 AND :to_2) AND saddle_lite.courier_id = :courier_id_2 AND saddle_lite.status = 'SUCCESS'
                  GROUP BY saddle_lite.agent_id
                  ORDER BY total_amount";

        $from = date("Y-m-d 00:00:00");
        $to = date("Y-m-d 23:59:59");

        $data = DB::select($query,[ 
            "from_1" =>$from, 
            "to_1" => $to,
            "courier_id_1" => $courier_id,
            "from_2" => $from, 
            "to_2" => $to,
            "courier_id_2" => $courier_id
            ]
        );

        $this->logger->debug("get todays agent breakdown for courier", $data);

        $response = [];

        for($i = 0; $i < count($data); $i++) {
            array_push($response,[
                "agent_name" => $data[$i]->agent_name,
                "total_amount" =>  $data[$i]->total_amount,
                "processed_percentage" => $data[$i]->successful_transactions < 1 ? 0 : round(($data[$i]->number_of_transactions / $data[$i]->successful_transactions) * 100, 2, PHP_ROUND_HALF_UP),
                "last_seen" =>  date("d-m-Y h:m:s",strtotime($data[$i]->last_seen))
            ]);
        }

        return $response;

    }

}

?>