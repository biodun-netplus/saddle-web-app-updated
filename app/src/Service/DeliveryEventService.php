<?php

namespace Dappur\Services;

use Dappur\Model\Transactions;
use Dappur\Model\CourierLocation;
use Dappur\Model\Courier;
use Dappur\Model\Merchants;
use Dappur\Model\UserType;
use Dappur\Model\Items;
use Dappur\Model\CourierMerchants;
use Dappur\Model\Users;
use Dappur\Services\Service;
use Dappur\Model\RateCard;
use Dappur\Model\Status;
use Dappur\Model\DeliveryAssignment;

use Illuminate\Database\Capsule\Manager as DB;



/**
 * Delivery Event Service
 */
class DeliveryEventService extends Service {

    protected $status;
    protected $item;
    protected $transaction;

    public function __construct($container, EmailNotificationService $emailNotificationService) {
        parent::__construct($container);
        $this->status = new Status;
        $this->item = new Items;
        $this->deliveryService = new DeliveryService($container);
        $this->transaction = new Transactions;
        $this->emailNotificationService = $emailNotificationService;

    }

    public function handlePickupEvent(int $agent_id, string $transaction_id, array $deliverys) {

        $transactionDetail = $this->transaction->getTransactionDetails($transaction_id);
        $transactionPrimaryID = $transactionDetail->id;
     
        $total_items = $this->item->getTransactionItemCount($transaction_id);
        $number_sent = count($deliverys);
       
        if($total_items == $number_sent)
        {
            $transactionUpdate = $this->transaction->getTransactionDetails($transaction_id);
            $transactionUpdate->transaction_status = Status::PICKED_UP_FROM_MERCHANT;
            $transactionUpdate->push();

            for($i=0;$i < $number_sent;$i++)
            {

                $item_id = trim($deliverys[$i]['item_id']);
                $this->deliveryService->assignAgentForDropOff($agent_id, $transactionPrimaryID, $item_id);

                $itemupdate = $this->item->where('item_id', '=', $item_id)->first();
                $itemupdate->delivery_status = Status::PICKED_UP_FROM_MERCHANT;

                $itemupdate->push();
                $data_array['status'] = 1;
                $data_array['msg'] = 'Status Updated';

            }

        }
        else
        {
            $transactionUpdate = $this->transaction->getTransactionDetails($transaction_id);
            $transactionUpdate->transaction_status = Status::PARTIAL_PICK_UP_FROM_MERCHANT;
            $transactionUpdate->save();

            for($i=0;$i < $number_sent;$i++)
            {

                $item_id = trim($deliverys[$i]['item_id']);
               
                $this->deliveryService->assignAgentForDropOff($agent_id, $transactionPrimaryID, $item_id, true);
               
                $itemupdate = $this->item->where('item_id', '=', $item_id)->first();
                $itemupdate->delivery_status = 1;
                $itemupdate->push();

                $data_array['status'] = 1;
                $data_array['msg'] = 'Status Updated';

            }
        }

        $this->emailNotificationService->notifyCustomerOfPickup($transaction_id);

        return $data_array;

    }

    public function handleDropoffEvent(string $transaction_id, string $payment_type, array $items = null) {
        
        $transactionUpdate = $this->transaction->getTransactionDetails($transaction_id);

        if($transactionUpdate == NULL) {
            return ["status" => 0, "msg" => 'Delivery order not found'];
        }

        $transactionUpdate->transaction_payment_type = $payment_type;
        $transactionUpdate->push();

        if(is_null($items)) {

            DB::table('items')
                ->where("transaction_id",'=',$transaction_id)
                ->update(['delivery_status' => Status::DELIVERED_TO_CUSTOMER]);
                $transactionUpdate->transaction_status = Status::DELIVERED_TO_CUSTOMER;
                $transactionUpdate->push();

        } else {

            $item_ids = [];
            for ($i=0; $i < count($items); $i++) { 
                $this->logger->debug("droping off ", [$items[$i]]);
                array_push($item_ids,$items[$i]['item_id']);
            }

            DB::table("items")
                ->whereIn('item_id',$item_ids)
                ->update(['delivery_status' => Status::DELIVERED_TO_CUSTOMER]);
                $this->updateTransactionStatus($transaction_id);

        }
 
        $this->emailNotificationService->notifyCustomerOfDelivery($transaction_id);

        return ["status" => 1, "msg" => "Status Updated"];
    }

    public function updateTransactionStatus(string $transaction_id)
    {

        $total = DB::table('items')
        ->where('transaction_id', '=', $transaction_id)
        ->count();

        $new = DB::table('items')
        ->whereIn('delivery_status', [0])
        ->where('transaction_id', '=', $transaction_id)
        ->count();

        $delivered = DB::table('items')
        ->whereIn('delivery_status', [3,301])
        ->where('transaction_id', '=', $transaction_id)
        ->count();
        
        $terminal = DB::table('items')
        ->whereIn('delivery_status', [8,5,6,801])
        ->where('transaction_id', '=', $transaction_id)
        ->count();
        
        $processing = DB::table('items')
            ->whereIn('delivery_status', [1,2,4,7,10,102,11])
            ->where('transaction_id', '=', $transaction_id)
            ->count();

         $this->logger->debug("Delivered:".$delivered." Terminal: ".$terminal." and Processing: ".$processing,[]);

         $transaction = Transactions::where('transaction_id','=',$transaction_id)->first();

         if($delivered == 0 && $terminal > 0 && $processing == 0 && $new == 0) {

            $transaction->transaction_status = 8;
            $transaction->save();

        } else if($delivered == 0 && $terminal >= 0 && $processing > 0 && $new >= 0) {

            $transaction->transaction_status = 10;
            $transaction->save();

        } elseif ($terminal > 0 && $delivered > 0 && $processing >= 0 && $new == 0) {

            $transaction->transaction_status = 30;

            if($transaction->pod_transaction = 1)
                $transaction->transaction_payment_type = "Cash";
            else
                $transaction->transaction_payment_type = "Card";

            $transaction->save();

        } elseif($delivered > 0 && $terminal == 0 && $processing == 0 && $new == 0) {

            $transaction->transaction_status = 3;
            $transaction->save();
        
        } else if ($total > 0) {
            $transaction->transaction_status = 0;
            $transaction->save();
        }
             
     }

}