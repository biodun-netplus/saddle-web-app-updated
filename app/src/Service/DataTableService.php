<?php

namespace Dappur\Services;

use Illuminate\Database\Capsule\Manager as DB;

/**
 * DataTable Service
 */
class DataTableService extends Service {

    public function __construct($container) {

        parent::__construct($container);

    }

    /**
     * 
     */
    public static function isSearchRequest(array $params) {

        return is_null($params['search']['value']) == false && empty($params['search']['value']) == false;

    }

    /**
     * filter table
     */
    public static function filterTable(array $cols, string $from_date, string $to_date, callable $func) {

        $queryParams = [];

        for ($i=0; $i < count($cols); $i++) {
            if(empty($cols[$i]['search']['value']) == false && is_null($cols[$i]['search']['value']) == false) {
                array_push( $queryParams, [
                     $cols[$i]['data'], 
                     '=', 
                     $cols[$i]['search']['value'] ] );
            }
        }
    
        if( (empty($from_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$from_date) === 0) || (empty($to_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$to_date) === 0) ) {
            
            return [
                "total_count" => 0,
                'total_filtered' => 0,
                "total" => []
            ];

        }

        return $func($queryParams);
    
    }

    /**
     * search table
     */
    public static function searchTable(string $from_date, string $to_date, callable $func) {

        if( (empty($from_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$from_date) == 0) || (empty($to_date) == false && preg_match('/\d{2}-\d{2}-\d{4}/',$to_date) == 0) ) {

            return [
                "error" => "invalid dates",
                "total_count" => 0,
                'total_filtered' => 0,
                "total" => []
            ];
        }

        return $func();

    }

}