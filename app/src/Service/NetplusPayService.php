<?php

namespace Dappur\Services;

use Dappur\Model\Transactions;
use Dappur\Model\CourierLocation;
use Dappur\Model\Courier;
use Dappur\Model\Merchants;
use Dappur\Model\UserType;
use Dappur\Model\Items;
use Dappur\Model\CourierMerchants;
use Dappur\Model\Users;
use Dappur\Model\Card;
use Dappur\Model\Config;
use Dappur\Services\Service;
use Dappur\Services\DeliveryService;

class NetplusPayService extends Service
{

    private $api_url = "https://netpluspay.com/api/v2/";
    private $transactions;
    private $usertype;
    private $merchants;
    private $deliveryService;
    private $courier;
    private $payment_merchant_id;
    private $card_on_file;

    public function __construct($container) {
        parent::__construct($container);
        $this->transactions = new Transactions;
        $this->merchants =  new Merchants;
        $this->usertype = new UserType;
        $this->deliveryService = new DeliveryService($container);
        $this->courier = new Courier;
        $this->card_on_file = new Card;
        $this->payment_merchant_id = NetplusPayService::getPaymentMerchantId();
    }

    private function getMerchantDetails($user_id) {
 
        $typeID = $this->usertype->getUserTypeID($user_id);
        $merchants = new Merchants;
        $merchant = $merchants->find($typeID);
        return $merchant;

    }

    public static function getPaymentMerchantId() {
        $config = Config::where("name",'liveid')
                                ->get();
        return $config[0]->value;
    }

    private function determineItemCost($items) {

        $amount = 0;

        for($i = 0; $i < count($items); $i++) {
            $amount = $amount + ($items[$i]["item_cost"] * $items[$i]["item_qty"]);
        }

        return $amount;

    }

    public function initiateDODPayment($user_id, $transaction, $payment_details) {

        $url = $this->api_url . "preauth";

        $data = array();

        $this->logger->debug("transaction id is ",[$transaction["transaction_id"]]);

        //calculate amount
        $this->logger->debug("calculating delivery amount");
        $delivery_cost = $this->deliveryService->determineDeliveryCost($transaction['courier_id'], $transaction["pickup"], $transaction["delivery"], $transaction["orders"]['items']);
        $this->logger->debug("delivery cost ",["delivery_cost" => $delivery_cost]);

        $this->logger->debug("calculating item cost");
        $item_cost = $this->determineItemCost($transaction["orders"]['items']);
        $this->logger->debug("item cost",["item_cost" => $item_cost]);

        $amount = $delivery_cost + $item_cost;

        if(is_string($delivery_cost)) {
            return [ "status" => 0, "message" => "unable to determine delivery cost" ];
        }

        $this->logger->debug("recieved amount",["amount" => $amount]);

        $data['narration'] = $merchant['merchant_name'] . " Payment From Saddle";
        $data['amount'] = $amount;
        $data['currency'] = "NGN";
        //$data['cardType'] = trim($payment_details['card_type']);
        $data['card_number'] = trim($payment_details['card_number']);
        $data['security_code'] = $payment_details['card_cvv'];
        $data['card_expiry'] = $payment_details['card_expiry'];
        $data['order_id'] = $transaction["transaction_id"];
        $data['merchant_id'] = $payment_details['merchant_id'];
        $data['email'] = $transaction['delivery']['customer_email'];
        $data['full_name'] = $transaction['delivery']['customer_name'];
        $req = json_encode($data);
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $req);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($req)));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: Application/json;charset=UTF-8"));
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
        if($result == false) {
            $this->logger->debug("curl error",[curl_error($curl)]);
            $this->logger->debug("curl error number",[curl_errno($curl)]);
        }

        $this->logger->debug("the payment result is", [$result]);

        $j_result = json_decode($result, true);

        if(isset($j_result['code']) == false || $j_result['code'] != "00") {
            $status = 0;
            $transaction_amount = 0;
            $code = '003';
            $message = "Unsuccessfull";
        } else {
            $status = 1;
            $transaction_amount = $amount;
            $code = '000';
            $message = "Successfull";
        }

        //TODO store in netplus transaction log
        return [ "status" => $status, "transaction_amount" => $transaction_amount, "code" => $code, "message" => $message ];

    }

    public function executeCardOnFileTransaction($transaction_id, $agent_id) {

        $trans_details = $this->transactions->getTransactionDetails($transaction_id);

        if(is_null($trans_details) || empty($trans_details)) {
            return ["status" => 0, "code" => '001', "msg" => 'No Transaction details'];
        }

        $card = $this->card_on_file->getAgentsActiveCardsOnFile($trans_details->courier_id, $agent_id);

        if(is_null($card) || empty($card)) {
            $card = $this->card_on_file->getCouriersActiveCardsOnFile($trans_details->courier_id);
        }


        if(is_null($card) || empty($card)) {
            return ["status" => 0, "code" => '002', "msg" => 'No card on file recorded.'];
        }

        $courier_details = $this->courier->find($trans_details->courier_id);
        $amount = ($trans_details->transaction_total_amount + $trans_details->delivery_cost);

        if(isset($card[0]->email)) {

            $email = $card[0]->email;
            $full_name = $card[0]->first_name . ' ' . $card[0]->last_name;

        } else {

            $email = $courier_details->courier_email;
            $full_name = $courier_details->courier_name;

        }

        $data = array(
            'full_name'=> $full_name,
            'amount' => $amount,
            'currency' => 'NGN',
            'narration' => 'Payment for delivery',
            'email' => $email,
            'merchantid' => $this->payment_merchant_id,
            'orderid' => $transaction_id
        );

        $req = json_encode($data);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $req);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($req)));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: Application/json;charset=UTF-8"));
        curl_setopt($curl, CURLOPT_URL, "https://netpluspay.com/api/v1/recurrent");
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($curl);
        if($result == false) {
            echo curl_error($curl);
            $this->logger->debug("executeCardOnFileTransaction curl error",[curl_error($curl)]);
            $this->logger->debug("executeCardOnFileTransaction curl error number",[curl_errno($curl)]);
        }

        $j_result = json_decode($result);
        $rs = get_object_vars($j_result);

        if(($j_result->result =="SUCCESS" || $j_result->result == "PENDING") && $j_result->status  == "CAPTURED") {

            $status = 1;
            $transaction_amount = $amount;
            $code = '000';

        } else {

            $status = 0;
            $transaction_amount = 0;
            $code = '003';

        }

        $this->card_on_file->logCardOnFileTransaction([
            'agent_id' => $agent_id, 
            'transaction_id' => $transaction_id, 
            'transaction_amount'=>$transaction_amount, 
            'status'=> $status]);

        $j_result->code = isset($code) ? $code : null;

        return $j_result;

    }

}
?>