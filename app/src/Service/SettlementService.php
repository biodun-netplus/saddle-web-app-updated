<?php

namespace Dappur\Services;

use Dappur\Model\Merchants;




/**
 * Settlement Service
 */
class SettlementService extends Service {

    protected $merchants;
    
    public const CARD_CHARGE = 0.0075;
    public const COURIER_PERCENTAGE = 0.9;

    public function __construct($container) {
        parent::__construct($container);
        $this->merchants = new Merchants;
    }


    public function populateSettlementForTransaction($settlements) {

        if(is_null($settlements) || empty($settlements) || isset($settlements->merchant_id) == false) {
            $settlements->settled_amount = 0;
            $settlements->due_to_merchant = 0;
            $settlements->due_to_courier = 0;
            $settlements->due_to_netplus = 0;
            return;            
        }

        $merchant = Merchants::where("id","=",$settlements->merchant_id)->first();

        $settlements->settled_amount = 0;
        $settlements->due_to_merchant = 0;
        $settlements->due_to_courier = 0;
        $settlements->due_to_netplus = 0;

        if(is_null($merchant)) {
            $settlements->settled_amount = 0;
            $settlements->due_to_merchant = 0;
            $settlements->due_to_courier = 0;
            $settlements->due_to_netplus = 0;
            return;
        }

        $settlements->pay_type = "Prepaid";

        if($merchant->settlement_type == "separated") {
            $this->logger->debug("separated");
            $settlements->settled_amount = $settlements->delivery_cost;
            $settlements->due_to_courier = $settlements->delivery_cost * SettlementService::COURIER_PERCENTAGE;
            $settlements->due_to_netplus = $settlements->delivery_cost * (1 - SettlementService::COURIER_PERCENTAGE);
            $settlements->due_to_merchant =  0;
        } else {
            $settlements->pay_type = "unknown";
            $settlements->settled_amount = $settlements->transaction_total_amount + $settlements->delivery_cost;
            $settlements->due_to_merchant = $settlements->settled_amount;
            $settlements->due_to_courier = 0;
            $settlements->due_to_netplus = 0;
        }

        if ($settlements->transaction_payment_type == "Prepaid") {
            $this->logger->debug("PREPAID");

            $settlements->settled_amount = $settlements->delivery_cost;
            $settlements->due_to_courier = $settlements->delivery_cost * SettlementService::COURIER_PERCENTAGE;
            $settlements->due_to_netplus = $settlements->delivery_cost * (1 - SettlementService::COURIER_PERCENTAGE);
            $settlements->due_to_merchant = $settlements->transaction_total_amount;
        }

        if ($settlements->transaction_payment_type == "Card" && $settlements->pod_transaction == 1) {
            $this->logger->debug("POD - Card");

            $settlements->pay_type = "POD - Card";
            $total_cost_of_transaction = $settlements->transaction_total_amount + $settlements->delivery_cost;
            $card_charge = SettlementService::CARD_CHARGE * $total_cost_of_transaction;
            $settlements->settled_amount =   $total_cost_of_transaction - $card_charge;
            $settlements->due_to_merchant = $settlements->delivery_cost * SettlementService::COURIER_PERCENTAGE;
            $settlements->due_to_netplus = $settlements->delivery_cost * (1 - SettlementService::COURIER_PERCENTAGE);
            $settlements->due_to_merchant = $settlements->transaction_total_amount - $card_charge;
        }
                                        
        if ($settlements->transaction_payment_type == "Card" && $settlements->preauth_transaction == 1) {
            $this->logger->debug("DOD - Card");

            $settlements->pay_type = "DOD - Card";
            $total_cost_of_transaction = $settlements->transaction_total_amount + $settlements->delivery_cost;
            $card_charge = SettlementService::CARD_CHARGE * $total_cost_of_transaction;
            $settlements->settled_amount = $total_cost_of_transaction - $card_charge;
            $settlements->due_to_courier = $settlements->delivery_cost * SettlementService::COURIER_PERCENTAGE;
            $settlements->due_to_netplus = $settlements->delivery_cost * (1 - SettlementService::COURIER_PERCENTAGE);
            $settlements->due_to_merchant = $settlements->transaction_total_amount - $card_charge;
        }

        if ($settlements->transaction_payment_type == "Cash" && $settlements->pod_transaction == 1) {
            $this->logger->debug("POD - Cash");

            $settlements->pay_type = "POD - Cash";
            $settlements->settled_amount = $settlements->transaction_total_amount + $settlements->delivery_cost;
            $settlements->due_to_courier = $settlements->delivery_cost * SettlementService::COURIER_PERCENTAGE;
            $settlements->due_to_netplus = $settlements->delivery_cost * (1 - SettlementService::COURIER_PERCENTAGE);
            $settlements->due_to_merchant = $settlements->transaction_total_amount;
        }

    }

    public function populateSettlementForB2BTransaction($settlements) {

        $transaction_amount = $settlements->transaction_total_amount;
        $delivery_charges = $settlements->delivery_cost;
        $total_cost_of_transaction = $transaction_amount + $delivery_charges;
        $card_charge = $total_cost_of_transaction * CARD_CHARGE;
        $settled_amount = $total_cost_of_transaction - $card_charge;
        
        $settlements->due_to_courier = $delivery_charges * SettlementService::COURIER_PERCENTAGE;
        $settlements->due_to_netplus = $delivery_charges * (1 - SettlementService::COURIER_PERCENTAGE);
        $settlements->due_to_merchant = $settled_amount;

    }

    public function populateSettlementForSaddleLiteTransaction($settlements) {

        if ($settlements->status == 'FAILED' || $settlements->status == 'PENDING') {
            $settlements->settled_amount = 0;
            return;
        }

        if($settlements->transaction_type == "CASH") {

            $settlements->settled_amount = isset($settlements->amount) ? $settlements->amount * 0.985 : 0;

            $settlements->settled_amount = $settlements->settled_amount > 100 ? $settlements->settled_amount - 100 : $settlements->settled_amount;

        } else if ($settlements->transaction_type == "CARD") {

            $settlements->settled_amount =  isset($settlements->amount) ?  $settlements->amount * 0.9925 : 0;

        }

    }



}