<?php

namespace Dappur\Controller;

use Awurth\Slim\Validation\Validator;
use Cartalyst\Sentinel\Sentinel;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Interop\Container\ContainerInterface;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;
use Slim\Exception\NotFoundException;
use Slim\Flash\Messages;
use Slim\Router;
use Slim\Views\Twig;
use PHPMailer\PHPMailer\PHPMailer as Mailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;
use Firebase\JWT\JWT;
use Tuupola\Base62;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;

//require 'path/to/PHPMailer/src/Exception.php';
//require 'path/to/PHPMailer/src/PHPMailer.php';
//require 'path/to/PHPMailer/src/SMTP.php';
/**
 * @property Twig view
 * @property Router router
 * @property Messages flash
 * @property Validator validator
 * @property Sentinel auth
 */
class Controller
{
    /**
     * Slim application container
     *
     * @var ContainerInterface
     */
    protected $container;
    protected $logger;

    public function __construct(ContainerInterface $container)
    {

        ini_set('output_buffering', '4096');
        $this->container = $container;
        $this->logger =  new Logger('saddle-webapp');
        $this->logger->pushHandler(new RotatingFileHandler(__DIR__.'/../../logs/saddle-webapp.log', 4, Logger::DEBUG));


    }

    /**
     * Redirect to route
     *
     * @param Response $response
     * @param string $route
     * @param array $params
     * @return Response
     */
    public function redirect(Response $response, $route, array $params = array())
    {
        return $response->withRedirect($this->router->pathFor($route, $params));
    }

    /**
     * Redirect to url
     *
     * @param Response $response
     * @param string $url
     *
     * @return Response
     */
    public function redirectTo(Response $response, $url)
    {
        return $response->withRedirect($url);
    }

    /**
     * Write JSON in the response body
     *
     * @param Response $response
     * @param mixed $data
     * @param int $status
     * @return int
     */
    public function json(Response $response, $data, $status = 200)
    {
        return $response->withJson($data, $status);
    }

    /**
     * Write text in the response body
     *
     * @param Response $response
     * @param string $data
     * @param int $status
     * @return int
     */
    public function write(Response $response, $data, $status = 200)
    {
        return $response->withStatus($status)->getBody()->write($data);
    }

    /**
     * Add a flash message
     *
     * @param string $name
     * @param string $message
     */
    public function flash($name, $message)
    {
        $this->flash->addMessage($name, $message);
    }

    /**
     * Create new NotFoundException
     *
     * @param Request $request
     * @param Response $response
     * @return NotFoundException
     */
    public function notFoundException(Request $request, Response $response)
    {
        return new NotFoundException($request, $response);
    }

    public function __get($property)
    {
        return $this->container->get($property);
    }

    public function mailer($to,$recipient,$subject,$html_file)
    {

        $email_validator = new EmailValidator();
        if(empty($to) && $email_validator->isValid($to, new RFCValidation()) != true )
            return;

        try {

            $message = (new \Swift_Message())
                ->setSubject($subject)
                ->setFrom($this->config["dispatch_email"])
                ->setTo($to)
                ->setBody($html_file)
                ->addPart($html_file,"text/html");

            $transport = (new \Swift_SmtpTransport($this->config["dispatch_email_server"],$this->config["dispatch_email_port"]))
                ->setUsername($this->config["dispatch_email"])
                ->setPassword($this->config["dispatch_email_password"]);

            $mailer = new \Swift_Mailer($transport);
            $mailer->send($message);
        
        } catch (Exception $e) {
            //an error occured while sending email
        }

    }

    private function _checkUser($loggedUser)
    {
        $usertype = \Dappur\Model\UserType::where('user_id', $loggedUser['id'])->get();
        return $usertype;
    }
    
    public function shippingPrice($delivery_state, $pickup_state, $weight_value)
    {

        $weight = round($weight_value);
        $total_shipping_price = 0;
        $delivery_state_details = $this->db->table('state')
            ->join('state_region', 'state.id', '=', 'state_region.state_id')
            ->select('state.id', 'state_region.region_id')
            ->where('name', ucfirst($delivery_state))->get();

        $pickup_state_details = $this->db->table('state')
            ->join('state_region', 'state.id', '=', 'state_region.state_id')
            ->select('state.id', 'state_region.region_id')
            ->where('name', ucfirst($pickup_state))->get();
        

        //d($delivery_state_details);
        //d($pickup_state_details);
        $delivery_region = $delivery_state_details[0]->region_id;
        $delivery_state_id = $delivery_state_details[0]->id;

        $pickup_region = $pickup_state_details[0]->region_id;
        $pickup_state_id = $pickup_state_details[0]->id;

        //pickup state and delivery state is same - IntraCity [State Actually]
            if($delivery_state_id == $pickup_state_id)
            {
                $base_price_query = $this->db->table('destination')
                        ->join('shipment_calculation', 'destination.id', '=', 'shipment_calculation.destination_id')
                        ->where('weight_range','0.5 - 3')
                        ->where('destination','IntraCity')->get();

                $result = count($base_price_query);
                
                if($result == 1)
                    {
                        $base_price = $base_price_query[0]->price;
                        $extra_price = 0;
                        //echo "yes";
                        if($weight > 3 )
                            {

                                $extraKg = $weight - 3;
                                $extra_pricing = $this->db->table('region')
                                    ->where('id',$delivery_region )->get();
                                //d($extra_pricing);
                                if($delivery_state == "Lagos" || $pickup_state == "Lagos")
                                     $priceperkg = 200;
                                else
                                    {
                                        $priceperkg = $extra_pricing[0]->pricePerKg;
                                        $extra_price += ($priceperkg * $extraKg);
                                    }
                                //d($priceperkg);  
                                //return $this->json($response,$extra_pricing->toJson(), 200);
                            }

                    }
                //d($result);
                //echo $weight.' '.$base_price.' '.$extra_price;
                
                $total_shipping_price = $base_price + $extra_price;
                //d($total_shipping_price);
                //die;
            }
            else
            {
                if(!empty($delivery_region))
                {
                    if($delivery_region != $pickup_region)
                    {
                        $base_price_query = $this->db->table('destination')
                            ->join('shipment_calculation', 'destination.id', '=', 'shipment_calculation.destination_id')
                            ->where('weight_range','0.5 - 3')
                            ->where('destination','Nationwide')->get();

                        $result = count($base_price_query);
                       
                        if($result == 1)
                        {
                            $base_price = $base_price_query[0]->price;
                            $extra_price = 0;

                            
                            if($weight > 3 )
                            {
                                $extraKg = $weight - 3;
                                $extra_pricing = $this->db->table('region')
                                                ->where('id',$delivery_region )->get();
                                //d($extraKg);
                                if($delivery_state == "lagos")
                                        $priceperkg = 200;
                                else
                                {
                                    $priceperkg = $extra_pricing[0]->pricePerKg;
                                    $extra_price += ($priceperkg * $extraKg);
                                    //d($extra_price);
                                }
                                    
                                //return $this->json($response,$extra_pricing->toJson(), 200);
                            }
                        }

                        $total_shipping_price = $base_price + $extra_price;
                    }
                
                    else
                    {
                        $base_price_query = $this->db->table('destination')
                            ->join('shipment_calculation', 'destination.id', '=', 'shipment_calculation.destination_id')
                            ->where('weight_range','0.5 - 3')
                            ->where('destination','Within Region')->get();

                        $result = count($base_price_query);
                        if($result == 1)
                        {
                            $base_price = $base_price_query[0]->price;
                            $extra_price = 0;

                            if($weight > 3 )
                            {
                                $extraKg = $weight - 3;
                                $extra_pricing = $this->db->table('region')
                                    ->where('id',$delivery_region )->get();
                            
                                if("lagos" === "lagos")
                                        $priceperkg = 200;
                                else
                                {
                                    $priceperkg = $extra_pricing[0]->pricePerKg;
                                    $extra_price += ($priceperkg * $extraKg);
                                }    
              
                            }

                        }

                        $total_shipping_price = $base_price + $extra_price;
                    }
                }
            } 
            
        return $total_shipping_price;
    }

    public function shippingPriceNew($delivery_state, $pickup_state, $weight_value, $courier_id, $pickup_lga, $delivery_lga, $courierIdCheck = false) {

        $weight = $weight_value;

        $couriers = new \Dappur\Model\Courier;

        $courierID = $couriers->where('courier_id','=',$courier_id)->select('id')->first();
        if(isset($courierID) == false) {
            return 'no record found for this location';
        }

        $courier_id = $courierID->id;
        $priceWeight = $this->db->table('rate_card')
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','=',$weight)
                        ->where('state_from', $pickup_state)
                        ->where('lga_to', $delivery_lga)
                        ->where('lga_from', $pickup_lga)
                        ->where('state_to', $delivery_state)
                        ->first();

        if(count($priceWeight) == 0) {

            $priceWeight = $this->db->table('rate_card')
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','>',$weight)
                        ->where('state_from', $pickup_state)
                        ->where('lga_to', $delivery_lga)
                        ->where('lga_from', $pickup_lga)
                        ->where('state_to', $delivery_state)
                        ->first();
            
            if(count($priceWeight) == 0){
                $priceWeight = $this->db->table('rate_card')
                            ->select('price','weight','additionalPrice')
                            ->where('courierNameId', $courier_id)
                            ->where('weight','<',$weight)
                            ->where('state_from', $pickup_state)
                            ->where('lga_to', $delivery_lga)
                            ->where('lga_from', $pickup_lga)
                            ->where('state_to', $delivery_state)
                            ->orderBy('weight','desc')
                            ->first();
            }

            if(count($priceWeight) == 0){
            
                $priceWeight = $this->db->table('rate_card')
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','=',$weight)
                        ->where('lga_to', $delivery_lga)
                        ->where('lga_from', 'all')
                        ->where('state_from', $pickup_state)
                        ->where('state_to', $delivery_state)
                        ->first();

            } else {

                $extra = $priceWeight->additionalPrice;
                $tedt = $priceWeight->weight;
        
                if($tedt < $weight){
                    
                    $extraWeight = $weight - $tedt;
                    $extarWeightTotal = $extraWeight * $extra*2;
                    $total_shipping_price = $priceWeight->price + $extarWeightTotal; 

                } else {
                    $total_shipping_price = trim($priceWeight->price);
                }
                
                return (int)$total_shipping_price;

            }
      
        } else {
              
            $total_shipping_price = trim($priceWeight->price);
            return (int)$total_shipping_price;
        }

        if(count($priceWeight) == 0){

            $priceWeight = $this->db->table('rate_card')
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','>',$weight)
                        ->where('state_from', $pickup_state)
                        ->where('lga_to', $delivery_lga)
                        ->where('lga_from', 'all')
                        ->where('state_to', $delivery_state)
                        ->first();
            
            if(count($priceWeight) == 0){
                // echo "ds";die;
                $priceWeight = $this->db->table('rate_card')
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','<',$weight)
                        ->where('state_from', $pickup_state)
                        ->where('lga_to', $delivery_lga)
                        ->where('lga_from', 'all')
                        ->where('state_to', $delivery_state)
                        ->orderBy('weight','desc')
                        ->first();
            }

            if(count($priceWeight) == 0){

               $priceWeight = $this->db->table('rate_card')
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','=',$weight)
                        ->where('lga_to', 'all')
                        ->where('lga_from', 'all')
                        ->where('state_from', $pickup_state)
                        ->where('state_to', $delivery_state)
                        ->first();

            } else {

                $extra = $priceWeight->additionalPrice;
                $tedt = $priceWeight->weight;
        
                if($tedt < $weight){
                    
                $extraWeight = $weight - $tedt;
                $extarWeightTotal = $extraWeight * $extra*2;
                $total_shipping_price = $priceWeight->price + $extarWeightTotal; 

                } else {

                $total_shipping_price = trim($priceWeight->price);
                }

                return (int)$total_shipping_price;

            }
      
        } else {
              
            $total_shipping_price = trim($priceWeight->price);
            return (int)$total_shipping_price;
        }

        if(count($priceWeight) == 0) {

            $priceWeight = $this->db->table('rate_card')
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','>',$weight)
                        ->where('state_from', $pickup_state)
                        ->where('lga_to', 'all')
                        ->where('lga_from', 'all')
                        ->where('state_to', $delivery_state)
                        ->first();

            if(count($priceWeight) == 0){
                $priceWeight = $this->db->table('rate_card')
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','<',$weight)
                        ->where('state_from', $pickup_state)
                        ->where('lga_to', 'all')
                        ->where('lga_from', 'all')
                        ->where('state_to', $delivery_state)
                        ->orderBy('weight','desc')
                        ->first();
            }

            if(count($priceWeight) == 0){
          
               $priceWeight = $this->db->table('rate_card')
                        ->select('price','weight','additionalPrice')
                        ->where('courierNameId', $courier_id)
                        ->where('weight','=',$weight)
                        ->where('lga_to', 'all')
                        ->where('lga_from', 'all')
                        ->where('state_from', $pickup_state)
                        ->where('state_to', $delivery_state)
                        ->first();

            } else {

                $extra = $priceWeight->additionalPrice;
                $tedt = $priceWeight->weight;
        
                if($tedt < $weight){
                    
                $extraWeight = $weight - $tedt;
                $extarWeightTotal = $extraWeight * $extra*2;
                $total_shipping_price = $priceWeight->price + $extarWeightTotal; 

                }
                else{
                

                $total_shipping_price = trim($priceWeight->price);
                }
                
                return (int)$total_shipping_price;

            }
      
        } else {
            $total_shipping_price = trim($priceWeight->price);
            return (int)$total_shipping_price;
        }

        if(count($priceWeight) == 0){
            return 'no record found for this location';
        } else {
  
            $extra = $priceWeight->additionalPrice;
            $tedt = $priceWeight->weight;
       
            if($tedt < $weight){
                 
              $extraWeight = $weight - $tedt;
              $extarWeightTotal = $extraWeight * $extra*2;
              $total_shipping_price = $priceWeight->price + $extarWeightTotal; 
           
            } else {

             $total_shipping_price = trim($priceWeight->price);
             $total_shipping_price =  str_replace('"', '', $total_shipping_price);
                          
            }
            
            return (int)$total_shipping_price;
        }

          // return 'no record found for this location';
         

          // $total_shipping_price = $priceWeight->price;

          $extra = $priceWeight->additionalPrice;
          $tedt = $priceWeight->weight;
       
          if($tedt < $weight){
               
            $extraWeight = $weight - $tedt;
            $extarWeightTotal = $extraWeight * $extra*2;
            $total_shipping_price = $priceWeight->price + $extarWeightTotal; 

          }
           else{

              $total_shipping_price = trim($priceWeight->price);
            }
            
        return (int)$total_shipping_price;
    
    }

    public function generateID($length)
    {
        $code = '';
        $possible = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        $maxlength = strlen($possible);
        if ($length > $maxlength) {
            $length = $maxlength;
        }

        $i = 0;
        while ($i < $length) {
            $char = substr($possible, mt_rand(0, $maxlength - 1), 1);
            if (!strstr($code, $char)) {
                $code .= $char;
                $i++;
            }
        }

        $r = md5($code);
        $r = substr($r, -7);

        return 'SA'.$r;
    }

    public function token($username,$password,$userid)
    {
      $now = new \ExpressiveDate();
      $now_timestamp = $now->getTimeStamp();
      $future = $now->addYears(10);

      $username = $username;
      $password = $password;
      $userid = $userid;
      //die;
      $jti = (new Base62)->encode(random_bytes(16));

      $payload = [
          "iat" => $now_timestamp,
          "exp" => $future->getTimeStamp(),
          "jti" => $jti,
          "sub" => $username.':'.$userid
      ];
      $secret = "supersecretkeyyoushouldnotcommittogithub";
      $token = JWT::encode($payload, $secret, "HS256");
      $data["token"] = $token;
      $data["expires"] = $future->getTimeStamp();
      return $data['token'];
    }

}
