<?php

namespace Dappur\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Firebase\JWT\JWT;
use Tuupola\Base62;
use Cartalyst\Sentinel\Users\EloquentUser as EloquentUser;

use Dappur\Services\NetplusPayService;
use Dappur\Services\AccountsService;
use Dappur\Services\SaddleLiteService;
use Dappur\Services\DeliveryEventService;
use Dappur\Services\DeliveryService;
use Dappur\Services\EmailNotificationService;

class ApiController extends Controller
{

    private $paymentService;
    private $accountsService;
    private $saddleLiteService;
    private $deliveryEventService;

    public function __construct($container) {
    
        parent::__construct($container);

        $this->paymentService = new NetplusPayService($container,$this->config['liveid']);
        $this->accountsService = new AccountsService($container);
        $this->saddleLiteService = new SaddleLiteService($container);
        $this->emailNotificationService = new EmailNotificationService(
                $container,
                $this->config['dispatch_email'],
                $this->config['dispatch_email_server'],
                $this->config['dispatch_email_port'],
                $this->config['dispatch_email_password']);
        $this->deliveryService = new DeliveryService($container);
        $this->deliveryEventService = new DeliveryEventService($container,$this->emailNotificationService);

    }

    public function auth(Request $request, Response $response)
    {

        if(filter_var($request->getParam('login'), FILTER_VALIDATE_EMAIL)) {
            $credentials = [
                'email' => $request->getParam('login'),
                'password' => $request->getParam('password')
            ];
        } else {
            $credentials = [
                'username' => $request->getParam('login'),
                'password' => $request->getParam('password')
            ];
        }
        
        $remember = false;
        $result = $this->auth->authenticate($credentials, $remember);

        if($result == false)
        {
            $http_status = 401;
            $data = ["status" => 0, "msg" => 'Username or Password Incorrect '];
        }
        else
        {
           $loggedUser = $this->auth->check();
           $userID = $loggedUser->id;
           $user_role = $loggedUser->getRoles();

            if( $user_role->contains('name','Agent') )
            {
              $http_status = 200;
              $msg = "Login Succesful";
              $user_id = $this->token( $request->getParam('login' ), $request->getParam( 'password' ), $userID);
              $courier = $this->accountsService->getCourierForAgent($userID);
              $email = $this->accountsService->getCardOnFileEmail($courier->courier_id, $courier->courier_email, $loggedUser->email);
            } else {
              $http_status = 401;
              $msg = "Not an agent";
              $user_id = 0;
              $email = "";
            }

            $data = ["status" => 1, "msg" => $msg, "email" => $email, "token" => $user_id ];
        }

        return $this->json($response, $data, $http_status);
    }

    // Endpoint for Wawoo Only
    public function getWawooCourier(Request $request, Response $response){
        $newArray = array( 
            "0" => array (
               "id" => 18,
               "courier_id" => 'SAfceb761',	
               "courier_name" => 'Red Star (FedEx)'
            ),
            "1" => array (
                "id" => 48,
                "courier_id" => 'SA493a731',	
                "courier_name" => 'Courier plus'
            )
        );

        $http_status = 200;

        return $this->json($response, $newArray, $http_status);

    }

    public function getCourier(Request $request, Response $response){

        $courier_details = $this->db->table('courier')
                                ->select('id','courier_id','courier_name')
                                ->where('status',1)
                                ->whereIn("id",function($query){
                                    $query->select("courierNameId")
                                        ->distinct('courierNameId')
                                        ->from('rate_card');
                                })
                                ->get();

        $newArray = $courier_details->toArray();

        $http_status = 200;

        return $this->json($response, $newArray, $http_status);
    
    }

    public function getCourierForRoute(Request $req, Response $res) {
 
        $q = $req->getQueryParams();

        if(!isset($q['pickup_state']) || !isset($q['dropoff_state'])) {
 
            $http_status = 200;
            return $this->json($response, [], $http_status);
            
        }

        if(!isset($q['dropoff_lga'])) {
            $q['dropoff_lga'] = 'all';
        }

        if(!isset($q['pickup_lga'])) {
            $q['dropoff_lga'] = 'all';
        }

        $pickup_state = strtolower(trim($q['pickup_state']));
        $pickup_lga = strtolower(trim($q['pickup_lga']));

        $dropoff_state = strtolower(trim($q['dropoff_state']));
        $dropoff_lga = strtolower(trim($q['dropoff_lga']));

        $couriers = $this->db->table('courier')
                                ->join('rate_card','courier.id','=', 'rate_card.courierNameId')
                                ->where('rate_card.state_from','=',$pickup_state)
                                ->where('rate_card.state_to','=',$dropoff_state)
                                ->whereIn('rate_card.lga_from',['all',$pickup_lga])
                                ->whereIn('rate_card.lga_to',['all',$dropoff_lga])
                                ->select('courier.courier_id','courier.courier_name','courier.courier_email')
                                ->distinct('courier.courier_id')
                                ->get();

        $http_status = 200;
        return $this->json($res, $couriers, $http_status);
    }

    public function getStatesWithLGA(Request $req, Response $res) {

        $states_ = new \Dappur\Model\State;
        $data = $states_->with('lgas')->get();

        return $this->json($res, $data, 200);

    }

    public function getStateLGA(Request $req, Response $res) {

        $q = $req->getQueryParams();
        $state = new \Dappur\Model\State;
        $data = $state->where('name',$q['state'])
                    ->with('lgas')->get();

        if(count($data) < 1) {
            return $this->json($res,[],404);
        }

        return $this->json($res,$data[0]->lgas,200);

    }

    public function getCourierRoutes(Request $req, Response $res) {
       
        $q = $req->getQueryParams();

        if(isset($q['courier_id']) == null) {
            return $this->json($res, [], 200);
        }

        $courier_id = $q['courier_id'];

        $routes = $this->db->table('rate_card')
                        ->select('state_from','lga_from','state_to','lga_to')
                        ->where("courierNameId",function( $query) use ($courier_id){
                            $query->select('id')
                                ->from('courier')
                                ->where('courier_id',$courier_id);
                        })
                        ->get();

        return $this->json($res, $routes, 200);
 
    }

    public function agentLocation(Request $request, Response $response){

        $body = $request->getParsedBody();
        
        $latitude = trim($body['lat']);
        $longitude = trim($body['long']);
        $agent_id = isset($body['agent_id']) ? $body['agent_id'] : null;

        if(empty($agent_id)) {
            $jwt = $request->getAttribute("jwt");
            $str = $jwt->sub;
            $pos = strpos($str,":");
            $agent_id = substr($str, $pos+1);
        }

        if(empty($agent_id))
        {
            $http_status = 401;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "AgentID can't be empty";
            return $this->json($response, $data_sent, $http_status);
        }

        if(empty($latitude))
        {
            $http_status = 401;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "latitude can't be empty";
            return $this->json($response, $data_sent, $http_status);
        }

        if(empty($longitude))
        {
            $http_status = 401;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "longitude can't be empty";
            return $this->json($response, $data_sent, $http_status);
        }

        $count = $this->db->table('role_users')->where('user_id', $agent_id)->where('role_id',10)->count();
        // print_r($count);die;

        if($count == 0){
            $http_status = 401;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "Agent does not exist, please check agent id";
            return $this->json($response, $data_sent, $http_status);
        }

        $this->db->table('usertype')->where('user_id', $agent_id)->where('user_type', 'Courier')->update(['longitude' => $longitude, 'latitude' => $latitude]);

        // $total_shipping_price = $this->shippingPriceNew($delivery_state,$pickup_state,$weight,$courier_id, $pickup_lga, $delivery_lga);

            $http_status = 200;
            $data_sent['status'] = 1;
            $data_sent['msg'] = "Location Updated Successfully";
        
       return $this->json($response, $data_sent, $http_status); 

    }

    private function printToFile($data) {
           $attachmentPath = '/home/deploy/apps/saddle-webapp/current/storage/text.log';
           $fh = fopen($attachmentPath, 'w');
           fwrite($fh, $data);
           fclose($fh);
    }

    //retrieve deliveries/transactions for pickup
    public function getPickUp(Request $request, Response $response)
    {

        $jwt = $request->getAttribute("jwt");
        $str = $jwt->sub;
        $pos = strpos($str,":");
        $agent_id = substr($str, $pos+1);


        $pickups = $this->db->table('delivery_assignment')
                        ->distinct()
                        ->where('agent_id','=', $agent_id)
                        ->where('type','pickup')
                        ->pluck('transaction_id');

        $transactions = new \Dappur\Model\Transactions;
        $merchant = new \Dappur\Model\Merchants;
        $items = new \Dappur\Model\Items;

        $data = $transactions
        ->join('items','transactions.transaction_id','=','items.transaction_id')
        ->where('transactions.pickup_handling','mercht-loc')
        ->whereIn('transactions.id',$pickups->toArray())
        ->where(function($query) {
            $query->where('transactions.transaction_status',0)
                ->orWhere('items.delivery_status',0);
        })
        ->select('transactions.*')->distinct('transactions.transaction_id')
        ->get();

        $result = $data->flatten();
        $data_array = array();

        if(count($result) == 1 )
        {
            $http_status = 200;
            $i = 0;
            $merchantDetail = $merchant->getMerchantDetail($result[$i]['merchant_id']);
            $price = $result[$i]['transaction_total_amount'];
            //$address = $result[$i]['transaction_total_amount'];
            $handling = $result[$i]['pickup_handling'];
            if($hanlding = "mercht-loc")
            {
               $address = $result[$i]['pickup_details'];
            }
            $quantity = $items->getItemsByTransaction($result[$i]['transaction_id'])->count();
            $getTransactionDetails = $transactions->getTransactionDetails($result[$i]['transaction_id']);

            $Customer_number = json_decode($getTransactionDetails->delivery_details);

            if(array_key_exists("customer_phone",(array)$Customer_number))
                 {
                  $number = isset($Customer_number->customer_phone) ? $Customer_number->customer_phone : null;
                 }else{
                  $number = '';
                 }

            $merchantNumber = json_decode($getTransactionDetails->pickup_details);

            if(array_key_exists("merchant_phone",(array)$merchantNumber))
                 {
                  $merchantnumber = $merchantNumber->merchant_phone;
                 }else{
                  $merchantnumber = '';
                 }

            //prepare data to send
            $data_array['merchantName'] = isset($merchantDetail->merchant_name) ? $merchantDetail->merchant_name : '';
            $data_array['merchantContact'] = $merchantnumber;
            $data_array['Customer_number'] = $number;
            $data_array['orderId'] = $result[$i]['transaction_id'];
            $data_array['quantity'] = $quantity;
            $data_array['price'] = $price;
            $address_details = json_decode($address);
            
            $data_array['address'] = isset($address_details->merchant_address) ? $address_details->merchant_address : '';
            $data_array['area'] = strtoupper(isset($address_details->merchant_lga) ? $address_details->merchant_lga : '');
            $data_array = array($data_array);

        }
        elseif(count($result) > 1)
        {

            $http_status = 200;
            for ($i=0; $i < count($result); $i++) {

                # code...
                //echo $i;
                $merchantDetail = $merchant->getMerchantDetail($result[$i]['merchant_id']);
                
                $price = $result[$i]['transaction_total_amount'];
                //$address = $result[$i]['transaction_total_amount'];
                $address = $result[$i]['pickup_details'];
                $quantity = $items->getItemsByTransaction($result[$i]['transaction_id'])->count();

                $getTransactionDetails = $transactions->getTransactionDetails($result[$i]['transaction_id']);
                $Customer_number = json_decode($getTransactionDetails->delivery_details);

                 if(array_key_exists("customer_phone",(array)$Customer_number))
                 {
                  $number = $Customer_number->customer_phone;
                 } else {
                  $number = '';
                 }

                $merchantNumber = json_decode($getTransactionDetails->pickup_details);

                if(array_key_exists("merchant_phone",(array)$merchantNumber)) {
                  $merchantnumber = $merchantNumber->merchant_phone;
                }else{
                  $merchantnumber = '';
                }

               //prepare data to send
                $data_array[$i]['merchantName'] = isset($merchantDetail->merchant_name) ? $merchantDetail->merchant_name : '';
                $data_array[$i]['merchantContact'] = $merchantnumber;
                $data_array[$i]['Customer_number'] = $number;
                $data_array[$i]['orderId'] = $result[$i]['transaction_id'];
                $data_array[$i]['quantity'] = $quantity;
                $data_array[$i]['price'] = $price;
                $address_details = json_decode($address);
                
                $data_array[$i]['address'] = isset($address_details->merchant_address) ? $address_details->merchant_address : '';
                $data_array[$i]['area'] = strtoupper(isset($address_details->merchant_lga) ? $address_details->merchant_lga : '');
 
            }
        }
        else{
            $http_status = 401;
            $data_array = ["status" => 1, "msg" => 'No Available PickUps'];

        }

        $sent_data = $data_array;  

        // mail('palash@geeksperhour.com', 'pickSent', print_r($sent_data, true));  
       
            
        return $this->json($response, $sent_data, $http_status);
    
    }


    public function shipping1(Request $request, Response $response)
    {
        $total_shipping_price = 0;

        $jwt = $request->getAttribute("jwt");
        $body = $request->getParsedBody();
        //print_r($body);
        //die;
        //does the token has the proper credentials? make this one function later
        $needle_postition = strpos($jwt->sub, ":");
        $strlen = strlen($jwt->sub);
        $username = substr($jwt->sub,0,$needle_postition);
        $userID = substr($jwt->sub,$needle_postition+1,$strlen);

        $delivery_state = trim($body['delivery_state']);
        $pickup_state = trim($body['pickup_state']);
        $weight = trim($body['weight']);


        if(empty($delivery_state))
        {
            $http_status = 401;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "Delivery State can't be empty";
            return $this->json($response, $data_sent, $http_status);
        }

        if(empty($pickup_state))
        {
            $http_status = 401;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "Pickup State can't be empty";
            return $this->json($response, $data_sent, $http_status);
        }
        
        if(empty($weight))
        {
            $http_status = 401;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "Item Weight can't be empty";
            return $this->json($response, $data_sent, $http_status);
        }


       // $total_shipping_price = $this->shippingPrice($delivery_state,$pickup_state,$weight);
        $total_shipping_price = $this->shippingPriceNew($delivery_state, $pickup_state, $weight_value, $courier_id, $pickup_lga, $delivery_lga);
       return $this->json($response, $total_shipping_price, 200); 
    }

    public function getCourierPrice(Request $req, Response $res) {

        $jwt = $request->getAttribute("jwt");
        $q = $req->getQueryParams();
       
        $needle_postition = strpos($jwt->sub, ":");
        $strlen = strlen($jwt->sub);
        $username = substr($jwt->sub,0,$needle_postition);
        $userID = substr($jwt->sub,$needle_postition+1,$strlen);

        $delivery_state = trim($q['delivery_state']);
        $pickup_state = trim($q['pickup_state']);
        $weight = trim($q['weight']);
        $courier_id = trim($q['courier_id']);
        $delivery_lga = trim($q['delivery_lga']);
        $pickup_lga = trim($q['pickup_lga']);

        
    }

    public function latestRateCardApi(Request $request, Response $response) {

        $total_shipping_price = 0;

        $jwt = $request->getAttribute("jwt");
       
        $needle_postition = strpos($jwt->sub, ":");
        $strlen = strlen($jwt->sub);
        $username = substr($jwt->sub,0,$needle_postition);
        $userID = substr($jwt->sub,$needle_postition+1,$strlen);

        $body = $request->getParsedBody();

        if(isset($body) == false || isset($body['delivery_state']) == false) {
            $body = $request->getQueryParams();
        }

        $delivery_state = trim($body['delivery_state']);
        $pickup_state = trim($body['pickup_state']);
        $weight = trim($body['weight']);
        $courier_id = trim($body['courier_id']);
        $delivery_lga = trim($body['delivery_lga']);
        $pickup_lga = trim($body['pickup_lga']);

        $transaction = new \Dappur\Model\Transactions;
        $item = new \Dappur\Model\Items;

        if(empty($delivery_state))
        {
            $http_status = 401;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "Delivery State can't be empty";
            return $this->json($response, $data_sent, $http_status);
        }

        if(empty($pickup_state))
        {
            $http_status = 401;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "Pickup State can't be empty";
            return $this->json($response, $data_sent, $http_status);
        }

        if(empty($delivery_lga))
        {
            $http_status = 401;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "Delivery LGA can't be empty";
            return $this->json($response, $data_sent, $http_status);
        }

        if(empty($pickup_lga))
        {
            $http_status = 401;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "Pickup LGA can't be empty";
            return $this->json($response, $data_sent, $http_status);
        }
        
        if(empty($weight))
        {
            $http_status = 401;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "Item Weight can't be empty";
            return $this->json($response, $data_sent, $http_status);
        }
        if(empty($courier_id))
        {
            $http_status = 401;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "courier Id can't be empty";
            return $this->json($response, $data_sent, $http_status);
        }

       

       $total_shipping_price = $this->shippingPriceNew($delivery_state,$pickup_state,$weight,$courier_id, $pickup_lga, $delivery_lga,true);
        
        if($total_shipping_price == 'no record found for this location')
        {

            $http_status = 404;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "no record found for this location";
             
            return $this->json($response, $data_sent, $http_status);
        }
     
       return $this->json($response, $total_shipping_price, 200); 
    
    }

    //retrieve deliveries/transactions for dropoff
    public function getDropoff(Request $request, Response $response)
    {

      $http_status = 200;

      $jwt = $request->getAttribute("jwt");
      $str = $jwt->sub;
      $pos = strpos($str,":");
      $agent_id = substr($str, $pos+1);

      $courier = $this->accountsService->getCourierForUser($agent_id);

      $dropoffs = $this->db->table('delivery_assignment')
	      ->where(function($query) {
		      $query->where('type','dropoff')
			    ->orwhere('type','dropoffpartial');
	      })
	      ->where('agent_id', $agent_id)
          ->pluck('transaction_id');
          
      $transactions = new \Dappur\Model\Transactions;
      $merchant = new \Dappur\Model\Merchants;

      $items = new \Dappur\Model\Items;

      $data = $transactions
        ->join('items','transactions.transaction_id','=','items.transaction_id')
		->whereIn('transactions.id',$dropoffs->toArray())
		->where('transactions.delivery_handling','to_customer')
		->where(function($query){
		      $query->whereIn('transactions.transaction_status',[1,2,4,14,101,102, 10, 7, 11])
		            ->whereNotIn('items.delivery_status',[0,3,301]);
		 })
		->select('transactions.*')->distinct('transactions.transaction_id')
        ->get();

      $result = $data->flatten();

      //echo($result);
      //die;
      $data_array = array();
      if(count($result) == 1 )
      {
          $i = 0;
          $price = $result[$i]['delivery_cost'];
          $address = $result[$i]['delivery_details'];

          $merchantDetail = $merchant->getMerchantDetail($result[$i]['merchant_id']);

          $getTransactionDetails = $transactions->getTransactionDetails($result[$i]['transaction_id']);

          $Customer_number = json_decode($getTransactionDetails->delivery_details);

             if(array_key_exists("customer_phone",(array)$Customer_number))
             {
              $number = $Customer_number->customer_phone;
             }else{
              $number = '';
             }

        $merchantNumber = json_decode($getTransactionDetails->pickup_details);

            if(array_key_exists("merchant_phone",(array)$merchantNumber))
                 {
                  $merchantnumber = $merchantNumber->merchant_phone;
                 }else{
                  $merchantnumber = '';
                 }

          $quantity = $items->getItemsByTransaction($result[$i]['transaction_id'])->count();

          if($result[$i]['pod_transaction'] == 0 and $result[$i]['preauth_transaction'] == 0 )
             {$payment_type = "Paid";}
          elseif($result[$i]['pod_transaction'] == 1 and $result[$i]['preauth_transaction'] == 0)
             {$payment_type = "POD";}
          elseif($result[$i]['pod_transaction'] == 0 and $result[$i]['preauth_transaction'] == 1)
            {$payment_type = "DOD";}
          $address_details = json_decode($address);



          //prepare data to send
          $data_array['customerName'] = $address_details->customer_name;
          $data_array['merchantContact'] = $merchantnumber;
          $data_array['Customer_number'] = $number;
          $data_array['orderId'] = $result[$i]['transaction_id'];
          $data_array['quantity'] = $quantity;
          $data_array['price'] = $price;
          $data_array['paymentType'] = $payment_type;
          $data_array["merchantid"] = is_null($courier->netpluspay_mid) ? $this->config["liveid"] : $courier->netpluspay_mid;

          //d($address_details);
          //die;
          $data_array['address'] = $address_details->customer_address;
          $data_array['area'] = strtoupper($address_details->customer_lga);
          $data_array = array($data_array);

      }
      elseif(count($result) > 1)
      {
          for ($i=0; $i < count($result); $i++) {

            $merchantDetail = $merchant->getMerchantDetail($result[$i]['merchant_id']);

            $getTransactionDetails = $transactions->getTransactionDetails($result[$i]['transaction_id']);

            $Customer_number = json_decode($getTransactionDetails->delivery_details);

             if(array_key_exists("customer_phone",(array)$Customer_number))
             {
              $number = $Customer_number->customer_phone;
             }else{
              $number = '';
             }

             $merchantNumber = json_decode($getTransactionDetails->pickup_details);

            if(array_key_exists("merchant_phone",(array)$merchantNumber))
                 {
                  $merchantnumber = $merchantNumber->merchant_phone;
                 }else{
                  $merchantnumber = '';
                 }


              $price = $result[$i]['delivery_cost'];
              
              $address = $result[$i]['delivery_details'];
              $quantity = $items->getItemsByTransaction($result[$i]['transaction_id'])->count();

              
              if($result[$i]['pod_transaction'] == "0" and $result[$i]['preauth_transaction'] == 0 )
                 {$payment_type = "Paid";}
              elseif($result[$i]['pod_transaction'] == "1" and $result[$i]['preauth_transaction'] == 0)
                 {
                   //echo "POD";
                   $payment_type = "POD";
                 }
              elseif($result[$i]['pod_transaction'] == "0" and $result[$i]['preauth_transaction'] == 1)
                {$payment_type = "DOD";}
              $address_details = json_decode($address);

              //prepare data to send
              $data_array[$i]['customerName'] = isset($address_details->customer_name) ? $address_details->customer_name : '';
              $data_array[$i]['merchantContact'] = $merchantnumber;
              $data_array[$i]['Customer_number'] = $number;
              $data_array[$i]['orderId'] = $result[$i]['transaction_id'];
              $data_array[$i]['quantity'] = $quantity;
              $data_array[$i]['price'] = $price;
              $address_details = json_decode($address);
	          $data_array[$i]['address'] = isset($address_details->customer_address) ? $address_details->customer_address : '';
              $data_array[$i]['area'] = strtoupper(isset($address_details->customer_lga) ? $address_details->customer_lga : '');
              $data_array[$i]['paymentType'] = $payment_type;
              $data_array[$i]["merchantid"] = is_null($courier->netpluspay_mid) ? $this->config["liveid"] : $courier->netpluspay_mid;

        }
       
      }
      else{
          $http_status = 401;
          $data_array = ["status" => 1, "msg" => 'No Available Dropoffs'];

      }
      // mail('palash@geeksperhour.com', 'sentdrop', print_r($data_array, true));


        $sent_data = $data_array;
        
       return $this->json($response, $sent_data, $http_status);
     }

    public function payMerchant(Request $request, Response $response)
    {

        $sent_data = $request->getParsedBody();
 
        $jwt = $request->getAttribute("jwt");
        $str = $jwt->sub;
        $pos = strpos($str,":");
        $agent_id = substr($str, $pos+1);
 
        $transaction_id = $sent_data['transactionID'];

        $j_result = $this->paymentService->executeCardOnFileTransaction($transaction_id, $agent_id);

        if($j_result->code == "003") {
            return $response->withStatus(400)
                ->withHeader("Content-Type", "application/json")
                ->write(json_encode($j_result, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        }

       return $response->withStatus(200)
           ->withHeader("Content-Type", "application/json")
           ->write(json_encode($j_result, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

    }

    public function getTransactionDetails(Request $request, Response $response, $transactionID)
    {
        $http_status = 400;
        $q = $request->getQueryParams();

	    $for = isset($q['for']) ? $q['for'] : 'pickup';
        if($for == 'pickup') {
            $items = $this->models['items']->getItemsReadyForPickupByTransaction($transactionID);
        } else if($for == 'dropoff') {
            $items = $this->models['items']->getItemsReadyForDropOffByTransaction($transactionID);
        } else {
           $http_status = 400;
           $sent_data = ["status" => 1, "msg" => 'Invalid Request'];
           return $response->withStatus($http_status)
	            ->withHeader("Content-Type", "application/json")
                ->write(json_encode($sent_data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        }

	//echo json_encode($items);
	//die();

        if($items->isNotEmpty())
        {
            $http_status = 200;
            $sent_data = $items->flatten();
        }
        else
        {
            $http_status = 404;
            $sent_data = ["status" => 1, "msg" => 'No Transaction details'];
        }

        return $response->withStatus($http_status)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($sent_data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
    
    public function updateTransactionStatus(Request $request, Response $response)
    {
        $transaction = new \Dappur\Model\Transactions;
        $item = new \Dappur\Model\Items;

        $transactions = $transaction->getTransactionsByCourier(18);
        $http_status = 200;
        $sent_data = $transactions->toArray();

        $i=0;
        for($i;$i < count($sent_data); $i++)
        {
            $this->deliveryEventService->updateTransactionStatus($sent_data[$i]['transaction_id']);
        }


    }

    public function capture(Request $request, Response $response)
    {
        $http_status = 200;
        $sent_data = $request->getParsedBody();

        $capture_url = 'https://netpluspay.com/capture.php';

        //Log.d("debug", jsonString);
        $data = array("amountCapture" => $sent_data["amount"],
                      "amountRefund" => "0",
                      "merchantid" => $this->config["liveid"],
                      "orderid" => $sent_data["orderId"],
                      "pan" => $sent_data['pan']);

        $data_string = json_encode($data);

        $ch = curl_init($capture_url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        // If the API is JSON, use json_decode.
        $data_sent = json_decode($result);

	if(!isset($data_sent->Error) && $data_sent->status == 'CAPTURED') {

           $transaction_id = $sent_data['orderId'];
           $transaction = new \Dappur\Model\Transactions;        
           $item = new \Dappur\Model\Items;        
           $transactionUpdate = $transaction->getTransactionDetails($transaction_id);
           $transactionUpdate->transaction_status = 3;
           $transactionUpdate->transaction_payment_type = 'Card';
           $total_items = $item->getTransactionItemCount($transaction_id);
        
           $transactionUpdate->push();
           //there is a need to work on this to only update selected or sent item id not all
           $itemUpdate = $item->where('transaction_id', '=', $transaction_id)->first();
           //$itemupdate->delivery_status = 1;
           $itemUpdate->delivery_status = 3;

           $transactionUpdate->push();
           $itemUpdate->push();

	}

        return $this->json($response, $data_sent, $http_status);

    }

    //create new delivery
    public function delivery(Request $request, Response $response)
    {
        $http_status = "";
        $body = $request->getParsedBody();

        $jwt = $request->getAttribute("jwt");
        $str = $jwt->sub;
        $pos = strpos($str,":");
        $userID = substr($str, $pos+1);

        $transaction = new \Dappur\Model\Transactions;
        $courier_location = new \Dappur\Model\CourierLocation;

        $courier_id = $body['transaction']['courier_id'];

        $couriers = new \Dappur\Model\Courier;
        $courier = $couriers->getCourierById($courier_id);

        $usertype = new \Dappur\Model\UserType;
        $typeID = $usertype->getUserTypeID($userID);
        $merchants = new \Dappur\Model\Merchants;

        $merchant = $merchants->find($typeID);
        $courierMerchant = new \Dappur\Model\CourierMerchants;


        $order_exist = $transaction->getTransactionByOrderNo($body['transaction']['order_id'],$merchant->id);
    
        if( $courier == null || $courier->count() <= 0 )
        {
            $data['status'] = 0;
            $data['message'] = "Courier ID is either NOT Valid or Disabled";
            $http_status = 401;
        }

        if($merchant->status == 0 )
        {
            $data['status'] = 0;
            $data['message'] = "Merchant is Disabled";
            $http_status = 401;
        }
        elseif ($order_exist > 0) {

            $data['status'] = 0;
            $data['message'] = "Order No exist already!";
            $http_status = 401;

        }
        else
        {
            $http_status = 200;

            $courierid = $courier->id;
            $merchantid = $merchant->id;
            $pod = $body['transaction']['POD'];
            $pickup_handling = $body['transaction']['pickup_handling'];
            $delivery_handling = $body['transaction']['delivery_handling'];
            $delivery_cost = $body['transaction']['delivery_cost'];
            $transaction_datetime =  new \ExpressiveDate();
            $pre_auth = $body['transaction']['pre_auth'];

            if($pickup_handling == 'mercht-loc')
            {
                $pickup = json_encode($body['transaction']['pickup']);
            }
            else if ($pickup_handling == 'courier-loc' ) {
                $pickup = json_encode($body['transaction']['pickup']);
            }

            if($delivery_handling == 'to_customer')
            {
                $delivery = json_encode($body['transaction']['delivery']);
            } else if ($delivery_handling == 'customer_pickup') {

                $delivery = json_encode($body['transaction']['delivery']);
         
            }

            $transaction->transaction_id = is_null($body['transaction']['transaction_id']) || empty($body['transaction']['transaction_id']) ? 'SA'.time().rand(100,999) : $body['transaction']['transaction_id'];
            $transaction->transaction_datetime = $transaction_datetime;
            $transaction->pod_transaction = $pod;
            $transaction->preauth_transaction = $pre_auth;
            $transaction->merchant_id = $merchantid;
            $transaction->pickup_handling = $pickup_handling;
            $transaction->delivery_handling = $delivery_handling;
            $transaction->courier_id = $courier->id;
            $transaction->transaction_status = 0; //means it is new
            $transaction->pickup_details = $pickup;
            $transaction->delivery_details = $delivery;
            $transaction->delivery_cost = $delivery_cost;
            $transaction->channel = 'external';

            if(isset($body['transaction']['order_id'])) {
                $transaction->transaction_order_no = $body['transaction']['order_id'];
            }

            if($pod == 0 && $pre_auth == 0)
                $transaction->transaction_payment_type = "Prepaid";
            elseif($pod == 0 && $pre_auth == 1)
                $transaction->transaction_payment_type = "DOD";
            elseif ($pod == 1) {
                # code...
                $transaction->transaction_payment_type = "POD";
            }

            $items = $body['transaction']['orders']['items'];
            $count = count($body['transaction']['orders']['items']);

            $itemIDArray = [];
            for ($i=0;$i < $count; $i++)
            {
                $item = new \Dappur\Model\Items;
                $item->item_id = $this->generateID(12);
                $itemIDArray[$i] = $item->item_id;
                $item->transaction_id = $transaction->transaction_id;
                $item->item_name = $items[$i]['item_name'];
                $item->item_color = $items[$i]['item_color'];
                $item->item_qty = $items[$i]['item_quantity'];
                $item->item_weight = empty($items[$i]['item_weight']) ? 0 : $items[$i]['item_weight'];
                $item->item_cost = $items[$i]['item_cost'];
                $item->item_size = $items[$i]['item_size'];
                $item->delivery_status = 0;
                $item->save();
                $total_amount[$i] = $items[$i]['item_cost'] * $items[$i]['item_quantity'];
            }

            $transaction->transaction_total_amount = array_sum($total_amount);
            $transaction->save();

            for($j = 0; $j < count($itemIDArray); $j++) {
                $this->deliveryService->assignAllAgentsForPickup($courierid, $transaction->id, $itemIDArray[$j]);
            }

            $data['status'] = 0;
            $data['message'] = "A new delivery has been created";

        }

        return $response->withStatus($http_status)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

    }

    public function callPickUp(Request $request, Response $response)
    {

        $jwt = $request->getAttribute("jwt");
        $str = $jwt->sub;
        $pos = strpos($str,":");
        $agent_id = substr($str, $pos+1);

        $sent_data = $request->getParsedBody();

        if(is_null($sent_data) || isset($sent_data['transactionID']) == false || empty($sent_data['items']) ) {
            return $response->withStatus(400)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode([
                "status" => 0,
                "msg" => "invalid or missing data"
            ], JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        }

        $transaction_id = trim($sent_data['transactionID']);
        $deliveries = $sent_data['items'];

        $data_array = $this->deliveryEventService->handlePickupEvent($agent_id, $transaction_id, $deliveries);

        return $response->withStatus(200)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode(array($data_array), JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

    }


    private function netplus_callrecurrent(array $data) {
        $req = json_encode($data);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $req);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($req)));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: Application/json;charset=UTF-8"));
        curl_setopt($curl, CURLOPT_URL, "https://netpluspay.com/api/v1/recurrent");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        $j_result = json_decode($result);
        return $j_result;
    }

    public function callDropOff(Request $request, Response $response)
    {
        $jwt = $request->getAttribute("jwt");
        $str = $jwt->sub;
        $pos = strpos($str,":");
        $agent_id = substr($str, $pos+1);

        $sent_data = $request->getParsedBody();

        if(is_null($sent_data)) {
            return $response->withStatus(400)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode(["status" => 0, "msg" => 'Invalid or missing parameters'], JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        }

        $transaction_id = trim($sent_data["transactionID"]);
        $payment_type = trim($sent_data["paymentType"]);
        $deliveries = isset($sent_data['items']) ? $sent_data['items'] : null;

        $data_array = $this->deliveryEventService->handleDropoffEvent($transaction_id, $payment_type, $deliveries);

        if($data_array["status"] = 0) {
            return $response->withStatus(400)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data_array, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));           
        }

        return $response->withStatus(200)
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode($data_array, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

    }

    public function practise(Request $request, Response $response)
    {
        $transaction = new \Dappur\Model\Transactions;
        $transactions = $transaction->get();
        d($transaction->getTransactionItemCount());
    }

    // tarck order API start

    public function trackOrderApi(Request $request, Response $response){

        $getAttribute = $request->getParsedBody();

        $transaction_id = $getAttribute['transaction_id'];

        if(empty($transaction_id))
        {
            $http_status = 401;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "Transaction ID can't be empty";
            return $this->json($response, $data_sent, $http_status);
        }
       
        $item = new \Dappur\Model\Items;
        $itemDetail = $item->getItemsByTransactionFortracking($transaction_id, 'api');



        if(count($itemDetail) != 0){

          return $this->json($response, $itemDetail, 200);
         
         }else{

            $http_status = 201;
            $data_sent['status'] = 0;
            $data_sent['msg'] = "Transaction Id doesn't exist. Try again.";
            return $this->json($response, $data_sent, $http_status);
        }
    }


    public function isCardOnFile(Request $request, Response $response, $transaction_id)
    {
        $couriers = new \Dappur\Model\Courier;
        $transaction = new \Dappur\Model\Transactions;
        $courierid = $transaction->getTransactionDetails($transaction_id)['courier_id'];
        $card = $this->db->table('card_on_file')
        ->join('courier', 'card_on_file.courier_id', '=', 'courier.id')
        ->where('courier.id', '=',$courierid)
        ->where('card_on_file.status','=',1)
        ->get();

        if(count($card) > 0)
        {
           $http_status = 200;
           $sent_data = [ 'cardOnFile' => "yes" ];
        }
        else
        {
           $http_status = 404;
           $sent_data = [ 'cardOnFile' => "no" ];
        }
           return $response->withStatus($http_status)
               ->withHeader("Content-Type", "application/json")
               ->write( json_encode($sent_data), JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT );
    }


    public function getToken(Request $request, Response $response)
    {
      //$username = ($request->getParsedBody());
      if(filter_var($request->getParam('login'), FILTER_VALIDATE_EMAIL)) {
          $credentials = [
              'email' => $request->getParam('login'),
              'password' => $request->getParam('password')
          ];
      } else {
          $credentials = [
              'username' => $request->getParam('login'),
              'password' => $request->getParam('password')
          ];
      }
      
      $remember = false;
      $result = $this->auth->authenticate($credentials, $remember);
    

      if($result == false)
      {
          $http_status = 401;
          $data = ["status" => 0, "message" => 'invalid logon '];
          $msg = 'invalid user details';    
      } else {
        $loggedUser = $this->auth->check();
        $userID = $loggedUser->id;
        $msg = "Login Succesful";
        //build token here
        $http_status = 200;
        $user_id = $this->token( $request->getParam( 'login' ), $request->getParam( 'password' ), $userID);
        $data = ["status" => 1, "message" => $msg, "token" => $user_id];
      }

      return $response->withStatus($http_status)
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }


    public function getItemDetail(Request $request, Response $response, $productID)
    {
        $http_status = 200;
        $item  = new \Dappur\Model\Items;
        $transaction = new \Dappur\Model\Transactions;
        $merchant = new \Dappur\Model\Merchants;
        $items = $item->getItemDetails($productID);

        $transaction_id = $items->toArray()['transaction_id'];
        $merchant_id = $transaction->getTransactionDetails($transaction_id)->toArray()['merchant_id'];
        //d($merchant->getMerchantName($merchant_id));
        $sent_data = ["item_cost" => $items->toArray()['item_cost'], "item_qty" => $items->toArray()['item_qty'], "merchant_name" =>$merchant->getMerchantName($merchant_id)];

        return $response->withStatus($http_status)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($sent_data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }

    public function getTokenForRateCard(Request $request, Response $response)
    {
      //$username = ($request->getParsedBody());
      if(filter_var($request->getParam('login'), FILTER_VALIDATE_EMAIL)) {
          $credentials = [
              'email' => $request->getParam('login'),
              'password' => $request->getParam('password')
          ];
      } else {
          $credentials = [
              'username' => $request->getParam('login'),
              'password' => $request->getParam('password')
          ];
      }
      
      //d($credentials);
      //die;

      $remember = FALSE;
      $result = $this->auth->authenticate($credentials, $remember);

      $loggedUser = $this->auth->check();
      $userID = $loggedUser->id;
      $user_role = $loggedUser->getRoles();

      $roleid = json_decode(json_encode($user_role, true));

      if($roleid[0]->pivot->role_id != 9){

          $http_status = 401;
          $data = ["status" => 0, "message" => 'Not allowed, merchants only'];

          return $response->withStatus($http_status)
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
      }
    

      if($result == false)
      {
          $http_status = 401;
          $data = ["status" => 0, "message" => 'invalid logon '];
          $msg = 'invalid user details';    
      }
      else
      {
        $loggedUser = $this->auth->check();
        $userID = $loggedUser->id;
        $msg = "Login Succesful";
        //build token here
        $http_status = 200;
        $user_id = $this->token( $request->getParam( 'login' ), $request->getParam( 'password' ), $userID);
        $data = ["status" => 1, "message" => $msg, "token" => $user_id, "userID" => $userID];
      }

      return $response->withStatus($http_status)
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }

    public function rateCard(Request $request, Response $response)
    {
      $userID = $request->getParams();
      $CourierMerchants = new \Dappur\Model\CourierMerchants;

      $courier = $CourierMerchants->getCourier($userID);

       if($courier->count() == 0 )
        {
            $data['status'] = 0;
            $data['message'] = "No ratecard found";
            $http_status = 401;
            return $response->withStatus($http_status)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        }

            $data['status'] = 1;
            $data['message'] = "No ratecard found";
            $data['data'] = $courier;
            $http_status = 200;


        return $response->withStatus($http_status)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

    }

    public function leadGenerator(Request $request, Response $response)  {

        $name = $request->getParam('name');
        $email = $request->getParam('email');
        $message = $request->getParam('message');

        if(!isset($name) || !isset($email) || !isset($message)) {
            $data = [
                "success" => false,
                "message" => "Unable to send mesasge."
            ];
            return $response->withStatus(400)
                ->withHeader("Content-Type", "application/json")
                ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        }

        //send mail if succesful
		$template_def = 'App/leadMail.twig';
		$template = $this->view->getEnvironment()->loadTemplate($template_def);
		$parameters  = array(
            'name' => $name,
            'email' => $email,
            'message' => $message
		);

        $email_body  = $template->renderBlock('main',   $parameters);
		$this->mailer("saddle@netplusadvisory.com","Send Package App",'Lead Notification',$email_body);
        $data = [
            "success" => true,
            "message" => "Successfully notified the team. We will be in touch shortly."
        ];

        return $response->withStatus(200)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)); 

    }

    public function logSaddleLiteCashTransaction(Request $request, Response $response) {

        $body = $request->getParsedBody();

        $userID = $this->getUserId($request->getAttribute("jwt"));

        $http_status = 400;

        $courier = $this->accountsService->getCourierForAgent($userID);

        if(is_null($courier)) {

            return $response->withStatus($http_status)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode([
                "status" => 0,
                "msg" => "agent error"
            ], JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

        }

        $body["transaction_type"] = "CASH";
        $body['courier_id'] = $courier->id;
        $body["agent_id"] = $userID;

        $res = $this->saddleLiteService->createNewSaddleLiteTransaction($body);
    
        if($res["status"] == 1)
            $http_status = 200;
        
        return $response->withStatus($http_status)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($res, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
 

    }

    public function logSaddleLiteCardTransaction(Request $request, Response $response) {

        $body = $request->getParsedBody();
        $userID = $this->getUserId($request->getAttribute("jwt"));

        $http_status = 400;

        $courier = $this->accountsService->getCourierForAgent($userID);

        if(is_null($courier)) {

            return $response->withStatus($http_status)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode([
                "status" => 0,
                "msg" => "agent error"
            ], JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

        }

        $body["transaction_type"] = "CARD";
        $body['courier_id'] = $courier->id;
        $body["agent_id"] = $userID;

        // echo json_encode($body);
        // return;

        $res = $this->saddleLiteService->createNewSaddleLiteTransaction($body);

        if($res["status"] == 1)
            $http_status = 200;

        return $response->withStatus($http_status)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($res, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)); 

    }

    private function getUserId($jwt) {

        $str = $jwt->sub;
        $pos = strpos($str,":");
        $agent_id = substr($str, $pos+1);

        return $agent_id;

    }

    public function logAgentDeposit(Request $request, Response $response) {

        $body = $request->getParsedBody();
        $agent_id = $this->getUserId($request->getAttribute("jwt"));

        $status = $this->saddleLiteService->logAgentDeposit($agent_id, $body["amount"], $body["slip_number"]);

        if($status == false) {
            return $response->withStatus(400)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode([
                "status" => 0,
                "msg" => "unable to log deposit"
            ], JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));         
        }

        return $response->withStatus(200)
        ->withHeader("Content-Type", "application/json")
        ->write(json_encode([
            "status" => 1,
            "msg" => "successfully logged deposit"
        ], JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

    }

}
