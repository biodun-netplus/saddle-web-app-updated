<?php

namespace Dappur\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Cartalyst\Sentinel\Users\EloquentUser as EloquentUser;
use Respect\Validation\Validator as V;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;
use Dappur\Model\Status;
use Dappur\Model\Transactions;
use Dappur\Services\AccountsService;
use Dappur\Services\NetplusPayService;
use Dappur\Services\SaddleLiteService;
use Dappur\Services\MetricService;
use Dappur\Services\CourierService;
use Dappur\Services\DeliveryService;
use Dappur\Services\DataTableService;
use Dappur\Services\SettlementService;
use Dappur\Services\EmailNotificationService;
use Dappur\Services\DeliveryEventService;

class AppController extends Controller
{

	protected $accountsService;
	protected $saddleLiteService;
	protected $metricService;
	protected $deliveryService;
	protected $settlementService;
	protected $emailNotificationService;

	public function __construct($container) {

		parent::__construct($container);
		$this->accountsService = new AccountsService($container);
		$this->saddleLiteService = new SaddleLiteService($container);
		$this->metricService = new MetricService($container);
		$this->courierService = new CourierService($container);
		$this->deliveryService = new DeliveryService($container);
		$this->settlementService = new SettlementService($container);
		$this->emailNotificationService = new EmailNotificationService(
			$container,
			$this->config['dispatch_email'],
			$this->config['dispatch_email_server'],
			$this->config['dispatch_email_port'],
			$this->config['dispatch_email_password']);
		$this->deliveryEventService = new DeliveryEventService($container,$this->emailNotificationService);

	}

	public function home(Request $request, Response $response)
	{

		$getParams = $request->getParams();

		if(isset($getParams['transaction_id'])){

			$item = new \Dappur\Model\Items;
			$itemDetail = $item->getItemsByTransactionFortracking($getParams['transaction_id'], 'web');

			if(count($itemDetail) != 0){
				return $this->view->render($response, 'App/trackingResult.twig',
					['row' => $itemDetail, 'transaction_id' => $getParams['transaction_id']]);
			}else{
				return $this->view->render($response, 'App/home.twig',
					['msg' => "Transaction Id doesn't exist. Try again."]);
			}

		}


		$loggedUser = $this->auth->check();
		
		if ($loggedUser == false)
			return $this->view->render($response, 'App/home.twig');
		
	   
		if ($this->auth->inRole('admin'))
			return $this->redirect($response, 'dashboard');
		else
			return $this->redirect($response,'user-home');
	}

	public function array_to_csv_download2($array, $filename = "export.csv", $delimiter=";") {
	    header('Content-Type: application/csv');
	    header('Content-Disposition: attachment; filename="'.$filename.'";');

	    // open the "output" stream
	    // see http://www.php.net/manual/en/wrappers.php.php#refsect2-wrappers.php-unknown-unknown-unknown-descriptioq
	    $f = fopen('php://output', 'w');

	    foreach ($array as $line) {
	        fputcsv($f, $line, $delimiter);
	    }
	}

	public function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
    // open raw memory as file so no temp files needed, you might run out of memory though
	    $f = fopen('php://memory', 'w'); 
	    // loop over the input array
	    foreach ($array as $line) { 
	        // generate csv lines from the inner arrays
	        fputcsv($f, $line, $delimiter); 
	    }
	    // reset the file pointer to the start of the file
	    fseek($f, 0);
	    // tell the browser it's going to be a csv file
	    header('Content-Type: application/csv');
	    // tell the browser we want to save it instead of displaying it
	    header('Content-Disposition: attachment; filename="'.$filename.'";');
	    // make php send the generated csv lines to the browser
	    fpassthru($f);
	}   

	public function report(Request $request, Response $response)
	{
		if ($request->isPost()){
			
			$requestparams = $request->getParams();
			$transactions = new \Dappur\Model\Transactions;
			$usertype = new \Dappur\Model\UserType;
			$loggedUser = $this->auth->check();
			$userID = $loggedUser->id;

			 $typeID = $usertype->getUserTypeID($userID);
			
			 $from = new \ExpressiveDate($requestparams['from']);
			 $to = new \ExpressiveDate($requestparams['to']);


			try
			{
			$tr=$transactions->downloadCSV($typeID,$from->getDate(), $to->getDate());
			print_r(json_encode($tr));
			
			//$this->array_to_csv_download($tr, "numbers.csv");

			/*--------------------------------------------------*/
			$fileName = 'Delivery-Report.csv';
 
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header('Content-Description: File Transfer');
			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename={$fileName}");
			header("Expires: 0");
			header("Pragma: public");

			$fh = @fopen( 'php://output', 'w' );

			$headerDisplayed = false;

			foreach ( $tr as $data ) {
			    // Add a header row if it hasn't been added yet
			    if ( !$headerDisplayed ) {
			        // Use the keys from $data as the titles
			        fputcsv($fh, array_keys($data));
			        $headerDisplayed = true;
			    }
			 
			    // Put the data into the stream
			    fputcsv($fh, $data);
			}
			// Close the file
			fclose($fh);
			// Make sure nothing else is sent, our file is done
			exit;
			/*----------------------------------------------------------------*/

			}
			catch(Exception $e)
			{
				 $this->flash('danger', $e->toString());
			}


			//return $this->redirect($response, 'delivery');
			
		}
		elseif($request->isGet())
		{
			return $this->view->render($response, 'App/report.twig');
		}
		
		
	}
	
	
	public function dashboard(Request $request, Response $response){
   
	   // echo "string";die;
		if (!$this->auth->inRole('admin'))
			return $this->redirect($response,'user-home');

		$couriers = new \Dappur\Model\Courier; 

		$merchants = new \Dappur\Model\Merchants;

		$delivery = new \Dappur\Model\Transactions;
		$users = new \Dappur\Model\Users;

		$date = new \ExpressiveDate;



		$status_data_count = array();
		$status_data_count['pending'] = $delivery->getTransactionCountByStatus(0);
		$status_data_count['delivered'] = $delivery->getTransactionCountByStatus(array(3, 301));
		//$status_data_count['delivered'] = $status_data_count['delivered'] + $delivery->getTransactionCountByStatus(301);
		$status_data_count['picked_up'] = $delivery->getTransactionCountByStatus(array(1,14,101,102,10,4,2,7));
		
		$status_data_count['returned'] = $delivery->getTransactionCountByStatus(5,6);
		$status_data_count['awaiting'] = $delivery->getTransactionCountByStatus(30);
		


		$latest_status_by_date = array();
		$latest_status_by_date['pending'] = $delivery->getLatestTransactionByStatus(0);
		$latest_status_by_date['delivered'] = $delivery->getLatestTransactionByStatus(3, 301);
		$latest_status_by_date['picked_up'] = $delivery->getLatestTransactionByStatus(1,14,101,102,10,4,2,7);
		$latest_status_by_date['returnded'] = $delivery->getLatestTransactionByStatus(5,6);
		$latest_status_by_date['awaiting'] = $delivery->getLatestTransactionByStatus(30);
		
		$transaction_value = array();
		$transaction_value['pending'] = $delivery->getTransactionValueByStatus( array(0));
		$transaction_value['delivered'] = $delivery->getTransactionValueByStatus(array(3,301));
		$transaction_value['picked_up'] = $delivery->getTransactionValueByStatus(array(1,14,101,102,10,4,2,7));
		$transaction_value['returned'] = $delivery->getTransactionValueByStatus(array(5,6));
		$transaction_value['awaiting'] = $delivery->getTransactionValueByStatus(array(30));


		return $this->view->render($response, 'App/dashboard.twig',[
			"no_of_couriers"=> $couriers->getCourierCount(),
			"no_of_merchants"=>$merchants->getMerchantCount(),
			"no_of_agents" => 0,
			"courier"=>$couriers,
			"deliveries" => $delivery->getTransactions(),
			"users" => $users->getUsers(),
			"date" =>$date,
			'transaction_value' => $transaction_value,
			"latest" => $latest_status_by_date,
			"status" => $status_data_count
		]);}

	public function assignAgent(Request $request, Response $response)
	{
		
		$req = $request->getParams();
		
		$transaction_id = $req['transaction_id'];
		$delivery_status = $req['delivery_status'];
		$agent_id = $req['agent_assigned'];
		$assignment = $req['assignment'];

		$items = $this->deliveryService->getItemsForTransaction($transaction_id);

		$transactionId = Transactions::where('transaction_id','=', $transaction_id)->value('id');

		if(is_null($items) || empty($items)) {
			return $this->redirect($response,'delivery-detail',['transaction_id'=>$transaction_id]);
		}

		if($assignment == 'dropoff') {
			for($i = 0; $i < count($items); $i++) {
				$this->deliveryService->assignAgentForDropOff($agent_id, $transactionId, $items[$i]->item_id);
			}
		} else if($assignment == 'pickup') {
			for($i = 0; $i < count($items); $i++) {
				$this->deliveryService->assignAgentForPickup($agent_id, $transactionId, $items[$i]->item_id);
			}
		}
		return $this->redirect($response,'delivery-detail',['transaction_id'=>$transaction_id]);

	}

	public function cardOnFile(Request $request, Response $response)
	{

		$courier = $this->models['courier'];
		$usertype = new \Dappur\Model\UserType;
		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;

		$is_agent = $this->accountsService->isAgent($userID);
		$courier = $this->accountsService->getCourierForUser($userID);

		$data = array();
		$data['total_amount'] = 1.00;
		$data['merchant_id'] = isset($courier->netpluspay_mid) ? $courier->netpluspay_mid : $this->config['liveid'];
		$data['narration'] = 'Saddle Card On File';
		$order_id = $this->generateID(12);
		$data['order_id'] =  $order_id;
		$data['return_url'] = "https://".$this->config['domain']."/netplus_response";

		$typeID = $usertype->getUserTypeID($userID);
		$courier_details = ($courier->find($typeID));

		if($is_agent) {

			$this->logger->debug("agent card on file request");
			$data['full_name'] = $loggedUser->first_name . ' ' . $loggedUser->last_name;
			$data['email'] = $loggedUser->email;
			$data['role'] = "Agent";

		} else {

			$this->logger->debug("courier card on file request");

			$data['full_name'] = $courier_details['courier_name'];
			$data['email'] = trim($courier_details['courier_email']);
			$data['role'] = "Courier";

		}

		return $this->view->render($response, 'App/card.twig',["info"=>$data]);

	}

	public function signup(Request $request, Response $response) {

		if ($request->isPost()) {

				$requestParams = $request->getParams();
				$courier = new \Dappur\Model\Courier;

				$courier_name = trim($requestParams['courier_name']);
				$courier_email = trim($requestParams['courier_email']);
				$courier_website = trim($requestParams['courier_website']);
				$courier_address = trim($requestParams['courier_address']);

				$email_chk = $courier->getCourierByEmail($courier_email);
				$name_chk = $courier->getCourierByName($courier_name);

				if(is_null($email_chk) || is_null($name_chk)) {

					$courier_id = $this->generateID(6);
					$courier->courier_id =  $courier_id;
					$courier->courier_name = $courier_name;
					$courier->courier_email = $courier_email;
					$courier->courier_website = $courier_website;
					$courier->status = "1";
					$courier->address = $courier_address;

					$courier->save();

					//user has elected to put card on file
					if(isset($requestParams['cardOnfile']))
					{

					  $data = array();
					  $data['full_name'] = $courier_name;
					  $data['email'] = $courier_email;
					  $data['total_amount'] = 1.00;
					
					  $data['merchant_id'] = $this->config['liveid'];
					  $data['narration'] = 'Saddle Courier Card On File';
					  $order_id = $this->generateID(12);
					  $data['order_id'] =  $order_id;
					  $data['return_url'] = "https://".$this->config['domain']."/netplus_response";

					  $card = new \Dappur\Model\Card;
					  $card->courier_id = $courier_id;
					  $card->order_id = $order_id;

					  //remember to change this to check if the payment was not succesful
					  $card->status = 1;
					  //var_dump('saving card');
					  //var_dump(json_encode((array)$card));
					  $result = $card->save();
					  //var_dump('card save');

					  return $this->view->render($response, 'App/card.twig',["info"=>$data]);

					} else {
						//send mail if succesful
						$message_body = 'App/signupMail.twig';
						$template = $this->view->getEnvironment()->loadTemplate($message_body);
							$parameters  = array(
								'customer' => $data['full_name']
							);
						$company  = $template->renderBlock('company',   $parameters);

						$this->mailer($courier_email,$courier_name,'Registration',$company);
						$this->flash('success', 'Your Signup was successful');
						return $this->redirect($response,'login');
					}
				}
				else
				{
					$this->flash('danger', 'Courier has registerd already');

				}

		}

		//page load first - GET
		return $this->view->render($response, 'App/signup.twig');
	}

	public function netplus_response(Request $request, Response $response)
	{

		$requestParams = $request->getParams();

		$usertype = new \Dappur\Model\UserType;
		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;
		$typeID = $usertype->getUserTypeID($userID);

		$this->logger->debug("nesplus response",["response" => $requestParams]);

		if($requestParams['code'] != '00') {
			$this->flash('danger', "Unable to complete transaction");
			return $this->redirect($response, 'card');
		}

		$this->accountsService->registerCardOnFile($typeID,$userID);

		$message_body = 'App/signupMail.twig';
		$template = $this->view->getEnvironment()->loadTemplate($message_body);
		$parameters  = array(
			'customer' => $loggedUser->first_name . ' ' . $loggedUser->last_name
		);


		$this->flash('success', 'Automatic merchant settlement was enabled successful');
		return $this->redirect($response, 'user-home');
	}

	public function deliveryDetails(Request $request, Response $response,$transaction_id)
	{
		$loggedUser = $this->auth->check();       
		 $userID = $loggedUser->id;
		 $usertype = new \Dappur\Model\UserType;
		 $type =  $usertype->getUserType($userID);
		 $typeID = $usertype->getUserTypeID($userID);

			if (!$this->auth->hasAccess('delivery.view')) {
					$this->flash('danger', 'You do not have permission to view users.');
					$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the users page", "user_id" => $loggedUser['id']));
					return $this->redirect($response, 'dashboard');

				}

			$delivery = $this->models['transaction'];
			$item = $this->models['items'];
			$courier = $this->models['courier'];
			$merchant = $this->models['merchant'];
			$agent = new \Dappur\Model\Users;
			$usertype = new \Dappur\Model\UserType;
			$status = $this->models['status'];

			$users = $agent->getUsers();
			
			//d(json_encode($status->getStatus()->flatten()));
			//die;
			
			for ($i=0; $i < count($users); $i++ )
			{
				$role = $users[$i]->getRoles()[0]['name'];
				//echo $role;
				if ($role == "Agent")
					{
						if( $typeID == $usertype->getUserTypeID($users[$i]->id) )
							{
								$agents[] = $users[$i];
			
							}
						
						
					}
				}

			$transaction = $delivery->getTransactionDetails($transaction_id);

			
			if($transaction->pickup_handling == 'courier-loc')
			{
				$location_id = $transaction->pickup_details;
				$courierlocation = $this->models['courier-location'];
				$pickup_details = $courierlocation->find(substr($location_id,1,-1));
			}

			//means the courier would pick up at Merchant
			if($transaction->pickup_handling == 'mercht-loc')
			{
				$pickup_details = json_decode($transaction->pickup_details);
			}

			//means item would be deliver to customer
			if($transaction->delivery_handling == "to_customer")
			{
				$delivery_details = json_decode($transaction->delivery_details);
			}

			//means customer would pick up either at courier or merchant
			if($transaction->delivery_handling == "customer_pickup")
			{
				$location_id = $transaction->delivery_details;
				$courierlocation = $this->models['courier-location'];
				$delivery_details = $courierlocation->find(substr($location_id,1,-1));
			}

			//d($pickup_details);
			//d($delivery_details);
			//$status[]
			$st = $status->getStatus();
			//echo $st[0]->status;
			//die;
			//echo count($status->getStatus());
			for ($i=0; $i < count($status->getStatus()); $i++ ){
				$situation[] = $st[$i]->status;
			}
			//d($situation);
			//die;

			$items = $item->getItemsByTransaction($transaction_id);
			return $this->view->render($response, 'App/delivery-detail.twig',
			['items'=>$items,
			'courier_name' => $courier->getCourierName($transaction->courier_id),
			'merchant' => $merchant->find($transaction->merchant_id),
			'pickup_details' => $pickup_details,
			'delivery_details'=> $delivery_details,
			'agents' =>$agents,
			'status' =>$status->getStatus(),
			'state' => json_encode($situation),
			'transaction'=>$transaction
			]);
		}

	public function userHome(Request $request, Response $response)
	{
		
		
		$usertype = new \Dappur\Model\UserType;
		$transactions = new \Dappur\Model\Transactions;
		$courier = new \Dappur\Model\Courier;
		$merchant = new \Dappur\Model\Merchants;
		$date = new \ExpressiveDate;
		$merchantCourier = new \Dappur\Model\CourierMerchants;

		$loggedUser = $this->auth->check();

		//d($loggedUser);
		
		//die;
		$userID = $loggedUser->id;
		$token = $this->token($loggedUser['username'], $loggedUser['password'],$userID);

		$type =  $usertype->getUserType($userID);
		$typeID = $usertype->getUserTypeID($userID);
		$status_data_count = array();

		if($type == "Courier")
		{

			$userCompany = $usertype->getUserTypeName($userID);
			$delivery = $transactions->getTransactionsByCourier($typeID);

			//$returned = [5,6];
			$status_data_count['returned'] = $transactions->getCourierTransactionCountByStatus(array(5,6), $typeID);
			$status_data_count['pending'] = $transactions->getCourierTransactionCountByStatus(array(0), $typeID);
			$status_data_count['delivered'] = $transactions->getCourierTransactionCountByStatus(array(3,301), $typeID);
			//$status_data_count['delivered'] = $status_data_count['delivered']
			//else if(data == 1 || data == 14 || data == 101 || data == 10 || data == 4 || data == 2 || data == 7)
			//1,2,4,14,101,102, 10, 11
			$status_data_count['picked_up'] = $transactions->getCourierTransactionCountByStatus( array(1,14,101,102,10,4,2,7), $typeID);

			
			
			$status_data_count['awaiting'] = $transactions->getCourierTransactionCountByStatus(array(30), $typeID);


			$merchant_cnt = $merchantCourier->getCourierMerchants($typeID);
			$count_of_merchant =  $merchant_cnt->count();
			//d($status_data_count);
			//die;
			//$usertype_Name = \Dappur\Model\Courier::where('id',$usertype[0]->type_id)->get()[0]->courier_name;
		}

		if($type == "Merchant")
		{
			$userCompany = $usertype->getUserTypeName($userID);
			$delivery = $transactions->getTransactionsByMerchant($typeID);

			$status_data_count['pending'] = $transactions->getMerchantTransactionCountByStatus(array(0), $typeID);
			$status_data_count['delivered'] = $transactions->getMerchantTransactionCountByStatus(array(3,301), $typeID);
			//$status_data_count['delivered'] = $status_data_count['delivered']
			$status_data_count['picked_up'] = $transactions->getMerchantTransactionCountByStatus(array(1,14,101,102,10,4,2,7), $typeID);
			$status_data_count['returned'] = $transactions->getMerchantTransactionCountByStatus(array(5,6), $typeID);
			$status_data_count['awaiting'] = $transactions->getMerchantTransactionCountByStatus(array(30), $typeID);

		}


		$latest_status_by_date = array();
		$latest_status_by_date['pending'] = $transactions->getLatestTransactionByStatus(array(0)) ;
		$latest_status_by_date['delivered'] = $transactions->getLatestTransactionByStatus(array(3, 301));
		$latest_status_by_date['picked_up'] = $transactions->getLatestTransactionByStatus( array(1,14,101,102,10,4,2,7));
		$latest_status_by_date['returnded'] = $transactions->getLatestTransactionByStatus( array(5,6));
		$latest_status_by_date['awaiting'] = $transactions->getLatestTransactionByStatus(array(30));


		//d($status_data_count);
		//die;
		$transaction_value = array();
		$transaction_value['pending'] = $transactions->getTransactionValueByStatus(array(0),$typeID, $type);
		$transaction_value['delivered'] = $transactions->getTransactionValueByStatus(array(3,301),$typeID, $type);
	   
		$transaction_value['picked_up'] = $transactions->getTransactionValueByStatus(array(1,14,101,102,10,4,2,7),$typeID, $type);
		$transaction_value['returned'] = $transactions->getTransactionValueByStatus(array(5,6),$typeID, $type);
		$transaction_value['awaiting'] = $transactions->getTransactionValueByStatus(array(30),$typeID, $type);
		
		//die;
		return $this->view->render($response, 'App/userhome.twig',
		[
			"usertype" => $type,
			"userclass" =>$userCompany,
			"courier"=> $courier,
			'transaction_value' => $transaction_value,
			"merchant"=> $merchant,
			"status" => $status_data_count,
			"deliverys" => $delivery,
			"date" =>$date,
			"merchants" => $count_of_merchant,
			"latest" => $latest_status_by_date,
			"token" => $token
		]);
		
	}
	public function delivery(Request $request, Response $response)
	 {	
	 	   
		if ($this->auth->inRole('admin'))
		{

			$transaction = new \Dappur\Model\Transactions;
			$merchant = $this->models['merchant'];

		}

		$aa=[];

		return $this->view->render($response, 'App/delivery.twig',
		['result' => $aa]);

	}

	public function trackOrder(Request $request, Response $response){

		$getParams = $request->getParams();

		$item = new \Dappur\Model\Items;
		$itemDetail = $item->getItemsByTransactionFortracking($getParams['transaction_id']);



		if(count($itemDetail) != 0){
			

			return $this->view->render($response, 'App/trackingResult.twig',
				['row' => $itemDetail, 'transaction_id' => $getParams['transaction_id']]);
		}else{
			return $this->view->render($response, 'App/home.twig',
				['msg' => "Transaction Id doesn't exist. Try again."]);
		}
		// print_r(count($itemDetail));die;
		// mail('palash@geeksperhour.com', 'dsafa', print_r($_REQUEST, true));
		// var_dump($itemDetail);die;
		
	}

	public function ManageDeliveryByDate(Request $request, Response $response){
		
		
		if ($request->isPost()) {

			$requestParams = $request->getParams();
				   
			if ($this->auth->inRole('admin'))
			{
				$transaction = new \Dappur\Model\Transactions;
				$transactions = $transaction->orderBy('updated_at','desc')->get();
				$merchant = $this->models['merchant'];
			}

			$aa=$this->deliveryListForDateRange($requestParams, $request);
			return $this->view->render($response, 'App/manageDeliveryByDate.twig',
				['result' => $aa]);
			}

			return $this->view->render($response, 'App/manageDeliveryByDate.twig');
	}

	public function deliveryListForDateRange($dateRange,$request){


		$getParams = $request->getParams();
		
		
		$transaction = new \Dappur\Model\Transactions;

		//print_r($getParams);
		//print_r($getParams['columns'][0]['search']['value']);
		//die;
		//$search_value[] = $getParams['columns'][0]['search']['value'];

		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;
		$usertype = new \Dappur\Model\UserType;
		$type =  $usertype->getUserType($userID);
		$typeID = $usertype->getUserTypeID($userID);
		
		$search_value = $getParams['search']['value'];
		$draw = $getParams['draw'];
		$length = $getParams['length'];
		$start = $getParams['start'];

	   
		if($type == "Courier")
		{
			
			$transactions = $transaction->getCourierTransactionsForDateRange($typeID,$dateRange);
			// print_r($transactions);die;
		}

		elseif($type == "Merchant")
		{

			$transactions = $transaction->getMerchantsTransactionsForDateRange($typeID,$dateRange);
		}
		elseif($type = 'admin'){

			$transactions = $transaction->getAllTransactionsForDateRange($dateRange);
		 //print_r($transactions);
		}
		// print_r(count($transactions));die;
		
		
		

		$result = $transactions['total']->forPage($draw,$length)->flatten();
		
			

		
		/*$data = ' { 
		"draw":'.$draw.',
		"data":'. $result .',
		"recordsTotal":'. $transactions['total_count'].',
		"recordsFiltered":'.$transactions['total_filtered'].'
		}';
	   
		
		echo $data;*/
		return $result;

	
	}

	//custom function for rate card created by courier-admin
    public function RateCard(Request $request, Response $response)
	 {

       	if($role == "courier-admin" || $role == "courier-user")
		{

		  	 $usertype = new \Dappur\Model\UserType;
			 $courierName =  $usertype->getUserTypeName($userID);
			
			 $courierList['id'] = $courierid;
			 $courierList['courier_name'] = $courierName;

			 $merchantsList = $courier_merchant->getCourierMerchants($courierid);
		}

		$loggedUser = $this->auth->check();       
		$userID = $loggedUser->id;
		$usertype = new \Dappur\Model\UserType;
		$courierid = $usertype->getUserTypeID($userID);
		$courierName =  $usertype->getUserTypeName($userID);
		if($courierid == 306)
		{
			$courierList['id'] = 30;
		}
		else
		{
			$courierList['id'] = $courierid;
		}
			    
		// echo  $courierList['id'];die;
		$courierList['courier_name'] = $courierName;

		if ($request->isPost()) {

			$requestParams = $request->getParams();
			$state_from = $requestParams['customer_state'];
			$state_to = $requestParams['customer_state1'];
			$lga_from = $requestParams['customer_lga'];
			$lga_to = $requestParams['customer_lga1'];
			$weight = $requestParams['weight'];	
			$Extra1 = $requestParams['Extra'];
			//$Extra = str_replace('N', ' ', $Extra1);
			$price = $requestParams['price'];
			 
			// echo $price = $requestParams['price'];die;
		 

			// if($requestParams['price1'])
			// {
			// 	$price = $requestParams['price1'];
 
			// }
			// else
			// {
			// 	$price = $requestParams['price'];
  
			// }
			// if($requestParams['weight1']){
			// 	$weight = $requestParams['weight1'];
			// }
			// else
			// {
			// 	$weight = $requestParams['weight'];
			// }
			// $price = $requestParams['price1'];
			// $weight = $requestParams['weight1'];
			// $price = $requestParams['price'];
			
			

			$result = $this->db->table('rate_card')->insert(
			['client_id' =>  $userID, 'state_from' => $state_from, 'state_to'=>$state_to, 'lga_from'=>$lga_from,'lga_to'=>$lga_to, 'weight'=> $weight,'price'=> $price,'additionalPrice'=> $Extra1, 'courierNameId'  => $courierList['id'] ]);
			
			if(!empty($result)){
				$this->flash('success','price has been added successfully.');
			}


		}

		    $courierid = $courierList['id'];

		    $ratecardshow = new \Dappur\Model\RateCard;
		    $showRecord = $ratecardshow->showRecord($courierid);
		// $aa=$this->deliveryListNew($getParams);

		// print_r(['result' => $showRecord]);
		return $this->view->render($response, 'App/RateCard.twig', ['result' => $showRecord]);
	 }
		//end of custom function for rate card created by courier-admin



			//manuall entry
	public function deliveryAdd(Request $request, Response $response)
	{

		if (!$this->auth->hasAccess('delivery.add')) 
		
		{

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to add delivery.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the users page", "user_id" => $loggedUser->id));
			return $this->redirect($response, 'dashboard');

		}
			  
			  

			  $merchantId = '';
			  $transaction = new \Dappur\Model\Transactions;
			  $couriers = new \Dappur\Model\Courier;
			  $merchants = new \Dappur\Model\Merchants;
			  $courier_location = new \Dappur\Model\CourierLocation;
			  $courier_merchant = new \Dappur\Model\CourierMerchants;
			  $RateCard = new \Dappur\Model\RateCard; //for showing ratecard

			  $userAccess = $this->userAccess;
			  $loggedUser = $this->auth->check();

			  $userID = $loggedUser->id;
			  // mail('palash@geeksperhour.com', 'userID', print_r($userID, true));
			  $usertype = new \Dappur\Model\UserType;
			  
			  $role = $userAccess['roles'][0];

			  if ($role == "admin")
			  {
				$courierList = $couriers->getActiveCouriers();
				$merchantsList = $merchants->getActiveMerchants();
			  }
			  if($role == "courier-admin" || $role == "courier-user")
			  {
				 $courierName =  $usertype->getUserTypeName($userID);
				 $courierid = $usertype->getUserTypeID($userID);
				 $the_courier = $couriers->where("id",$courierid)->first();
				 $courierList['id'] = $the_courier->id;
				 $courierList['courier_name'] = $the_courier->courier_name;
				 $courierList['courier_id'] =  $the_courier->courier_id;
				 $merchantsList = $courier_merchant->getCourierMerchants($courierid);
				 $agents = $this->accountsService->getAgentsForCourier($courierid);
			
			  }

			  if($role == "merchant-user")
			  {

				 $merchantName =  $usertype->getUserTypeName($userID);
				 $merchantId = $usertype->getUserTypeID($userID);
				 $courierList = $courier_merchant->getMerchantsCourier($merchantId);
				 $merchantsList = $merchants->get()->where('id',$merchantId);
				 $transaction->channel = 'external';
                 
			  }
			
			   if ($request->isPost()) {
			   
				$loggonUser = $this->auth;
				$requestParams = $request->getParams();

				$order_no = $requestParams['order_no']; 
				$merchant_id = $requestParams['merchant_name'];  
				$order_exist = $transaction->getTransactionByOrderNo($order_no, $merchant_id);

				if(is_numeric($requestParams['calculatePrice']) == false)
				{
					$this->flash('error', 'Delivery cost is invalid');
					return $this->redirect($response, 'delivery-add');
				}

				$the_courier = $couriers->where("courier_id",$requestParams['courier_name'])->first();
				$courier_id = $the_courier->id;
				
				$pod = $requestParams['pay_on_delivery'];
				$pickup_handling = $requestParams['pickup_handling'];
				$delivery_handling = $requestParams['delivery_handling'];


				$items_name = $requestParams['items_name'];
				$items_size = $requestParams['items_size'];
				$items_qty = $requestParams['items_quantity'];
				$items_weight = $requestParams['items_weight'];
				$items_color = $requestParams['items_color'];
				$items_cost = $requestParams['items_cost'];
				$items_status = $requestParams['items_status'];
				$assigned_agent = (int) $requestParams['assign_agent'];

				$additonal_cost = 0;
				if( isset($requestParams['on_forwarding']))
				{
					$additonal_cost = 1000;
				}
				///die;

				try {

					if($pickup_handling == "mercht-loc") {
						
							$merchant_address = $requestParams['merchant_address'];
							$merchant_contactname = $requestParams['merchant_contactname'];
							$merchant_email = $requestParams['merchant_email'];
							$merchant_phone = $requestParams['merchant_phone'];
							$merchant_state = $requestParams['merchant_state'];
							$merchant_lga = $requestParams['merchant_lga'];

							$pickup_details = array();
							$pickup_details['merchant_address'] = $merchant_address;
							$pickup_details['merchant_contactname'] = $merchant_contactname;
							$pickup_details['merchant_email'] = $merchant_email;
							$pickup_details['merchant_phone'] = $merchant_phone;
							$pickup_details['merchant_state'] = $merchant_state;
							$pickup_details['merchant_lga'] = $merchant_lga;
							$pickup = json_encode($pickup_details);
					
					} elseif ($pickup_handling == "courier-loc") {
					
						$pickup = json_encode($requestParams['courier_location_handling']);
					
					}

					if($delivery_handling == "to_customer") {
					
						$delivery_details = array();
						$delivery_details['customer_name'] = $requestParams['customer_name'];
						$delivery_details['customer_email'] = $requestParams['customer_email'];
						$delivery_details['customer_phone'] = $requestParams['customer_phone'];
						$delivery_details['customer_address'] = $requestParams['customer_address'];
						$delivery_details['customer_lga'] = $requestParams['customer_lga'];
						$delivery_details['customer_state'] = $requestParams['customer_state'];
						$delivery = json_encode($delivery_details);
					
					}

					if($delivery_handling == "customer_pickup") {
						
						$delivery = json_encode($requestParams['delivery_location_handling']);
					
					}

					$transaction_id = 'SA'.time().rand(100,999);
					$transaction_datetime =  new \ExpressiveDate();

					if($role == "merchant-user" || $role == "admin")
					{

						$transaction->channel = "external";

					} else if ($role == "courier-admin" || $role == "courier-user") {

						$transaction->channel = "internal";

					}

					$transaction->transaction_id = $transaction_id;
					$transaction->transaction_datetime = $transaction_datetime;
					$transaction->pod_transaction = $pod;
					
					if($pod = 0)
					{
						$transaction->transaction_payment_type = 'Prepaid';
					}
					else
					{
						$transaction->transaction_payment_type = "POD";
					}

					$transaction->merchant_id = $merchant_id;
					$transaction->pickup_handling = $pickup_handling;
					$transaction->delivery_handling = $delivery_handling;
					$transaction->courier_id = $courier_id;
					$transaction->transaction_status = 0; //means it is new
					$transaction->pickup_details = $pickup;
					$transaction->delivery_details = $delivery;

					$count = count($items_name);
					$item_arr = [];
					for ($i=0;$i < $count; $i++)
					{
						$item = new \Dappur\Model\Items;
						$itemID = $this->generateID(12);

						$itemIDArray[$i] = $itemID;
						$item->item_id = $itemID;
						$item->transaction_id = $transaction_id;
						$item->item_name = $items_name[$i];
						$item->item_color = $items_color[$i];
						$item->item_qty = $items_qty[$i];
						$item->item_weight = $items_weight[$i];
						$item->item_cost = $items_cost[$i];
						$item->item_size = $items_size[$i];
						$item->delivery_status = $items_status[$i];
						$item->save();
						array_push($item_arr, $item);
						$total_weight[$i] += $items_weight[$i];
						$total_amount[$i] = $items_cost[$i] * $items_qty[$i];
					}

					$transaction->transaction_total_amount = array_sum($total_amount);
	

					if(is_null($requestParams['calculatePrice'])) {

						$transaction->delivery_cost = 600;

					} else {

						$transaction->delivery_cost = $requestParams['calculatePrice'];

					}
					
					$transaction->transaction_order_no = $order_no;
					$transaction->save();

					$userID = $loggedUser->id;

					$courier = $this->accountsService->getCourierForUser($userID);

					if(is_null($courier)) {
						$courier = $the_courier;
					}

					for($i = 0; $i < count($item_arr); $i++) {

						if($item_arr[$i]->delivery_status == Status::PENDING && ($assigned_agent == 0 || is_null($assigned_agent))) {
							
							$this->deliveryService->assignAllAgentsForPickup($courier->id, $transaction->id, $item_arr[$i]->item_id);
						
						} else if($item_arr[$i]->delivery_status == Status::PICKED_UP_FROM_MERCHANT && ($assigned_agent == 0 || is_null($assigned_agent)) ) {

							$this->deliveryService->assignAllAgentsForDropOff($courier->id, $transaction->id, $item_arr[$i]->item_id);

						} else if ($item_arr[$i]->delivery_status == Status::PENDING && $assigned_agent != 0 && isset($assigned_agent)) {

							$this->deliveryService->assignAgentForPickup($assigned_agent, $transaction->id, $item_arr[$i]->item_id);

						} else if ($item_arr[$i]->delivery_status == Status::PICKED_UP_FROM_MERCHANT && $assigned_agent != 0 && isset($assigned_agent)) {

							$this->deliveryService->assignAgentForDropOff($assigned_agent, $transaction->id, $item_arr[$i]->item_id);

						} 

					}
				
					$this->deliveryEventService->updateTransactionStatus($transaction->transaction_id);
					$this->flash('success', 'Delivery created successfully. Delivery Cost is: '. $requestParams['calculatePrice']);
					//exit;

				}
				catch(Exception $e)
				{
					$this->flash('danger', $e->toString());
				}


				//$this->logger->addInfo("Role updated successfully", array("role_id" => $role_id));
				return $this->redirect($response, 'delivery');
			  }

			  return $this->view->render($response, 'App/delivery-add.twig',[
				  'merchantId' => $merchantId,
				  'user_type' => $role,
				  "couriers" => $courierList,
				  "merchants" => $merchantsList,
				  "agents" => $agents,
				  "courier_location" => $courier_location ->where('courier_id','=',$courier_id) ->get()
			]);
	}

    public function deliveryPost(Request $request, Response $response)
	{
			  $merchantId = '';
			  $transaction = new \Dappur\Model\Transactions;
			  $couriers = new \Dappur\Model\Courier;
			  $merchants = new \Dappur\Model\Merchants;
			  $courier_location = new \Dappur\Model\CourierLocation;
			  $courier_merchant = new \Dappur\Model\CourierMerchants;

		$output = json_encode(
	            array(
	                'merchant_address' => '',
	                'merchant_contactname' => '',
	                'merchant_email' => '',
	                'merchant_phone' => '',
	                'merchant_state' => '',
	                'merchant_lga' => ''
	    ));

	    die($output);
	}	

	public function getMerchantDetail()
	{
	  $merchants = new \Dappur\Model\Merchants;

	  $data = $merchants->getMerchantByIdForAddDelivery($_POST['merchantId']);

	  echo json_encode($data);
	}		  

	public function deliveryEdit(Request $request, Response $response, $id)
	{
		$users = new \Dappur\Model\Users;
		$email_validator = new EmailValidator();

		if (!$this->auth->hasAccess('delivery.update')) {

			$loggedUser = $this->auth->check();
			$this->flash('danger', 'You do not have permission to edit delivery.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the edit delivery page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'admin-users');
		}

		if ($request->isXhr())  {
			
			$status = $this->models['status'];
			$item = $this->models['items'];
			$req = $request->getParams();

			$st = $req['status'];
			$status_id = $status->getStatusID($st);
			$qty = $req['quantity'];
			$item = \Dappur\Model\Items::find($id);
			$transactions = new \Dappur\Model\Transactions; 
			$transactionID = $item->transaction_id;
			$item->delivery_status = $status_id;
			$item->item_qty = $qty;
			$transaction = $transactions->getTransactionDetails($transactionID);
			 
			if($status_id == Status::PICKED_UP_FROM_MERCHANT) {

				$loggedUser = $this->auth->check();
				$userID = $loggedUser->id;
				
				$courier = $this->accountsService->getCourierForUser($userID);
				$this->deliveryService->assignAllAgentsForDropoff($courier->id, $transaction->id, $item->item_id);
				//$this->emailNotificationService->notifyCustomerOfPickup($transaction->transaction_id);

			}

			if($status_id == Status::DELIVERED_TO_CUSTOMER) {

				//$this->emailNotificationService->notifyCustomerOfDelivery($transaction->transaction_id);

			}

			if($status_id == Status::RETURNED_TO_MERCHANT) {

				//$this->emailNotificationService->notifyCustomerOfReturn($transaction->transaction_id);

			}

			if($status_id == Status::FAILED_DELIVERY_ATTEMPT) {

				//$this->emailNotificationService->notifyCustomerOfFailedDelivery($transaction->transaction_id);
			
			}

			if($status_id == Status::CANCELLED) {

				//$this->emailNotificationService->notifyCustomerOfFailedDelivery($transaction->transaction_id);
				$this->deliveryService->removeAllDeliveryAssignmentsForItem($item->item_id);

			}

			//$transaction->save();
			$item->save();
			$rslt = $this->deliveryEventService->updateTransactionStatus($transactionID);

			echo "Update Successful";
			 
		}
	
	}

	public function tracking(Request $request, Response $response)
	{
		

		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;
		$usertype = new \Dappur\Model\UserType;
		$type =  $usertype->getUserType($userID);
		$typeID = $usertype->getUserTypeID($userID);

		$usertype = new \Dappur\Model\UserType;

		$agentsAll = $usertype->getAgentForMap($type, $typeID);

		$data['agents'] = $agentsAll;

		return $this->view->render($response, 'App/map.twig',$data);
	}

	public function deliveryList(Request $req, Response $res)
	{

		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;

		$params = $req->getParams();

		$columns = $params['columns'];

		$search_value = $params['search']['value'];
		$draw = $params['draw'];
		$length = $params['length'];
		$start = $params['start'];
		$from_date = $params['from_date'];
		$to_date = $params['to_date'];
		$tx_status = $params['status'];

		$order_info = [
			"name" => $columns[$params["order"][0]['column']]["data"],
			"dir" => $params["order"][0]['dir']
		];
		$page = round($start / $length) + 1;

		if($this->accountsService->isAdmin($userID)) {

			$this->logger->debug("admin request");

			if(DataTableService::isSearchRequest($params)) {

				$transactions = $this->deliveryService->searchDeliveriesForAdmin($search_value, $order_info, $from_date, $to_date);
	
			} else {
	
				$transactions = $this->deliveryService->filterDeliveriesForAdmin($columns, $order_info, $from_date, $to_date, $tx_status);
	
			}

		} else if($this->accountsService->isCourierUserType($userID)) {

			$this->logger->debug("request courier usertype request");

			$courier = $this->accountsService->getCourierForUser($userID);
			$this->logger->debug("courier id = " . $courier->id);


			if(is_null($courier)) {
				$this->logger->debug("courer is nul");

				$data = ' {
					"draw":'.$draw.',
					"data": []' .',
					"recordsTotal": 0'.',
					"recordsFiltered": 0'.'
					}';
				echo $data;
				return;

			}

			if(DataTableService::isSearchRequest($params)) {

				$this->logger->debug("data table is search request");
				$this->logger->debug("calling search successful deliverys");

				$transactions = $this->deliveryService->searchDeliveriesForCourier($courier->id,$search_value,$order_info, $from_date, $to_date);
	
			} else {
	
				$this->logger->debug("data table is not search request");
				$this->logger->debug("filter successful deliverys for courier");


				$transactions = $this->deliveryService->filterDeliveriesForCourier($courier->id, $columns,$order_info, $from_date, $to_date, $tx_status);
	
			}

		} else if ($this->accountsService->isMerchantUserType($userID)) {

			$this->logger->debug("merchant user type request");

			$merchant = $this->accountsService->getMerchantForUser($userID);
			if(is_null($merchant)) {

				$data = ' { 
					"draw":'.$draw.',
					"data": []' .',
					"recordsTotal": 0'.',
					"recordsFiltered": 0'.'
					}';
				echo $data;
				return;

			}

			if(DataTableService::isSearchRequest($params)) {

				$this->logger->debug("data table is search request");
				$this->logger->debug("calling search successful deliverys");

				$transactions = $this->deliveryService->searchDeliveriesForMerchant($merchant->id,$search_value,$order_info, $from_date, $to_date);
	
			} else {

				$this->logger->debug("data table is not search request");
				$this->logger->debug("filter successful deliverys");
	
				$transactions = $this->deliveryService->filterDeliveriesForMerchant($merchant->id, $columns,$order_info, $from_date, $to_date, $tx_status);

			}

		}

		for($i = 0; $i < count($transactions['total']); $i++) {
			$this->deliveryService->populateCustomerAddressForTransaction($transactions['total'][$i]);
		}

		if(empty($transactions['total'])) {

			$result = $transactions['total'];

		} else if(!empty($search_value)) {

			$result = $transactions['total'];

		} else {

			$result = $transactions['total']->forPage($page,$length)->flatten();

		}

		$data = ' {
			"draw":'.$draw.',
			"data":'. json_encode($result) .',
			"recordsTotal":'. $transactions['total_count'].',
			"recordsFiltered":'.$transactions['total_filtered'].'
		}';
		
		echo $data;
		
	}

	public function settlementList(Request $request, Response $response)
	{
		$getParams = $request->getParams();
		
		$transaction = new \Dappur\Model\Transactions;

		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;
		$usertype = new \Dappur\Model\UserType;
		$type =  $usertype->getUserType($userID);
		$typeID = $usertype->getUserTypeID($userID);
		$columns = $getParams['columns'];
		
		$search_value = $getParams['search']['value'];
		$draw = $getParams['draw'];
		$length = $getParams['length'];
		$start = $getParams['start'];
		$page = round($start / $length) + 1;
	   
		if($type == "Courier")
		{
			$transactions = $transaction->getCourierTransactionsforSettlement($typeID,$columns,$search_value);
			// print_r($transactions);die;
		}
		elseif($type == "Merchant")
		{
			$transactions = $transaction->getMerchantsTransactionsforSettlement($typeID,$columns,$search_value);
		}
		elseif($type = 'admin'){

			$transactions = $transaction->getAllTransactionsforSettlement($columns,$search_value);
			// print_r(count($transactions));die;
		}

		
		
		if(!empty($search_value))
			$result = $transactions['total'];
		else
			$result = $transactions['total']->forPage($page,$length)->flatten();

		
		$data = ' { 
		"draw":'.$draw.',
		"data":'. $result .',
		"recordsTotal":'. $transactions['total_count'].',
		"recordsFiltered":'.$transactions['total_filtered'].'
		}';
	   
		
		echo $data;
		
	}

	public function deliveryListNew()
	{		

		// $getParams = $request->getParams();
		
		
		$transaction = new \Dappur\Model\Transactions;

		//print_r($getParams);
		//print_r($getParams['columns'][0]['search']['value']);
		//die;
		//$search_value[] = $getParams['columns'][0]['search']['value'];

		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;
		$usertype = new \Dappur\Model\UserType;
		$type =  $usertype->getUserType($userID);
		$typeID = $usertype->getUserTypeID($userID);
		
		$search_value = $getParams['search']['value'];
		$draw = $getParams['draw'];
		$length = $getParams['length'];
		$start = $getParams['start'];

		if($type == "Courier")
		{
			
			$transactions = $transaction->getCourierTransactions($typeID,$search_value,$start,$length);
			// print_r($transactions);die;
		}

		elseif($type == "Merchant")
		{

			$transactions = $transaction->getMerchantsTransactions($typeID,$search_value,$start,$length);
		}
		elseif($type = 'admin'){

			$transactions = $transaction->getAllTransactionsNew();
		 //print_r($transactions);
		}
		// print_r($transactions);die;
		
		
		if(!empty($search_value))
			$result = $transactions['total'];
		else
			$result = $transactions['total']->forPage($draw,$length)->flatten();

		
		/*$data = ' { 
		"draw":'.$draw.',
		"data":'. $result .',
		"recordsTotal":'. $transactions['total_count'].',
		"recordsFiltered":'.$transactions['total_filtered'].'
		}';
	   
		
		echo $data;*/
		return $result;

	}

	public function feed(Request $request, Response $response)
	{
		return $this->view->render($response, 'App/feed.twig');
	}

	public function b2bSettlements(Request $request, Response $response)
	{

	 	if(!$this->auth->hasAccess('settlement.view')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to view settlement.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the settlement page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');

		}

		return $this->view->render($response, 'App/b2b.twig');

	}

	public function b2bList(Request $req, Response $response) {

		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;

		if(!$this->auth->hasAccess('settlement.view')) {

			$data = ' { 
				"draw":'.$draw.',
				"data": []' .',
				"recordsTotal": 0'.',
				"recordsFiltered": 0'.'
				}';
			echo $data;
			return;

		}

		$params = $req->getParams();

		$columns = $params['columns'];

		$search_value = $params['search']['value'];
		$draw = $params['draw'];
		$length = $params['length'];
		$start = $params['start'];
		$from_date = $params['from_date'];
		$to_date = $params['to_date'];

		$order_info = [
			"name" => $columns[$params["order"][0]['column']]["data"],
			"dir" => $params["order"][0]['dir']
		];
		$page = round($start / $length) + 1;

		if($this->accountsService->isAdmin($userID)) {

			$this->logger->debug("admin request");

			if(DataTableService::isSearchRequest($params)) {

				$transactions = $this->deliveryService->searchSuccessfullB2BDeliveriesForAdmin($search_value, $order_info, $from_date, $to_date);
	
			} else {
	
				$transactions = $this->deliveryService->filterSuccessfullB2BDeliveriesForAdmin($columns, $order_info, $from_date, $to_date);
	
			}

		} else if($this->accountsService->isCourierUserType($userID)) {

			$this->logger->debug("request courier usertype request");

			$courier = $this->accountsService->getCourierForUser($userID);
			$this->logger->debug("courier id = " . $courier->id);


			if(is_null($courier)) {
				$this->logger->debug("courer is nul");

				$data = ' {
					"draw":'.$draw.',
					"data": []' .',
					"recordsTotal": 0'.',
					"recordsFiltered": 0'.'
					}';
				echo $data;
				return;

			}

			if(DataTableService::isSearchRequest($params)) {

				$this->logger->debug("data table is search request");
				$this->logger->debug("calling search successful deliverys");

				$transactions = $this->deliveryService->searchSuccessfullB2BDeliveriesForCourier($courier->id,$search_value,$order_info, $from_date, $to_date);
	
			} else {
	
				$this->logger->debug("data table is not search request");
				$this->logger->debug("filter successful deliverys for courier");


				$transactions = $this->deliveryService->filterSuccessfullB2BDeliveriesForCourier($courier->id, $columns,$order_info, $from_date, $to_date);
	
			}

		} else if ($this->accountsService->isMerchantUserType($userID)) {

			$this->logger->debug("merchant user type request");

			$merchant = $this->accountsService->getMerchantForUser($userID);
			if(is_null($merchant)) {

				$data = ' { 
					"draw":'.$draw.',
					"data": []' .',
					"recordsTotal": 0'.',
					"recordsFiltered": 0'.'
					}';

					echo $data;
				return;

			}

			if(DataTableService::isSearchRequest($params)) {

				$this->logger->debug("data table is search request");
				$this->logger->debug("calling search successful deliverys");

				$transactions = $this->deliveryService->searchSuccessfullB2BDeliveriesForMerchant($merchant->id,$search_value,$order_info, $from_date, $to_date);

			} else {

				$this->logger->debug("data table is not search request");
				$this->logger->debug("filter successful deliverys");

				$transactions = $this->deliveryService->filterSuccessfullB2BDeliveriesForMerchant($merchant->id, $columns,$order_info, $from_date, $to_date);

			}

		}

		for($i = 0; $i < count($transactions['total']); $i++) {
			$this->settlementService->populateSettlementForB2BTransaction($transactions['total'][$i]);
		}

		if(empty($transactions['total'])) {

			$result = $transactions['total'];

		} else if(!empty($search_value)) {

			$result = $transactions['total'];

		} else {

			$result = $transactions['total']->forPage($page,$length)->flatten();

		}

		$data = ' {
			"draw":'.$draw.',
			"data":'. json_encode($result) .',
			"recordsTotal":'. $transactions['total_count'].',
			"recordsFiltered":'.$transactions['total_filtered'].'
		}';
		
		echo $data;

	}

	public function deliveryBulk(Request $request, Response $response)
	{

		$csv = null;
		$couriers = null;
		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;
		$usertype = new \Dappur\Model\UserType;
		$type =  $usertype->getUserType($userID);
		$typeID = $usertype->getUserTypeID($userID);

		$couriers = new \Dappur\Model\Courier;
		$merchants = new \Dappur\Model\Merchants;
		$courier_location = new \Dappur\Model\CourierLocation;
		$courier_merchant = new \Dappur\Model\CourierMerchants;

		if ($request->isPost()) {

				$error = null;
				$csvfile = $request->getUploadedFiles()['file'];
				$csv = \League\Csv\Reader::createFromPath($csvfile->file);

				$csv->setOffset(1); //because we don't want to insert the header
				$requestParams = $request->getParams();
				if($requestParams['type'] == 'Courier') {
					$courier_id  = $typeID;
					$merchant_id = $requestParams['merchant_name'];
				} elseif ($requestParams['type'] == 'Merchant')
				{
					$courier_id  = $requestParams['courier_name'];
					$merchant_id = $typeID;
				}

				$courier = \Dappur\Model\Courier::where("id","=",$courier_id)->first();

				$func = function ($row) {
					return $row;
				};

				$results = $csv->fetch($func);
				$pickup_assignments = [];
				$dropoff_assignments = [];

				$weight_data = [];
				$agent_data = [];
				foreach ($results as $result) {

					$transaction = new \Dappur\Model\Transactions;
					if($result[1]==''){
						break;
					}

					$item_name = $result[0];
					$item_color = $result[1];
					$item_size = $result[2];
					$item_cost = (float)$result[3];
					$item_weight = (float)$result[4];
					$item_qty = (int)$result[5];
					$pod = $result[6];
					$payment_type = $result[7];
					$pickup_type = $result[8];
					$pickup_lga = $result[9];
					$pickup_state = $result[10];
					$pickup_address = $result[11];
					$pickup_contactname = $result[12];
					$pickup_contactnumber = $result[13];
					$pickup_contactemail = $result[14];
					$dropoff_type = $result[15];
					$delivery_lga = $result[16];
					$delivery_state = $result[17];
					$delivery_contactname = $result[18];
					$delivery_contactnumber = $result[19];
					$delivery_contactemail = $result[20];
					$delivery_address = $result[21];
					$status = (int)$result[22];
					$OnForwarding = $result[23];
					$order_no = $result[24];
					$transID = isset($result[25]) && empty($result[25])  == false ? $result[25] : 'SA'.time().rand(100,999); 
					$agent_username = isset($result[26])  && empty($result[26])  == false ? $result[26] : null;
					$delivery_cost_payment = isset($result[27])  && empty($result[27])  == false ? $result[27] : null;

					$additonal_cost = 0;
					if( isset($OnForwarding) && $OnForwarding == TRUE )
					{
						$additonal_cost = 1000;
					}

					$tran = $transaction->getTransactionDetails($transID);

					if(empty($item_size))
					{
						$item_size = 0;
					}

					if(empty($item_cost))
					{
						$item_cost = 0;
					}

					$pickup_details = array();
					$pickup_details['merchant_address'] = $pickup_address;
					$pickup_details['merchant_contactname'] = $pickup_contactname;
					$pickup_details['merchant_email'] = $pickup_contactemail;
					$pickup_details['merchant_phone'] = $pickup_contactnumber;
					$pickup_details['merchant_state'] = $pickup_state;
					$pickup_details['merchant_lga'] = $pickup_lga;
					$pickup = json_encode($pickup_details);
				   				   
					$delivery_details = array();
					$delivery_details['customer_name'] = $delivery_contactname;
					$delivery_details['customer_email'] = $delivery_contactemail;
					$delivery_details['customer_phone'] = $delivery_contactnumber;
					$delivery_details['customer_address'] = $delivery_address;
					$delivery_details['customer_lga'] =  $delivery_lga;
					$delivery_details['customer_state'] = $delivery_state;
					$this->logger->debug("delivery details ->", [$delivery_details]);
					$delivery = json_encode($delivery_details);
				
					$transaction_datetime =  new \ExpressiveDate();
					$transaction->transaction_id = $transID;
					$transaction->transaction_datetime = $transaction_datetime;
					$transaction->pod_transaction = $pod;
					$transaction->merchant_id = $merchant_id;
					$transaction->pickup_handling = $pickup_type;
					$transaction->delivery_handling = $dropoff_type;
					$transaction->courier_id = $courier_id;

					$transaction->transaction_status = $status;
					$transaction->pickup_details = $pickup;
					$transaction->delivery_details = $delivery;
					$transaction->transaction_total_amount = $item_cost * $item_qty;
					// $delivery_cost = $this->shippingPriceNew($delivery_state, $pickup_state, $weight, $courier_id, $pickup_lga, $delivery_lga, true);
					$transaction->delivery_cost = $delivery_cost + $additonal_cost;
					$transaction->transaction_order_no = $order_no;

					$item = new \Dappur\Model\Items;
					$item->item_id = $this->generateID(12);
					$item->transaction_id = $transID;
					$item->item_name = $item_name;
					$item->item_color = $item_color;
					$item->item_qty = $item_qty;

					$item->item_weight = $item_weight;
					$item->item_cost = $item_cost;
					$item->item_size = $item_size;

					$item->delivery_status = $status;

					if( is_null($tran) )
					{
						$transaction->save();
						$item->save();
					}
					else {
						$item->save();
					}

					if(is_null($agent_username) == false) {
						if(isset($agent_data[$transID]) == false) {
							$agent = $this->accountsService->getAgentsByUserName($agent_username);
		
							if(is_null($agent) == false) 
								$agent_data[$transID] = $agent->id;
						}
					}

					if(isset($weight_data[$transID])) {

						$weight_data[$transID]["weight"] = $weight_data[$transID]["weight"] + (float)$item->item_weight;
						$weight_data[$transID]["total_cost"] = $weight_data[$transID]["total_cost"] + ((float)$item_cost * (float)$item_qty);

					} else {

						$weight_data[$transID] = [
							"weight" => (float)$item->item_weight,
							"delivery_state" => $delivery_state,
							"pickup_state" => $pickup_state,
							"delivery_lga" => $delivery_lga,
							"pickup_lga" => $pickup_lga,
							"total_cost" => $item_cost * $item_qty,
							"no_charge" => isset($delivery_cost_payment) && ($delivery_cost_payment == 'no_charge')
						];

					}

					if($item->delivery_status == Status::PENDING) {

						$assignment = [
							"item_id" => $item->item_id,
							"transaction_id" => $transaction->id
						];

						if(isset($agent_data[$item->transaction_id])) {

							$assignment["agent_id"] = $agent_data[$item->transaction_id];

						}

						array_push($pickup_assignments,$assignment);

					}

					if($item->delivery_status == Status::PICKED_UP_FROM_MERCHANT) {

						$assignment = [
							"item_id" => $item->item_id,
							"transaction_id" => $transaction->id
						];

						if(isset($agent_data[$item->transaction_id])) {

							$assignment["agent_id"] = $agent_data[$item->transaction_id];

						}

						array_push($dropoff_assignments,$assignment);

					}

				};

				$this->logger->debug("weight data ->",$weight_data);

				if(empty($weight_data) == false) {

					foreach($weight_data as $trans_id => $data) {

						if($data["no_charge"]) {
							Transactions::where("transaction_id","=",$trans_id)->update(["delivery_cost" => 0 ]);
						} else {
							$delivery_cost = $this->shippingPriceNew($data["delivery_state"], $data["pickup_state"], $data["weight"], $courier->courier_id, $data["pickup_lga"], $data["delivery_lga"], true);
							Transactions::where("transaction_id","=",$trans_id)->update(["delivery_cost" => ($delivery_cost  + $additional_cost) ]);
						}
						Transactions::where("transaction_id","=",$trans_id)->update(["transaction_total_amount" => $data["total_cost"]]);
					}

				}

				if(empty($pickup_assignments) == false) {
					$this->deliveryService->assignAllAgentsForPickupBulk($courier_id, $pickup_assignments);
				}

				if(empty($dropoff_assignments) == false) {
					$this->deliveryService->assignAllAgentsForDropoffBulk($courier_id, $dropoff_assignments);
				}

				$this->flash('success', 'Delivery created successfully');

			} elseif($request->isGet()) {

			try {


				 if($type == 'Merchant')
				 {

					$couriers = $courier_merchant->getMerchantsCourier($typeID);

				 }
				 elseif ($type = 'Courier') {
					 # code...
					$couriers = $courier_merchant->getCourierMerchants($typeID);

				 }

				} catch (ModelNotFoundException $ex) {
				  // Error handling code
					d($ex);
				}


		}

		return $this->view->render($response, 'App/delivery-bulk.twig',[
			'csv'=>$csv,
			'couriers'=>$couriers,
			'type'=> $type

		]);
	}

	public function settlement(Request $request, Response $response)
	{
		// die('testing');
		if(!$this->auth->hasAccess('settlement.view')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to view settlement.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the settlement page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');

		}

		$payment_type = array();
		// $transaction = new \Dappur\Model\Transactions;
		$payment_type['prepaid'] = "Prepaid";
		$payment_type['cash'] = "Cash";
		$payment_type['card'] = "Card";
		$transaction = $this->models['transaction'];
		$courier = $this->models['courier'];
		$merchant = $this->models['merchant'];

		if ($this->auth->inRole('admin'))
		{
			$transactions = $transaction->limit(5000)->orderBy('updated_at','desc')->get();
		}
		else
		{
			$userAccess = $this->userAccess;
			$loggedUser = $this->auth->check();
			$userID = $loggedUser->id;
			$usertype = new \Dappur\Model\UserType;
			$type =  $usertype->getUserType($userID);
			$typeID = $usertype->getUserTypeID($userID);

			if($type == "Courier")
			{
				$transactions = $transaction->getTransactionsByCourierSettlement($typeID);
			}
			elseif($type == "Merchant")
			{

				$transactions = $transaction->getTransactionsByMerchantSettlement($typeID);
			}
	
		}
		// $delivery_address = $transactions['delivery_details'];


		// code by GPH

		//d($merchant);
		//die;
		
		$newArray = array();
		foreach($transactions as $transaction) {
			
			$customer_addressBoolean = strpos($transaction->delivery_details, 'customer_address');
			$aaaa = strpos($transaction->delivery_details, 'address');
			if($customer_addressBoolean == true){
				$json = json_decode($transaction->delivery_details, true);
				$customerAddress = $json['customer_address'].", ".$json['customer_lga'].", ".$json['customer_state'];
				$transaction->customer_address_new = $customerAddress;
				
			}elseif($aaaa == true){
				$json = json_decode($transaction->delivery_details, true);
				$customerAddress = $json['address'].", ".$json['city'].", ".$json['state'].", ".$json['country'];
				$transaction->customer_address_new = $customerAddress;
				
			}else{
				$transaction->customer_address_new = $transaction->delivery_details;
			}
		
			$newArray[] =  $transaction;

		}

		// code by GPH
		$transactions = $newArray;

		//die(json_encode($transactions));


		// return $this->view->render($response, 'App/settlement.twig',
		// ['result' => [],'merchant' => $merchant]);
		return $this->view->render($response, 'App/settlement.twig',
		['transactions' => $transactions,
		'type'=>$type,
		'payment_type' => $payment_type,
		'merchant' => $merchant ]);
	}

	public function getMerchantNametSettlementAjax(Request $request){

		$requestParams = $request->getParams();
		$transaction = $this->models['transaction'];
		$merchant = $this->models['merchant'];

		if(!empty($requestParams['mtype'])){
			$mtype =$merchant->find($requestParams['merchantId']);
			// mail('palash@geeksperhour.com', 'rr', print_r($mtype, true));
			echo json_encode($mtype);die;
		}else{
			$merchant_name = $transaction->getClientName($requestParams['merchantId']);

		// mail('palash@geeksperhour.com', 'rr', print_r($requestParams, true));
			echo $merchant_name;
		}

		
	}

	public function assign(Request $request, Response $response)
	{
		return $this->view->render($response, 'App/assign.twig');
	}

	public function rolesAdd(Request $request, Response $response){

		if (!$this->auth->hasAccess('role.create')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to create roles.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the create role page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'admin-users');

		}

		if ($request->isPost()) {
			$roles = new \Dappur\Model\Roles;

			$role_name = $request->getParam('role_name');
			$role_slug = $request->getParam('role_slug');
			$role_permission = array();

			$this->validator->validate($request, [
				'role_name' => V::length(2, 25)->alpha('\''),
				'role_slug' => V::slug()
			]);

			if ($this->validator->isValid()) {

				$role = $this->auth->getRoleRepository()->createModel()->create([
					'name' => $role_name,
					'slug' => $role_slug,
					'permissions' => $role_permission
				]);

				if ($role) {
					$this->flash('success', 'Role has been successfully added.');
					$this->logger->addInfo("Role added successfully", array("role_name" => $role_name, "role_slug" => $role_slug));
					return $this->redirect($response, 'admin-users');
				}else{
					$this->flash('danger', 'There was a problem adding the role.');
					$this->logger->addError("Problem adding role.", array("role_name" => $role_name, "role_slug" => $role_slug));
					return $this->redirect($response, 'admin-users');
				}
			}else{
				$this->flash('danger', 'There was a problem adding the role.');
				$this->logger->addError("Problem adding role.", array("role_name" => $role_name, "role_slug" => $role_slug));
				return $this->redirect($response, 'admin-users');
			}
		}

	}

	public function rolesDelete(Request $request, Response $response){


		if (!$this->auth->hasAccess('role.delete')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to delete roles.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the delete role page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'admin-users');

		}

		$requestParams = $request->getParams();

		if (is_numeric($requestParams['role_id']) && $role != 1) {

			$remove_user_roles = new \Dappur\Model\RoleUsers;

			$remove_user_roles->where('role_id', '=', $requestParams['role_id'])->delete();

			$remove_role = new \Dappur\Model\Roles;
			$remove_role = $remove_role->find($requestParams['role_id']);


			if ($remove_role->delete()) {
				$this->flash('success', 'Role has been removed.');
				$this->logger->addInfo("Role removed successfully", array("role_id" => $role));
			}else{
				$this->flash('danger', 'There was a problem removing the role.');
				$this->logger->addError("Problem removing role.", array("role_id" => $role));
			}
		}else{
			$this->flash('danger', 'There was a problem removing the role.');
			$this->logger->addError("Problem removing role.", array("role_id" => $role));
		}

		return $this->redirect($response, 'admin-users');

	}

	public function rolesEdit(Request $request, Response $response, $roleid){
		if (!$this->auth->hasAccess('role.update')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to edit roles.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the edit role page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'admin-users');

		}

		$roles = new \Dappur\Model\Roles;
		$role = $roles->find($roleid);

		if ($role) {
			if ($request->isPost()) {
				// Get Vars
				$role_name = $request->getParam('role_name');
				$role_slug = $request->getParam('role_slug');
				$role_id = $request->getParam('role_id');
				$perm_name = $request->getParam('perm_name');
				$perm_value = $request->getParam('perm_value');

				// Validate Data
				$validate_data = array(
					'role_name' => array(
						'rules' => V::length(2, 25)->alpha('\''),
						'messages' => array(
							'length' => 'Must be between 2 and 25 characters.',
							'alpha' => 'Letters only and can contain \''
							)
					),
					'role_slug' => array(
						'rules' => V::slug(),
						'messages' => array(
							'slug' => 'May only contain lowercase letters, numbers and hyphens.'
							)
					)
				);

				$this->validator->validate($request, $validate_data);

				//Validate Role Name
				$check_name = $role->where('id', '!=', $role_id)->where('name', '=', $role_name)->get()->count();
				if ($check_name > 0) {
					$this->validator->addError('role_name', 'Role name is already in use.');
				}

				//Validate Role Name
				$check_slug = $role->where('id', '!=', $role_id)->where('slug', '=', $role_slug)->get()->count();
				if ($check_slug > 0) {
					$this->validator->addError('role_slug', 'Role slug is already in use.');
				}

				// Create Permissions Array
				$permissions_array = array();
				foreach ($perm_name as $pkey => $pvalue) {
					if ($perm_value[$pkey] == "true") {
						$val = true;
					}else{
						$val = false;
					}
					$permissions_array[$pvalue] = $val;
				}



				if ($this->validator->isValid()) {

					$update_role = $role;
					$update_role->name = $role_name;
					$update_role->slug = $role_slug;
					$update_role->save();

					$role_perms = $this->auth->findRoleById($role_id);
					$role_perms->permissions = $permissions_array;
					$role_perms->save();


					$this->flash('success', 'Role has been updated successfully.');
					$this->logger->addInfo("Role updated successfully", array("role_id" => $role_id));
					return $this->redirect($response, 'admin-users');
				}
			}

			return $this->view->render($response, 'App/roles-edit.twig', ['role' => $role]);

		}else{
			$this->flash('danger', 'Sorry, that role was not found.');
			return $response->withRedirect($this->router->pathFor('admin-users'));
		}
	}

	public function users(Request $request, Response $response){

		//d($this->auth->hasAccess('user.view'));
		//die;

		if (!$this->auth->hasAccess('user.view')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to view users.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the users page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');

		}

		$users = new \Dappur\Model\Users;
		$roles = new \Dappur\Model\Roles;
		$usertype = new \Dappur\Model\UserType;

		$userAccess = $this->userAccess;
		$role = $userAccess['roles'][0];
		$userlist = $users->get();
		// print_r($userlist);die;
		//d($userlist[0]->getRoles()[0]->name);
		//echo $role_id;
		$typeID = 0;
		//d($userlist);
		//die;
		if ($role == "courier-admin")
		{
			$loggedUser = $this->auth->check();
			$userID = $loggedUser->id;
			$usertype = new \Dappur\Model\UserType;
			$typeID = $usertype->getUserTypeID($userID);
			
		}
		else {
			$userlist = $users->all()->flatten();
			
		}

		//die;
		return $this->view->render($response, 'App/users.twig',
		["users" => $userlist,
		"typeID" =>$typeID,
		"usertype" => $usertype,
		"roles" => $roles->get()]);

	}

	public function agents(Request $request, Response $response){

		if (!$this->auth->hasAccess('agent.view'))
		{

			$loggedUser = $this->auth->check();
			$this->flash('danger', 'You do not have permission to see agent.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the agents list page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'home');

		}

		$agents = $this->models['agent'];
		$courier = $this->models['courier'];
		//d($agents);
		//die;
		return $this->view->render(
			$response, 'App/agents.twig', [
				"courier" => $courier,
				"agents" => $agents->getAgents()
				]);
	}

	public function agentAdd(Request $request, Response $response){

		if (!$this->auth->hasAccess('agent.create')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to access the settings.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the add agent", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'home');

		}

		$requestParams = $request->getParams();
		$courier = $this->models['courier'];
		if ($request->isPost()) {
			$agent = $this->models['agent'];

			$courierName = $courier->getCourierName($requestParams['courier_name']);
			//echo substr($courierName,0,5);
			//echo strtoupper(substr($courierName,0,5).$this->generateID(2));
			//die;
			$agent->agent_id = strtoupper(substr($courierName,0,5).$this->generateID(4));
			$agent->courier_id = $request->getParam('courier_name');
			$agent->name = $request->getParam('agent_name');;
			$agent->phone = $request->getParam('agent_phone');
			//$postdata['status'] = (!isset($request->getParam('status')) ? "1" : "0");
			$status = $request->getParam('status');
			$agent->status = isset($status) ? "1" : "0";
			//echo isset($status) ? "1" : "0";
			//die;
			$agent->save();

			$this->flash('success', $request->getParam('agent_name').' has been added successfully.');
			$this->logger->addInfo("Agent added successfully", array("message" => "New Agent Added by", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'admin-agents');

		}
		//d($roles->get());
		return $this->view->render($response, 'App/agents-add.twig',
		[
			'requestParams' => $requestParams,
			'couriers' => $courier->get()
		]);


	}

	public function couriers(Request $request, Response $response){

		if (!$this->auth->hasAccess('courier.view'))
		{

			$loggedUser = $this->auth->check();
			$this->flash('danger', 'You do not have permission to view couriers.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the couriers list page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');

		}

		$courier = new \Dappur\Model\Courier;
		return $this->view->render(
			$response, 'App/couriers.twig', ["couriers" => $courier->getCouriers()]);
	}

	public function merchants(Request $request, Response $response) {

		if (!$this->auth->hasAccess('merchant.view')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to view merchants.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the users page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');

		}

		$userAccess = $this->userAccess;
		$role = $userAccess['roles'][0];
		
		if ($role == "courier-admin")
		{
			$loggedUser = $this->auth->check();
			$userID = $loggedUser->id;
			$usertype = new \Dappur\Model\UserType;
			$type =  $usertype->getUserType($userID);
			$typeID = $usertype->getUserTypeID($userID);
			$courierMerchant = new \Dappur\Model\CourierMerchants;
			$merchants = $courierMerchant->getCourierMerchants($typeID);
		}
		else
		{
			$merchant = new \Dappur\Model\Merchants;
			$merchants = $merchant->get();
		}
			
		return $this->view->render($response, 'App/merchants.twig', ["merchants" => $merchants]);
	}

	public function merchantsAdd(Request $request, Response $response){


		$requestParams = $request->getParams();

		if (!$this->auth->hasAccess('merchant.create')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to access the settings.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the add user page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');

		}

		$merchants = new \Dappur\Model\Merchants;
		$banks = new \Dappur\Model\Banks;
		$courier = new \Dappur\Model\Courier;
		//d($this->auth->getUser()->getUserId());
		//die;
		if ($request->isPost()) {

			$this->logger->debug('in add merchants post',[]);

			$merchant = new \Dappur\Model\Merchants;
			$courier = new \Dappur\Model\Courier;
			$merchant->merchant_id = $this->generateID(10);
			$merchant->merchant_name = $request->getParam('merchant_name');
			$merchant->email = $request->getParam('email');
			$merchant->contact_number = $request->getParam('contact_number');
			$merchant->website = $request->getParam('website');
			$merchant->account_name = $request->getParam('account_name');
			$merchant->account_number = $request->getParam('account_number');
			$merchant->bank_id = $request->getParam('bank_id');
			$merchant->merchant_type = $request->getParam('merchant_type');
			$merchant->settlement_type = $request->getParam('settlement_type');
			$merchant->return_percentage = $request->getParam('return_percentage');
			$merchant->merchant_add = $request->getParam('merchant_add');
			$status = $request->getParam('status');
			$merchant->status = (isset($status) ? 1 : 0);

			if(!isset($merchant->merchant_name) || 
				!isset($merchant->email) || 
				!isset($merchant->contact_number) ||
				!isset($merchant->merchant_type) || $merchant->merchant_type == 'ns' ||
				!isset($merchant->settlement_type) || $merchant->settlement_type == 'ns' ||
				!isset($merchant->return_percentage) ||
				!isset($merchant->merchant_add)
				) {

				$this->logger->debug("form has one or more entries missing");
			
				$this->flash('danger', 'Form has one or more missing entries.');
				return $response->withRedirect($this->router->pathFor('admin-merchants-add'));	
		
			}

			$account_name = isset($merchant->account_name) && empty($merchant->account_name) == false ? 1 : 0;
			$account_number = isset($merchant->account_number) && empty($merchant->account_number) == false ? 1 : 0;
			$bank_id = $merchant->bank_id != 'ns' ? 1 : 0;

			$account_sum = $account_name + $account_number + $bank_id;

			if($account_sum > 0 && $account_sum < 3 ) {

				$this->logger->debug("account sum is " . $account_sum,[]);
				$this->logger->debug("bank details are invalid");
				$this->flash('danger', 'Bank details are invalid');
				return $response->withRedirect($this->router->pathFor('admin-merchants-add'));

			}

			if($merchant->bank_id == 'ns') {
				$merchant->bank_id = null;
			}

			$merchant->save();
		   
			$userAccess = $this->userAccess;
			$role = $userAccess['roles'][0];
			if($role == 'admin')
			{
				$selectedcourier = $request->getParam('couriers');
				foreach ($selectedcourier as $rkey => $rvalue)
					{
						$courier_merchant = new \Dappur\Model\CourierMerchants;
						$courier_merchant->merchant_id = $merchant->id;
						$courier_merchant->courier_id = $rvalue;
						$courier_merchant->save();
					}
			}
			else {
				$usertype = new \Dappur\Model\UserType;
				$typeID = $usertype->getUserTypeID($requestParams['user_id']);
				$courier_merchant = new \Dappur\Model\CourierMerchants;
				$courier_merchant->merchant_id = $merchant->id;
				$courier_merchant->courier_id = $typeID;
				$courier_merchant->save();
			}
			
			//die;
			$this->flash('success', $request->getParam('merchant_name').' has been added successfully.');
			$this->logger->addInfo("Merchant added successfully", array("message" => "New Merchants Added by", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'admin-merchants');

		}
		//d($roles->get());
		return $this->view->render($response, 'App/merchants-add.twig',
		[
			'banks' => $banks->get(),
			'couriers'=>$courier->get(),
			'requestParams' => $requestParams
		]);
	}

	public function merchantsEdit(Request $request, Response $response, $merchantid)
	{
		 if (!$this->auth->hasAccess('merchant.edit')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to edit Merchants.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the edit merchant", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');
			}

			$requestParams = $request->getParams();
			$merchants = new \Dappur\Model\Merchants;
			$merchant = $merchants->where('id', '=', $merchantid)->first();
			$banks = new \Dappur\Model\Banks;
			$courier = new \Dappur\Model\Courier;
			$courier_merchant = new \Dappur\Model\CourierMerchants;

			if($merchant)
			{

				 if ($request->isPost()) {
					//d($courier);
					//exit;
					$merchantUpdate = $merchant;
					$merchantUpdate->merchant_name = trim($request->getParam('merchant_name'));
					$merchantUpdate->email = trim($request->getParam('email'));
					$merchantUpdate->contact_number = trim($request->getParam('contact_number'));
					$merchantUpdate->website = trim($request->getParam('website'));
					$merchantUpdate->account_name = trim($request->getParam('account_name'));
					$merchantUpdate->account_number = trim($request->getParam('account_number'));
					$merchantUpdate->bank_id = trim($request->getParam('bank_id'));
					$merchantUpdate->merchant_type = trim($request->getParam('merchant_type'));
					$merchantUpdate->settlement_type = trim($request->getParam('settlement_type'));
					$merchantUpdate->return_percentage = trim($request->getParam('return_percentage'));
					$merchantUpdate->merchant_add = trim($request->getParam('merchant_add'));
					$status = trim($request->getParam('status'));
					$merchantUpdate->status = (isset($status) ? "1" : "0");
					//$merchant->merchant_name = $request->getParam('merchant_name');
					$merchantUpdate->save();
					
					$userAccess = $this->userAccess;
					$role = $userAccess['roles'][0];
					if($role == 'admin')
					{
						$selectedcourier = $request->getParam('couriers');
						foreach ($selectedcourier as $rkey => $rvalue)
							{
								$courier_merchant = new \Dappur\Model\CourierMerchants;
								$courier_merchant->merchant_id = trim($merchantUpdate->id);
								$courier_merchant->courier_id = trim($rvalue);
								$courier_merchant->save();
							}
					}
					
					 $this->flash('success', 'Merchant has been updated successfully.');
						$this->logger->addInfo("Merchant updated successfully", array("Merchant ID" => $merchantid));
						return $this->redirect($response, 'admin-merchants');
				 }

				return $this->view->render($response, 'App/merchants-edit.twig',
				['banks' => $banks->get(),
				'merchant' => $merchant,
				'courierMerchant' =>$courier_merchant,
				'couriers' => $courier->get(),
				'requestParams' => $requestParams]);
			}
			else{
				$this->flash('danger', 'Sorry, that Merchant was not found.');
				return $response->withRedirect($this->router->pathFor('admin-merchants'));
			}
	}

	public function merchantsDelete(Request $request, Response $response)
	{
		if (!$this->auth->hasAccess('merchant.delete')) {

			$loggedUser = $this->auth->check();
			$this->flash('danger', 'You do not have permission to delete Merchants.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the delete merchant", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');
			}

			if($request->isPost())
			{
				$requestParams = $request->getParams();
				$merchant_id = $requestParams['merchant_id'];
				$merchant = new \Dappur\Model\Merchants;
				if($merchant->find($merchant_id)->delete())
				{
				   $this->flash('success', 'Merchant has been deleted successfully.');
					$this->logger->addInfo("Merchant deleted successfully", array("merchant" => $merchant));
					return $this->redirect($response, 'admin-merchants');
				}else{
					$this->flash('danger'.'There was an error deleting the merchant.');
					$this->logger->addInfo("Merchant ", array("merchant" => $merchant));
					return $this->redirect($response, 'admin-merchants');
				}

			}

	}

	public function couriersEdit(Request $request, Response $response, $courierid)
	{
		 if (!$this->auth->hasAccess('courier.edit')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to edit Courier.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the edit courier", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');

			}

			$requestParams = $request->getParams();
			$couriers = new \Dappur\Model\Courier;
			$courier = $couriers->where('id', '=', $courierid)->first();

			if($courier)
			{

				 if ($request->isPost()) {
					//d($courier);
					//exit;
					$courierUpdate = $courier;

					$courierUpdate->courier_name = $request->getParam('courier_name');
					$courierUpdate->courier_email = $request->getParam('courier_email');;
					$courierUpdate->courier_website = $request->getParam('courier_website');
					//$postdata['status'] = (!isset($request->getParam('status')) ? "1" : "0");
					$status = $request->getParam('status');
					$courierUpdate->status = (isset($status) ? "1" : "0");
					$courierUpdate->address = $requestParams['address'];
					$courierUpdate->split_percentage = $request->getParam('split_percentage');
					//d($requestParams);
					//exit;
					$courierUpdate->save();

					 $this->flash('success', 'Courier has been updated successfully.');
						$this->logger->addInfo("Courier updated successfully", array("Courier ID" => $courierid));
						return $this->redirect($response, 'admin-couriers');
				 }

				return $this->view->render($response, 'App/couriers-edit.twig', ['courier' => $courier,'requestParams' => $requestParams]);
			}
			else{
				$this->flash('danger', 'Sorry, that courier was not found.');
				return $response->withRedirect($this->router->pathFor('admin-couriers'));
			}
	}

	public function couriersDelete(Request $request, Response $response)
	{
		if (!$this->auth->hasAccess('courier.delete')) {

			$loggedUser = $this->auth->check();
			$this->flash('danger', 'You do not have permission to delete Courier.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the delete courier", "courier_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');
			}

			if($request->isPost())
			{
				$requestParams = $request->getParams();
				$courier_id = $requestParams['courier_id'];
				$courier = new \Dappur\Model\Courier;
				if($courier->find($courier_id)->delete())
				{
				   $this->flash('success', 'Courier has been deleted successfully.');
					$this->logger->addInfo("Courier deleted successfully", array("courier" => $courier));
					return $this->redirect($response, 'admin-couriers');
				}else{
					$this->flash('danger'.'There was an error deleting the courier.');
					$this->logger->addInfo("User ", array("courier" => $courier));
					return $this->redirect($response, 'admin-couriers');
				}

			}

	}

	public function couriersAdd(Request $request, Response $response){


		$requestParams = $request->getParams();

		if (!$this->auth->hasAccess('courier.create')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to access the settings.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the add user page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');

		}

		$couriers = new \Dappur\Model\Courier;

		if ($request->isPost()) {


			$courier = new \Dappur\Model\Courier;
			$courier->courier_id = $this->generateID(10);
			$courier->courier_name = $request->getParam('courier_name');
			$courier->courier_email = $request->getParam('courier_email');;
			$courier->courier->website = $request->getParam('courier_website');
			//$postdata['status'] = (!isset($request->getParam('status')) ? "1" : "0");
			$status = $request->getParam('status');
			$courier->status = (isset($status) ? "1" : "0");
			$courier->address = $request->getParam('address');
			$courier->split_percentage = $request->getParam('split_percentage');
			$courier->save();

			$this->flash('success', $request->getParam('courier_name').' has been added successfully.');
			$this->logger->addInfo("Courier added successfully", array("message" => "New Couried Added by", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'admin-couriers');

		}
		//d($roles->get());
		return $this->view->render($response, 'App/couriers-add.twig', ['requestParams' => $requestParams]);


	}

	public function usersAdd(Request $request, Response $response){

		$requestParams = $request->getParams();
		$loggedUser = $this->auth->check();
		//d($request);
		//exit;
		if (!$this->auth->hasAccess('user.create')) {

			

			$this->flash('danger', 'You do not have permission to access the settings.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the add user page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');

		}



		$users = new \Dappur\Model\Users;
		$roles = new \Dappur\Model\Roles;
		$couriers = new \Dappur\Model\Courier;
		$merchants = new \Dappur\Model\Merchants;
		$usertype = new \Dappur\Model\UserType;
		$courier_merchant = new \Dappur\Model\CourierMerchants;

		$userAccess = $this->userAccess;
		$role = $userAccess['roles'][0];

		$userID = $loggedUser->id;
		$type =  $usertype->getUserType($userID);
		$typeID = $usertype->getUserTypeID($userID);


		//echo $type." : ". $typeID;
		//die;

		//d($role);
		if($role == 'courier-admin')
		{
			$a_roles = $roles->get();
			$p_roles = $a_roles->shift();
			$merchant = $courier_merchant->getCourierMerchants($typeID);

		}

		else {
			$a_roles = $roles->get();
			$merchant = $merchants->get();
		}

		

		if ($request->isPost()) {
			$user_id = $request->getParam('user_id');
			$first_name = $request->getParam('first_name');
			$last_name = $request->getParam('last_name');
			$email = $request->getParam('email');
			$username = $request->getParam('username');
			$password = $request->getParam('password');
			$user_roles = $request->getParam('roles');
			$perm_name = $request->getParam('perm_name');
			$perm_value = $request->getParam('perm_value');

			$permissions_array = array();


			foreach ($perm_name as $pkey => $pvalue) {
				if ($perm_value[$pkey] == "true") {
					$val = true;
				}else{
					$val = false;
				}
				$permissions_array[$pvalue] = $val;
			}

			// Check if roles exist
			$roles_array = array();
			foreach ($user_roles as $rkey => $rvalue) {
				if (!$this->auth->findRoleBySlug($rvalue)) {
					$this->validator->addError('roles', 'Role does not exist.');
				}else{
					$roles_array[] = $rvalue;
				}
			}

			// Validate Data
			$validate_data = array(
				'first_name' => array(
					'rules' => V::length(2, 25)->alpha('\''),
					'messages' => array(
						'length' => 'Must be between 2 and 25 characters.',
						'alpha' => 'Letters only and can contain \''
						)
				),
				'last_name' => array(
					'rules' => V::length(2, 25)->alpha('\''),
					'messages' => array(
						'length' => 'Must be between 2 and 25 characters.',
						'alpha' => 'Letters only and can contain \''
						)
				),
				'email' => array(
					'rules' => V::noWhitespace()->email(),
					'messages' => array(
						'email' => 'Enter a valid email address.',
						'noWhitespace' => 'Must not contain any spaces.'
						)
				),
				'username' => array(
					'rules' => V::noWhitespace()->alnum(),
					'messages' => array(
						'slug' => 'Must be alpha numeric with no spaces.',
						'noWhitespace' => 'Must not contain any spaces.'
						)
				),
				'password' => array(
					'rules' => V::noWhitespace()->length(6, 25),
					'messages' => array(
						'noWhitespace' => 'Must not contain spaces.',
						'length' => 'Must be between 6 and 25 characters.'
					)
				),
				'password_confirm' => array(
					'rules' => V::equals($password),
					'messages' => array(
						'equals' => 'Passwords do not match.'
					)
				)
			);

			$this->validator->validate($request, $validate_data);

			//Check username
			$check_username = $users->where('id', '!=', $user_id)->where('username', '=', $username)->get()->count();
			if ($check_username > 0) {
				$this->validator->addError('username', 'Username is already in use.');
			}

			//Check Email
			$check_email = $users->where('id', '!=', $user_id)->where('email', '=', $email)->get()->count();
			if ($check_email > 0) {
				$this->validator->addError('email', 'Email address is already in use.');
			}

			if ($this->validator->isValid()) {

				$user = $this->auth->registerAndActivate([
					'first_name' => $first_name,
					'last_name' => $last_name,
					'email' => $email,
					'username' => $username,
					'password' => $password,
					'permissions' => [
						'user.delete' => 0
					]
				]);

				$user_perms = $user;

				$user_perms->permissions = $permissions_array;
				$user_perms->save();

				foreach ($roles->get() as $rolekey => $rolevalue) {
					echo $rolevalue['slug'] . "<br>";

					if (!in_array($rolevalue['slug'], $roles_array)) {
						if ($rolevalue['slug'] == 'admin' && $user_id == 1) {
							continue;
						}else{
							$role = $this->auth->findRoleBySlug($rolevalue['slug']);
							$role->users()->detach($user);
						}

					}

				}

				foreach ($roles_array as $rakey => $ravalue) {
					if ($user->inRole($ravalue)) {
						continue;
					}else{
						$role = $this->auth->findRoleBySlug($ravalue);
						$role->users()->attach($user);
					}
				}

				//attach either courier / merchant here
			   if($request->getParam("courier_name") != "ns"){

					$usertype->user_type = "Courier";
					$usertype->type_id = $request->getParam("courier_name");
					$usertype->user_id = $user->id;
					$usertype->save();
				}

				if($request->getParam("merchant_name") != "ns") {

					$usertype->user_type = "Merchant";
					$usertype->type_id = $request->getParam("merchant_name");
					$usertype->user_id = $user->id;
					$usertype->save();
				}

				//send mail if succesful
				$message_body = 'App/signupMail.twig';
				$template = $this->view->getEnvironment()->loadTemplate($message_body);
				$parameters  = array(
					'customer' => $first_name.' '.$last_name
				);
				$company  = $template->renderBlock('company',   $parameters);
				$this->mailer($email,$first_name.' '.$last_name,'Registration',$company);

				$this->flash('success', $username.' has been added successfully.');
				$this->logger->addInfo("User added successfully", array("first_name" => $first_name, "last_name" => $last_name, "email" => $email, "username" => $username));
				return $this->redirect($response, 'admin-users');
			}
		}
		

		return $this->view->render($response, 'App/users-add.twig', [
				'roles' => $a_roles,
				'couriers' => $couriers->get(),
				'courier' => $typeID,
				'merchants' => $merchant,
				'requestParams' => $requestParams
			]);


	}

	public function usersEdit(Request $request, Response $response, $userid){

		if (!$this->auth->hasAccess('user.update')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to edit users.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the edit user page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');

		}

		$requestParams = $request->getParams();
		$users = new \Dappur\Model\Users;
		$user = $users->where('id', '=', $userid)->first();
		$roles = new \Dappur\Model\Roles;

		if ($user) {
			if ($request->isPost()) {


				$user_id = $request->getParam('user_id');
				$first_name = $request->getParam('first_name');
				$last_name = $request->getParam('last_name');
				$email = $request->getParam('email');
				$username = $request->getParam('username');
				$user_roles = $request->getParam('roles');
				$perm_name = $request->getParam('perm_name');
				$perm_value = $request->getParam('perm_value');

				// Create Permissions Array
				$permissions_array = array();
				if(null !== $perm_name){
					foreach ($perm_name as $pkey => $pvalue) {
						if ($perm_value[$pkey] == "true") {
							$val = true;
						}else{
							$val = false;
						}
						$permissions_array[$pvalue] = $val;
					}
				}

				// Check if roles exist
				$roles_array = array();
				if(null !== $user_roles){
					foreach ($user_roles as $rkey => $rvalue) {
						if (!$this->auth->findRoleBySlug($rvalue)) {
							$this->validator->addError('roles', 'Role does not exist.');
						}else{
							$roles_array[] = $rvalue;
						}
					}
				}

				// Validate Data
				$validate_data = array(
					'first_name' => array(
						'rules' => V::length(2, 25)->alpha('\''),
						'messages' => array(
							'length' => 'Must be between 2 and 25 characters.',
							'alpha' => 'Letters only and can contain \''
							)
					),
					'last_name' => array(
						'rules' => V::length(2, 25)->alpha('\''),
						'messages' => array(
							'length' => 'Must be between 2 and 25 characters.',
							'alpha' => 'Letters only and can contain \''
							)
					),
					'email' => array(
						'rules' => V::noWhitespace()->email(),
						'messages' => array(
							'email' => 'Enter a valid email address.',
							'noWhitespace' => 'Must not contain any spaces.'
							)
					),
					'username' => array(
						'rules' => V::noWhitespace()->alnum(),
						'messages' => array(
							'slug' => 'Must be alpha numeric with no spaces.',
							'noWhitespace' => 'Must not contain any spaces.'
							)
					)
				);

				$this->validator->validate($request, $validate_data);

				//Check username
				$check_username = $users->where('id', '!=', $user_id)->where('username', '=', $username)->get()->count();
				if ($check_username > 0) {
					$this->validator->addError('username', 'Username is already in use.');
				}

				//Check Email
				$check_email = $users->where('id', '!=', $user_id)->where('email', '=', $email)->get()->count();
				if ($check_email > 0) {
					$this->validator->addError('email', 'Email address is already in use.');
				}

				if ($this->validator->isValid()) {

					// Get User Info
					$user = $this->auth->findById($user_id);

					// Update User Info
					$update_user = $user;
					$update_user->first_name = $first_name;
					$update_user->last_name = $last_name;
					$update_user->email = $email;
					$update_user->username = $username;
					$update_user->save();

					$user_perms = $user;
					$user_perms->permissions = $permissions_array;
					$user_perms->save();

					foreach ($roles->get() as $rolekey => $rolevalue) {
						echo $rolevalue['slug'] . "<br>";

						if (!in_array($rolevalue['slug'], $roles_array)) {
							if ($rolevalue['slug'] == 'admin' && $user_id == 1) {
								continue;
							}else{
								$role = $this->auth->findRoleBySlug($rolevalue['slug']);
								$role->users()->detach($user);
							}

						}

					}

					foreach ($roles_array as $rakey => $ravalue) {
						if ($user->inRole($ravalue)) {
							continue;
						}else{
							$role = $this->auth->findRoleBySlug($ravalue);
							$role->users()->attach($user);
						}
					}

					$this->flash('success', $username.' has been updated successfully.');
					$this->logger->addInfo("User updated successfully", array("first_name" => $first_name, "last_name" => $last_name, "email" => $email, "username" => $username));
					return $this->redirect($response, 'admin-users');
				}
			}

			return $this->view->render($response, 'App/users-edit.twig', ['user' => $user, 'roles' => $roles->get(), 'requestParams' => $requestParams]);
		}else{
			$this->flash('danger', 'Sorry, that user was not found.');
			return $response->withRedirect($this->router->pathFor('admin-users'));
		}

	}

	public function usersDelete(Request $request, Response $response){

		if (!$this->auth->hasAccess('user.delete')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to delete users.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the edit user page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');

		}

		$requestParams = $request->getParams();
		if($requestParams['user_id'])
		{

			$remove_user_roles = new \Dappur\Model\RoleUsers;
			$remove_user_roles->where('user_id', '=', $requestParams['user_id'])->delete();

			$remove_user = new \Dappur\Model\Users;
			$remove_user = $remove_user->find($requestParams['user_id']);

			if($remove_user->find($requestParams['user_id'])->delete())
			{
				$this->flash('success', 'User has been deleted successfully.');
				$this->logger->addInfo("User deleted successfully", array("user" => $remove_user));
				return $this->redirect($response, 'admin-users');
			}else{
				$this->flash('danger'.'There was an error deleting the user.');
				$this->logger->addInfo("User ", array("user" => $remove_user));
				return $this->redirect($response, 'admin-users');
			}
		}

	}

	public function settingsGlobal(Request $request, Response $response){

		if (!$this->auth->hasAccess('config.global')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to access the settings.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the global settings page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');

		}

		$timezones = $this->getTimezones();
		$theme_list = $this->getThemeList();
		$config = new \Dappur\Model\Config;
		$global_config = $config->get();

		if ($request->isPost()) {

			$allPostVars = $request->getParsedBody();

			// Validate Domain
			if (array_key_exists('domain', $allPostVars)){
				$this->validator->validate($request, ['domain' => array('rules' => V::domain(), 'messages' => array('domain' => 'Please enter a valid domain.'))]);
			}

			// Validate Reply To Email
			if (array_key_exists('replyto-email', $allPostVars)){
				$this->validator->validate($request, ['replyto-email' => array('rules' => V::noWhitespace()->email(), 'messages' => array('noWhitespace' => 'Must not contain any spaces.', 'email' => 'Enter a valid email address.'))]);
			}

			// Validate Google Analytics
			if (isset($allPostVars['ga']) && !empty($allPostVars['ga'])){
				$this->validator->validate($request, ['ga' => array('rules' => V::regex('/(UA|YT|MO)-\d+-\d+/'), 'messages' => array('regex' => 'Enter a valid UA Tracking Code'))]);
			}

			// Additional Validation
			foreach ($allPostVars as $key => $value) {
				if (strip_tags($value) != $value) {
					$this->validator->addError($key, 'Please do not use any HTML Tags');
					$this->logger->addWarning("possible scripting attack", array("message" => "HTML tags were blocked from being put into the config."));
				}

				if ($key == "theme" && !in_array($value, $theme_list)) {
					$this->validator->addError($key, 'Not a valid global setting.');
				}
			}


			if ($this->validator->isValid()) {

				foreach ($allPostVars as $key => $value) {
					$updateRow = new \Dappur\Model\Config;
					$updateRow->where('name', $key)->update(['value' => $value]);
				}

				$this->flash('success', 'Global settings have been updated successfully.');
				return $this->redirect($response, 'settings-global');
			}


		}

		return $this->view->render($response, 'App/global-settings.twig', array("globalConfig" => $global_config, "themeList" => $theme_list, "timezones" => $timezones));

	}

	public function settingsGlobalAdd(Request $request, Response $response){

		if (!$this->auth->hasAccess('config.global')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to add settings.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the global settings add page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');

		}


		$timezones = $this->getTimezones();
		$theme_list = $this->getThemeList();
		$config = new \Dappur\Model\Config;
		$global_config = $config->get();

		if ($request->isPost()) {

			$allPostVars = $request->getParsedBody();

			$this->validator->validate($request, array('add_name' => array('rules' => V::slug()->length(4, 32), 'messages' => array('slug' => 'May only contain lowercase letters, numbers and hyphens.', 'length' => 'Must be between 4 and 32 characters.'))));
			$this->validator->validate($request, array('add_description' => array('rules' => V::alnum()->length(4, 32), 'messages' => array('alnum' => 'May only contain letters and numbers.', 'length' => 'Must be between 4 and 32 characters.'))));

			if ($allPostVars['add_type'] == "string") {
				// Check for HTML Tags
				if (strip_tags($allPostVars['add_value']) != $allPostVars['add_value']) {
					$this->validator->addError('add_value', 'Please do not use any HTML Tags');
					$this->logger->addWarning("possible scripting attack", array("message" => "HTML tags were blocked from being put into the config."));
				}
			} else {
				$this->validator->addError('add_value', 'Not a valid global setting.');
			}

			$check_config = $config->where('name', '=', $allPostVars['add_name'])->get()->count();
			if ($check_config > 0) {
				$this->validator->addError('add_name', 'Name is already in use.');
			}

			if ($this->validator->isValid()) {

				$configOption = new \Dappur\Model\Config;
				$configOption->name = $allPostVars['add_name'];
				$configOption->description = $allPostVars['add_description'];
				$configOption->type = $allPostVars['add_type'];
				$configOption->value = $allPostVars['add_value'];
				$configOption->save();


				$this->flash('success', 'Global settings successfully added.');
				return $this->redirect($response, 'settings-global');
			}


		}

		return $this->view->render($response, 'App/global-settings.twig', array("globalConfig" => $global_config, "themeList" => $theme_list, "timezones" => $timezones));

	}

	public function myAccount(Request $request, Response $response){

		$requestParams = $request->getParams();

		$loggedUser = $this->auth->check();

		$users = new \Dappur\Model\Users;

		//get four
		$courier = $this->accountsService->getCourierForUser($loggedUser["id"]);
		$requestParams["netpluspay_mid"] = $courier->netpluspay_mid;

		if (!$loggedUser) {

			$this->flash('danger', 'You need to be logged in to access this page.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the my account page", "user_id" => $loggedUser['id']));
			return $this->redirect($response, 'dashboard');

		}

		if ($request->isPost()) {

			$first_name = $request->getParam('first_name');
			$last_name = $request->getParam('last_name');
			$email = $request->getParam('email');
			$username = $request->getParam('username');
			$password = $request->getParam('password');
			$password_confirm = $request->getParam('password_confirm');

			if (null !== $request->getParam('update_account')) {

				$validate_data = array(
					'first_name' => array(
						'rules' => V::length(2, 25)->alpha('\''),
						'messages' => array(
							'length' => 'Must be between 2 and 25 characters.',
							'alpha' => 'Letters only and can contain \''
							)
					),
					'last_name' => array(
						'rules' => V::length(2, 25)->alpha('\''),
						'messages' => array(
							'length' => 'Must be between 2 and 25 characters.',
							'alpha' => 'Letters only and can contain \''
							)
					),
					'email' => array(
						'rules' => V::noWhitespace()->email(),
						'messages' => array(
							'email' => 'Enter a valid email address.',
							'noWhitespace' => 'Must not contain any spaces.'
							)
					),
					'username' => array(
						'rules' => V::noWhitespace()->alnum(),
						'messages' => array(
							'slug' => 'Must be alpha numeric with no spaces.',
							'noWhitespace' => 'Must not contain any spaces.'
							)
					)
				);

				if ($loggedUser['username'] != $username) {
					$check_username = $users->where('id', '!=', $user_id)->where('username', '=', $username)->get()->count();
					if ($check_username > 0) {
						$this->validator->addError('username', 'Username is already in use.');
					}
				}

				if ($loggedUser['email'] != $email) {
					$check_email = $users->where('id', '!=', $user_id)->where('email', '=', $email)->get()->count();
					if ($check_email > 0) {
						$this->validator->addError('email', 'Email address is already in use.');
					}
				}

				$this->validator->validate($request, $validate_data);

				if ($this->validator->isValid()) {

					$new_information = [
						'first_name' => $first_name,
						'last_name' => $last_name,
						'email' => $email,
						'username' => $username
					];

					$update_user = $this->auth->update($loggedUser, $new_information);

					if ($update_user) {

						$this->courierService->updateCouriersNetplusPayMID($courier->id, $request->getParam("netpluspay_mid"));

						$this->flash('success', 'Your account has been updated successfully.');
						$this->logger->addInfo("My Account: User successfully updated.", array("first_name" => $first_name, "last_name" => $last_name, "email" => $email, "username" => $username, "user_id" => $loggedUser['id']));
						return $this->redirect($response, 'my-account');

					} else {
						$this->flash('danger', 'There was an error updating your account information.');
						$this->logger->addInfo("My Account: An unknown error occured updating user.", array("first_name" => $first_name, "last_name" => $last_name, "email" => $email, "username" => $username, "user_id" => $loggedUser['id']));
					}
				}
			}

			if (null !== $request->getParam('change_password')) {
				// Validate Data
				$validate_data = array(
					'password' => array(
					'rules' => V::noWhitespace()->length(6, 25),
					'messages' => array(
						'length' => 'Must be between 6 and 25 characters.',
						'noWhitespace' => 'Must not contain any spaces.'
						)
					),
					'password_confirm' => array(
						'rules' => V::equals($password),
						'messages' => array(
							'equals' => 'Passwords do not match.'
							)
					)
				);

				$this->validator->validate($request, $validate_data);

				if ($this->validator->isValid()) {

					$new_information = [
						'password' => $password,
					];

					$update_user = $this->auth->update($loggedUser, $new_information);

					if ($update_user) {
						$this->flash('success', 'Your password has been updated successfully.');
						$this->logger->addInfo("My Account: Password successfully changed", array("user_id" => $loggedUser['id']));
						return $this->redirect($response, 'my-account');
					} else {
						$this->flash('danger', 'There was an error changing your password.');
						$this->logger->addInfo("My Account: An unknown error occured changing a password.", array("user_id" => $loggedUser['id']));
					}
				}
			}

		}

		return $this->view->render($response, 'App/my-account.twig', array("requestParams" => $requestParams));
	}

	private function getThemeList(){

		$public_assets = array_filter(glob('../public/assets/*'), 'is_dir');
		$internal_assets = array_filter(glob('../app/views/*'), 'is_dir');

		$public_array = array();
		$internal_array = array();
		foreach ($public_assets as $key => $value) {
			$public_array[] = substr($value, strrpos($value, '/') + 1);
		}

		foreach ($internal_assets as $key => $value) {
			$internal_array[] = substr($value, strrpos($value, '/') + 1);
		}

		foreach ($internal_array as $key => $value) {
			if (!in_array($value, $public_array)) {
				unset($internal_array[$key]);
			}
		}

		return $internal_array;
	}

	private function getTimezones(){

		$zones_array = array();
		$timestamp = time();
		foreach(timezone_identifiers_list() as $key => $zone) {
			date_default_timezone_set($zone);
			$zones_array[$key]['zone'] = $zone;
			$zones_array[$key]['diff_from_GMT'] = 'UTC/GMT ' . date('P', $timestamp);
		}
		return $zones_array;
	}

	public function getInvoice(Request $request, Response $response, $transactionID)
	{
		$this->sayMpdf($transactionID);
	}


	private function sayMpdf($transactionID)
	{
		$transaction = new \Dappur\Model\Transactions;
		$item = new \Dappur\Model\Items;
		$transactions = $transaction->find($transactionID);
		$courier = $this->models['courier'];
		
		$items = $item->getItemsByTransaction($transactions['transaction_id']);
		$delivery_address = $transactions['delivery_details'];
		$pickup_details = $transactions['pickup_details'];
		$transactionType = $transactions['transaction_payment_type'];
		$orderID = $transactions['transaction_id'];
		$courierName=$courier->getCourierName($transactions['courier_id']);
		$courierDetails=$courier->getCourierByName($courierName);
		$courierAddress = $courierDetails->address;
		$courierEmail = $courierDetails->courier_email;
		$courierWebsite = $courierDetails->courier_website;
		
		//$courierName="Redstar .";
		
		if($transactionType == "Prepaid")
		{
			$trans_amount = 0;
			$delivery_cost = 0;
			$vat = $delivery_cost * 0.05;
		}
		else
		{
			$trans_amount = $transactions['transaction_total_amount'];
			$delivery_cost = $transactions['delivery_cost'];
			$vat = $delivery_cost * 0.05;
		}

		
		$total_cost = $trans_amount + $vat + $delivery_cost;
		$generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
		
		$bcode = base64_encode($generator->getBarcode($orderID, $generator::TYPE_CODE_128));

		$message_body = 'App/invoice.twig';
		$template = $this->view->getEnvironment()->loadTemplate($message_body);
		$parameters  = array(
			'orderID' => $orderID,
			'courierName' => $courierName,
			'courierAddress' => $courierAddress,
			'courierEmail' => $courierEmail,
			'courierWebsite' => $courierWebsite,
			'barcode'=>$bcode,
			'items' => $items,
			'total_amount' => $total_amount,
			'delivery_details' => json_decode($delivery_address),
			'pickup_details' => json_decode($pickup_details),
			'delivery_cost' => $delivery_cost,
			'vat' => $vat,
			'total_cost' => $total_cost,
			'transactionType' => $transactionType
		);
		$html  = $template->renderBlock('invoice',   $parameters);

	   
		$mpdf = new \Mpdf\Mpdf([
			'margin_left' => 20,
			'margin_right' => 15,
			'margin_top' => 48,
			'margin_bottom' => 25,
			'margin_header' => 10,
			'margin_footer' => 10
		]);
		$mpdf->SetProtection(array('print'));
		$mpdf->SetTitle($courierName.". - Invoice");
		$mpdf->SetAuthor($courierName);
	   
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->WriteHTML($html);

		$filepath = file_put_contents('my.pdf', $mpdf->Output());
		header("Pragma: public");
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header('Content-Type: application/pdf');
		header("Content-Disposition: attachment; filename=file.pdf" );
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($filepath));
		readfile($filepath);


	}
	
	//function to get price according to weight and destination selected 
	public function getPriceCourier(Request $request){

		$json = $request->getParams();

		$weight = 0;

		foreach ($json['items_weight'] as $value) {
			$weight = $weight + $value;
		}


		$pickup_lga = $json['merchant_lga'];
		$courier_id = $json['courier_name'];
		$pickup_state = $json['merchant_state'];
		$delivery_state = $json['customer_state'];
		$delivery_lga = $json['customer_lga'];

		$total_shipping_price = $this->shippingPriceNew($delivery_state, $pickup_state, $weight, $courier_id, $pickup_lga, $delivery_lga, true);

		echo json_encode($total_shipping_price);

	}

	function getRatecardRecords(Request $request)
	{  
		    $loggedUser = $this->auth->check();       
            $userID = $loggedUser->id;
            $usertype = new \Dappur\Model\UserType;
            $courierid = $usertype->getUserTypeID($userID);
		    $courierName =  $usertype->getUserTypeName($userID);
		    $courierList['id'] = $courierid;
		    $courierid = $courierList['id'];

		    $ratecardshow = new \Dappur\Model\RateCard;
		    $showRecord    = $ratecardshow->showRecord($courierid);
		   // $json = $request->getParams();
	
		echo $var = json_encode($showRecord);

	}
	function recordDelete(Request $request, Response $response)
	{
		$requestParams = $request->getParams();
		
		//$remove_user_roles = new \Dappur\Model\RoleUsers;
		$ratecardshow = new \Dappur\Model\RateCard;
		$courierid = $requestParams['id'];
	    $showRecord    = $ratecardshow->deleteRecord($courierid);

	    echo "ok";

		// $showRecord->where('id', '=', $requestParams['record_id'])->delete();
	}

	function ratecardedit(Request $request, Response $response, $record_id)
	{
		

		$ratecardshow = new \Dappur\Model\RateCard;
	    $showRecord    = $ratecardshow->editRecord($record_id);
		$loggedUser = $this->auth->check();       
		$userID = $loggedUser->id;
		$usertype = new \Dappur\Model\UserType;
		$courierid = $usertype->getUserTypeID($userID);
	
		if ($request->isPost()) {
		
			$customer_state = $_POST['customer_state'];
			$customer_state1 = $_POST['customer_state1'];
			$customer_lga = $_POST['customer_lga'];
			$customer_lga1 = $_POST['customer_lga1'];
			$weight = $_POST['weight'];
			$price = $_POST['price'];
			$extra = $_POST['Extra'];
			$rowID = $_POST['rowID'];

			$update = $this->db->table('rate_card')->where('id' ,'=',$rowID)->update(['state_from' => $customer_state, 'state_to'=>$customer_state1, 'lga_from'=>$customer_lga,'lga_to'=>$customer_lga1, 'weight'=> $weight,'price'=> $price,'additionalPrice'=> $extra]);

			return $this->redirect($response, 'RateCard');

		}	

		return $this->view->render($response, 'App/RateCardedit.twig',['requestParams' => $showRecord]);

	}

	public function getAgentCardsOnFile($request, $response) {

		$loggedUser = $this->auth->check();       
		$user_id = $loggedUser->id;
		$usertype = new \Dappur\Model\UserType;

		$courier_id = $usertype->getUserTypeID($user_id);

		$cards = $this->accountsService->getAgentCardsOnFileForCourier($courier_id);
		return $this->view->render($response,'App/agent-cards.twig',['cards' => $cards, 'statuses' => '["ACTIVE", "INACTIVE"]' ]);

	}

	public function updateAgentCard($request, $response, $card_id) {

		$loggedUser = $this->auth->check();       
		$user_id = $loggedUser->id;
		$usertype = new \Dappur\Model\UserType;
		$courier_id = $usertype->getUserTypeID($user_id);

		if ($request->isXhr()) {

			$req = $request->getParams();
			$this->logger->debug("update agent card called, payload",$req);
			$status = $req['status'];

			$this->logger->debug("update agent card called with",[ "card_id" => $card_id, "status" => $status ]);

			if(is_null($status)) {
				echo "Update Unsuccessful";
				return;
			}

			$this->accountsService->updateAgentCardStatus($card_id, $status == 'ACTIVE' ? 1 : 0 );

			echo "Updated Successfully";

		}

	}

	public function getSaddleLiteHistory(Request $request, Response $response) {

		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;

		if($this->accountsService->isAdmin($userID)) {

			$agents = $this->accountsService->getAllAgents();
			$this->logger->debug("get agents ", [$agents]);


		} else if($this->accountsService->isCourierUser($userID) || $this->accountsService->isCourierAdmin($userID)) {

			$courier = $this->accountsService->getCourierForUser($userID);
			$agents = $this->accountsService->getAgentsForCourier($courier->id);

			$this->logger->debug("get agents for courier " . $courier->id, [$agents]);


		} else {

			$agents = [];

		}


		return $this->view->render($response, 'App/saddlelite-history.twig',
		['agents' => $agents]);
	
	}

	public function saddleLiteList(Request $request, Response $response) { 

		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;

		$params = $request->getParams();

		$columns = $params['columns'];

		$search_value = $params['search']['value'];
		$draw = $params['draw'];
		$length = $params['length'];
		$start = $params['start'];
		$from_date = $params['from_date'];
		$to_date = $params['to_date'];
		$agent = (int)$params["agent"];

		$order_info = [
			"name" => $columns[$params["order"][0]['column']]["data"],
			"dir" => $params["order"][0]['dir']
		];
		$page = round($start / $length) + 1;

		if($this->accountsService->isAdmin($userID)) {

			if(DataTableService::isSearchRequest($params)) {

				$transactions = $this->saddleLiteService->searchSaddleLiteTransactionsForAdmin($search_value,$order_info, $from_date, $to_date);
	
			} else {
	
				$transactions = $this->saddleLiteService->handleColumnFilterForAdmin($columns,$order_info, $from_date, $to_date, $agent);
	
			}

		} else if ($this->accountsService->isCourierUserType($userID)) {

			$courier = $this->accountsService->getCourierForUser($userID);

			if(is_null($courier)) {
				$data = ' { 
					"draw":'.$draw.',
					"data": []' .',
					"recordsTotal": 0'.',
					"recordsFiltered": 0'.'
					}';
				echo $data;
				return;
			}

			if(DataTableService::isSearchRequest($params)) {

				$transactions = $this->saddleLiteService->searchSaddleLiteTransactions($courier->id,$search_value,$order_info, $from_date, $to_date);
	
			} else {
	
				$transactions = $this->saddleLiteService->handleColumnFilter($courier->id, $columns,$order_info, $from_date, $to_date, $agent);
	
			}

		}

		for($i = 0; $i < count($transactions['total']); $i++) {
			$this->settlementService->populateSettlementForSaddleLiteTransaction($transactions['total'][$i]);
		}
	
		if(empty($transactions['total'])) {

			$result = $transactions['total'];

		} else if(!empty($search_value)) {

			$result = $transactions['total'];

		} else {

			$result = $transactions['total']->forPage($page,$length)->flatten();

		}

		$data = ' { 
		"draw":'.$draw.',
		"data":'. json_encode($result) .',
		"recordsTotal":'. $transactions['total_count'].',
		"recordsFiltered":'.$transactions['total_filtered'].'
		}';
		
		echo $data;

	}

	public function saddleLiteDashboard(Request $request, Response $response) {

		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;

		$courier = $this->accountsService->getCourierForUser($userID);

		$agent_activity_breakdown = $this->metricService->getTodaysSaddleLiteAgentActivityBreakDown($courier->id);
		$agent_transaction_breakdown = $this->metricService->getTodaysSaddleLiteAgentTransactionBreakDown($courier->id);

		return $this->view->render($response, 'App/saddlelite-dashboard.twig',
		[
			"agent_activity_breakdown" => $agent_activity_breakdown,
			"agent_transaction_breakdown" => $agent_transaction_breakdown
		]);

	}

	public function getTodaysSummary(Request $request, Response $response) {

		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;

		$courier = $this->accountsService->getCourierForUser($userID);

		if(is_null($courier)) {
			return $response->withStatus(400)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode("[]", JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
		}

		$summary = $this->metricService->getTodaysSaddleLiteTransactionSummaryForCourier($courier->id);

        return $response->withStatus(200)
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($summary, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
	}

	public function getTodaysPayments(Request $request, Response $response) {

		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;
		$courier = $this->accountsService->getCourierForUser($userID);

		if(is_null($courier)) {
			return $response->withStatus(400)
				->withHeader("Content-Type", "application/json")
				->write(json_encode("[]", JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
		}

		$transaction_series = $this->metricService->getTodaysSaddleLiteSuccessfullPaymentsPerHourForCourier($courier->id);

		return $response->withStatus(200)
				->withHeader("Content-Type", "application/json")
				->write(json_encode($transaction_series, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

	}

	public function settlementView(Request $req, Response $res) {

		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;

		if(!$this->auth->hasAccess('settlement.view')) {

			$loggedUser = $this->auth->check();

			$this->flash('danger', 'You do not have permission to view settlement.');
			$this->logger->addError("Unauthorized Access", array("message" => "Unauthorized access was attempted on the settlement page", "user_id" => $loggedUser['id']));
			return $this->redirect($res, 'dashboard');

		}

		return $this->view->render($res, 'App/settlement-view.twig',
		[
			'type' => $this->accountsService->getUserType($userID),
			'payment_types' => ["CASH", "CARD", "PREPAID"]
		]);

	}

	public function mainSettlementList(Request $req, Response $res) {

		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;

		if(!$this->auth->hasAccess('settlement.view')) {

			$data = ' { 
				"draw":'.$draw.',
				"data": []' .',
				"recordsTotal": 0'.',
				"recordsFiltered": 0'.'
				}';
			echo $data;
			return;

		}

		$params = $req->getParams();

		$columns = $params['columns'];

		$search_value = $params['search']['value'];
		$draw = $params['draw'];
		$length = $params['length'];
		$start = $params['start'];
		$from_date = $params['from_date'];
		$to_date = $params['to_date'];

		$order_info = [
			"name" => $columns[$params["order"][0]['column']]["data"],
			"dir" => $params["order"][0]['dir']
		];
		$page = round($start / $length) + 1;

		if($this->accountsService->isAdmin($userID)) {

			$this->logger->debug("admin request");

			if(DataTableService::isSearchRequest($params)) {

				$transactions = $this->deliveryService->searchSuccessfullDeliveriesForAdmin($search_value, $order_info, $from_date, $to_date);
	
			} else {
	
				$transactions = $this->deliveryService->filterSuccessfullDeliveriesForAdmin($columns, $order_info, $from_date, $to_date);
	
			}

		} else if($this->accountsService->isCourierUserType($userID)) {

			$this->logger->debug("request courier usertype request");

			$courier = $this->accountsService->getCourierForUser($userID);
			$this->logger->debug("courier id = " . $courier->id);


			if(is_null($courier)) {
				$this->logger->debug("courer is nul");

				$data = ' {
					"draw":'.$draw.',
					"data": []' .',
					"recordsTotal": 0'.',
					"recordsFiltered": 0'.'
					}';
				echo $data;
				return;

			}

			if(DataTableService::isSearchRequest($params)) {

				$this->logger->debug("data table is search request");
				$this->logger->debug("calling search successful deliverys");

				$transactions = $this->deliveryService->searchSuccessfullDeliveriesForCourier($courier->id,$search_value,$order_info, $from_date, $to_date);
	
			} else {
	
				$this->logger->debug("data table is not search request");
				$this->logger->debug("filter successful deliverys for courier");


				$transactions = $this->deliveryService->filterSuccessfullDeliveriesForCourier($courier->id, $columns,$order_info, $from_date, $to_date);
	
			}

		} else if ($this->accountsService->isMerchantUserType($userID)) {

			$this->logger->debug("merchant user type request");

			$merchant = $this->accountsService->getMerchantForUser($userID);
			if(is_null($merchant)) {

				$data = ' { 
					"draw":'.$draw.',
					"data": []' .',
					"recordsTotal": 0'.',
					"recordsFiltered": 0'.'
					}';
				echo $data;
				return;

			}

			if(DataTableService::isSearchRequest($params)) {

				$this->logger->debug("data table is search request");
				$this->logger->debug("calling search successful deliverys");

				$transactions = $this->deliveryService->searchSuccessfullDeliveriesForMerchant($merchant->id,$search_value,$order_info, $from_date, $to_date);
	
			} else {

				$this->logger->debug("data table is not search request");
				$this->logger->debug("filter successful deliverys");
	
				$transactions = $this->deliveryService->filterSuccessfullDeliveriesForMerchant($merchant->id, $columns,$order_info, $from_date, $to_date);

			}

		}

		for($i = 0; $i < count($transactions['total']); $i++) {
			$this->settlementService->populateSettlementForTransaction($transactions['total'][$i]);
			$this->deliveryService->populateCustomerAddressForTransaction($transactions['total'][$i]);
		}

		if(empty($transactions['total'])) {

			$result = $transactions['total'];

		} else if(!empty($search_value)) {

			$result = $transactions['total'];

		} else {

			$result = $transactions['total']->forPage($page,$length)->flatten();

		}

		$data = ' {
			"draw":'.$draw.',
			"data":'. json_encode($result) .',
			"recordsTotal":'. $transactions['total_count'].',
			"recordsFiltered":'.$transactions['total_filtered'].'
		}';
		
		echo $data;

	}

	public function getSaddleDeposits(Request $request, Response $response) {

		if (!$this->auth->inRole('admin') && !$this->auth->inRole('courier-admin'))
			return $this->redirect($response,'user-home');

		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;

		if($this->accountsService->isAdmin($userID)) {

			$agents = $this->accountsService->getAllAgents();
			$this->logger->debug("get agents ", [$agents]);


		} else if($this->accountsService->isCourierUser($userID) || $this->accountsService->isCourierAdmin($userID)) {

			$courier = $this->accountsService->getCourierForUser($userID);
			$agents = $this->accountsService->getAgentsForCourier($courier->id);

			$this->logger->debug("get agents for courier WTF " . $courier->id, [$agents]);


		} else {

			$agents = [];

		}

		$this->logger->debug("what the agent ", [$agents]);

		

		return $this->view->render($response, 'App/saddlelite-deposits.twig',
		['agents' => $agents]);

	}

	public function getSaddleDepositsList(Request $request, Response $response) {
	
		$loggedUser = $this->auth->check();
		$userID = $loggedUser->id;

		$params = $request->getParams();

		$columns = $params['columns'];

		$search_value = $params['search']['value'];
		$draw = $params['draw'];
		$length = $params['length'];
		$start = $params['start'];
		$from_date = $params['from_date'];
		$to_date = $params['to_date'];
		$agent = (int)$params["agent"];

		$order_info = [
			"name" => $columns[$params["order"][0]['column']]["data"],
			"dir" => $params["order"][0]['dir']
		];
		$page = round($start / $length) + 1;

		if($this->accountsService->isAdmin($userID)) {

			if(DataTableService::isSearchRequest($params)) {

				$transactions = $this->saddleLiteService->searchSaddleLiteDepositsForAdmin($search_value,$order_info, $from_date, $to_date);
	
			} else {
	
				$transactions = $this->saddleLiteService->handleDepositsFilterForAdmin($columns,$order_info, $from_date, $to_date, $agent);
	
			}

		} else if ($this->accountsService->isCourierUserType($userID)) {

			$courier = $this->accountsService->getCourierForUser($userID);

			if(is_null($courier)) {
				$data = ' { 
					"draw":'.$draw.',
					"data": []' .',
					"recordsTotal": 0'.',
					"recordsFiltered": 0'.'
					}';
				echo $data;
				return;
			}

			if(DataTableService::isSearchRequest($params)) {

				$transactions = $this->saddleLiteService->searchSaddleLiteDeposits($courier->id,$search_value,$order_info, $from_date, $to_date);
	
			} else {
	
				$transactions = $this->saddleLiteService->handleDepositsFilter($courier->id, $columns,$order_info, $from_date, $to_date, $agent);
	
			}

		}
	
		if(empty($transactions['total'])) {

			$result = $transactions['total'];

		} else if(!empty($search_value)) {

			$result = $transactions['total'];

		} else {

			$result = $transactions['total']->forPage($page,$length)->flatten();

		}

		$data = ' { 
		"draw":'.$draw.',
		"data":'. json_encode($result) .',
		"recordsTotal":'. $transactions['total_count'].',
		"recordsFiltered":'.$transactions['total_filtered'].'
		}';
		
		echo $data;
	}

}
