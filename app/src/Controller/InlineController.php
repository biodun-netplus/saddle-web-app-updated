<?php

namespace Dappur\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator as V;
use Firebase\JWT\JWT;

use Dappur\Services\DeliveryService;
use Dappur\Services\NetplusPayService;

class InlineController extends Controller {

    private $deliveryService;
    private $paymentService;

    public function __construct($container) {

        parent::__construct($container);
        $this->deliveryService = new DeliveryService($container);
        $this->paymentService = new NetplusPayService($container);

    }

    private function pickupAddressIsValid($pickup) {

        $pickup["country"] = "Nigeria";

        $this->logger->debug("pick up details:",[json_encode($pickup)]);

        $valid = isset($pickup["merchant_contactname"]) && isset($pickup["merchant_address"]) && isset($pickup["merchant_email"]) && isset($pickup["merchant_state"]) && isset($pickup["merchant_lga"]) && isset($pickup["merchant_phone"]) && isset($pickup["merchant_city"]);
        $this->logger->debug("pickupAddressIsValid ",[$valid]);

        return $valid;

    }

    private function dropoffAddressIsValid($dropoff) {

        $valid = isset($dropoff["customer_name"]) && isset($dropoff["customer_address"]) && isset($dropoff["customer_city"]) && isset($dropoff["customer_state"]) && isset($dropoff["customer_phone"]) && isset($dropoff["customer_lga"]);
        $dropoff["country"] = "Nigeria";
        $this->logger->debug("dropoffAddressIsValid ",[$valid]);

        return $valid;
    }

    private function courierIsValid($courier_id) {

        if(empty($courier_id))
            return false;

        $count = $this->db->table("courier")->where("courier_id",$courier_id)->count();

        $valid = $count > 0;


        return $valid;
    }

    private function itemsAreValid($items) {

        if(is_array($items) == false) {
            return false;
        }

        $valid = true;

        for($i = 0; $i < count($items); $i++) {
            $valid = $valid && isset($items[$i]["item_name"]) && isset($items[$i]["item_size"]) && isset($items[$i]["item_qty"]) && isset($items[$i]["item_cost"]);
        }

        $this->logger->debug("itemsAreValid ",[$valid]);
        return $valid;

    }

    private function orderIsValid($order) {

        $pickup_valid = $this->pickupAddressIsValid($order["pickup"]);
        $dropoff_valid = $this->dropoffAddressIsValid($order["delivery"]);
        $courier_valid = $this->courierIsValid($order["courier_id"]);        
        $items_valid = $this->itemsAreValid($order["orders"]["items"]);

        $valid = $pickup_valid && $dropoff_valid && $courier_valid && $items_valid;

        $this->logger->debug("orderIsValid ",[$valid]);

        return $valid;

    }

    private function paymentDetailsIsValid($payment_details) {

        $this->logger->debug("card_number is valid " , [isset($payment_details["card_number"])]);
        $this->logger->debug("card_expiry is valid " , [isset($payment_details["card_expiry"])]);
        $this->logger->debug("card_cvv is valid " , [isset($payment_details["card_cvv"])]);

        $valid = isset($payment_details["card_number"]) && isset($payment_details["card_expiry"]) && isset($payment_details["card_cvv"]);

        $this->logger->debug("paymentDetailsIsValid ",[$valid]);

        return $valid;

    }

    public function initiateTransaction(Request $req, Response $res) {

        //reads and validates the delivery order details
        $jwt = $req->getAttribute("jwt");
        $str = $jwt->sub;
        $pos = strpos($str,":");
        $userid = substr($str, $pos+1);

        $body = $req->getParsedBody();

        $trans_id = $this->deliveryService->generateOrderID(12);

        $order = $body['transaction'];
        $order['transaction_id'] = $trans_id;
        $order["pre_auth"] = 1;
        $payment_details = $body['payment_details'];

        if($this->orderIsValid($order) == false) {
            return $this->json($res, ["status" => 1, "msg" => "Delivery Details are Invalid"], 400);
        }

        if($this->paymentDetailsIsValid($payment_details) == false) {
            return $this->json($res, ["status" => 1, "msg" => "Payment Details Is Invalid"], 400);
        }

        $this->logger->debug('calling initiate payment');
        $payment_status  = $this->paymentService->initiateDODPayment($userid,$order,$payment_details);

        if($payment_status['status'] != 1) {
            return $this->json($res, [
                "status" => "01", 
                "data" => $payment_status 
            ], 400);
        }

        $create_order_result = $this->deliveryService->createDeliveryOrder($userid, $order);

        if($create_order_result["status"] != 1) {
            return $this->json($res, [
                "status" => "01",
                "data" => $create_order_result         
            ], 400);
        }

        return $this->json($res, [ 
            "status" => "00", 
            "data" => [
                "transaction_id" => $create_order_result["transaction_id"], 
                "amount" => $payment_status["transaction_amount"]
                ]
            ], 200);

    }

}

?>