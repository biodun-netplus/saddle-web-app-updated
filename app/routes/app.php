<?php

//the homepage
$app->map(['GET', 'POST'],'/', 'AppController:home')->setName('home');

//courier sigup page for card on file
$app->map(['GET', 'POST'], '/signup', 'AppController:signup')->setName('signup');


//dashboard page for all other users apart from admin
$app->get('/userhome', 'AppController:userHome')->setName('user-home')
->add(new Dappur\Middleware\AuthMiddleware($container));
$app->post('/trackOrder','AppController:trackOrder')->setName('track-order');


//other links
$app->get('/tracking', 'AppController:tracking')->setName('tracking')->add(new Dappur\Middleware\AuthMiddleware($container));
$app->get('/feed', 'AppController:feed')->setName('news-feed')->add(new Dappur\Middleware\AuthMiddleware($container));
$app->get('/shipping', 'AppController:shipping')->setName('shipping')->add(new Dappur\Middleware\AuthMiddleware($container));
$app->get('/invoice[/{transaction_id}]', 'AppController:getInvoice')->setName('invoice')->add(new Dappur\Middleware\AuthMiddleware($container));
$app->map(['GET', 'POST'],'/netplus_response', 'AppController:netplus_response')->setName('netplus_response');

//settlement related matters
$app->group('/settlement', function() use ($app)
	{
		$app->get('/dashboard', 'AppController:settlement')->setName('settlements-dashboard');
		$app->get('/view', 'AppController:settlementView')->setName('settlements-view');
		$app->post('/list', 'AppController:mainSettlementList')->setName('settlements-list');
		$app->get('/b2b', 'AppController:b2bSettlements')->setName('b2b-settlement');
		$app->post('/b2b-list', 'AppController:b2bList')->setName('b2b-list');
		$app->get('/card','AppController:cardOnFile')->setName('card');
		$app->get('/agent-cards','AppController:getAgentCardsOnFile')->setName("agent-cards");
		$app->put('/update-agent-card[/{agent_id}]','AppController:updateAgentCard')->setName("update-agent-card");
		$app->get('/csv','AppController:settlementCsv');
		$app->map(['POST'], '/getMerchantNametSettlementAjax', 'AppController:getMerchantNametSettlementAjax');
	})->add(new Dappur\Middleware\AuthMiddleware($container));

 //Delivery Management
//delivery related matters
$app->group('/delivery', function() use ($app)
    {
        //list / show all deliveries
		$app->get('', 'AppController:delivery')->setName('delivery');
		// $app->get('/RateCard', 'AppController:RateCard')->setName('RateCard'); //custom 
		$app->map(['GET', 'POST'],'/RateCard', 'AppController:RateCard')->setName('RateCard'); // ratecard by GPH

		$app->map(['GET', 'POST'],'/ManageDeliveryByDate', 'AppController:ManageDeliveryByDate')->setName('ManageDeliveryByDate');
		$app->post('/list', 'AppController:deliveryList')->setName('list-delivery');
		$app->post('/settlementList', 'AppController:settlementList')->setName('settlement-list');
        $app->map(['GET', 'POST'], '/add', 'AppController:deliveryAdd')->setName('delivery-add');
        $app->map(['GET', 'POST'], '/bulk', 'AppController:deliveryBulk')->setName('delivery-bulk');
        $app->map(['GET', 'POST'], '/edit[/{id}]', 'AppController:deliveryEdit')->setName('delivery-edit');
        $app->map(['POST'], '/getMerchant', 'AppController:getMerchantDetail');
        $app->map(['POST'], '/getAllRecords', 'AppController:getRatecardRecords');
	    $app->post('/recordDelete', 'AppController:recordDelete')
				->setName('recordDelete');
        $app->map(['GET', 'POST'], '/ratecardedit[/{record_id}]', 'AppController:ratecardedit')
				->setName('ratecardedit');        
        $app->map(['POST'], '/getPrice', 'AppController:getPriceCourier');//new line for price
        
        $app->map(['GET', 'POST'], '/post', 'AppController:deliveryPost')->setName('delivery-post');
        $app->map(['GET','POST'], '/report' ,'AppController:report')->setName('csv-report');
		$app->get('/details[/{transaction_id}]', 'AppController:deliveryDetails')->setName('delivery-detail');
		$app->post('/assign','AppController:assignAgent')->setName('assign-agent');
	})->add(new Dappur\Middleware\AuthMiddleware($container));

$app->group("/saddlelite", function() use ($app)
	{

		$app->get("",'AppController:saddleLiteDashboard')->setName("saddlelite-dashboard");
		$app->get("/history",'AppController:getSaddleLiteHistory')->setName("saddlelite-history");
		$app->post("/list",'AppController:saddleLiteList')->setName("saddlelite-list");
		$app->get("/deposits",'AppController:getSaddleDeposits')->setName("saddlelite-deposits");
		$app->post("/deposits-list",'AppController:getSaddleDepositsList')->setName("saddlelite-deposits-list");
		$app->get("/todaysSummary",'AppController:getTodaysSummary')->setName("saddlelite-dashboard-summary");
		$app->get("/todaysPayments",'AppController:getTodaysPayments')->setName("saddlelite-dashboard-payments");


	})->add(new Dappur\Middleware\AuthMiddleware($container));

$app->group('/admin', function () use($app) {

		// Dashboard Home for admin
		$app->get('', 'AppController:dashboard')->setName('dashboard');
		// Users Routes
		$app->group('/users', function() use ($app) {
			// User List
			$app->get('', 'AppController:users')
				->setName('admin-users');

			// Add New User
			$app->map(['GET', 'POST'], '/add', 'AppController:usersAdd')
				->setName('admin-users-add');

			// Edit User
			$app->map(['GET', 'POST'], '/edit[/{user_id}]', 'AppController:usersEdit')
				->setName('admin-users-edit');

			// Delete User
			$app->post('/delete', 'AppController:usersDelete')
				->setName('admin-users-delete');

			//User Roles
			$app->group('/roles', function() use ($app) {
				$app->post('/delete', 'AppController:rolesDelete')
					->setName('admin-roles-delete');
				$app->map(['GET', 'POST'], '/edit[/{role}]', 'AppController:rolesEdit')
					->setName('admin-roles-edit');
				$app->post('/add', 'AppController:rolesAdd')
					->setName('admin-roles-add');
			});
		});
		//Courier Management
		$app->group('/couriers', function() use ($app)
			{
				//list couriers
				$app->get('', 'AppController:couriers')
				->setName('admin-couriers');
				$app->get('/assign', 'AppController:assign')->setName('assign');

				//Add New Courier
				// Add New User
			$app->map(['GET', 'POST'], '/add', 'AppController:couriersAdd')
				->setName('admin-couriers-add');

			$app->map(['GET', 'POST'], '/edit[/{courier_id}]', 'AppController:couriersEdit')
				->setName('admin-couriers-edit');

			$app->map(['GET', 'POST'], '/delete[/{courier_id}]', 'AppController:couriersDelete')
				->setName('admin-couriers-delete');

			});

		//Merchant Management
		$app->group('/merchants', function() use ($app)
			{
				//list couriers
				$app->get('', 'AppController:merchants')
				->setName('admin-merchants');

				//Add New Courier
				// Add New User
			$app->map(['GET', 'POST'], '/add', 'AppController:merchantsAdd')
				->setName('admin-merchants-add');

			$app->map(['GET', 'POST'], '/edit[/{merchant_id}]', 'AppController:merchantsEdit')
				->setName('admin-merchants-edit');

			$app->map(['GET', 'POST'], '/delete[/{merchant_id}]', 'AppController:merchantsDelete')
				->setName('admin-merchants-delete');

			});

		//Banks Management
		$app->group('/banks', function() use ($app)
			{
				//list banks
				$app->get('', 'AppController:banks')
				->setName('admin-banks');

				//Add New Courier
				// Add New User
			$app->map(['GET', 'POST'], '/add', 'AppController:banksAdd')
				->setName('admin-banks-add');

			$app->map(['GET', 'POST'], '/edit', 'AppController:banksEdit')
				->setName('admin-banks-edit');

			});

			$app->group('/agents', function() use ($app)
			{
				//list banks
				$app->get('', 'AppController:agents')
				->setName('admin-agents');

				//Add New Courier
				// Add New User
			$app->map(['GET', 'POST'], '/add', 'AppController:agentAdd')
				->setName('admin-agent-add');

			$app->map(['GET', 'POST'], '/edit', 'AppController:agentEdit')
				->setName('admin-agent-edit');

			});
			// Global Settings
		$app->map(['GET', 'POST'], '/settings', 'AppController:settingsGlobal')->setName('settings-global');
		$app->post('/settings/add', 'AppController:settingsGlobalAdd')
			->setName('settings-global-add');

	})
	//->add(new Dappur\Middleware\AdminMiddleware($container))
	->add(new Dappur\Middleware\AuthMiddleware($container));

	// My Account
	$app->map(['GET', 'POST'], '/my-account', 'AppController:myAccount')->setName('my-account')
	->add(new Dappur\Middleware\AuthMiddleware($container));
