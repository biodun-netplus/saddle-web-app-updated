<?php

$app->post("/trackOrderApi", 'ApiController:trackOrderApi');
$app->post('/leadGenerator','ApiController:leadGenerator');

$app->group("/api", function() use($app) {

  $app->group("/v2", function() use($app) {

      $app->post("/delivery", 'ApiController:delivery');
      $app->post("/rateCard", 'ApiController:rateCard');
      $app->post("/pickup","ApiController:getPickup");
      $app->get('/orders[/{transaction_id}]','ApiController:getTransactionDetails');
      $app->post("/postToPickUp","ApiController:callPickUp");
      $app->post("/postToDropOff","ApiController:callDropOff");
      $app->post("/payMerchant","ApiController:payMerchant");
      $app->get("/item-detail[/{productID}]","ApiController:getItemDetail");
      $app->post("/capture","ApiController:capture");
      $app->map(["GET","POST"],"/shipping_price", "ApiController:latestRateCardApi");
      $app->post("/agentLocation", "ApiController:agentLocation");
      $app->post("/dropoff","ApiController:getDropoff");
      $app->get("/onfile[/{transactionID}]","ApiController:isCardOnFile");
      $app->get("/getCourier","ApiController:getCourier");
      $app->get("/getWawooCourier", "ApiController:getWawooCourier");
      $app->get("/getCourierRoutes","ApiController:getCourierRoutes");
      $app->get('/getCourierForRoute',"ApiController:getCourierForRoute");
      $app->get("/getStates","ApiController:getStatesWithLGA");
      $app->get("/getStateLga","ApiController:getStateLGA");
      $app->post("/logSaddleLiteCardTransaction","ApiController:logSaddleLiteCardTransaction");
      $app->post("/logSaddleLiteCashTransaction","ApiController:logSaddleLiteCashTransaction");
      $app->post("/logDeposit","ApiController:logAgentDeposit");

      $app->post("/inline","InlineController:initiateTransaction");

	});

  $app->post('/auth','ApiController:auth');
  $app->get('/automate','ApiController:updateTransactionStatus');
	$app->get('/test','ApiController:practise');
  $app->post('/token','ApiController:getToken');
  $app->post('/tokenRateCard','ApiController:getTokenForRateCard');

});
